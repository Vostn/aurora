﻿using Aurora.Unity;

namespace Aurora.TimeSystem.Unity
{
	public static class UnityTimeTracker
	{
		private static TimeStepper timeStepper = new TimeStepper();

		public static TimeTransformWrapper UnityFrameTimeTransform { get { if (unityFrameTimeTransform == null) InitialiseWrappers(); return unityFrameTimeTransform; } private set { unityFrameTimeTransform = value; } }
		public static TimeTransformWrapper UnityFixedTimeTransform { get { if (unityFixedTimeTransform == null) InitialiseWrappers(); return unityFixedTimeTransform; } private set { unityFixedTimeTransform = value; } }

		private static TimeTransformWrapper unityFrameTimeTransform;
		private static TimeTransformWrapper unityFixedTimeTransform;

		private static bool applicationPaused = false;
#if UNITY_EDITOR
		private static bool editorPaused = false;
#endif

		static UnityTimeTracker()
		{
			timeStepper.Unpause();

			UnityEvents.On_Update += UnityEvents_On_Update;
			UnityEvents.On_FixedUpdate += UnityEvents_On_FixedUpdate;
			UnityEvents.On_ApplicationPause += UnityEvents_On_ApplicationPause;
		}

		private static void InitialiseWrappers()
		{
			UnityFrameTimeTransform = UnityEngine.ScriptableObject.CreateInstance<TimeTransformWrapper>();
			UnityFixedTimeTransform = UnityEngine.ScriptableObject.CreateInstance<TimeTransformWrapper>();

			UnityFrameTimeTransform.name = "Unity Default Frame Time Transform";
			UnityFixedTimeTransform.name = "Unity Default Fixed Time Transform";

			UnityFrameTimeTransform.hideFlags = UnityEngine.HideFlags.NotEditable;
			UnityFixedTimeTransform.hideFlags = UnityEngine.HideFlags.NotEditable;
		}

		private static void UnityEvents_On_Update()
		{
#if UNITY_EDITOR
			if (UnityEditor.EditorApplication.isPaused)
				timeStepper.IncrementStep(UnityEngine.Time.fixedDeltaTime);
			else
#endif
				timeStepper.IncrementStep();

			RealTimeTracker.IncrementStep();
			UnityFrameTimeTransform.TimeTransform.IncrementStep(timeStepper.DeltaTime);
		}

		private static void UnityEvents_On_FixedUpdate()
		{
			UnityFixedTimeTransform.TimeTransform.IncrementStep(UnityEngine.Time.fixedDeltaTime);
		}


#if UNITY_EDITOR

		[UnityEditor.Callbacks.DidReloadScripts]
		[UnityEditor.InitializeOnLoadMethod]
		[UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void UnityInitialise()
		{
			if (UnityEngine.Application.isPlaying)
				UnityEditor.EditorApplication.pauseStateChanged += EditorApplication_pauseStateChanged;
		}

		[UnityEditor.InitializeOnLoadMethod]
		private static void UnityOpen()
		{
			UnityEditor.EditorApplication.playModeStateChanged += EditorApplication_playModeStateChanged;
		}

		private static void EditorApplication_playModeStateChanged(UnityEditor.PlayModeStateChange state)
		{
			if (state == UnityEditor.PlayModeStateChange.EnteredEditMode)
				UnityInitialise();
		}

		private static void EditorApplication_pauseStateChanged(UnityEditor.PauseState state)
		{
			bool oldPauseState = editorPaused || applicationPaused;

			if (state == UnityEditor.PauseState.Paused)
				editorPaused = true;
			else if (state == UnityEditor.PauseState.Unpaused)
				editorPaused = false;

			bool newPauseState = editorPaused || applicationPaused;

			if(!oldPauseState && newPauseState)
				timeStepper.Pause();
			else if (oldPauseState && !newPauseState)
				timeStepper.Unpause();
		}

#endif

		private static void UnityEvents_On_ApplicationPause(bool paused)
		{
#if UNITY_EDITOR
			bool oldPauseState = editorPaused || applicationPaused;
#else
			bool oldPauseState = applicationPaused;
#endif

			if (paused)
				applicationPaused = true;
			else
				applicationPaused = false;

#if UNITY_EDITOR
			bool newPauseState = editorPaused || applicationPaused;
#else
			bool newPauseState = applicationPaused;
#endif

			if(!oldPauseState && newPauseState)
				timeStepper.Pause();
			else if (oldPauseState && !newPauseState)
				timeStepper.Unpause();
		}	
	}
}