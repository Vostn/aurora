﻿using UnityEngine;
using Aurora.Unity;

namespace Aurora.TimeSystem.Unity
{
	public abstract class TimeBehaviour : MonoBehaviour
	{
		[SerializeField]
		private TimeOperation timeOperation = TimeOperation.LocalTime;

		private enum TimeOperation
		{
			LocalTime,
			ApplicationTime,
			RealTime,
		}

		private FrameTimeTransformComponent frameTimeTransform;
		private FixedTimeTransformComponent fixedTimeTransform;

		public FrameTimeTransformComponent FrameTimeTransformComponent
		{
			get
			{
				if (frameTimeTransform == null)
					frameTimeTransform = gameObject.GetOrAddComponent<FrameTimeTransformComponent>();
				return frameTimeTransform;
			}
		}

		public FixedTimeTransformComponent FixedTimeTransformComponent
		{
			get
			{
				if (fixedTimeTransform == null)
					fixedTimeTransform = gameObject.GetOrAddComponent<FixedTimeTransformComponent>();
				return fixedTimeTransform;
			}
		}

		private TimeOperation CurrentTimeOperation => timeOperation;

		public double TimeScale
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FrameTimeTransformComponent.TimeScale;
				else
					throw new System.NotImplementedException();
			}
		}

		public double FixedTimeScale
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FixedTimeTransformComponent.TimeScale;
				else
					throw new System.NotImplementedException();
			}
		}

		public double LocalTimeScale
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FrameTimeTransformComponent.LocalTimeScale;
				else
					throw new System.NotImplementedException();
			}
		}

		public double LocalFixedTimeScale
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return 1;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FixedTimeTransformComponent.LocalTimeScale;
				else
					throw new System.NotImplementedException();
			}
		}

		public double DeltaTime
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return RealTimeTracker.RealTimeTransform.DeltaTime;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return UnityTimeTracker.UnityFrameTimeTransform.TimeTransform.DeltaTime;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FrameTimeTransformComponent.DeltaTime;
				else
					throw new System.NotImplementedException();
			}
		}

		public double FixedDeltaTime
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return UnityTimeTracker.UnityFixedTimeTransform.TimeTransform.DeltaTime;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return UnityTimeTracker.UnityFixedTimeTransform.TimeTransform.DeltaTime;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FixedTimeTransformComponent.DeltaTime;
				else
					throw new System.NotImplementedException();
			}
		}

		public double Time
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return RealTimeTracker.RealTimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return UnityTimeTracker.UnityFrameTimeTransform.TimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FrameTimeTransformComponent.Time;
				else
					throw new System.NotImplementedException();
			}
		}

		public double FixedTime
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return UnityTimeTracker.UnityFixedTimeTransform.TimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return UnityTimeTracker.UnityFixedTimeTransform.TimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FixedTimeTransformComponent.Time;
				else
					throw new System.NotImplementedException();
			}
		}

		public double LocalTime
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return RealTimeTracker.RealTimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return UnityTimeTracker.UnityFrameTimeTransform.TimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FrameTimeTransformComponent.LocalTime;
				else
					throw new System.NotImplementedException();
			}
		}

		public double LocalFixedTime
		{
			get
			{
				if (CurrentTimeOperation == TimeOperation.RealTime)
					return UnityTimeTracker.UnityFixedTimeTransform.TimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.ApplicationTime)
					return UnityTimeTracker.UnityFixedTimeTransform.TimeTransform.Time;
				else if (CurrentTimeOperation == TimeOperation.LocalTime)
					return FixedTimeTransformComponent.LocalTime;
				else
					throw new System.NotImplementedException();
			}
		}
	}
}