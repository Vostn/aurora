﻿namespace Aurora.TimeSystem.Unity
{
	public class FixedTimeTransformComponent : TimeTransformComponent
	{
		protected override TimeTransformWrapper GetDefaultParent()
		{
			return UnityTimeTracker.UnityFixedTimeTransform;
		}
	}
}