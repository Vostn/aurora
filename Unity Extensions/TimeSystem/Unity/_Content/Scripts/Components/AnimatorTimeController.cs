﻿using UnityEngine;

namespace Aurora.TimeSystem.Unity
{
	public class AnimatorTimeController : TimeBehaviour
	{
		private Animator animator;
		private double animatorSpeedCache;
		
		public void Initialise(Animator animator)
		{
			this.animator = animator;
			animatorSpeedCache = animator.speed;
			animator.speed = 0;
		}

		private void Update()
		{
			if (animator != null)
			{
				if (animator.updateMode == AnimatorUpdateMode.Normal)
				{
					if (animator.speed == 0)
						animator.speed = (float)animatorSpeedCache;
					else
						animatorSpeedCache = animator.speed;
					animator.Update((float)DeltaTime);
					animator.speed = 0;
				}
				else if (animator.updateMode == AnimatorUpdateMode.UnscaledTime)
				{
					if (animator.speed == 0)
						animator.speed = (float)animatorSpeedCache;
					else
						animatorSpeedCache = animator.speed;
					animator.Update((float)Time);
					animator.speed = 0;
				}
			}
		}

		private void FixedUpdate()
		{
			if (animator != null)
			{
				if (animator.updateMode == AnimatorUpdateMode.AnimatePhysics)
				{
					if (animator.speed == 0)
						animator.speed = (float)animatorSpeedCache;
					else
						animatorSpeedCache = animator.speed;
					animator.Update((float)FixedDeltaTime);
					animator.speed = 0;
				}
			}
		}
	}
}