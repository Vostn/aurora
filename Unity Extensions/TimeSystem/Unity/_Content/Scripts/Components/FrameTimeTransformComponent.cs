﻿using UnityEngine;

namespace Aurora.TimeSystem.Unity
{
	[ExecuteInEditMode]
	public class FrameTimeTransformComponent : TimeTransformComponent
	{
		protected override TimeTransformWrapper GetDefaultParent()
		{
			return UnityTimeTracker.UnityFrameTimeTransform;
		}
	}
}