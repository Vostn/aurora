﻿using UnityEngine;
using Aurora.Unity;

namespace Aurora.TimeSystem.Unity
{
	[ExecuteInEditMode]
	public abstract class TimeTransformComponent : MonoBehaviour
	{
		[SerializeField]
		private bool fallbackToDefaultParent = true;

		[SerializeField]
		protected TimeTransformWrapper parentTransform = null;

		[SerializeField]
		[Delayed]
		protected double localTimeScale = 1;

		private TimeTransform timeTransform;
		public TimeTransform TimeTransform { get { if (timeTransform == null) InitialiseTimeTransform(); return timeTransform; } private set { timeTransform = value; } }

		private void Awake()
		{
			if (!Application.isPlaying)
				return;

			InitialiseTimeTransform();

			Rigidbody rigidbody = GetComponent<Rigidbody>();
			if(rigidbody != null)
				gameObject.GetOrAddComponent<RigidbodyTimeController>().Initialise(rigidbody);

			Animator animator = GetComponent<Animator>();
			if(animator != null)
				gameObject.GetOrAddComponent<AnimatorTimeController>().Initialise(animator);
		}

		private void InitialiseTimeTransform()
		{
			TimeTransform = new TimeTransform(0, 0, localTimeScale);
			if (parentTransform == null)
				SetDefaultParent();
			if (parentTransform != null)
				TimeTransform.SetParent(parentTransform.TimeTransform);
		}

		private void OnDrawGizmosSelected()
		{
			if (!Application.isPlaying)
				ValidateParent();
		}

		private void Update()
		{
			if (Application.isPlaying)
				ValidateParent();
		}

		private void ValidateParent()
		{
			if (parentTransform == null)
				SetDefaultParent();
		}

		private void Reset()
		{
			OnValidate();
		}

		private void OnValidate()
		{
			localTimeScale = System.Math.Max(localTimeScale, 0.0001);
			ValidateParent();

			if (Application.isPlaying)
			{
				if(parentTransform != null)
					TimeTransform.SetParent(parentTransform.TimeTransform);
				else 
					TimeTransform.SetParent(null);
				TimeTransform.LocalTimeScale = localTimeScale;
			}
		}

		protected abstract TimeTransformWrapper GetDefaultParent();

		private void SetDefaultParent()
		{
			if (!fallbackToDefaultParent)
				return;
			if (Application.isPlaying)
				TimeTransform.SetParent(GetDefaultParent().TimeTransform);
			parentTransform = GetDefaultParent();
		}

		public void SetParent(TimeTransformWrapper parent)
		{
			TimeTransform.SetParent(parent.TimeTransform);
		}

		public void AddChild(TimeTransform child)
		{
			TimeTransform.AddChild(child);
		}

		public double TimeScale => TimeTransform.TimeScale;
		public double LocalTimeScale => TimeTransform.LocalTimeScale;

		public double DeltaTime => TimeTransform.DeltaTime;

		public double Time => TimeTransform.Time;
		public double LocalTime => TimeTransform.LocalTime;

		public int StepCount => TimeTransform.StepCount;
	}
}