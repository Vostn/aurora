﻿using UnityEngine;

namespace Aurora.TimeSystem.Unity
{
	public class RigidbodyTimeController : TimeBehaviour
	{
		private new Rigidbody rigidbody;
		private double rigidbodyMass;
		private Vector3 rigidbodyVelocity;

		private double lastFixedTimeScale = 1;
		
		public void Initialise(Rigidbody rigidbody)
		{
			this.rigidbody = rigidbody;
			rigidbodyMass = rigidbody.mass * lastFixedTimeScale;
			rigidbodyVelocity = rigidbody.velocity / (float)lastFixedTimeScale;
		}

		private void FixedUpdate()
		{
			if (rigidbody != null)
			{
				rigidbodyVelocity = rigidbody.velocity / (float)lastFixedTimeScale;
				rigidbodyMass = (float)(rigidbody.mass * lastFixedTimeScale);

				rigidbody.velocity = rigidbodyVelocity * (float)FixedTimeScale;
				rigidbody.mass = (float)(rigidbodyMass / FixedTimeScale);

				lastFixedTimeScale = FixedTimeScale;

				if (rigidbody.useGravity)
				{
					rigidbody.sleepThreshold = Physics.sleepThreshold * (float)FixedTimeScale;
					rigidbody.AddForce(-Physics.gravity, ForceMode.Acceleration);
					rigidbody.AddForce(Physics.gravity * (float)FixedTimeScale, ForceMode.Acceleration);
				}
			}
		}
	}
}