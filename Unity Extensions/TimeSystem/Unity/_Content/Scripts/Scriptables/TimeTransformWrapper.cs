﻿using UnityEngine;

namespace Aurora.TimeSystem.Unity
{
	public class TimeTransformWrapper : ScriptableObject
	{
		[SerializeField]
		private double localTimeScale = 1;

		[SerializeField]
		private AutoStepType autoStepType = AutoStepType.None;

		private enum AutoStepType
		{
			Rendering,
			Physics,
			None,
		}

		private TimeTransform timeTransform;
		public TimeTransform TimeTransform { get { ValidateTimeTransform(); return timeTransform; } private set { timeTransform = value; } }
		
		private void OnValidate()
		{
			localTimeScale = System.Math.Max(localTimeScale, 0);
			if(Application.isPlaying)
				TimeTransform.LocalTimeScale = localTimeScale;
			HookupAutoStep();
		}

		private void ValidateTimeTransform()
		{
			if (timeTransform == null)
			{
				TimeTransform = new TimeTransform(0, 0, localTimeScale);
				HookupAutoStep();
			}
		}

		private void HookupAutoStep()
		{
			if (autoStepType == AutoStepType.Rendering)
				TimeTransform.SetParent(UnityTimeTracker.UnityFrameTimeTransform.TimeTransform);
			else if (autoStepType == AutoStepType.Physics)
				TimeTransform.SetParent(UnityTimeTracker.UnityFixedTimeTransform.TimeTransform);
			else if (autoStepType == AutoStepType.None)
				TimeTransform.SetParent(null);
			else
				throw new System.NotImplementedException();
		}
	}
}