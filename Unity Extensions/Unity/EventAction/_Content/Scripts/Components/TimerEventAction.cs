﻿using System.Collections.Generic;
using UnityEngine;
using Aurora.TimeSystem;
using Aurora.TimeSystem.Unity;

namespace Aurora.Unity.EventAction
{
	public class TimerEventAction : EventActionBase
	{
		[SerializeField]
		private double[] times;

		private List<Hourglass> hourglasses = new List<Hourglass>();

		[SerializeField]
		private double[] repeatTimes;

		private List<Hourglass> repeatHourglasses = new List<Hourglass>();

		[SerializeField]
		private bool preWarmedRepeat;

		protected override void OnEventActionReset()
		{
			ReinitialiseTimers();
		}

		protected override void OnEventActionPause()
		{
			for (int i = 0; i < hourglasses.Count; ++i)
				hourglasses[i].Paused = true;
			for (int i = 0; i < repeatHourglasses.Count; ++i)
				repeatHourglasses[i].Paused = true;
		}

		protected override void OnEventActionResume()
		{
			for (int i = 0; i < hourglasses.Count; ++i)
				hourglasses[i].Paused = false;
			for (int i = 0; i < repeatHourglasses.Count; ++i)
				repeatHourglasses[i].Paused = false;
		}

		protected override void Update()
		{
			base.Update();
			if ((settingsFlags & EventActionSettingFlags.TriggerOnlyOnce) == 0 || !triggered)
			{
				ManageRepeatTimers();
				ManageRegularTimers();
			}
		}

		private void Start()
		{
			ReinitialiseTimers();
		}

		private void ReinitialiseTimers()
		{
			hourglasses.Clear();
			repeatHourglasses.Clear();

			for (int i = 0; i < times.Length; ++i)
			{
				Hourglass newHourglass = new Hourglass(times[i], CurrentTimeTransform);
				newHourglass.Paused = !isActiveAndEnabled;
				hourglasses.Add(newHourglass);
			}
			for (int i = 0; i < repeatTimes.Length; ++i)
			{
				Hourglass newHourglass;
				newHourglass = new Hourglass(0, CurrentTimeTransform);
				if (!preWarmedRepeat)
					newHourglass.TimeRemaining = repeatTimes[i];
				newHourglass.Paused = !isActiveAndEnabled;
				repeatHourglasses.Add(newHourglass);
			}
		}

		private void ManageRegularTimers()
		{
			for (int i = 0; i < hourglasses.Count; ++i)
			{
				if (hourglasses[i].Expired)
				{
					Trigger();
					hourglasses.RemoveAt(i--);
				}
			}
		}
		
		private void ManageRepeatTimers()
		{
			double clampValue = DeltaTime * 0.001f; //Maximum of 1000 triggers per frame.
			for (int i = 0; i < repeatHourglasses.Count; ++i)
			{
				Hourglass targetHourglass = repeatHourglasses[i];
				while (targetHourglass.Expired)
				{
					Trigger();
					targetHourglass.ExpiryTime += System.Math.Max(clampValue, repeatTimes[i]);
				}
			}
		}
	}
}