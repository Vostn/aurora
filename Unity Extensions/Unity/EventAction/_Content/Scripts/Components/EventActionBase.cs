﻿using System.Collections.Generic;
using UnityEngine;
using Aurora.TimeSystem.Unity;
using Aurora.TimeSystem;
using Aurora.Unity;

namespace Aurora.Unity.EventAction
{
	public abstract class EventActionBase : TimeBehaviour, IEventReceiver<PoolDeposit>
	{
		protected List<Hourglass> delayedActionTimers = new List<Hourglass>();

		protected bool triggered = false;
		protected bool actioned = false;

		[SerializeField]
		protected UpdateType updateType = UpdateType.Rendering;

		[SerializeField]
		[EnumMask]
		protected EventActionSettingFlags settingsFlags = (EventActionSettingFlags)0;

		[SerializeField]
		protected float delayToAction = 0;

		[SerializeField]
		private EventActionDisableAction actionOnDisable = EventActionDisableAction.Reset;

		[SerializeField]
		private UnityEngine.Events.UnityEvent onTrigger;

		public void ActionImmediate()
		{
			if ((settingsFlags & EventActionSettingFlags.ActionOnlyOnce) == 0 || !actioned)
			{
				actioned = true;
				onTrigger.Invoke();
			}
		}

		public void ActionDelayed(float delay)
		{
			if ((settingsFlags & EventActionSettingFlags.ActionOnlyOnce) == 0 || !actioned)
			{
				Hourglass newHourglass = new Hourglass(delay, CurrentTimeTransform) { Paused = !isActiveAndEnabled };
				delayedActionTimers.Add(newHourglass);
			}
		}

		protected TimeTransform CurrentTimeTransform { get { return updateType == UpdateType.Rendering ? FrameTimeTransformComponent.TimeTransform : FixedTimeTransformComponent.TimeTransform; } }

		public new void DestroyObject(Object target)
		{
			if (ObjectPool.ObjectIsPooledInstance(target))
				ObjectPool.Deposit(target);
			else
				Destroy(target);
		}

		public void OrphanTransform(Transform target)
		{
			target.SetParent(null);
		}

		void IEventReceiver<PoolDeposit>.Triggered(PoolDeposit data)
		{
			ResetState();
		}

		protected virtual void Awake()
		{
		}

		protected virtual void Update()
		{
			for (int i = 0; i < delayedActionTimers.Count; ++i)
			{
				if (delayedActionTimers[i].Expired)
				{
					ActionImmediate();
					if (i >= delayedActionTimers.Count)
						break;
					delayedActionTimers.RemoveAt(i--);
				}
			}
		}

		protected void Trigger()
		{
			if ((settingsFlags & EventActionSettingFlags.TriggerOnlyOnce) == 0 || !triggered)
			{
				triggered = true;
				if (delayToAction <= 0)
					ActionImmediate();
				else
					ActionDelayed(delayToAction);
			}
		}

		protected abstract void OnEventActionReset();

		protected abstract void OnEventActionPause();

		protected abstract void OnEventActionResume();

		protected virtual void OnEnable()
		{
			ResumeState();
		}

		protected virtual void OnDisable()
		{
			if (actionOnDisable == EventActionDisableAction.Reset)
				ResetState();
			else if (actionOnDisable == EventActionDisableAction.Pause)
				PauseState();
		}

		private void ResumeState()
		{
			for (int i = 0; i < delayedActionTimers.Count; ++i)
				delayedActionTimers[i].Paused = false;
			OnEventActionResume();
		}

		private void PauseState()
		{
			for (int i = 0; i < delayedActionTimers.Count; ++i)
				delayedActionTimers[i].Paused = true;
			OnEventActionPause();
		}

		private void ResetState()
		{
			triggered = false;
			actioned = false;
			delayedActionTimers.Clear();
			OnEventActionReset();
		}

		private void OnValidate()
		{
			delayToAction = Mathf.Max(0, delayToAction);
		}
	}
}