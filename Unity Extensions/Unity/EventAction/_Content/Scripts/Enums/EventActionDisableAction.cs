﻿namespace Aurora.Unity.EventAction
{
	public enum EventActionDisableAction
	{
		Reset,
		Pause,
		None,
	}
}