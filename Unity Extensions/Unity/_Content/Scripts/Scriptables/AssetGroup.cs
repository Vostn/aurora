﻿using System.Collections.Generic;
using UnityEngine;
using System.Collections.ObjectModel;

namespace Aurora.Unity
{
	public abstract class AssetGroup<T> : ScriptableObject where T : Object
	{
		[SerializeField]
		private List<T> assets;

		public ReadOnlyCollection<T> Assets
		{
			get
			{
				if (assets == null)
					assets = new List<T>(0);
				if (readOnlyAssets == null)
					readOnlyAssets = assets.AsReadOnly();
				return readOnlyAssets;
			}
		}

		private ReadOnlyCollection<T> readOnlyAssets;
	}
}