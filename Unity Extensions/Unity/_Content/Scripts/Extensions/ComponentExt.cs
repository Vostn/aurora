﻿using UnityEngine;

namespace Aurora.Unity
{
	public static class ComponentExt
	{
		public static T GetOrAddComponent<T>(this Component target) where T : Component
		{
			return target.gameObject.GetOrAddComponent<T>();
		}
	}
}