﻿using UnityEngine;

namespace Aurora.Unity
{
	public static class IEventReceiverExt
	{
		public static void ExecuteEvent<T>(this GameObject target, T data, bool includeChildren = false) where T : IEventData
		{
			IEventReceiver<T>[] receivers;
			if(includeChildren)
				receivers = target.GetComponentsInChildren<IEventReceiver<T>>();
			else
				receivers = target.GetComponents<IEventReceiver<T>>();
			foreach (IEventReceiver<T> receiver in receivers)
				receiver.Triggered(data);
		}
	}
}
