﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Aurora.Unity
{
	public static class ScrollRectExt
	{
		private static bool TransformBelongsToThis(this ScrollRect scrollRect, Transform targetTransform)
		{
			while (true)
			{
				if (targetTransform.parent == null)
					return false;
				if (targetTransform.parent == scrollRect.transform)
					return true;
				targetTransform = targetTransform.parent;
				if (targetTransform.GetComponent<ScrollRect>() != null)
					return false;
			}
		}

		public static bool TryFocusOnSelected(this ScrollRect scrollRect, double focusSpeed = double.MaxValue)
		{
			GameObject target = EventSystem.current.currentSelectedGameObject;
			if (target == null)
				return false;
			if (!scrollRect.TransformBelongsToThis(target.transform))
				return false;
			scrollRect.FocusOn(target, focusSpeed);
			return true;
		}

		public static bool TryFocusOn(this ScrollRect scrollRect, Component focusTarget, double focusSpeed = double.MaxValue)
		{
			if (focusTarget == null)
				return false;
			if (!scrollRect.TransformBelongsToThis(focusTarget.transform))
				return false;
			scrollRect.FocusOn(focusTarget, focusSpeed);
			return true;
		}

		public static bool TryFocusOn(this ScrollRect scrollRect, GameObject focusTarget, double focusSpeed = double.MaxValue)
		{
			if (focusTarget == null)
				return false;
			if (!scrollRect.TransformBelongsToThis(focusTarget.transform))
				return false;
			scrollRect.FocusOn(focusTarget, focusSpeed);
			return true;
		}

		private static void FocusOn(this ScrollRect scrollRect, Component focusTarget, double focusSpeed = double.MaxValue)
		{
			scrollRect.FocusOn(focusTarget.gameObject, focusSpeed);
		}

		private static void FocusOn(this ScrollRect scrollRect, GameObject focusTarget, double focusSpeed = double.MaxValue)
		{
			if (focusSpeed <= 0)
			{
				Debug.LogWarning($"{nameof(focusSpeed)} must be greater than zero!");
				focusSpeed = System.Math.Max(focusSpeed, double.Epsilon);
			}

			Vector3 relativeFocusPosition = scrollRect.content.InverseTransformPoint(focusTarget.transform.position);
			Vector2 anchoredPosition = -relativeFocusPosition;
			anchoredPosition -= Vector2.Scale(scrollRect.content.pivot - (Vector2.one * 0.5f), scrollRect.viewport.rect.size);
			Vector2 sizeDifference = scrollRect.content.rect.size - scrollRect.viewport.rect.size;
			anchoredPosition.x = Mathf.Clamp(anchoredPosition.x, Mathf.Lerp(-sizeDifference.x, 0, scrollRect.content.pivot.x), Mathf.Lerp(0, sizeDifference.x, scrollRect.content.pivot.x));
			anchoredPosition.y = Mathf.Clamp(anchoredPosition.y, Mathf.Lerp(-sizeDifference.y, 0, scrollRect.content.pivot.y), Mathf.Lerp(0, sizeDifference.y, scrollRect.content.pivot.y));
			scrollRect.content.anchoredPosition = Vector2.Lerp(scrollRect.content.anchoredPosition, anchoredPosition, Mathf.Clamp01((float)(TimeSystem.RealTimeTracker.RealTimeTransform.DeltaTime * focusSpeed)));
			scrollRect.velocity = Vector2.zero;
		}
	}
}