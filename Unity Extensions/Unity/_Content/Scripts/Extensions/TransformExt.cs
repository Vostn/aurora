﻿using UnityEngine;

namespace Aurora.Unity
{
	public static class TransformExt
	{
		public static void ApplyIsometry(this Transform transformToModify, Transform targetIsometry)
		{
			transformToModify.transform.position = targetIsometry.transform.position;
			transformToModify.transform.rotation = targetIsometry.transform.rotation;
		}

		public static void ApplyIsometry(this Transform transformToModify, Isometry targetIsometry)
		{
			transformToModify.transform.position = (Vector3)targetIsometry.Position;
			transformToModify.transform.rotation = (UnityEngine.Quaternion)targetIsometry.Rotation;
		}
	}
}