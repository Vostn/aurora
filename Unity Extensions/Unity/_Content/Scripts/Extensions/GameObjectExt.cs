﻿using UnityEngine;

namespace Aurora.Unity
{
	public static class GameObjectExt
	{
		public static T GetOrAddComponent<T>(this GameObject target) where T : Component
		{
			T newComponent = target.GetComponent<T>();
			if (newComponent == null)
				return target.AddComponent<T>();
			else
				return newComponent;
		}
	}
}