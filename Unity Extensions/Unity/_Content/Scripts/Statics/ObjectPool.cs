﻿using System.Collections.Generic;
using UnityEngine;

namespace Aurora.Unity
{
	public static class ObjectPool
	{
		private static Dictionary<Object, Pool> poolsFromTemplate = new Dictionary<Object, Pool>();
		private static Dictionary<Object, Pool> poolsFromPooledObject = new Dictionary<Object, Pool>();

#if DEBUG

		static ObjectPool()
		{
			TimeSystem.Unity.UnityTimeTracker.UnityFrameTimeTransform.TimeTransform.OnIncrementStep += CheckForUnreturnedObjects;
		}

		/// <summary>
		/// Enumerates all withdrawn Objects. When one has been found to have been withdrawn, destroyed, and not deposited, it is cleaned up, and an error is printed.
		/// Should only be run in DEBUG mode.
		/// </summary>
		private static void CheckForUnreturnedObjects()
		{
			foreach (Object target in poolsFromPooledObject.Keys)
			{
				if (target.GetNullState() == NullState.FakeNull)
				{
					DestroyedObjectError(target);
					poolsFromPooledObject[target].DepositObject(target);
					return;
				}
			}
		}

#endif

		private static void DestroyedObjectError(Object target)
		{
			const string destroyedObjectWarning = "A pooled object was incorrectly destroyed!";
			Debug.LogWarning(destroyedObjectWarning, target);
		}

		public static void InitialisePool(Object template, int poolSize)
		{
			if (template == null)
			{
				Debug.LogError("Provided template is null!");
				return;
			}
			if (poolSize <= 0)
				Debug.LogWarning($"Pool size is negative or zero! (Template: \"{template}\")");
			Pool targetPool = null;
			if (poolsFromTemplate.TryGetValue(template, out targetPool))
			{
				if (targetPool.CurrentPoolSize < poolSize)
					targetPool.SetPoolSize(poolSize);
			}
			else
				poolsFromTemplate.Add(template, new Pool(template, poolSize));
		}

		public static T Withdraw<T>(T template) where T : Object
		{
			return Withdraw(template, Vector3.zero, UnityEngine.Quaternion.identity, null);
		}

		public static T Withdraw<T>(T template, Vector3 position, UnityEngine.Quaternion rotation, Transform parent = null) where T : Object
		{
			if (template == null)
			{
				Debug.LogError("Provided template is null!");
				return null;
			}
			Pool targetPool = null;
			if (!poolsFromTemplate.TryGetValue(template, out targetPool))
			{
				targetPool = new Pool(template, 1);
				poolsFromTemplate.Add(template, targetPool);
			}
			return (T)targetPool.WithdrawObject(position, rotation, parent);
		}

		public static void Deposit<T>(T target) where T : Object
		{
			NullState nullState = target.GetNullState();
			if (nullState == NullState.TrueNull)
			{
				Debug.LogError("Deposit target is null!");
				return;
			}
			else if (nullState == NullState.FakeNull)
				Debug.LogError("Deposit target has already been destroyed!");

			Pool targetPool = null;
			if (poolsFromPooledObject.TryGetValue(target, out targetPool))
				targetPool.DepositObject(target);
			else
				Debug.LogError("Deposit target has no source pool!");
		}

		public static bool PoolExistsForTemplate(Object template)
		{
			return poolsFromTemplate.ContainsKey(template);
		}

		public static bool ObjectIsPooledInstance(Object targetObject)
		{
			return poolsFromPooledObject.ContainsKey(targetObject);
		}

		private class Pool
		{
			private readonly Object template;
			private readonly Transform objectFolder;
			private int currentPoolSize = 0;
			private readonly Queue<Object> availableObjects = new Queue<Object>();

			public int CurrentPoolSize
			{
				get { return currentPoolSize; }
			}

			public Pool(Object template, int poolSize)
			{
				this.template = template;
				objectFolder = new GameObject($"Object Pool Folder ({template.name})").transform;
				Object.DontDestroyOnLoad(objectFolder.gameObject);
				SetPoolSize(poolSize);
			}

			public Object WithdrawObject()
			{
				return WithdrawObject(Vector3.zero, UnityEngine.Quaternion.identity, null);
			}

			public Object WithdrawObject(Vector3 position, UnityEngine.Quaternion rotation, Transform parent = null)
			{
				while (availableObjects.Count > 0 && availableObjects.Peek() == null)
				{
					DestroyedObjectError(availableObjects.Dequeue());
					--currentPoolSize;
				}
				if (availableObjects.Count == 0)
					SetPoolSize(CurrentPoolSize + 1);
				Object withdrawnObject = availableObjects.Dequeue();
				GameObject gameObjectTarget = withdrawnObject as GameObject;
				if (gameObjectTarget == null)
					gameObjectTarget = (withdrawnObject as Component)?.gameObject;
				if (gameObjectTarget != null)
				{
					gameObjectTarget.SetActive(true);
					gameObjectTarget.transform.position = position;
					gameObjectTarget.transform.rotation = rotation;
					gameObjectTarget.transform.SetParent(parent);
					gameObjectTarget.SetActive(true);
					gameObjectTarget.ExecuteEvent(new PoolWithdraw(gameObjectTarget), true);
				}
				return withdrawnObject;
			}

			public void DepositObject(Object target)
			{
				NullState nullState = target.GetNullState();
				if (nullState == NullState.TrueNull)
				{
					Debug.LogError("Deposit target is null!");
					return;
				}
				else if (nullState == NullState.FakeNull)
				{
					poolsFromPooledObject.Remove(target);
					--currentPoolSize;
				}
				else
				{
					availableObjects.Enqueue(target);
					GameObject gameObjectTarget = target as GameObject;
					if (gameObjectTarget == null)
						gameObjectTarget = (target as Component)?.gameObject;
					if (gameObjectTarget != null)
					{
						gameObjectTarget.SetActive(false);
						gameObjectTarget.transform.SetParent(objectFolder);
						gameObjectTarget.ExecuteEvent(new PoolDeposit(), true);
					}
				}
			}

			public void SetPoolSize(int newPoolSize)
			{
				if (newPoolSize < 0)
					Debug.LogWarning($"Pool size is negative or zero! (Template: \"{template}\")");
				newPoolSize = Mathf.Max(0, newPoolSize);
				while (CurrentPoolSize > newPoolSize && availableObjects.Count > 0)
					RemoveDepositedObject();
				while (CurrentPoolSize < newPoolSize)
					AddNewPooledObject();
			}

			private void AddNewPooledObject()
			{
				Object newObject = Object.Instantiate(template);
				newObject.name = newObject.name.Replace("(Clone)", "(Pooled)");
				GameObject gameObjectTarget = newObject as GameObject;
				if (gameObjectTarget == null)
					gameObjectTarget = (newObject as Component)?.gameObject;
				if (gameObjectTarget != null)
				{
					gameObjectTarget.name = gameObjectTarget.name.Replace("(Clone)", "(Pooled)");
					gameObjectTarget.SetActive(false);
					gameObjectTarget.transform.SetParent(objectFolder);
					gameObjectTarget.ExecuteEvent(new PoolDeposit(), true);
				}
				poolsFromPooledObject.Add(newObject, this);
				availableObjects.Enqueue(newObject);
				++currentPoolSize;
			}

			private void RemoveDepositedObject()
			{
				int startingCount = CurrentPoolSize;
				while (availableObjects.Count > 0 && availableObjects.Peek().GetNullState() != NullState.NotNull)
				{
					Object removedObject = availableObjects.Dequeue();
					DestroyedObjectError(removedObject);
					poolsFromPooledObject.Remove(removedObject);
					--currentPoolSize;
				}
				if (CurrentPoolSize == startingCount)
				{
					Object targetObject = availableObjects.Dequeue();
					poolsFromPooledObject.Remove(targetObject);
					Object.Destroy(targetObject);
					--currentPoolSize;
				}
			}
		}
	}
}