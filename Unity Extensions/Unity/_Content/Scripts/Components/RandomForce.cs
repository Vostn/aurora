﻿using UnityEngine;

namespace Aurora.Unity
{
	public class RandomForce : MonoBehaviour, IEventReceiver<PoolWithdraw>
	{
		[SerializeField]
		private ForceType forceType = ForceType.InitialImpulse;

		[SerializeField]
		private bool ignoreMass = false;

		[SerializeField]
		[Tooltip("The base direction for the force to be modified from.")]
		private Transform forceDirection;

		[SerializeField]
		private DoubleRange forceAmount = new DoubleRange(0.5, 1);

		[SerializeField]
		private DoubleRange spread = new DoubleRange(0, 0.25);

		private Double3 calculatedForce;
		private new Rigidbody rigidbody;
		private Random rand;

		private enum ForceType
		{
			InitialImpulse,
			ConstantForce,
		}

		void IEventReceiver<PoolWithdraw>.Triggered(PoolWithdraw data)
		{
			Initialise();
		}

		private void Reset()
		{
			forceDirection = transform;
		}

		private void Awake()
		{
			rigidbody = GetComponent<Rigidbody>();
			rand = new Random((ulong)System.DateTime.Now.Ticks, 0);
			Initialise();
		}

		private void OnValidate()
		{
			if (spread.Minimum < 0 || spread.Minimum > 1 || spread.Maximum < 0 || spread.Maximum > 1)
				spread = new DoubleRange(Math.Clamp(spread.Minimum, 0, 1), Math.Clamp(spread.Maximum, 0, 1));
		}

		private void Initialise()
		{
			CalculateRandomForce();
			if (forceType == ForceType.InitialImpulse)
				ApplyForce();
		}

		private void FixedUpdate()
		{
			if (forceType == ForceType.ConstantForce)
				ApplyForce();
		}

		private void CalculateRandomForce()
		{
			calculatedForce = rand.SpreadVector(forceDirection.forward, 0, rand.NextDouble(spread.Minimum, spread.Maximum)) * rand.NextDouble(forceAmount.Minimum, forceAmount.Maximum);
		}

		private void ApplyForce()
		{
			if (rigidbody == null)
			{
				Debug.LogWarning("No Rigidbody is on this GameObject! Force not applied.");
				return;
			}

			ForceMode mode = forceType == ForceType.InitialImpulse ? (ignoreMass ? ForceMode.VelocityChange : ForceMode.Impulse) : (ignoreMass ? ForceMode.Acceleration : ForceMode.Force);
			rigidbody.AddForce((Vector3)calculatedForce, mode);
		}
	}
}