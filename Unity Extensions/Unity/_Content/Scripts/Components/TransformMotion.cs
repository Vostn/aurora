﻿using UnityEngine;
using Aurora.TimeSystem;
using Aurora.TimeSystem.Unity;

namespace Aurora.Unity
{
	[ExecuteInEditMode]
	public class TransformMotion : TimeBehaviour
	{
		[SerializeField]
		[Tooltip("The update loop that this motion occurs in. UpdateType.Physics must be used for Kinematic Rigidbodies to have correct behaviour.")]
		private UpdateType updateType = UpdateType.Rendering;

		[SerializeField]
		[Tooltip("Does this transform have translation motion?")]
		private bool translation = false;

		[SerializeField]
		[Tooltip("The speed at which this transform translates, in units/s")]
		[Visibility(nameof(translation), MatchType.Equal, 1)]
		[Indent(1)]
		private float translationSpeed = 0.5f;

		[SerializeField]
		[Tooltip("The axis on which this transform translates.")]
		[Visibility(nameof(translation), MatchType.Equal, 1)]
		[Indent(1)]
		private Vector3 translationAxis = Vector3.right;

		[SerializeField]
		[Tooltip("All translations will occur with respect to this transform's axes.")]
		[Visibility(nameof(translation), MatchType.Equal, 1)]
		[Indent(1)]
		private Transform translationSpaceIdentity = null;

		[SerializeField]
		[Tooltip("Does this transform have rotation motion?")]
		private bool rotation = false;

		[SerializeField]
		[Tooltip("The speed at which this transform rotates, in the chosen units per second")]
		[Visibility(nameof(rotation), MatchType.Equal, 1)]
		[Indent(1)]
		private float rotationSpeed = 45.0f;

		[SerializeField]
		[Tooltip("The axis on which this transform rotates.")]
		[Visibility(nameof(rotation), MatchType.Equal, 1)]
		[Indent(1)]
		private Vector3 rotationAxis = Vector3.up;

		[SerializeField]
		[Tooltip("All rotations will treat this transform's rotation as the Identity rotation.")]
		[Visibility(nameof(rotation), MatchType.Equal, 1)]
		[Indent(1)]
		private Transform rotationSpaceIdentity = null;

		[SerializeField]
		[Tooltip("All rotations will occur around this transform's position.")]
		[Visibility(nameof(rotation), MatchType.Equal, 1)]
		[Indent(1)]
		private Transform rotationSpacePivot = null;

		[SerializeField]
		[Tooltip("Defines the unit of the rotation speed.")]
		[Visibility(nameof(rotation), MatchType.Equal, 1)]
		[Indent(1)]
		private RotationUnit rotationUnit = RotationUnit.Degrees;

		[SerializeField]
		[HideInInspector]
		private RotationUnit activeRotationUnit = RotationUnit.Degrees;

		private new Rigidbody rigidbody;

		public enum RotationUnit
		{
			MilliTau,
			Degrees,
		}
		
		public void SetRotationSpeed(float newRotationSpeed, RotationUnit newRotationUnit)
		{
			rotationSpeed = newRotationSpeed;
			rotationUnit = newRotationUnit;
		}

		public void SetRotationAxis(Vector3 newAxis)
		{
			rotationAxis = newAxis;
		}

		public void SetRotationSpaceIdentity(Transform newSpace)
		{
			rotationSpaceIdentity = newSpace;
		}

		public void SetRotationSpacePivot(Transform newSpace)
		{
			rotationSpacePivot = newSpace;
		}

		public void SetTranslationSpaceIdentity(Transform newSpace)
		{
			translationSpaceIdentity = newSpace;
		}

		public void SetRotate(bool willRotate)
		{
			rotation = willRotate;
		}

		public void SetTranslation(bool willTranslate)
		{
			translation = willTranslate;
		}

		public void TranslationSpeed(float translateSpeed)
		{
			translationSpeed = translateSpeed;
		}

		public void RotationSpeed(float rotateSpeed)
		{
			rotationSpeed = rotateSpeed;
		}

		private void Reset()
		{
			translationSpaceIdentity = transform;
			rotationSpaceIdentity = transform;
			rotationSpacePivot = transform;
		}

		private void OnValidate()
		{
			if (activeRotationUnit != rotationUnit)
			{
				if (rotationUnit == RotationUnit.Degrees)
				{
					if (activeRotationUnit == RotationUnit.MilliTau)
						rotationSpeed = (float)(rotationSpeed * Math.MilliTauToDeg);
				}
				if (rotationUnit == RotationUnit.MilliTau)
				{
					if (activeRotationUnit == RotationUnit.Degrees)
						rotationSpeed = (float)(rotationSpeed * Math.DegToMilliTau);
				}
				activeRotationUnit = rotationUnit;
			}
			rigidbody = GetComponent<Rigidbody>();
		}

		private void Start()
		{
			OnValidate();
		}

		private void Update()
		{
			if (Application.isPlaying && updateType == UpdateType.Rendering)
			{
				UpdateRotation(DeltaTime);
				UpdateTranslation(DeltaTime);
			}
		}

		private void FixedUpdate()
		{
			if (Application.isPlaying && updateType == UpdateType.Physics)
			{
				UpdateRotation(FixedDeltaTime);
				UpdateTranslation(FixedDeltaTime);
			}
		}

		private void UpdateRotation(double timePeriod)
		{
			if (rotation)
			{
				double rotationAmount = 0;
				if (rotationUnit == RotationUnit.MilliTau)
					rotationAmount = (float)(Math.MilliTauToDeg * rotationSpeed * timePeriod);
				else if (rotationUnit == RotationUnit.Degrees)
					rotationAmount = rotationSpeed * timePeriod;
				Vector3 transformedRotationAxis = rotationAxis;
				if (rotationSpaceIdentity != null)
					transformedRotationAxis = rotationSpaceIdentity.TransformDirection(rotationAxis);
				Vector3 rotationOrigin = Vector3.zero;
				if (rotationSpacePivot != null)
					rotationOrigin = rotationSpacePivot.position;
				if (rigidbody && updateType == UpdateType.Physics)
				{
					UnityEngine.Quaternion worldRotation = UnityEngine.Quaternion.AngleAxis((float)rotationAmount, transformedRotationAxis);
					transformedRotationAxis = UnityEngine.Quaternion.Inverse(rigidbody.rotation) * transformedRotationAxis;
					UnityEngine.Quaternion localRotation = UnityEngine.Quaternion.AngleAxis((float)rotationAmount, transformedRotationAxis);
					rigidbody.MoveRotation(rigidbody.rotation * localRotation);
					Vector3 newPosition = (worldRotation * (rigidbody.position - rotationOrigin)) + rotationOrigin;
					rigidbody.MovePosition(newPosition);
				}
				else
					transform.RotateAround(rotationOrigin, transformedRotationAxis, (float)rotationAmount);
			}
		}

		private void UpdateTranslation(double timePeriod)
		{
			if (translation)
			{
				Vector3 transformedTranslationAxis = translationAxis;
				if (translationSpaceIdentity != null)
					transformedTranslationAxis = translationSpaceIdentity.TransformDirection(translationAxis).normalized;
				Vector3 translationAmount = transformedTranslationAxis * translationSpeed * (float)timePeriod;
				if (rigidbody && updateType == UpdateType.Physics)
					rigidbody.MovePosition(rigidbody.position + translationAmount);
				else
					transform.Translate(translationAmount, Space.World);
			}
		}
	}
}