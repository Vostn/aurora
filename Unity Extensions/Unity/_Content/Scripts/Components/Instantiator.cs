﻿using UnityEngine;
using System.Collections.Generic;

namespace Aurora.Unity
{
	public class Instantiator : MonoBehaviour
	{
		[SerializeField]
		private Type type;

		[SerializeField]
		[Visibility(nameof(type), MatchType.Equal, (int)Type.Single)]
		private GameObject prefab;

		[SerializeField]
		[Visibility(nameof(type), MatchType.NotEqual, (int)Type.Single)]
		private PrefabGroup prefabGroup;

		[SerializeField]
		private Transform position;

		[SerializeField]
		private Transform rotation;

		[SerializeField]
		private Transform parent;

		private readonly List<int> indiciesAvailable = new List<int>();

		public enum Type
		{
			Single,
			Sequential,
			Random,
			Shuffle,
		}

		[ContextMenu("Instantiate")]
		public void Instantiate()
		{
			if (type == Type.Single)
			{
				CreatePrefab(prefab);
				return;
			}

			if (prefabGroup == null)
			{
				Debug.LogWarning($"PrefabGroup is null!");
				return;
			}

			if (prefabGroup.Assets.Count <= 0)
			{
				Debug.LogWarning($"PrefabGroup {prefabGroup} contains no prefabs!");
				return;
			}

			if (type == Type.Random)
			{
				CreatePrefab(prefabGroup.Assets[UnityEngine.Random.Range(0, prefabGroup.Assets.Count - 1)]);
				return;
			}

			if (type == Type.Sequential || type == Type.Shuffle)
			{
				if (indiciesAvailable.Count <= 0)
					for (int i = 0; i < prefabGroup.Assets.Count; ++i)
						indiciesAvailable.Add(i);

				int indexToPick = 0;
				if (type == Type.Shuffle)
					indexToPick = UnityEngine.Random.Range(0, indiciesAvailable.Count - 1);

				CreatePrefab(prefabGroup.Assets[indiciesAvailable[indexToPick]]);
				indiciesAvailable.RemoveAt(indexToPick);
			}

			throw new System.NotImplementedException($"No logic is implemented for given Instantiation type! (Name:\"{type}\")(Value:{(int)type})");
		}

		private void CreatePrefab(GameObject targetPrefab)
		{
			if (targetPrefab == null)
			{
				Debug.LogWarning("Prefab to instantiate is null!");
				return;
			}

			if (ObjectPool.PoolExistsForTemplate(targetPrefab))
				ObjectPool.Withdraw(targetPrefab, position.position, rotation.rotation, parent);
			else
				Instantiate(targetPrefab, position.position, rotation.rotation, parent);
		}

		private void Reset()
		{
			position = transform;
			rotation = transform;
		}
	}
}