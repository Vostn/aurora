﻿using System.Collections.Generic;
using UnityEngine;
using Aurora.Collections;

namespace Aurora.Unity
{
#if UNITY_EDITOR

	[ExecuteInEditMode]
#endif
	public class AverageSpline : MonoBehaviour
	{
		[SerializeField]
		private double averagingRange = 0;

		[SerializeField]
		private bool looped = false;

		[SerializeField]
		private List<Double3> serialisedNodes = new List<Double3>();

		private ListPath<Double3> nodePath;
		private ListPath<Double3> integratedSplinePath;

		private delegate T SampleMethodSigniture<T>(IReadOnlyPath<Double3> spline, double averagingRange, double sampleLocation, bool looped, ref AverageSpline<Double3>.Traveller traveller);

		public int NodeCount
		{
			get { return nodePath.NodeCount; }
		}

		public double IntegratedSplineLength
		{
			get
			{
				if (integratedSplinePath == null)
					Reintegrate();
				if (Looped)
					return integratedSplinePath.TotalLengthLooped;
				return integratedSplinePath.TotalLength;
			}
		}

		public double SplineLength
		{
			get
			{
				if (Looped)
					return nodePath.TotalLengthLooped;
				return nodePath.TotalLength;
			}
		}

		public bool Looped
		{
			get { return looped; }
		}

		private void Awake()
		{
			ReadSerialisedNodes();
#if !UNITY_EDITOR
			serialisedNodes = null;
#endif
		}

		public Double3 SamplePosition(double sampleLocationOnSpline, Space space, bool constantSpeed)
		{
			AverageSpline<Double3>.Traveller traveller = new AverageSpline<Double3>.Traveller();
			return SamplePosition(sampleLocationOnSpline, space, constantSpeed, ref traveller);
		}

		public Double3 SamplePosition(double sampleLocationOnSpline, Space space, bool constantSpeed, ref AverageSpline<Double3>.Traveller traveller)
		{
			Double3 value = InternalSample(AverageSpline<Double3>.SamplePosition, sampleLocationOnSpline, space, constantSpeed, looped, ref traveller);
			if (space == Space.World)
				value = LocalToWorldSpace(value);
			return value;
		}

		public Double3 SampleTangent(double sampleLocationOnSpline, Space space, bool constantSpeed)
		{
			AverageSpline<Double3>.Traveller traveller = new AverageSpline<Double3>.Traveller();
			return SampleTangent(sampleLocationOnSpline, space, constantSpeed, ref traveller);
		}

		public Double3 SampleTangent(double sampleLocationOnSpline, Space space, bool constantSpeed, ref AverageSpline<Double3>.Traveller traveller)
		{
			Double3 value = InternalSample(AverageSpline<Double3>.SampleTangent, sampleLocationOnSpline, space, constantSpeed, looped, ref traveller);
			if (space == Space.World)
				value = LocalToWorldSpace(value);
			return value;
		}

		public Tangency<Double3> SampleTangency(double sampleLocationOnSpline, Space space, bool constantSpeed)
		{
			AverageSpline<Double3>.Traveller traveller = new AverageSpline<Double3>.Traveller();
			return SampleTangency(sampleLocationOnSpline, space, constantSpeed, ref traveller);
		}

		public Tangency<Double3> SampleTangency(double sampleLocationOnSpline, Space space, bool constantSpeed, ref AverageSpline<Double3>.Traveller traveller)
		{
			Tangency<Double3> value =InternalSample(AverageSpline<Double3>.SampleTangency, sampleLocationOnSpline, space, constantSpeed, looped, ref traveller);
			if (space == Space.World)
				value = LocalToWorldSpace(value);
			return value;
		}

		private T InternalSample<T>(SampleMethodSigniture<T> sampleFunction, double sampleLocationOnSpline, Space space, bool constantSpeed, bool looped, ref AverageSpline<Double3>.Traveller traveller)
		{
			T value;
			if (!constantSpeed)
			{
				if (nodePath.NodeCount <= 0)
					return default;
				value = sampleFunction.Invoke(nodePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
			}
			else
			{
				if (integratedSplinePath == null)
					Reintegrate();
				if (integratedSplinePath.NodeCount <= 0)
					return default;
				value = sampleFunction.Invoke(integratedSplinePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
			}
			return value;
		}

		private void Reintegrate()
		{
			integratedSplinePath = AverageSpline<Double3>.Integrate(nodePath, averagingRange, 1, looped);
		}

		private void ReadSerialisedNodes()
		{
			nodePath = new ListPath<Double3>();
			for(int i = 0; i < serialisedNodes.Count; ++i)
				nodePath.AddNode(serialisedNodes[i]);
		}

		private void Reset()
		{
			ReadSerialisedNodes();
		}

		private void OnValidate()
		{
			averagingRange = Math.Max(averagingRange, 0);
			ReadSerialisedNodes();
#if UNITY_EDITOR
			RebuildSerialisedNodes();
#endif
			Reintegrate();
		}

		private void NewNode()
		{
			AddNode(Double3.Uniform(0), Space.Self);
		}

		public void AddNode(Double3 point, Space space)
		{
			if (space == Space.World)
				point = LocalToWorldSpace(point);
			nodePath.AddNode(point);
			OnPathChanged();
		}

		public void SetNode(int index, Double3 position, Space space)
		{
			if (space == Space.World)
				position = WorldToLocalSpace(position);
			nodePath.SetNode(index, position);
			OnPathChanged();
		}

		public Double3 GetNode(int index, Space space)
		{
			Double3 node = nodePath[index];
			if (space == Space.World)
				node = LocalToWorldSpace(node);
			return node;
		}

		public void InsertNode(int index, Double3 position, Space space)
		{
			if (space == Space.World)
				position = LocalToWorldSpace(position);
			nodePath.InsertNode(index, position);
			OnPathChanged();
		}

		public void RemoveNode(int index)
		{
			nodePath.RemoveNode(index);
			OnPathChanged();
		}

		private void OnPathChanged()
		{
			if (Application.isPlaying)
				integratedSplinePath = null;
#if UNITY_EDITOR
			RebuildSerialisedNodes();
#endif
		}

#if UNITY_EDITOR

		private void RebuildSerialisedNodes()
		{
			serialisedNodes.Clear();
			for (int i = 0; i < nodePath.NodeCount; ++i)
				serialisedNodes.Add(nodePath.GetNode(i));
		}

#endif

		private Double3 LocalToWorldSpace(Double3 point)
		{
			//TODO: Create Double4x4 type, and create ITransformable.
			return transform.TransformPoint((Vector3)point);
		}

		private Double3 WorldToLocalSpace(Double3 point)
		{
			//TODO: Create Double4x4 type, and create ITransformable.
			return transform.InverseTransformPoint((Vector3)point);
		}

		private Tangency<Double3> LocalToWorldSpace(Tangency<Double3> tangency)
		{
			//TODO: Create Double4x4 type, and create ITransformable.
			return new Tangency<Double3>(transform.TransformPoint((Vector3)tangency.Position), transform.TransformVector((Vector3)tangency.Tangent));
		}

		private Tangency<Double3> WorldToLocalSpace(Tangency<Double3> tangency)
		{
			//TODO: Create Double4x4 type, and create ITransformable.
			return new Tangency<Double3>(transform.InverseTransformPoint((Vector3)tangency.Position), transform.InverseTransformVector((Vector3)tangency.Tangent));
		}
	}
}