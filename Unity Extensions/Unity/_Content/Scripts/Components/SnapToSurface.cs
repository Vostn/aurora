﻿using UnityEngine;

namespace Aurora.Unity
{
	/// <summary>
	/// Designer friendly way to snap objects together.
	/// </summary>
	[DisallowMultipleComponent]
	[ExecuteInEditMode]
	public class SnapToSurface : MonoBehaviour
	{
		[Banner("Detection Settings")]
		[SerializeField]
		private Transform detectionOrigin;

		[SerializeField]
		private float detectionDistance = 3.0f;

		[SerializeField]
		private Vector3 detectionDirection = Vector3.down;

		[SerializeField]
		private float detectionSourceOffset = 0;

		[SerializeField]
		private Space targetSpace = Space.World;

		[SerializeField]
		private LayerMask targetLayers;

		[Banner("Behaviour Settings")]
		[SerializeField]
		private float snapOffset = 0.0f;

		[SerializeField]
		private bool rotateToSurfaceNormal = false;

		[SerializeField]
		private bool onlyInEditor = true;

		private void Snap()
		{
			RaycastHit hitInfo;
			Vector3 worldSpaceDirection = detectionDirection;
			if (targetSpace == Space.Self)
				worldSpaceDirection = transform.TransformDirection(detectionDirection);
			Vector3 startPosition = detectionOrigin.transform.position + (worldSpaceDirection * detectionSourceOffset);

			bool hit = Physics.Raycast(startPosition, worldSpaceDirection, out hitInfo, detectionDistance - detectionSourceOffset, targetLayers);

			if (hit)
				transform.position = hitInfo.point + (worldSpaceDirection * snapOffset);

			if (rotateToSurfaceNormal)
			{
				Vector3 currentUp = transform.up;
				Vector3 newUp = hitInfo.normal;
				Vector3 axis = Vector3.Cross(currentUp, newUp);
				float angle = Vector3.Angle(currentUp, newUp);
				transform.Rotate(axis, angle, Space.World);
			}
		}

		private void Update()
		{
			if (Application.isEditor || !onlyInEditor)
				Snap();
		}

		private void Reset()
		{
			detectionOrigin = transform;
		}

		private void OnValidate()
		{
			detectionDistance = Mathf.Max(0, detectionDistance);
			if (detectionOrigin == null)
				detectionOrigin = transform;
		}
	}
}