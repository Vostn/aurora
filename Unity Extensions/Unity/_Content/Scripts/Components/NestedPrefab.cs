﻿using UnityEngine;
using System.Collections.Generic;

namespace Aurora.Unity
{
#if UNITY_EDITOR

	[ExecuteInEditMode]
#endif
	public sealed class NestedPrefab : MonoBehaviour
	{
		[SerializeField]
		private PrefabMode prefabMode = PrefabMode.Single;

		[SerializeField]
		[AssetsOnlyAttribute]
		[Visibility(nameof(prefabMode), MatchType.Equal, (int)PrefabMode.Single)]
		private GameObject targetPrefab;

		[SerializeField]
		[Visibility(nameof(prefabMode), MatchType.Equal, (int)PrefabMode.Random)]
		private PrefabGroup targetPrefabGroup;

		[SerializeField]
		[HideInInspector]
		private GameObject currentPrefabInstance;

#if UNITY_EDITOR

		[SerializeField]
		[HideInInspector]
		private GameObject registeredTargetPrefabSingle;

		[SerializeField]
		[HideInInspector]
		private PrefabGroup registeredTargetPrefabGroup;

		private double randomPrefabCyclePeriod;
		private double randomPrefabLastCycleTime;
		private int randomPrefabCycleIndex;

		private GameObject currentVisualPrefab;

		private GameObject TargetVisualPrefab
		{
			get
			{
				if (prefabMode == PrefabMode.Single)
					return targetPrefab;
				else if (prefabMode == PrefabMode.Random)
				{
					if (targetPrefabGroup == null)
						return null;
					if (targetPrefabGroup.Assets.Count <= 0)
						return null;
					return targetPrefabGroup.Assets[randomPrefabCycleIndex];
				}
				else
					throw new System.NotImplementedException();
			}
		}

#endif

		private enum PrefabMode
		{
			Single,
			Random,
		}

#if UNITY_EDITOR

		[UnityEditor.Callbacks.DidReloadScripts]
		private static void HookupUpdate()
		{
			NestedPrefab[] allInstances = FindObjectsOfType<NestedPrefab>();
			for (int i = 0; i < allInstances.Length; ++i)
				UnityEditor.EditorApplication.update += allInstances[i].EditorUpdate;
		}

#endif

		private void Awake()
		{
			Update();
#if UNITY_EDITOR
			if (!Application.isPlaying)
				UnityEditor.EditorApplication.update += EditorUpdate;
#endif
		}

		private static void DestroyObject(GameObject target)
		{
			if (target != null)
			{
#if UNITY_EDITOR
				target.hideFlags = HideFlags.None;
				if (Application.isPlaying)
#endif
					Destroy(target);
#if UNITY_EDITOR
				else
					DestroyImmediate(target);
#endif
			}
		}

		private void DestroyInstance()
		{
			if (currentPrefabInstance != null)
			{
				DestroyObject(currentPrefabInstance);
			}
		}

		private void OnDestroy()
		{
			DestroyInstance();
#if UNITY_EDITOR
			UnityEditor.EditorApplication.update -= EditorUpdate;
#endif
		}

		private void SpawnPrefab(GameObject prefabToSpawn)
		{
			DestroyInstance();
			if (prefabToSpawn != null)
			{
#if UNITY_EDITOR
				UnityEditor.SerializedObject serializedObject = new UnityEditor.SerializedObject(this);
				serializedObject.Update();
				if (!Application.isPlaying)
				{
					currentPrefabInstance = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab(prefabToSpawn);
					currentPrefabInstance.transform.SetParent(transform);
					currentPrefabInstance.transform.localPosition = Vector3.zero;
					currentPrefabInstance.transform.localRotation = UnityEngine.Quaternion.identity;
					currentPrefabInstance.transform.localScale = prefabToSpawn.transform.localScale;
					currentPrefabInstance.name += "(Prefab)";
				}
				else
				{
#endif
					currentPrefabInstance = Instantiate(prefabToSpawn, transform.position, transform.rotation, transform);
					currentPrefabInstance.name = currentPrefabInstance.name.Replace("(Clone)", "(Prefab)");
#if UNITY_EDITOR
				}

				serializedObject.FindProperty(nameof(currentPrefabInstance)).objectReferenceValue = currentPrefabInstance;
				serializedObject.ApplyModifiedPropertiesWithoutUndo();

				if (!Application.isPlaying)
					HideGameObject(currentPrefabInstance, true);
				currentVisualPrefab = prefabToSpawn;
#endif
			}
		}

		private void Update()
		{
			UpdatePrefab();
		}

#if UNITY_EDITOR

		private void EditorUpdate()
		{
			Update();
			if (prefabMode == PrefabMode.Random && targetPrefabGroup != null && (randomPrefabLastCycleTime + randomPrefabCyclePeriod < UnityEditor.EditorApplication.timeSinceStartup || randomPrefabCycleIndex >= targetPrefabGroup.Assets.Count))
			{
				randomPrefabCyclePeriod = UnityEngine.Random.Range(0.75f, 1.25f);
				randomPrefabLastCycleTime = UnityEditor.EditorApplication.timeSinceStartup;
				randomPrefabCycleIndex = UnityEngine.Random.Range(0, targetPrefabGroup.Assets.Count);
			}
		}

		private void HideGameObject(GameObject target, bool recursive = false)
		{
			target.hideFlags = HideFlags.DontSaveInEditor | HideFlags.DontSaveInBuild | HideFlags.NotEditable;
			if (recursive)
				foreach (Transform child in target.transform)
					HideGameObject(child.gameObject);
		}

		private bool HasCircularReference()
		{
			Stack<GameObject> targetPrefabStack = new Stack<GameObject>();
			GameObject prefabSource = UnityEditor.PrefabUtility.GetCorrespondingObjectFromSource(gameObject);
			if (prefabSource == null)
				return false;
			GameObject prefabRoot = UnityEditor.PrefabUtility.GetNearestPrefabInstanceRoot(prefabSource);
			targetPrefabStack.Push(prefabRoot);
			return HasCircularReference(this, ref targetPrefabStack);
		}

		private static bool HasCircularReference(NestedPrefab root, ref Stack<GameObject> targetPrefabStack)
		{
			bool circularReference = HasCircularReference(root.targetPrefab, ref targetPrefabStack);
			if (root.targetPrefabGroup != null)
				for (int i = 0; i < root.targetPrefabGroup.Assets.Count; ++i)
					circularReference |= HasCircularReference(root.targetPrefabGroup.Assets[i], ref targetPrefabStack);
			return circularReference;
		}

		private static bool HasCircularReference(GameObject prefabInstance, ref Stack<GameObject> targetPrefabStack)
		{
			if (prefabInstance == null)
				return false;
			if (targetPrefabStack.Contains(prefabInstance))
				return true;
			targetPrefabStack.Push(prefabInstance);
			NestedPrefab[] scriptChildren = prefabInstance.GetComponentsInChildren<NestedPrefab>(true);
			foreach (NestedPrefab scriptChild in scriptChildren)
			{
				if (HasCircularReference(scriptChild, ref targetPrefabStack))
					return true;
			}
			targetPrefabStack.Pop();
			return false;
		}

		private bool ValidateRegisteredTargetPrefabSingle()
		{
			if (registeredTargetPrefabSingle != targetPrefab)
			{
				if (targetPrefab != null)
				{
					if (HasCircularReference())
					{
						UnityEditor.EditorUtility.DisplayDialog("Invalid Target Prefab!", "Using this target Prefab would create a circular reference of nested prefabs!", "Okay");
						targetPrefab = registeredTargetPrefabSingle;
						return false;
					}
				}
				registeredTargetPrefabSingle = targetPrefab;
				return true;
			}
			return false;
		}

		private bool ValidateRegisteredTargetPrefabGroup()
		{
			if (registeredTargetPrefabGroup != targetPrefabGroup)
			{
				if (targetPrefabGroup != null)
				{
					if (HasCircularReference())
					{
						UnityEditor.EditorUtility.DisplayDialog("Invalid Target PrefabGroup!", "Using this target PrefabGroup would create a circular reference of nested prefabs!", "Okay");
						targetPrefabGroup = registeredTargetPrefabGroup;
						return false;
					}
				}
				registeredTargetPrefabGroup = targetPrefabGroup;
				return true;
			}
			return false;
		}

#endif

		private void UpdatePrefab()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (ValidateRegisteredTargetPrefabSingle() || ValidateRegisteredTargetPrefabGroup() || currentPrefabInstance == null || currentVisualPrefab != TargetVisualPrefab)
					SpawnPrefab(TargetVisualPrefab);
			}
			else if (ValidateRegisteredTargetPrefabSingle() || ValidateRegisteredTargetPrefabGroup() || currentPrefabInstance == null)
			{
#else
			if (currentPrefabInstance == null)
			{
#endif
				if (prefabMode == PrefabMode.Single)
					SpawnPrefab(targetPrefab);
				else if (prefabMode == PrefabMode.Random)
					SpawnPrefab(targetPrefabGroup.Assets[UnityEngine.Random.Range(0, targetPrefabGroup.Assets.Count)]);
				else
					throw new System.NotImplementedException();
			}
		}
	}
}