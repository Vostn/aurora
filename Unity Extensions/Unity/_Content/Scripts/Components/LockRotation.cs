﻿using UnityEngine;

namespace Aurora.Unity
{
	/// <summary>
	/// Component used by designers to lock transform rotation values. Helps with animation or movement.
	/// </summary>
	[ExecuteInEditMode]
	public class LockRotation : MonoBehaviour
	{
		/// <summary>
		/// The space in which the rotation is locked.
		/// </summary>
		[SerializeField]
		private LockSpace lockSpace = LockSpace.Parent;

		/// <summary>
		/// The rotation that the transform will be locked too.
		/// </summary>
		[SerializeField]
		private UnityRotation rotation;

		/// <summary>
		/// The custom space in which the rotation is locked.
		/// </summary>
		[SerializeField]
		[Visibility(nameof(lockSpace), MatchType.Equal, (int)LockSpace.Custom)]
		private Transform customLockSpace = null;
		
		private Transform currentLockSpace;

		private enum LockSpace
		{
			Parent,
			World,
			Custom
		}

		private void Reset()
		{
			UpdateRotationValue();
		}

		private void Awake()
		{
			CalculateCustomLockSpace();
		}

		private void CalculateCustomLockSpace()
		{
			if (lockSpace == LockSpace.Parent)
				currentLockSpace = transform.parent;
			else if (lockSpace == LockSpace.World)
				currentLockSpace = null;
			else if (lockSpace == LockSpace.Custom)
				currentLockSpace = customLockSpace;
			else
				throw new System.NotImplementedException();
		}

		private void OnValidate()
		{
			CalculateCustomLockSpace();
		}

		private void UpdateRotationValue()
		{
			rotation = currentLockSpace == null ? transform.rotation : (transform.rotation * UnityEngine.Quaternion.Inverse(currentLockSpace.rotation));
		}

		private void Update()
		{
			ResetRotation();
		}

		private void ResetRotation()
		{
			transform.rotation = currentLockSpace == null ? rotation.Rotation : (currentLockSpace.rotation * rotation);
		}

		private void LateUpdate()
		{
			ResetRotation();
		}
	}
}