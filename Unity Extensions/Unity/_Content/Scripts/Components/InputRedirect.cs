﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using Aurora.TimeSystem.Unity;

namespace Aurora.Unity
{
	public class InputRedirect : MonoBehaviour, ISelectHandler
	{
		[SerializeField]
		private Mode mode;

		[SerializeField]
		[Label("Target Root")]
		[Visibility(nameof(mode), MatchType.Equal, (int)Mode.NearestChildOf)]
		private Transform childrenTarget;

		[SerializeField]
		[Label("Targets")]
		[Visibility(nameof(mode), MatchType.Equal, (int)Mode.NearestExplicit)]
		private List<Selectable> explicitTargets;
		
		public enum Mode
		{
			NearestChildOf,
			NearestExplicit,
		}

		void ISelectHandler.OnSelect(BaseEventData eventData)
		{
			Selectable closestSelectable = null;
			if (mode == Mode.NearestChildOf)
				closestSelectable = GetNearest(childrenTarget.GetComponentsInChildren<Selectable>());
			if (mode == Mode.NearestExplicit)
				closestSelectable = GetNearest(explicitTargets);
			else
				throw new System.NotImplementedException();

			//We delay this call because Unity cannot reselect within the same frame.
			if (closestSelectable != null)
				Scheduler.Schedule(new Hourglass(double.Epsilon, UnityTimeTracker.UnityFrameTimeTransform.TimeTransform), () => { closestSelectable.Select(); });
		}

		private Vector3 AverageVector3(IEnumerable<Vector3> vectors)
		{
			Vector3 sum = Vector3.zero;
			foreach (Vector3 vec in vectors)
				sum += vec;
			return sum / 4f;
		}

		private Selectable GetNearest(IEnumerable<Selectable> targets)
		{
			Vector3 position = transform.position;
			Vector3[] rectCorners = new Vector3[4];
			RectTransform rectTransform = transform as RectTransform;
			if (rectTransform != null)
			{
				rectTransform.GetWorldCorners(rectCorners);
				position = AverageVector3(rectCorners);
			}
			float shortestDistance = float.MaxValue;
			Selectable closestSelectable = null;
			foreach (Selectable target in targets)
			{
				if (!target.gameObject.activeInHierarchy)
					continue;
				Vector3 targetPosition = target.transform.position;
				RectTransform targetRectTransform = target.transform as RectTransform;
				if (targetRectTransform != null)
				{
					targetRectTransform.GetWorldCorners(rectCorners);
					targetPosition = AverageVector3(rectCorners);
				}
				float distance = Vector3.Distance(targetPosition, position);
				if (distance < shortestDistance)
				{
					shortestDistance = distance;
					closestSelectable = target;
				}
			}
			return closestSelectable;
		}
	}
}