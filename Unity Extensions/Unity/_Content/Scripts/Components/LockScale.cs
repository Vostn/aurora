﻿using UnityEngine;

namespace Aurora.Unity
{
	/// <summary>
	/// Component used by designers to lock transform scale values. Helps with animation or movement.
	/// </summary>
	[ExecuteInEditMode]
	public class LockScale : MonoBehaviour
	{
		[SerializeField]
		private LockSpace lockSpace;

		/// <summary>
		/// Marks which scale axes should be locked.
		/// </summary>
		[SerializeField]
		private Bool3 lockScale;

		/// <summary>
		/// The values that the transform scale should be locked to.
		/// </summary>
		[SerializeField]
		[Visibility(nameof(lockScale) + ".x", MatchType.Equal, 1)]
		[Visibility(nameof(lockScale) + ".y", MatchType.Equal, 1)]
		[Visibility(nameof(lockScale) + ".z", MatchType.Equal, 1)]
		private Vector3 scale;

		private enum LockSpace
		{
			Parent,
			World
		}

		private void Reset()
		{
			UpdateScaleValues();
		}

		private void OnValidate()
		{
			UpdateScaleValues();
		}

		private void UpdateScaleValues()
		{
			Vector3 scaleValue = transform.localScale;
			if (lockSpace == LockSpace.World)
				scaleValue = transform.lossyScale;

			if (!lockScale.x)
				scale.x = scaleValue.x;
			if (!lockScale.y)
				scale.y = scaleValue.y;
			if (!lockScale.z)
				scale.z = scaleValue.z;
		}

		private void Update()
		{
			ResetPosition();
		}

		private void ResetPosition()
		{
			UpdateScaleValues();

			Vector3 scaleValue = transform.localScale;
			if (lockSpace == LockSpace.World)
				scaleValue = transform.lossyScale;

			if (lockScale.x)
				scaleValue.x = scale.x;
			if (lockScale.y)
				scaleValue.y = scale.y;
			if (lockScale.z)
				scaleValue.z = scale.z;

			if (lockScale.x || lockScale.y || lockScale.z)
				transform.localScale = scaleValue;
			if (lockSpace == LockSpace.World && transform.parent != null)
			{
				Vector3 parScale = transform.parent.lossyScale;
				transform.localScale = Vector3.Scale(scaleValue, new Vector3(1 / parScale.x, 1 / parScale.y, 1 / parScale.z));
			}
		}

		private void LateUpdate()
		{
			ResetPosition();
		}
	}
}