﻿using UnityEngine;

namespace Aurora.Unity
{
	using Aurora.Collections;
	using Aurora.TimeSystem.Unity;

	public class Jitter1D : TimeBehaviour
	{
		[SerializeField]
		[Tooltip("The update loop this jitter gets applied on.")]
		private UpdateType updateType = UpdateType.Rendering;
		
		[SerializeField]
		[Tooltip("The path distance the smoothing takes into account.")]
		private double averagingRange = 1;

		[SerializeField]
		[Tooltip("How fast the sample travels along the path.")]
		private double speed = 1;

		[SerializeField]
		[Tooltip("The distance range between each generated point in the jitter path.")]
		private DoubleRange perPointDistance;

		[SerializeField]
		[Tooltip("The possible range all points in the jitter path.")]
		private DoubleRange overallPointLimit;

		[SerializeField]
		private double outputScale = 1;

		[SerializeField]
		private FloatEvent onUpdate;

		private QueuePath<Double1> pointPath = new QueuePath<Double1>();
		private double sampleLocationOnSpline = 0;

		private void Awake()
		{
			AddInitialPosition();
		}

		private void Reset()
		{
			pointPath = new QueuePath<Double1>();
			sampleLocationOnSpline = 0;
			perPointDistance = new DoubleRange(0, 1);
			onUpdate = new FloatEvent();
			overallPointLimit = new DoubleRange(0, 1);
		}

		private void Update()
		{
			if (updateType != UpdateType.Rendering)
				return;
			InternalUpdate(DeltaTime);
		}

		private void FixedUpdate()
		{
			if (updateType != UpdateType.Physics)
				return;
			InternalUpdate(FixedDeltaTime);
		}

		private void InternalUpdate(double deltaTime)
		{
			sampleLocationOnSpline += speed * deltaTime;
			onUpdate.Invoke((float)Sample());
		}

		private void OnValidate()
		{
			averagingRange = System.Math.Max(averagingRange, 0);
			if (perPointDistance.Minimum < 0 || perPointDistance.Maximum < 0.001)
				perPointDistance = new DoubleRange(System.Math.Max(perPointDistance.Minimum, 0), System.Math.Max(perPointDistance.Maximum, 0.001));
		}

		private void AddInitialPosition()
		{
			double newPosition = UnityEngine.Random.Range((float)overallPointLimit.Minimum, (float)overallPointLimit.Maximum);
			pointPath.EnqueueNode(newPosition);
		}

		private void EnqueuePosition()
		{
			double newPosition = pointPath[pointPath.NodeCount - 1];
			newPosition += UnityEngine.Random.Range((float)perPointDistance.Minimum, (float)perPointDistance.Maximum) * (UnityEngine.Random.value > 0.5f ? 1 : -1);

			if (newPosition > overallPointLimit.Maximum)
				newPosition = overallPointLimit.Maximum;
			else if (newPosition < overallPointLimit.Minimum)
				newPosition = overallPointLimit.Minimum;

			pointPath.EnqueueNode(newPosition);
		}

		private void ValidateKeyframes()
		{
			if (pointPath.NodeCount > 1)
			{
				double nodeGap = (pointPath[0] - pointPath[1]).Magnitude;
				while (pointPath.NodeCount > 1 && nodeGap < sampleLocationOnSpline - (averagingRange * 0.5))
				{
					sampleLocationOnSpline -= nodeGap;
					pointPath.DequeueNode();
					nodeGap = (pointPath[0] - pointPath[1]).Magnitude;
				}
			}

			while (pointPath.TotalLength < sampleLocationOnSpline + (averagingRange * 0.5))
				EnqueuePosition();
		}

		private double Sample()
		{
			ValidateKeyframes();
			return AverageSpline<Double1>.SamplePosition(pointPath, averagingRange, sampleLocationOnSpline, false) * outputScale;
		}
	}
}