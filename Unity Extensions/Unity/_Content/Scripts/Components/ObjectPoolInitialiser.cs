﻿using UnityEngine;
using System.Diagnostics;
using System.Collections.Generic;
using Aurora.TimeSystem.Unity;

namespace Aurora.Unity
{
	public class ObjectPoolInitialiser : MonoBehaviour
	{
		[SerializeField]
		private Object templateObject;

		[SerializeField]
		private int initialPoolSize = 1;

		[SerializeField]
		private SpawnType spawnType = SpawnType.InstantBatch;

		[SerializeField]
		[Tooltip("Budget for spawning in milliseconds")]
		[Range(0, 16)]
		[Visibility(nameof(spawnType), MatchType.Equal, (int)SpawnType.BudgetedIncremental)]
		private float spawnBudget = 2;

		private static long lastFrameSpawned = 0;
		private static List<ObjectPoolInitialiser> budgetedInitialisers;
		private int spawnedObjectCount = 0;

		private enum SpawnType
		{
			InstantBatch,
			BudgetedIncremental,
		}

		private void Awake()
		{
			if (spawnType == SpawnType.InstantBatch)
			{
				ObjectPool.InitialisePool(templateObject, initialPoolSize);
			}
			else if (spawnType == SpawnType.BudgetedIncremental)
			{
				if (budgetedInitialisers == null)
					budgetedInitialisers = new List<ObjectPoolInitialiser>();
				budgetedInitialisers.Add(this);
			}
		}

		private void OnDestroy()
		{
			budgetedInitialisers.Remove(this);
		}

		private void Update()
		{
			if (lastFrameSpawned < UnityTimeTracker.UnityFrameTimeTransform.TimeTransform.StepCount)
			{
				SpawnBudgetedForFrame();
				lastFrameSpawned = UnityTimeTracker.UnityFrameTimeTransform.TimeTransform.StepCount;
			}
		}

		private static void SpawnBudgetedForFrame()
		{
			Stopwatch budgetStopwatch = new Stopwatch();
			double offset = 0;
			for (int i = 0; i < budgetedInitialisers.Count; ++i)
			{
				ObjectPoolInitialiser initialiser = budgetedInitialisers[i];
				if (offset > initialiser.spawnBudget)
				{
					offset -= initialiser.spawnBudget;
					continue;
				}
				budgetStopwatch.Restart();
				while (true)
				{
					ObjectPool.InitialisePool(initialiser.templateObject, ++initialiser.spawnedObjectCount);
					if (initialiser.spawnedObjectCount >= initialiser.initialPoolSize)
					{
						budgetedInitialisers.RemoveAt(i--);
						break;
					}
					double elapsedTime = budgetStopwatch.Elapsed.TotalMilliseconds;
					if (elapsedTime + offset > initialiser.spawnBudget)
					{
						offset = elapsedTime - initialiser.spawnBudget;
						break;
					}
				}
			}
		}

#if DEBUG

		private void OnValidate()
		{
			initialPoolSize = System.Math.Max(initialPoolSize, 1);
		}

#endif
	}
}