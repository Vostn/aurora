﻿using UnityEngine;

namespace Aurora.Unity
{
	public class RandomInitialTransform : MonoBehaviour, IEventReceiver<PoolWithdraw>
	{
		[SerializeField]
		[Button(nameof(ApplyRandomTransform), "Apply Now")]
		private bool applyAtRuntime = true;

		[SerializeField]
		private bool randomRotation = false;

		[SerializeField]
		[Indent(1)]
		[Visibility(nameof(randomRotation), MatchType.Equal, 1)]
		private Axes rotationType;

		[SerializeField]
		[Indent(1)]
		[Visibility(nameof(randomRotation), MatchType.Equal,1)]
		private Transform rotationSpace;

		[SerializeField]
		[Indent(1)]
		[Visibility(nameof(rotationType),MatchType.Equal, (int)Axes.CustomAxis)]
		[Visibility(nameof(randomRotation),MatchType.Equal,1)]
		private Vector3 rotationAxis;

		[SerializeField]
		private bool randomScale = false;

		[SerializeField]
		[Indent(1)]
		[Visibility(nameof(randomScale), MatchType.Equal, 1)]
		private ScaleType scaleType = ScaleType.OverrideOriginalValue;

		[SerializeField]
		[Indent(1)]
		[Visibility(nameof(randomScale), MatchType.Equal, 1)]
		private AnimationCurve scaleDistribution;

		public enum Axes
		{
			AllAxes,
			XAxis,
			YAxis,
			ZAxis,
			CustomAxis,
		}

		public enum ScaleType
		{
			OverrideOriginalValue,
			MultiplyOriginalValue,
		}

		private void Reset()
		{
			rotationSpace = transform;
		}

		public void ApplyRandomTransform()
		{
			if (randomRotation)
				ApplyRotation();
			if (randomScale)
				ApplyScale();
		}

		void IEventReceiver<PoolWithdraw>.Triggered(PoolWithdraw data)
		{
			Awake();
		}

		private void Awake()
		{
			if (applyAtRuntime)
				ApplyRandomTransform();
		}

		private void ApplyScale()
		{
			float min = float.MaxValue;
			float max = float.MinValue;
			if (scaleDistribution.keys.Length == 0)
				Debug.LogWarning("No curve keyframes defined! Scale has not been changed.", this);
			else
			{
				for (int i = 0; i < scaleDistribution.keys.Length; ++i)
				{
					Keyframe key = scaleDistribution.keys[i];
					min = Mathf.Min(key.time, min);
					max = Mathf.Max(key.time, max);
				}
				float sampleValue = scaleDistribution.Evaluate(UnityEngine.Random.Range(min, max));
				if (scaleType == ScaleType.OverrideOriginalValue)
					transform.localScale = sampleValue * Vector3.one;
				else if (scaleType == ScaleType.MultiplyOriginalValue)
					transform.localScale *= sampleValue;
				else
					Debug.LogWarning("No scale type defined! Scale has not been changed.", this);
			}
		}

		private void ApplyRotation()
		{
			if (rotationType == Axes.AllAxes)
				transform.rotation = UnityEngine.Random.rotationUniform;
			else
			{
				float randomAngle = UnityEngine.Random.Range(0.0f, 360.0f);
				UnityEngine.Quaternion newRotation = UnityEngine.Quaternion.identity;

				switch (rotationType)
				{
					case Axes.XAxis:
						newRotation = UnityEngine.Quaternion.AngleAxis(randomAngle, Vector3.right);
						break;

					case Axes.YAxis:
						newRotation = UnityEngine.Quaternion.AngleAxis(randomAngle, Vector3.up);
						break;

					case Axes.ZAxis:
						newRotation = UnityEngine.Quaternion.AngleAxis(randomAngle, Vector3.forward);
						break;

					case Axes.CustomAxis:
						if (rotationAxis == Vector3.zero)
							Debug.Log("Custom Rotation Axis is Vector3.zero. Rotation has not been changed.", this);
						else
							newRotation = UnityEngine.Quaternion.AngleAxis(randomAngle, rotationAxis.normalized);
						break;

					default:
						break;
				}
				
				if(rotationSpace == null)
					transform.rotation = newRotation;
				else
					transform.rotation = rotationSpace.rotation * newRotation;
			}
		}
	}
}