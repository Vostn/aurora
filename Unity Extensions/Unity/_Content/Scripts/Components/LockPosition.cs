﻿using UnityEngine;

namespace Aurora.Unity
{
	/// <summary>
	/// Component used by designers to lock transform position values. Helps with animation or movement.
	/// </summary>
	[ExecuteInEditMode]
	public class LockPosition : MonoBehaviour
	{
		/// <summary>
		/// The space in which the position values are locked.
		/// </summary>
		[SerializeField]
		private LockSpace lockSpace = LockSpace.Parent;

		/// <summary>
		/// The custom space in which the position values are locked.
		/// </summary>
		[SerializeField]
		[Visibility(nameof(lockSpace), MatchType.Equal, (int)LockSpace.Custom)]
		private Transform customLockSpace = null;

		/// <summary>
		/// Marks which position axes should be locked.
		/// </summary>
		[SerializeField]
		private Bool3 lockPosition;

		/// <summary>
		/// The values that the transform position should be locked to.
		/// </summary>
		[SerializeField]
		[Visibility(nameof(lockPosition) + ".x", MatchType.Equal, 1)]
		[Visibility(nameof(lockPosition) + ".y", MatchType.Equal, 1)]
		[Visibility(nameof(lockPosition) + ".z", MatchType.Equal, 1)]
		private Vector3 position;

		private Transform currentLockSpace;

		private enum LockSpace
		{
			Parent,
			World,
			Custom
		}

		private void Reset()
		{
			UpdatePositionValues();
		}

		private void Awake()
		{
			CalculateCustomLockSpace();
		}

		private void CalculateCustomLockSpace()
		{
			if (lockSpace == LockSpace.Parent)
				currentLockSpace = transform.parent;
			else if (lockSpace == LockSpace.World)
				currentLockSpace = null;
			else if (lockSpace == LockSpace.Custom)
				currentLockSpace = customLockSpace;
			else
				throw new System.NotImplementedException();
		}

		private void OnValidate()
		{
			CalculateCustomLockSpace();
			UpdatePositionValues();
		}

		private void UpdatePositionValues()
		{
			Vector3 positionValue = currentLockSpace == null ? transform.position : currentLockSpace.InverseTransformPoint(transform.position);

			if (!lockPosition.x)
				position.x = positionValue.x;
			if (!lockPosition.y)
				position.y = positionValue.y;
			if (!lockPosition.z)
				position.z = positionValue.z;
		}

		private void Update()
		{
			ResetPosition();
		}

		private void ResetPosition()
		{
			UpdatePositionValues();

			Vector3 positionValue = currentLockSpace == null ? transform.position : currentLockSpace.InverseTransformPoint(transform.position);

			if (lockPosition.x)
				positionValue.x = position.x;
			if (lockPosition.y)
				positionValue.y = position.y;
			if (lockPosition.z)
				positionValue.z = position.z;

			if (lockPosition.x || lockPosition.y || lockPosition.z)
				transform.position = currentLockSpace == null ? positionValue : currentLockSpace.TransformPoint(positionValue);
		}

		private void LateUpdate()
		{
			ResetPosition();
		}
	}
}