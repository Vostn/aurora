﻿using UnityEngine;
using Aurora.TimeSystem.Unity;

namespace Aurora.Unity
{
	public class AverageSplineTraveller : TimeBehaviour
	{
		[SerializeField]
		private AverageSpline spline;

		[SerializeField]
		private double speed;

		[SerializeField]
		private bool constantSpeed = false;

		[SerializeField]
		private RotationType rotationType;

		private AverageSpline<Double3>.Traveller traveller;

		[SerializeField]
		[Visibility(nameof(rotationType), MatchType.Equal, (int)RotationType.FaceTangent)]
		private Double3 rotationUpVector = Vector3.up;

		public double SplineLocation { get; set; }

		private System.Func<double, double, double, double> clampFunc;

		public AverageSpline Spline
		{
			get { return spline; }
			set { spline = value; }
		}

		public double Speed
		{
			get { return speed; }
			set { speed = value; }
		}

		private enum RotationType
		{
			None,
			FaceTangent,
			FollowTangent,
		}

		private void Awake()
		{
			traveller = new AverageSpline<Double3>.Traveller();
		}

		private void Update()
		{
			SplineLocation += DeltaTime * speed;
			clampFunc = Math.Clamp;
			if (spline.Looped)
				clampFunc = Math.Repeat;
			if (constantSpeed)
				SplineLocation = clampFunc(SplineLocation, 0, spline.IntegratedSplineLength);
			else
				SplineLocation = clampFunc(SplineLocation, 0, spline.SplineLength);

			if(rotationType == RotationType.None)
			{
				Double3 position = spline.SamplePosition(SplineLocation, Space.World, constantSpeed, ref traveller);
				transform.position = (Vector3)position;
			}
			else if(rotationType == RotationType.FaceTangent || rotationType == RotationType.FollowTangent)
			{
				Tangency<Double3> tangency = spline.SampleTangency(SplineLocation, Space.World, constantSpeed, ref traveller);
				transform.position = (Vector3)tangency.Position;
				if (rotationType == RotationType.FaceTangent)
					transform.rotation = UnityEngine.Quaternion.LookRotation((Vector3)tangency.Tangent, (Vector3)rotationUpVector);
				else if (rotationType == RotationType.FollowTangent)
					transform.rotation = UnityEngine.Quaternion.LookRotation((Vector3)tangency.Tangent, transform.up);
				else
					throw new System.NotImplementedException();
			}
			else
				throw new System.NotImplementedException();
		}
	}
}