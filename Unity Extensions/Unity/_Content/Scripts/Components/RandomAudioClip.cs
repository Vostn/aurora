﻿using UnityEngine;

namespace Aurora.Unity
{
	public class RandomAudioClip : MonoBehaviour
	{
		[SerializeField]
		private AudioClipGroup audioClips;

		[SerializeField]
		private AudioSource audioSource;
		
		private void OnEnable()
		{
			if (audioSource == null)
			{
				AudioSourceWarning();
				return;
			}
			if (audioSource.playOnAwake)
			{
				PickNewSound();
				audioSource.Play();
			}
		}

		private void AudioSourceWarning()
		{
			Debug.LogWarning($"{nameof(RandomAudioClip)} doesn't have a reference to an {nameof(AudioSource)}!");
		}

		private void PickNewSound()
		{
			if (audioSource == null)
			{
				AudioSourceWarning();
				return;
			}
			if (audioClips == null)
			{
				Debug.LogError($"{nameof(RandomAudioClip)} doesn't have a reference to an AudioClipGroup!");
				return;
			}
			if (audioClips.Assets.Count == 0)
			{
				Debug.LogError($"{nameof(RandomAudioClip)} {nameof(AudioClipGroup)} reference doesn't have any AudioClips!");
				return;
			}
			audioSource.clip = audioClips.Assets[UnityEngine.Random.Range(0, audioClips.Assets.Count)];
		}
	}
}