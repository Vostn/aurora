﻿using UnityEngine;

namespace Aurora.Unity
{
	public class AudioVariation : MonoBehaviour
	{
		/// <summary>
		/// The audio source component we are modifying.
		/// </summary>
		[SerializeField]
		private AudioSource audioSource;

		/// <summary>
		/// The type of variance we are applying.
		/// </summary>
		[SerializeField]
		private VarianceType varianceType = VarianceType.Percentage;

		/// <summary>
		/// The value of the volume variance. Function depends on VarianceType.
		/// </summary>
		[SerializeField]
		public float volumeVariance;

		/// <summary>
		/// The value of the pitch variance. Function depends on VarianceType.
		/// </summary>
		[SerializeField]
		public float pitchVariance;

#if UNITY_EDITOR

		/// <summary>
		/// current audio variance type. Used to track changes.
		/// </summary>
		[SerializeField]
		[HideInInspector]
		private VarianceType currentVarianceType = VarianceType.Percentage;

#endif

		/// <summary>
		/// Variance type enumeration.
		/// </summary>
		private enum VarianceType
		{
			/// <summary>
			/// The variance is percentage based (0-100).
			/// </summary>
			Percentage,

			/// <summary>
			/// The variance is fraction based (0-1)
			/// </summary>
			Fraction,
		}

		/// <summary>
		/// Initialisation
		/// </summary>
		private void Awake()
		{
			///When this instance awakes, we apply the variance to the audio source.
			if (varianceType == VarianceType.Percentage)
			{
				audioSource.volume *= 0.01f * (100.0f + UnityEngine.Random.Range(-volumeVariance, volumeVariance));
				audioSource.pitch *= 0.01f * (100.0f + UnityEngine.Random.Range(-pitchVariance, pitchVariance));
			}
			else if (varianceType == VarianceType.Fraction)
			{
				audioSource.volume *= 1.0f + UnityEngine.Random.Range(-volumeVariance, volumeVariance);
				audioSource.pitch *= 1.0f + UnityEngine.Random.Range(-pitchVariance, pitchVariance);
			}
		}

#if UNITY_EDITOR

		/// <summary>
		/// Monobehaviour.OnVlaidate function. Called in editor when changes are made.
		/// </summary>
		private void OnValidate()
		{
			//If we change the variance type, we must also change the value magnitudes to match.
			if (currentVarianceType != varianceType)
			{
				if (currentVarianceType == VarianceType.Fraction && varianceType == VarianceType.Percentage)
				{
					volumeVariance *= 100.0f;
					pitchVariance *= 100.0f;
				}
				else if (currentVarianceType == VarianceType.Percentage && varianceType == VarianceType.Fraction)
				{
					volumeVariance /= 100.0f;
					pitchVariance /= 100.0f;
				}
				currentVarianceType = varianceType;
			}
		}

#endif
	}
}