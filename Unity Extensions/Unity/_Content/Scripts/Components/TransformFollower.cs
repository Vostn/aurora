﻿using UnityEngine;
using Aurora.TimeSystem.Unity;
using Aurora.Collections;

namespace Aurora.Unity
{
	public class TransformFollower : TimeBehaviour
	{
		[SerializeField]
		private Transform transformToFollow;

		[SerializeField]
		private bool followPosition = true;

		[SerializeField]
		private bool followRotation = true;

		[SerializeField]
		private FollowType followType;

		[SerializeField]
		[Visibility(nameof(followType), MatchType.Equal, (int)FollowType.Distance)]
		private float followDistance = 1;

		[SerializeField]
		[Visibility(nameof(followType), MatchType.Equal, (int)FollowType.TimeDelay)]
		private float followDelay = 1;

		[SerializeField]
		[Tooltip("The minimum distance between recorded points")]
		private float recordingDistanceThreshold = 0.01f;

		private readonly IndexedQueue<TargetRecord> positionHistory = new IndexedQueue<TargetRecord>();

		private enum FollowType
		{
			TimeDelay,
			Distance,
		}

		private void LateUpdate()
		{
			if (transformToFollow == null)
			{
				positionHistory.Clear();
				return;
			}
			positionHistory.Enqueue(new TargetRecord(transformToFollow.position, transformToFollow.rotation, (float)Time));
			if (followType == FollowType.Distance || followType == FollowType.TimeDelay)
			{
				float followValue = 0;
				if (followType == FollowType.Distance)
					followValue = followDistance;
				else if (followType == FollowType.TimeDelay)
					followValue = followDelay;
				else
					throw new System.NotImplementedException();

				if (followValue == 0)
				{
					if (followPosition)
						transform.position = transformToFollow.position;
					if (followRotation)
						transform.rotation = transformToFollow.rotation;
					return;
				}

				float totalDistance = 0;
				float stepDistance = 0;

				if (positionHistory.Count > 1)
				{
					int pointIndex = positionHistory.Count - 1;
					while (pointIndex > 0)
					{
						if (followType == FollowType.Distance)
							stepDistance = (positionHistory[pointIndex].Position - positionHistory[pointIndex - 1].Position).magnitude;
						else if (followType == FollowType.TimeDelay)
							stepDistance = (positionHistory[pointIndex].Time - positionHistory[pointIndex - 1].Time);
						else
							throw new System.NotImplementedException();

						if (totalDistance + stepDistance > followValue)
							break;
						else
							totalDistance += stepDistance;
						--pointIndex;
					}
					if (pointIndex > 0)
					{
						TargetRecord recordA = positionHistory[pointIndex];
						TargetRecord recordB = positionHistory[pointIndex - 1];
						float lerpValue = followValue;
						if (pointIndex != positionHistory.Count - 1)
						{
							lerpValue -= totalDistance;
							int requiredPoints = positionHistory.Count - (pointIndex - 1);
							while (positionHistory.Count > requiredPoints)
								positionHistory.Dequeue();
						}
						lerpValue /= stepDistance;
						if (followPosition)
							transform.position = Vector3.Lerp(recordA.Position, recordB.Position, lerpValue);
						if (followRotation)
							transform.rotation = UnityEngine.Quaternion.Slerp(recordA.Rotation, recordB.Rotation, lerpValue);
					}
					else
						ApplyFirstRecord();
				}
				else
					ApplyFirstRecord();

				if (positionHistory.Count > 1 && Vector3.Distance(positionHistory[positionHistory.Count - 2].Position, positionHistory[positionHistory.Count - 1].Position) < recordingDistanceThreshold)
					positionHistory.Unqueue();
			}
			else
				throw new System.NotImplementedException();
		}

		private void ApplyFirstRecord()
		{
			TargetRecord record = positionHistory[0];
			if (followPosition)
				transform.position = record.Position;
			if (followRotation)
				transform.rotation = record.Rotation;
		}

		private void OnValidate()
		{
			followDistance = System.Math.Max(followDistance, 0);
			followDelay = System.Math.Max(followDelay, 0);
			recordingDistanceThreshold = System.Math.Max(recordingDistanceThreshold, 0);
		}

		private struct TargetRecord
		{
			public Vector3 Position { get; private set; }
			public UnityEngine.Quaternion Rotation { get; private set; }
			public float Time { get; private set; }

			public TargetRecord(Vector3 position, UnityEngine.Quaternion rotation, float time)
			{
				Position = position;
				Rotation = rotation;
				Time = time;
			}
		}
	}
}