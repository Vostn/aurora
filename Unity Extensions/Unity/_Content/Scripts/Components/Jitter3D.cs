﻿using UnityEngine;

namespace Aurora.Unity
{
	using Aurora.Collections;
	using Aurora.TimeSystem.Unity;

	public class Jitter3D : TimeBehaviour
	{
		[SerializeField]
		[Tooltip("The update loop this jitter gets applied on.")]
		private UpdateType updateType = UpdateType.Rendering;

		[SerializeField]
		[Tooltip("The path distance the smoothing takes into account.")]
		private double averagingRange = 1;

		[SerializeField]
		[Tooltip("How fast the sample travels along the path.")]
		private double speed = 1;

		[SerializeField]
		[Tooltip("The distance range between each generated point in the jitter path.")]
		private DoubleRange perPointDistance;

		[SerializeField]
		[Tooltip("The possible range all points in the jitter path.")]
		private LimitConfig overallPointLimit;

		[SerializeField]
		private double outputScale = 1;

		[SerializeField]
		private Vector3Event onUpdate;

		private QueuePath<Double3> pointPath = new QueuePath<Double3>();
		private double sampleLocationOnSpline = 0;

		private void Awake()
		{
			AddInitialPosition();
		}

		private void Reset()
		{
			pointPath = new QueuePath<Double3>();
			sampleLocationOnSpline = 0;
			perPointDistance = new DoubleRange(0, 1);
			onUpdate = new Vector3Event();
			overallPointLimit = new LimitConfig()
			{
				BoundingBox = new Bounds(Vector3.zero, Vector3.one),
				CurrentLimitType = LimitConfig.LimitType.Spherical,
				Range = new Double3Range(Double3.Uniform(-1), Double3.Uniform(1)),
				SphericalRadiusLimit = new DoubleRange(0, 1)
			};
		}

		private void Update()
		{
			if (updateType != UpdateType.Rendering)
				return;
			InternalUpdate(DeltaTime);
		}

		private void FixedUpdate()
		{
			if (updateType != UpdateType.Physics)
				return;
			InternalUpdate(FixedDeltaTime);
		}

		private void InternalUpdate(double deltaTime)
		{
			sampleLocationOnSpline += speed * deltaTime;
			onUpdate.Invoke((Vector3)Sample());
		}

		private void OnValidate()
		{
			averagingRange = System.Math.Max(averagingRange, 0);
			if (perPointDistance.Minimum < 0 || perPointDistance.Maximum < 0.001)
				perPointDistance = new DoubleRange(System.Math.Max(perPointDistance.Minimum, 0), System.Math.Max(perPointDistance.Maximum, 0.001));
			if (overallPointLimit.CurrentLimitType == LimitConfig.LimitType.Spherical)
				if (overallPointLimit.SphericalRadiusLimit.Minimum < 0 || overallPointLimit.SphericalRadiusLimit.Maximum < 0.001)
					overallPointLimit.SphericalRadiusLimit = new DoubleRange(System.Math.Max(overallPointLimit.SphericalRadiusLimit.Minimum, 0), System.Math.Max(overallPointLimit.SphericalRadiusLimit.Maximum, 0.001));
		}

		private void AddInitialPosition()
		{
			Vector3 newPosition = Vector3.zero;
			switch (overallPointLimit.CurrentLimitType)
			{
				case LimitConfig.LimitType.Spherical:
					DoubleRange limits = overallPointLimit.SphericalRadiusLimit;
					newPosition = (UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range((float)limits.Minimum, (float)limits.Maximum));
					break;

				case LimitConfig.LimitType.BoundingBox:
					Bounds bounds = overallPointLimit.BoundingBox;
					newPosition = new Vector3(UnityEngine.Random.Range(bounds.min.x, bounds.min.x), UnityEngine.Random.Range(bounds.min.y, bounds.min.y), UnityEngine.Random.Range(bounds.min.z, bounds.min.z));
					break;

				case LimitConfig.LimitType.Range:
					Double3Range range = overallPointLimit.Range;
					newPosition = new Vector3(UnityEngine.Random.Range((float)range.Minimum.x, (float)range.Maximum.x), UnityEngine.Random.Range((float)range.Minimum.y, (float)range.Maximum.y), UnityEngine.Random.Range((float)range.Minimum.z, (float)range.Maximum.z));
					break;

				default:
					throw new System.NotImplementedException();
			}

			pointPath.EnqueueNode(newPosition);
		}

		private void EnqueuePosition()
		{
			Double3 newPosition = pointPath[pointPath.NodeCount - 1];
			newPosition += (UnityEngine.Random.onUnitSphere * UnityEngine.Random.Range((float)perPointDistance.Minimum, (float)perPointDistance.Maximum));

			switch (overallPointLimit.CurrentLimitType)
			{
				case LimitConfig.LimitType.Spherical:
					double newPositionSqrMag = newPosition.SqrMagnitude;
					if (newPositionSqrMag > overallPointLimit.SphericalRadiusLimit.Maximum * overallPointLimit.SphericalRadiusLimit.Maximum)
						newPosition = newPosition.Normalised * overallPointLimit.SphericalRadiusLimit.Maximum;
					else if (newPositionSqrMag < overallPointLimit.SphericalRadiusLimit.Minimum * overallPointLimit.SphericalRadiusLimit.Minimum)
						newPosition = newPosition.Normalised * overallPointLimit.SphericalRadiusLimit.Minimum;
					break;

				case LimitConfig.LimitType.BoundingBox:
					Double3Range boundsLimits = new Double3Range(overallPointLimit.BoundingBox.min, overallPointLimit.BoundingBox.max);
					newPosition = Double3.Clamp(newPosition, boundsLimits.Minimum, boundsLimits.Maximum);
					break;

				case LimitConfig.LimitType.Range:
					Double3Range rangeLimits = overallPointLimit.Range;
					newPosition = Double3.Clamp(newPosition, rangeLimits.Minimum, rangeLimits.Maximum);
					break;

				default:
					throw new System.NotImplementedException();
			}
			pointPath.EnqueueNode(newPosition);
		}

		private void ValidateKeyframes()
		{
			if (pointPath.NodeCount > 1)
			{
				double nodeGap = pointPath.DistanceToNextNode(0);
				while (pointPath.NodeCount > 1 && nodeGap < sampleLocationOnSpline - (averagingRange * 0.5))
				{
					sampleLocationOnSpline -= nodeGap;
					pointPath.DequeueNode();
					nodeGap = pointPath.DistanceToNextNode(0);
				}
			}

			while (pointPath.TotalLength < sampleLocationOnSpline + (averagingRange * 0.5))
				EnqueuePosition();
		}

		private Double3 Sample()
		{
			ValidateKeyframes();
			return AverageSpline<Double3>.SamplePosition(pointPath, averagingRange, sampleLocationOnSpline, false) * outputScale;
		}

		[System.Serializable]
		private struct LimitConfig
		{
			[SerializeField]
			private LimitType currentLimitType;

			[SerializeField]
			[Indent(1)]
			[Visibility(nameof(currentLimitType), MatchType.Equal, (int)LimitType.Spherical)]
			private DoubleRange sphericalRadiusLimit;

			[SerializeField]
			[Indent(1)]
			[Visibility(nameof(currentLimitType), MatchType.Equal, (int)LimitType.BoundingBox)]
			private Bounds boundingBox;

			[SerializeField]
			[Indent(1)]
			[Visibility(nameof(currentLimitType), MatchType.Equal, (int)LimitType.Range)]
			private Double3Range range;

			public enum LimitType
			{
				Spherical,
				BoundingBox,
				Range,
			}

			public LimitType CurrentLimitType
			{
				get { return currentLimitType; }
				set { currentLimitType = value; }
			}

			public DoubleRange SphericalRadiusLimit
			{
				get { return sphericalRadiusLimit; }
				set { sphericalRadiusLimit = value; }
			}

			public Bounds BoundingBox
			{
				get { return boundingBox; }
				set { boundingBox = value; }
			}

			public Double3Range Range
			{
				get { return range; }
				set { range = value; }
			}
		}
	}
}