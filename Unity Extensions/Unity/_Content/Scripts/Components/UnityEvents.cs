﻿using UnityEngine;

namespace Aurora.Unity
{
	[AddComponentMenu("")]
	public class UnityEvents : MonoBehaviour
	{
		private static UnityEvents instance = null;

		public static event System.Action On_Update;

		public static event System.Action On_FixedUpdate;

		public static event System.Action On_LateUpdate;

		public static event System.Action On_ApplicationQuit;

		public static event System.Action<bool> On_ApplicationFocus;

		public static event System.Action<bool> On_ApplicationPause;

		public static event System.Action On_GUI;

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void VerifyInstance()
		{
			GameObject container = new GameObject("Aurora Unity Event Receiver");
			instance = container.AddComponent<UnityEvents>();
			container.hideFlags = HideFlags.NotEditable;
			DontDestroyOnLoad(instance);
		}

		private void OnGUI()
		{
			On_GUI?.Invoke();
		}

		private void OnApplicationQuit()
		{
			On_ApplicationQuit?.Invoke();
		}

		private void Update()
		{
			On_Update?.Invoke();
		}

		private void FixedUpdate()
		{
			On_FixedUpdate?.Invoke();
		}

		private void OnApplicationFocus(bool focus)
		{
			On_ApplicationFocus?.Invoke(focus);
		}

		private void OnApplicationPause(bool paused)
		{
			On_ApplicationPause?.Invoke(paused);
		}

		private void LateUpdate()
		{
			On_LateUpdate?.Invoke();
		}
	}
}