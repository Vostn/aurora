﻿using System.Collections.Generic;
using UnityEngine;
using System;
using Aurora.TimeSystem.Unity;

namespace Aurora.Unity
{
	[ExecuteInEditMode]
	public class PitchShiftOnRepeat : TimeBehaviour
	{
		private static Dictionary<string, PitchStepInfo> allPitchStepInfo = new Dictionary<string, PitchStepInfo>();

		[SerializeField]
		private AudioSource audioSource;

		[SerializeField]
		private PitchDirection pitchDirection = PitchDirection.PitchUp;

		[SerializeField]
		private FloatRange pitchRange = new FloatRange(1, 2);

		[SerializeField]
		private int totalPitchSteps = 10;

		[SerializeField]
		private float firstRevertDelay = 1;

		[SerializeField]
		private float revertDelay = 0.5f;

		[SerializeField]
		[Hide]
		private string guid;
		
		private enum PitchDirection
		{
			PitchUp,
			PitchDown,
		}

		private void Reset()
		{
			GetNewGUID();
		}

		private void GetNewGUID()
		{
			guid = Guid.NewGuid().ToString();
		}

		private void Awake()
		{
			if (!Application.isPlaying)
				GetNewGUID();
		}

		private void AudioSourceWarning()
		{
			Debug.LogWarning($"{nameof(PitchShiftOnRepeat)} doesn't have an {nameof(AudioSource)} reference!");
		}

		private void UpdatePitch()
		{
			if (audioSource == null)
			{
				AudioSourceWarning();
				return;
			}

			PitchStepInfo pitchStepInfo = new PitchStepInfo(0, float.NegativeInfinity);
			if (allPitchStepInfo.ContainsKey(guid))
				pitchStepInfo = allPitchStepInfo[guid];

			float timeSinceLastPlay = (float)Time - pitchStepInfo.TimeLastPlayed;

			int pitchStepsTaken = pitchStepInfo.PitchStepsTaken;

			if (pitchStepsTaken > 0 && timeSinceLastPlay > firstRevertDelay)
			{
				timeSinceLastPlay -= firstRevertDelay;
				pitchStepsTaken -= 1;
				while (pitchStepsTaken > 0 && timeSinceLastPlay > revertDelay)
				{
					timeSinceLastPlay -= revertDelay;
					pitchStepsTaken -= 1;
				}
			}
			
			pitchStepsTaken = Mathf.Clamp(pitchStepsTaken+1, 0, totalPitchSteps);

			if (pitchDirection == PitchDirection.PitchUp)
			{
				double multiplier = System.Math.Pow(pitchRange.Maximum / pitchRange.Minimum, 1f / totalPitchSteps);
				audioSource.pitch = pitchRange.Minimum * (float)System.Math.Pow(multiplier, pitchStepsTaken);
			}
			else if (pitchDirection == PitchDirection.PitchDown)
			{
				double multiplier = System.Math.Pow(pitchRange.Minimum / pitchRange.Maximum, 1f / totalPitchSteps);
				audioSource.pitch = pitchRange.Maximum * (float)System.Math.Pow(multiplier, pitchStepsTaken);
			}
			else
				throw new NotImplementedException();

			allPitchStepInfo[guid] = new PitchStepInfo(pitchStepsTaken, (float)Time);
		}

		private void OnEnable()
		{
			if (!Application.isPlaying)
				return;
			if (audioSource == null)
			{
				AudioSourceWarning();
				return;
			}
			if (audioSource.playOnAwake)
				UpdatePitch();
		}

		private struct PitchStepInfo
		{
			public int PitchStepsTaken { get; private set; }
			public float TimeLastPlayed { get; private set; }

			public PitchStepInfo(int pitchStepsTaken, float timeLastPlayed)
			{
				TimeLastPlayed = timeLastPlayed;
				PitchStepsTaken = pitchStepsTaken;
			}
		}
	}
}