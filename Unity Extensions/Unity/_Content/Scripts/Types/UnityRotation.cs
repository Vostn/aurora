﻿using UnityEngine;

namespace Aurora.Unity
{
	[System.Serializable]
	public struct UnityRotation
	{
		[SerializeField]
		[HideInInspector]
		private UnityEngine.Quaternion rotation;

#pragma warning disable 0414

		[SerializeField]
		[HideInInspector]
		private Vector3 eulerRotation;

#pragma warning restore 0414

		public UnityEngine.Quaternion Rotation
		{
			get { return rotation; }
			set
			{
				eulerRotation = value.eulerAngles;
				rotation = value;
			}
		}

		public Vector3 EulerRotation
		{
			get { return rotation.eulerAngles; }
			set
			{
				eulerRotation = value;
				rotation.eulerAngles = value;
			}
		}

		public UnityRotation(UnityEngine.Quaternion rotation)
		{
			this.rotation = rotation;
			this.eulerRotation = rotation.eulerAngles;
		}

		public UnityRotation(Vector3 eulerRotation)
		{
			this.rotation = UnityEngine.Quaternion.Euler(eulerRotation);
			this.eulerRotation = eulerRotation;
		}

		public static implicit operator UnityEngine.Quaternion(UnityRotation rotation)
		{
			return rotation.Rotation;
		}

		public static implicit operator UnityRotation(Vector3 eulerRotation)
		{
			return new UnityRotation(eulerRotation);
		}

		public static implicit operator UnityRotation(UnityEngine.Quaternion rotation)
		{
			return new UnityRotation(rotation);
		}
	}
}