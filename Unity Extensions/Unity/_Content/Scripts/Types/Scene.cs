﻿using UnityEngine;

#if UNITY_EDITOR

using UnityEditor;

#endif

namespace Aurora.Unity
{
	/// <summary>
	/// Stores information about a Scene
	/// </summary>
	[System.Serializable]
	public struct Scene
#if UNITY_EDITOR
		: ISerializationCallbackReceiver
#endif
	{
		/// <summary>
		/// The build index for this scene.
		/// </summary>
		[SerializeField]
		[HideInInspector]
		private int buildIndex;

		
#if UNITY_EDITOR

		/// <summary>
		/// A reference to the SceneAsset this struct refers to.
		/// </summary>
		[SerializeField]
		private SceneAsset scene;

#endif

		[SerializeField]
		[HideInInspector]
		private string name;

		public string Name
		{
			get { return name; }
		}

		/// <summary>
		/// The build index of the scene.
		/// </summary>
		public int BuildIndex
		{
			get { return buildIndex; }
		}

#if UNITY_EDITOR

		void ISerializationCallbackReceiver.OnBeforeSerialize()
		{
			name = string.Empty;
			if (scene)
				name = scene.name;

			// First we set the build index to the default invalid value.
			// If we find a valid value within this function, this will be changed.
			buildIndex = -1;

			// First, we check if we have a scene attached
			if (scene)
			{
				// If we do, then we request the path of the asset from the asset database.
				string rawScenePath = AssetDatabase.GetAssetPath(scene);

				// This variable will store the build index of the scenes as we iterate over the array
				int sampledBuildIndex = 0;

				// Here we retrieve and store the array of scenes in the build settings.
				// The reason that we sample, modify, and re-set this array is because "EditorBuildSettings.scenes" is a
				// property that returns a copy of the scene array, not a pointer to the actual array stored by EditorBuildSettings.
				EditorBuildSettingsScene[] scenesArray = EditorBuildSettings.scenes;

				for (int i = 0; i < scenesArray.Length; ++i)
				{
					// We check to see if this is the scene that we are serialising, and that it is active.
					if (scenesArray[i].path == rawScenePath && scenesArray[i].enabled)
					{
						// If it is, then we set our build index
						buildIndex = sampledBuildIndex;
						// Then we escape.
						return;
					}
					// If it's not, we increment the build index, and move to the next scene.
					else if (scenesArray[i].enabled)
						sampledBuildIndex++;
				}
			}
		}
		
		void ISerializationCallbackReceiver.OnAfterDeserialize()
		{
			// Intentionally empty
		}

#endif
	}
}