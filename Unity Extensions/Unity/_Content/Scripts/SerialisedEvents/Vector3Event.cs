﻿using UnityEngine;
using UnityEngine.Events;

namespace Aurora.Unity
{
	[System.Serializable]
	public class Vector3Event : UnityEvent<Vector3> { };
}