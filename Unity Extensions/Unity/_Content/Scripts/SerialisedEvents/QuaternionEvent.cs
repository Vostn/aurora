﻿using UnityEngine.Events;

namespace Aurora.Unity
{
	[System.Serializable]
	public class QuaternionEvent : UnityEvent<UnityEngine.Quaternion> { };
}