﻿using UnityEngine.Events;

namespace Aurora.Unity
{
	[System.Serializable]
	public class FloatEvent : UnityEvent<float> { };
}