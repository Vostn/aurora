﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public class InfoBoxAttribute : UnityEngine.PropertyAttribute
	{
		public enum Type
		{
			None,
			Info,
			Warning,
			Error,
		}

		public InfoBoxAttribute(string text, Type boxType = Type.None)
		{
			BoxType = boxType;
			Text = text;
		}

		public Type BoxType { get; private set; }
		public string Text { get; private set; }
	}
}