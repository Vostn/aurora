﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public class BannerAttribute : UnityEngine.PropertyAttribute
	{
		public readonly string title;
		public readonly string subtitle;

		public BannerAttribute(string title = "", string subtitle = "")
		{
			this.title = title;
			this.subtitle = subtitle;
		}
	}
}