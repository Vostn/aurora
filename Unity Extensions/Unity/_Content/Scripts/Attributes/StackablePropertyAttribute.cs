﻿namespace Aurora.Unity
{
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true, Inherited = true)]
	public abstract class StackablePropertyAttribute : UnityEngine.PropertyAttribute
	{
		protected StackablePropertyAttribute()
		{
			base.order = ((int.MaxValue / 4) * 3);
		}

		public new int order
		{
			get
			{
				return base.order - ((int.MaxValue / 4) * 3);
			}
			set
			{
				base.order = value + ((int.MaxValue / 4) * 3);
			}
		}
	}
}