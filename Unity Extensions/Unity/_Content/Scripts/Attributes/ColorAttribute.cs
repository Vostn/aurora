﻿using UnityEngine;

namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
	public class ColorAttribute : StackablePropertyAttribute
	{
		public Color DisplayColor { get; private set; }

		public ColorAttribute(float red, float green, float blue)
		{
			DisplayColor = new Color(red,green,blue);
		}
	}
}