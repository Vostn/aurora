﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public class ButtonAttribute : StackablePropertyAttribute
	{
		public string FunctionName { get; private set; }
		public string LabelText { get; private set; }

		public ButtonAttribute(string functionName, string labelText)
		{
			FunctionName = functionName;
			LabelText = labelText;
		}
	}
}