﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
	public class LabelAttribute : StackablePropertyAttribute
	{
		public string LabelTitle { get; private set; }

		private LabelAttribute()
		{

		}

		public LabelAttribute(string labelTitle)
		{
			LabelTitle = labelTitle;
		}
	}
}
