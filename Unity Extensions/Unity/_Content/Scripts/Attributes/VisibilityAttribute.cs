﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = true)]
	public sealed class VisibilityAttribute : StackablePropertyAttribute
	{
		/// <summary>
		/// The name of the enum property to check.
		/// </summary>
		public string TargetProperty { get; private set; }

		/// <summary>
		/// The match type that must occur for the property to be enabled.
		/// </summary>
		public MatchType MatchType { get; private set; }

		/// <summary>
		/// The enum value that the target enum property will be compared to.
		/// </summary>
		public int ComparisonValue { get; private set; }

		public VisibilityAttribute(string targetProperty, MatchType matchType, int comparisonValue)
		{
			TargetProperty = targetProperty;
			MatchType = matchType;
			ComparisonValue = comparisonValue;
		}

		private VisibilityAttribute()
		{
		}
	}
}