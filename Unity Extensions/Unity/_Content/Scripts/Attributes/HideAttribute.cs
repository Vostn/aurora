﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
	public class HideAttribute : StackablePropertyAttribute
	{
	}
}