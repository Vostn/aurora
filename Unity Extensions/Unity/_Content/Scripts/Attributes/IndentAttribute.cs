﻿namespace Aurora.Unity
{
	[System.Diagnostics.Conditional("UNITY_EDITOR")]
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
	public class IndentAttribute : StackablePropertyAttribute
	{
		public int IndentLevel { get; private set; } = 1;

		public IndentAttribute(int indentLevel)
		{
			IndentLevel = System.Math.Max(indentLevel, 0);
		}
	}
}