﻿namespace Aurora.Unity
{
	[System.AttributeUsage(System.AttributeTargets.Field, AllowMultiple = false)]
	public class TypeIs : UnityEngine.PropertyAttribute
	{
		public System.Type RequiredType { get; private set; }

		private TypeIs()
		{
		}

		public TypeIs(System.Type requiredType)
		{
			RequiredType = requiredType;
		}
	}
}