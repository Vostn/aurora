﻿using UnityEngine;

namespace Aurora.Unity
{
	public static class IsometryExt
	{
		public static void ApplyToTransform(this Isometry isometry, Transform target)
		{
			target.ApplyIsometry(isometry);
		}
	}
}
