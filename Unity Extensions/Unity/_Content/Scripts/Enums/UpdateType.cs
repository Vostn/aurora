﻿namespace Aurora.Unity
{
	public enum UpdateType
	{
		Rendering = 0,
		Physics = 1,
	}
}