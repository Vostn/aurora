﻿namespace Aurora
{
	public interface IEventReceiver<T> where T : IEventData
	{
		void Triggered(T data);
	}
}
