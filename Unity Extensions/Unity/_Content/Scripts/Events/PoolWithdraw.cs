﻿using UnityEngine;

namespace Aurora
{
	public class PoolWithdraw : IEventData
	{
		public GameObject Root { private set; get; }

		public PoolWithdraw(GameObject root)
		{
			Root = root;
		}
	}
}