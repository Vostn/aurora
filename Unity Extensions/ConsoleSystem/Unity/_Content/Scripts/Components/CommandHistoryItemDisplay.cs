﻿using UnityEngine;
using UnityEngine.UI;

namespace Aurora.ConsoleSystem.Unity
{
	[AddComponentMenu("")]
	public class CommandHistoryItemDisplay : MonoBehaviour
	{
		[SerializeField]
		private GameObject highlightBacking;

		[SerializeField]
		private Text text;

		public void SetHighlight(bool highlight)
		{
			highlightBacking.SetActive(highlight);
		}

		public void Initialise(string text)
		{
			this.text.text = text;
		}
	}
}