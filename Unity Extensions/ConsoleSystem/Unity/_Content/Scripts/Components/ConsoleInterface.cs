﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Reflection;
using System.Text;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Aurora.Unity;

namespace Aurora.ConsoleSystem.Unity
{
	[AddComponentMenu("")]
	public class ConsoleInterface : MonoBehaviour
	{
		private static ConsoleInterface consoleInstance;

		[SerializeField]
		private InputField inputField;

		[SerializeField]
		private Transform logTextParent;

		[SerializeField]
		private GameObject toggleParent;

		[SerializeField]
		private TextLogItemDisplay textLogEntryTemplate;

		[SerializeField]
		private CommandPredictionItemDisplay commandPredictionTemplate;

		[SerializeField]
		private ScrollRect predictionScrollRect;

		[SerializeField]
		private CommandHistoryItemDisplay commandHistoryTemplate;

		[SerializeField]
		private ScrollRect historyScrollRect;

		[SerializeField]
		private Text userInputDisplay;

		[SerializeField]
		private GameObject userInputHighlighter;

		private bool supressChangeCallback = false;
		private GameObject lastSelectedObject = null;
		private TextLogItemDisplay currentTextLogEntry;

		private readonly Queue<TextLogItemDisplay> allTextLogEntries = new Queue<TextLogItemDisplay>();

		private readonly Queue<TextLogItem> threadedLogQueue = new Queue<TextLogItem>();

		private readonly List<CommandPredictionItemDisplay> commandPredictionObjects = new List<CommandPredictionItemDisplay>();
		private readonly List<CommandHistoryItemDisplay> commandHistoryObjects = new List<CommandHistoryItemDisplay>();

		[RuntimeInitializeOnLoadMethod(RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void Initialise()
		{
#if AURORA_CONSOLE || DEBUG
			GameObject consolePrefab = Resources.Load("Console Prefab") as GameObject;
			Console.AddCommands(Assembly.GetAssembly(typeof(Object)));
			Instantiate(consolePrefab, Vector3.zero, UnityEngine.Quaternion.identity);
			consoleInstance.gameObject.name = "Aurora Console";
			DontDestroyOnLoad(consoleInstance.gameObject);
#endif
		}

		private static void UnityLogMessageReceived(string condition, string stackTrace, LogType type)
		{
			if (Console.DebugInfoLevel > 0)
			{
				string text = "";
				bool error = type == LogType.Assert || type == LogType.Error || type == LogType.Exception;
				if (Console.DebugInfoLevel == DebugInfoLevel.AllStackTrace || (Console.DebugInfoLevel == DebugInfoLevel.ErrorStackTrace && error))
					text = condition + System.Environment.NewLine + stackTrace;
				else
					text = condition;
				if (type == LogType.Log)
					Console.PrintText(text);
				if (type == LogType.Warning)
					Console.PrintWarning(text);
				if (error)
					Console.PrintError(text);
			}
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerExposed)]
		private static void ClearConsole()
		{
			foreach (TextLogItemDisplay textLogEntry in consoleInstance.allTextLogEntries)
				ObjectPool.Deposit(textLogEntry);
			consoleInstance.allTextLogEntries.Clear();
		}

		private void Awake()
		{
			if (consoleInstance == null)
				consoleInstance = this;
			else
			{
				Destroy(this);
				Debug.LogError($"Second instance of {nameof(ConsoleInterface)} created! This is not supported! Destroying second instance...", this);
			}
			Application.logMessageReceivedThreaded += UnityLogMessageReceived;
			inputField.onValueChanged.AddListener(OnInputChanged);
			Console.NewEntryLogged += Console_NewEntryLogged;
			ObjectPool.InitialisePool(commandPredictionTemplate, 20);
			ObjectPool.InitialisePool(commandHistoryTemplate, 20);
			ObjectPool.InitialisePool(textLogEntryTemplate, 20);
		}

		private void OnDestroy()
		{
			if (consoleInstance == this)
			{
				Console.NewEntryLogged -= Console_NewEntryLogged;
				inputField.onValueChanged.RemoveListener(OnInputChanged);
				Application.logMessageReceivedThreaded -= UnityLogMessageReceived;
			}
		}

		private void Console_NewEntryLogged(TextLogItem newEntry)
		{
			lock (threadedLogQueue)
				threadedLogQueue.Enqueue(newEntry);
		}

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.BackQuote))
				ToggleConsole();
			if (toggleParent.activeInHierarchy)
				UpdateConsole();
		}

		private void ProcessQueuedLogs()
		{
			TextLogItem newEntry = DequeueLogItem();
			while (newEntry != null)
			{
				if (currentTextLogEntry == null || currentTextLogEntry.TextComponent.text.Length > 2000)
				{
					currentTextLogEntry = ObjectPool.Withdraw(textLogEntryTemplate);
					currentTextLogEntry.gameObject.SetActive(true);
					currentTextLogEntry.TextComponent.text = "";
					currentTextLogEntry.transform.SetParent(logTextParent, false);
					currentTextLogEntry.transform.SetAsLastSibling();
					allTextLogEntries.Enqueue(currentTextLogEntry);
				}
				else
					currentTextLogEntry.TextComponent.text += System.Environment.NewLine;

				if (newEntry.Type == TextLogItemType.Error)
					currentTextLogEntry.TextComponent.text += "<color=red>";
				else if (newEntry.Type == TextLogItemType.Warning)
					currentTextLogEntry.TextComponent.text += "<color=orange>";
				else if (newEntry.Type == TextLogItemType.HelpHeader)
					currentTextLogEntry.TextComponent.text += "<color=lime>";
				else if (newEntry.Type == TextLogItemType.InputEcho)
					currentTextLogEntry.TextComponent.text += "<color=cyan>";

				currentTextLogEntry.TextComponent.text += NullifyRichText(newEntry.Text);

				if (newEntry.Type != TextLogItemType.Information)
					currentTextLogEntry.TextComponent.text += "</color>";

				if (allTextLogEntries.Count > 50)
					ObjectPool.Deposit(allTextLogEntries.Dequeue());

				newEntry = DequeueLogItem();
			}
		}

		private TextLogItem DequeueLogItem()
		{
			TextLogItem newEntry = null;
			lock (threadedLogQueue)
				if (threadedLogQueue.Count > 0)
					newEntry = threadedLogQueue.Dequeue();
			return newEntry;
		}

		private void ToggleConsole()
		{
			toggleParent.SetActive(!toggleParent.activeSelf);
			if (toggleParent.activeInHierarchy)
			{
				if (EventSystem.current != null)
				{
					lastSelectedObject = EventSystem.current.currentSelectedGameObject;
					inputField.Select();
					inputField.text = string.Empty;
					inputField.MoveTextEnd(false);
				}
			}
			else
			{
				if (EventSystem.current != null)
					EventSystem.current.SetSelectedGameObject(lastSelectedObject);
				lastSelectedObject = null;
				Console.CurrentUserInput.Text = "";
				Console.CurrentUserInput.CaretIndex = 0;
			}
		}

		private void UpdateConsole()
		{
			if (EventSystem.current != null)
			{
				if (EventSystem.current.currentSelectedGameObject != inputField.gameObject)
				{
					inputField.Select();
					inputField.MoveTextEnd(false);
				}
				if (!inputField.isFocused)
				{
					inputField.ActivateInputField();
					inputField.MoveTextEnd(false);
				}
			}

			ProcessQueuedLogs();

			bool upPressed = Input.GetKeyDown(KeyCode.UpArrow);
			bool downPressed = Input.GetKeyDown(KeyCode.DownArrow);
			bool tabPressed = Input.GetKeyDown(KeyCode.Tab);

			//Prevents the Up and Down arrows from moving the caret position natively.
			if (upPressed || downPressed)
				inputField.caretPosition = Console.CurrentUserInput.CaretIndex;

			//Update any change in actual caret position made by the user
			if (Console.CurrentUserInput.CaretIndex != inputField.caretPosition)
			{
				Console.CurrentUserInput.CaretIndex = inputField.caretPosition;
				RebuildPredictionAndHistory();
			}

			CommandInputContext currentContext = Console.CurrentUserInput.SelectedContext;

			if ((upPressed || tabPressed || downPressed) && currentContext != null)
			{
				if (upPressed)
					--currentContext.SelectionIndex;
				else if (tabPressed || downPressed)
					++currentContext.SelectionIndex;
				ChangeInputFieldText(Console.CurrentUserInput.Text, currentContext.StartIndex + currentContext.CurrentText.Length, true);
			}

			if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.KeypadEnter))
			{
				Console.SubmitCommand(inputField.text);
				ChangeInputFieldText("", 0);
			}

			int selectionIndex = currentContext?.SelectionIndex ?? 0;

			userInputHighlighter.SetActive(selectionIndex == 0);

			//History
			CommandHistoryItemDisplay targetHistoryEntry = null;
			int leafIndex = 0;
			int totalIndex = 0;
			foreach (CommandHistoryItemDisplay historticalCommandCallEntry in commandHistoryObjects)
			{
				bool target = leafIndex++ == (commandHistoryObjects.Count + selectionIndex);
				historticalCommandCallEntry.SetHighlight(target);
				if (target)
					targetHistoryEntry = historticalCommandCallEntry;
				totalIndex++;
			}

			if (targetHistoryEntry != null)
				historyScrollRect.TryFocusOn(targetHistoryEntry);
			else
				historyScrollRect.content.anchoredPosition = new Vector2(0, 0);

			//Prediction
			CommandPredictionItemDisplay targetPredictionEntry = null;
			leafIndex = 1;
			totalIndex = 0;
			foreach (CommandPredictionItemDisplay prediction in commandPredictionObjects)
			{
				if (prediction.EntryType == CommandPredictionItemDisplay.Type.Command)
				{
					bool target = leafIndex++ == selectionIndex;
					prediction.SetHighlight(target);
					if (target)
						targetPredictionEntry = prediction;
				}
				totalIndex++;
			}

			if (targetPredictionEntry != null)
				predictionScrollRect.TryFocusOn(targetPredictionEntry);
			else
				predictionScrollRect.content.anchoredPosition = new Vector2(0, 0);
		}

		private static string NullifyRichText(string originalText)
		{
			if (originalText == string.Empty)
				return originalText;

			StringBuilder stringBuilder = new StringBuilder();

			stringBuilder.Append(originalText);

			int tagOpenIndex = 0;
			do
			{
				if (tagOpenIndex + 1 > originalText.Length)
					break;
				tagOpenIndex = stringBuilder.ToString().IndexOf('<', tagOpenIndex);
				if (tagOpenIndex != -1)
				{
					stringBuilder.Insert(tagOpenIndex + 1, "<b></b>");
					tagOpenIndex += 8;
				}
			} while (tagOpenIndex != -1);

			return stringBuilder.ToString();
		}

		private void ChangeInputFieldText(string text, int caretIndex, bool supressCallback = false)
		{
			if (supressCallback)
				supressChangeCallback = true;
			inputField.text = text;			
			Console.CurrentUserInput.CaretIndex = inputField.caretPosition = caretIndex;
			if (supressCallback)
				supressChangeCallback = false;
		}

		private void RebuildPredictionAndHistory()
		{
			foreach (CommandHistoryItemDisplay obj in commandHistoryObjects)
				ObjectPool.Deposit(obj);
			commandHistoryObjects.Clear();

			foreach (CommandPredictionItemDisplay obj in commandPredictionObjects)
				ObjectPool.Deposit(obj);
			commandPredictionObjects.Clear();

			CommandInputContext selectedContext = Console.CurrentUserInput.SelectedContext;
			if (selectedContext == null || selectedContext.OriginalText.Length == 0)
				return;

			//History
			ReadOnlyCollection<string> historyCalls = selectedContext.FilteredHistoricCommands;

			Transform currentHistoryRoot = commandHistoryTemplate.transform.parent;
			foreach (string historicCommandCall in historyCalls)
			{
				CommandHistoryItemDisplay scriptComponent = ObjectPool.Withdraw(commandHistoryTemplate);
				Transform newRoot = scriptComponent.transform;
				newRoot.gameObject.SetActive(true);
				newRoot.SetParent(currentHistoryRoot);
				scriptComponent.Initialise(historicCommandCall);
				commandHistoryObjects.Add(scriptComponent);
			}

			//Prediction
			ReadOnlyCollection<Command> predictedCommands = selectedContext.PredictedCommands;

			Transform currentPredictionRoot = commandPredictionTemplate.transform.parent;
			List<string> identifierStack = new List<string>();
			foreach (Command predictedCommand in predictedCommands)
			{
				while (identifierStack.Count > 0 && predictedCommand.Identifiers.Count < identifierStack.Count)
				{
					currentPredictionRoot = currentPredictionRoot.parent;
					identifierStack.RemoveAt(identifierStack.Count - 1);
				}

				for (int i = identifierStack.Count - 1; i >= 0; --i)
				{
					if (predictedCommand.Identifiers[i] != identifierStack[i])
					{
						currentPredictionRoot = currentPredictionRoot.parent;
						identifierStack.RemoveAt(identifierStack.Count - 1);
						i = identifierStack.Count;
					}
				}

				while (identifierStack.Count < predictedCommand.Identifiers.Count)
				{
					CommandPredictionItemDisplay scriptComponent = ObjectPool.Withdraw(commandPredictionTemplate);
					scriptComponent.gameObject.SetActive(true);
					Transform newRoot = scriptComponent.transform;
					newRoot.SetParent(currentPredictionRoot);
					currentPredictionRoot = newRoot;
					string displayString = predictedCommand.Identifiers[identifierStack.Count];
					if (identifierStack.Count == predictedCommand.Identifiers.Count - 1)
						scriptComponent.Initialise(CommandPredictionItemDisplay.Type.Command, displayString);
					else
						scriptComponent.Initialise(CommandPredictionItemDisplay.Type.Namespace, $"<color=grey>{displayString}</color>");
					scriptComponent.SetHighlight(false);
					commandPredictionObjects.Add(scriptComponent);
					identifierStack.Add(predictedCommand.Identifiers[identifierStack.Count]);
				}
			}
		}

		private void OnInputChanged(string input)
		{
			if (!supressChangeCallback)
			{
				Console.CurrentUserInput.Text = input;
				Console.CurrentUserInput.CaretIndex = inputField.caretPosition;

				if (Console.CurrentUserInput.SelectedContext == null)
					userInputDisplay.text = string.Empty;
				else
					userInputDisplay.text = Console.CurrentUserInput.SelectedContext.OriginalText;

				RebuildPredictionAndHistory();
			}
		}
	}
}