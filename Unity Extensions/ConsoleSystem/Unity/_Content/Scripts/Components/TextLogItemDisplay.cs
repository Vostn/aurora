﻿using UnityEngine;
using UnityEngine.UI;

namespace Aurora.ConsoleSystem.Unity
{
	[AddComponentMenu("")]
	public class TextLogItemDisplay : MonoBehaviour
	{
		[SerializeField]
		private Text textComponent;

		public Text TextComponent
		{			
			get { return textComponent; }
		}
	}
}