﻿using UnityEngine;
using UnityEngine.UI;

namespace Aurora.ConsoleSystem.Unity
{
	[AddComponentMenu("")]
	public class CommandPredictionItemDisplay : MonoBehaviour
	{
		[SerializeField]
		private GameObject highlightBacking;

		[SerializeField]
		private Text text;

		public Type EntryType { get; private set; }

		public enum Type
		{
			Command,
			Namespace,
		}

		public void SetHighlight(bool highlight)
		{
			highlightBacking.SetActive(highlight);
		}

		public void Initialise(Type entryType, string text)
		{
			EntryType = entryType;
			this.text.text = text;
		}
	}
}