﻿#if !DISABLESTEAMWORKS && AURORA_STEAMWORKS_NET_UNITY && UNITY_2017_1_OR_NEWER

using Steamworks;
using System.Collections.Generic;
using System.IO;
using Aurora.Diagnostics;
using Aurora.TimeSystem;

namespace Aurora.PersistenceSystem.Unity
{
	public sealed class SteamworksNETPersistence : IPersistence
	{
		private Dictionary<string, PersistenceData> dataDictionary = new Dictionary<string, PersistenceData>();
		private Queue<KeyValuePair<string, PersistenceData>> dataWaitingStats = new Queue<KeyValuePair<string, PersistenceData>>();

		private const string filePath = "AuroraPersistance.sav";

		private StatPullStatus statPullStatus = StatPullStatus.NotCompleted;

		bool IPersistence.Online => true;

		private enum StatPullStatus
		{
			NotCompleted,
			Completed,
		}

		public SteamworksNETPersistence()
		{
			RealTimeTracker.RealTimeTransform.OnIncrementStep += Update;
		}

		private void Update()
		{
			if (statPullStatus == StatPullStatus.NotCompleted)
			{
				if (SteamUser.BLoggedOn())
					if (SteamUserStats.RequestCurrentStats())
						statPullStatus = StatPullStatus.Completed;
			}
			if (statPullStatus == StatPullStatus.Completed)
			{
				while (dataWaitingStats.Count > 0)
				{
					KeyValuePair<string, PersistenceData> data = dataWaitingStats.Dequeue();
					switch (data.Value.DataType)
					{
						case DataType.AchievementCompletion:
							StoreAchievementCompletion(data.Key, data.Value);
							break;

						case DataType.AchievementProgress:
							StoreAchievementProgress(data.Key, data.Value);
							break;
					}
				}
			}
		}

		private void ReadFromFile(System.Action<string, PersistenceData> dataLoadedCallback)
		{
			if (!SteamRemoteStorage.FileExists(filePath))
				WriteToFile();
			int byteArraySize = SteamRemoteStorage.GetFileSize(filePath);
			byte[] fileData = new byte[byteArraySize];
			int bytesRead = SteamRemoteStorage.FileRead(filePath, fileData, byteArraySize);
			if (bytesRead <= 0)
			{
				Debug.LogError("SteamworksNETPersistence failed to read Steam cloud file data!");
				return;
			}
			using (BinaryReader reader = new BinaryReader(new MemoryStream(fileData)))
			{
				if (reader.BaseStream.Position < reader.BaseStream.Length)
				{
					string key = reader.ReadString();
					PersistenceData parsedData = default(PersistenceData);
					if (!PersistenceData.TryParse(reader.ReadString(), out parsedData))
						Debug.LogError("The string is not a valid PersistenceData representation!");
					else
						dataLoadedCallback.Invoke(key, parsedData);
				}
			}
		}

		private void WriteToFile()
		{
			using (BinaryWriter writer = new BinaryWriter(new MemoryStream()))
			{
				foreach (var kvp in dataDictionary)
				{
					writer.Write(kvp.Key);
					writer.Write(kvp.Value.ToString());
				}
				byte[] byteArray = ((MemoryStream)writer.BaseStream).ToArray();
				if (!SteamRemoteStorage.FileWrite(filePath, byteArray, byteArray.Length))
					Debug.LogError("Steamworks.NET Failed to write the file!");
			}
		}

		private void StoreAchievementCompletion(string key, PersistenceData data)
		{
			bool parsedData = false;
			bool setResult = false;
			if (bool.TryParse(data.Data, out parsedData))
			{
				if (parsedData)
					setResult = SteamUserStats.SetAchievement(key);
				else
					setResult = SteamUserStats.ClearAchievement(key);
				SteamUserStats.StoreStats();
			}
			else
				Debug.LogError("Data must be in boolen format!");
			if (!setResult)
				Debug.LogError($"Achievement State Change call failed! Perhaps achievement \"{key}\" doesn't exist in Steamworks?");
		}

		private void StoreAchievementProgress(string key, PersistenceData data)
		{
			float floatDataValue;
			int intDataValue;
			if (int.TryParse(data.Data.ToString(), out intDataValue))
			{
				if (!SteamUserStats.SetStat(key, intDataValue))
					Debug.LogError($"SetStat call failed! Perhaps the statistic \"{key}\" doesn't exists in Steamworks, or the numeric type doens't match?");
				else
					SteamUserStats.StoreStats();
			}
			else if (float.TryParse(data.Data.ToString(), out floatDataValue))
			{
				if (!SteamUserStats.SetStat(key, floatDataValue))
					Debug.LogError($"SetStat call failed! Perhaps the statistic \"{key}\" doesn't exists in Steamworks, or the numeric type doens't match?");
				else
					SteamUserStats.StoreStats();
			}
			else
			{
				Debug.LogError($"\"{nameof(data)}\" parameter must be in numeric format!");
				return;
			}
		}

		void IPersistence.ChangeData(string key, PersistenceData data)
		{
			if (dataDictionary.ContainsKey(key))
				dataDictionary[key] = data;
			else
				dataDictionary.Add(key, data);

			dataWaitingStats.Enqueue(new KeyValuePair<string, PersistenceData>(key, data));
		}

		void IPersistence.RemoveData(string key) => dataDictionary.Remove(key);

		void IPersistence.Finalise() => WriteToFile();

		void IPersistence.LoadData(System.Action<string, PersistenceData> dataLoadedCallback) => ReadFromFile(dataLoadedCallback);
	}
}

#endif