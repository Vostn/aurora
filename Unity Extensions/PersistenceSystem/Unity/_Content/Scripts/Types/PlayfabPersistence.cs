﻿#if !DISABLE_PLAYFABCLIENT_API && AURORA_PLAYFAB_UNITY && UNITY_2017_1_OR_NEWER

using System.Collections.Generic;
using Aurora.TimeSystem;
using PlayFab;
using PlayFab.ClientModels;
using Aurora.Diagnostics;

namespace Aurora.PersistenceSystem.Unity
{
	/// <summary>
	/// Implementation of IBackendPersistence for PlayFab.
	/// </summary>
	public sealed class PlayFabPersistence : IPersistence
	{
		private Queue<KeyValuePair<string, string>> dataToBeSynced = new Queue<KeyValuePair<string, string>>();
		private Queue<KeyValuePair<string, PersistenceData>> dataToBeLoaded = new Queue<KeyValuePair<string, PersistenceData>>();

		private const double syncPeriod = 5;

		private InitialSyncFromServerStatus initialSyncFromServerStatus = InitialSyncFromServerStatus.NotCompleted;
		private double lastSyncToServerTime = 0;

		private System.Action<string, PersistenceData> dataLoadedCallback;

		bool IPersistence.Online => true;

		private enum InitialSyncFromServerStatus
		{
			NotCompleted,
			Waiting,
			Completed,
		}

		public PlayFabPersistence()
		{
			RealTimeTracker.RealTimeTransform.OnIncrementStep += Time_OnIncrementStep;
			SyncFromServer();
		}

		public static void UnhandledPlayFabError(PlayFabError error)
		{
			if (error.Error == PlayFabErrorCode.ServiceUnavailable)
			{
				Debug.LogWarning("Connection to PlayFab server could not be made!");
				LogPlayfabError(Debug.LogWarning, error);
			}
			else
			{
				Debug.LogError("Unhandled PlayFab error!");
				LogPlayfabError(Debug.LogError, error);
			}
		}

		private static void LogPlayfabError(System.Action<object> LogFunction, PlayFabError error)
		{
			LogFunction(error.Error);
			LogFunction(error.ErrorMessage);
			if (error.ErrorDetails != null)
			{
				foreach (KeyValuePair<string, List<string>> pair in error.ErrorDetails)
				{
					LogFunction("Key: " + pair.Key);
					if (pair.Value.Count > 0)
						LogFunction("Values: ");
					foreach (string content in pair.Value)
						LogFunction(content);
				}
			}
		}

		private void Time_OnIncrementStep()
		{
			if (initialSyncFromServerStatus == InitialSyncFromServerStatus.NotCompleted)
			{
				SyncFromServer();
			}
			if (RealTimeTracker.RealTimeTransform.Time - lastSyncToServerTime > syncPeriod)
			{
				lastSyncToServerTime = RealTimeTracker.RealTimeTransform.Time;
				SyncToServer();
			}
		}

		private void SyncToServer()
		{
			int index = 0;
			int totalCount = 0;
			UpdateUserDataRequest request = null;
			foreach (var kvp in dataToBeSynced)
			{
				if (index == 0)
				{
					request = new UpdateUserDataRequest();
					request.Data = new Dictionary<string, string>();
				}
				request.Data.Add(kvp.Key, kvp.Value);
				++index;
				++totalCount;
				if (index == 10 || totalCount == dataToBeSynced.Count)
				{
					PlayFabClientAPI.UpdateUserData(request, null, UnhandledPlayFabError);
					index = 0;
				}
			}
			dataToBeSynced.Clear();
		}

		private void SyncFromServer()
		{
			initialSyncFromServerStatus = InitialSyncFromServerStatus.Waiting;
			PlayFabClientAPI.GetUserData(new GetUserDataRequest(),
			(result) =>
			{
				foreach (var kvp in result.Data)
				{
					PersistenceData parsedData = default(PersistenceData);
					if (!PersistenceData.TryParse(kvp.Value.Value, out parsedData))
					{
						Debug.LogError("The string is not a valid PersistenceData representation!");
						continue;
					}
					if (dataLoadedCallback != null)
						dataLoadedCallback.Invoke(kvp.Key, parsedData);
					else
						dataToBeLoaded.Enqueue(new KeyValuePair<string, PersistenceData>(kvp.Key, parsedData));
				}
				initialSyncFromServerStatus = InitialSyncFromServerStatus.Completed;
			},
			(error) =>
			{
				initialSyncFromServerStatus = InitialSyncFromServerStatus.NotCompleted;
				UnhandledPlayFabError(error);
			});
		}

		void IPersistence.ChangeData(string key, PersistenceData data)
		{
			dataToBeSynced.Enqueue(new KeyValuePair<string, string>(key, data.ToString()));
		}

		void IPersistence.RemoveData(string key)
		{
			dataToBeSynced.Enqueue(new KeyValuePair<string, string>(key, null));
		}

		void IPersistence.Finalise()
		{
			//Intentionally empty.
		}

		void IPersistence.LoadData(System.Action<string, PersistenceData> dataLoadedCallback)
		{
			this.dataLoadedCallback = dataLoadedCallback;
			foreach (var kvp in dataToBeLoaded)
				dataLoadedCallback.Invoke(kvp.Key, kvp.Value);
			dataToBeLoaded.Clear();
		}
	}
}

#endif