﻿using Aurora.Unity;
using UnityEngine;

namespace Aurora.PersistenceSystem.Unity
{
	public static class PersistenceManagerLifetimeManager
	{
		[RuntimeInitializeOnLoadMethod]
		private static void Initialise()
		{
			UnityEvents.On_ApplicationQuit += PersistenceManager.Finalise;
		}
	}
}
