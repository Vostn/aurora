﻿using UnityEngine;
using JetBrains.Annotations;
using System.Globalization;

#if UNITY_EDITOR

using UnityEditor;
using UnityEditor.Callbacks;

#endif

namespace Aurora.LocalisationSystem.Unity
{
	public static class LanguageLoader
	{
		private static bool languagesLoaded = false;

#if UNITY_EDITOR

		[InitializeOnLoadMethod]
		[DidReloadScripts]
#endif
		[RuntimeInitializeOnLoadMethod]
		private static void LoadLanguages()
		{
			if (languagesLoaded)
				return;

			Localisation.UnloadAllLangauges();
			TextAsset[] languageFiles = Resources.LoadAll<TextAsset>("Language/");
			for (int i = 0; i < languageFiles.Length; ++i)
				Localisation.RegisterLanguage(Language.CreateFromFile(languageFiles[i].text));
			Localisation.SetActiveCulture(CultureInfo.CurrentCulture);

			languagesLoaded = true;
		}

#if UNITY_EDITOR

		[UsedImplicitly]
		private class LanguageUpdater : AssetPostprocessor
		{
			[UsedImplicitly]
			private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
			{
				if (CheckAssets(importedAssets))
					return;
				if (CheckAssets(deletedAssets))
					return;
				if (CheckAssets(movedFromAssetPaths))
					return;
				CheckAssets(movedAssets);
			}

			private static bool CheckAssets(string[] assets)
			{
				foreach (string asset in assets)
				{
					if (AssetDatabase.GetMainAssetTypeAtPath(asset) == typeof(TextAsset) && asset.Contains("Resources/Language/"))
					{
						languagesLoaded = false;
						LoadLanguages();
						return true;
					}
				}
				return false;
			}
		}

#endif
	}
}