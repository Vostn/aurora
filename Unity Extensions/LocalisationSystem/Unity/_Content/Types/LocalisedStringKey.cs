﻿using UnityEngine;

namespace Aurora.LocalisationSystem.Unity
{
	[System.Serializable]
	public struct LocalisedStringKey
	{
		[SerializeField]
		private string stringKey;

		public static implicit operator string(LocalisedStringKey key)
		{
			return key.stringKey;
		}

		public override string ToString()
		{
			return stringKey;
		}
	}
}