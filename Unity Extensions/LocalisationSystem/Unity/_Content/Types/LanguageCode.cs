﻿using System.Globalization;
using UnityEngine;

namespace Aurora.LocalisationSystem.Unity
{
	[System.Serializable]
	public struct LanguageCode
	{
		[SerializeField]
		private string languageCode;

		public static implicit operator string(LanguageCode key)
		{
			return key.languageCode;
		}

		public static implicit operator CultureInfo(LanguageCode key)
		{
			return new CultureInfo(key);
		}
	}
}