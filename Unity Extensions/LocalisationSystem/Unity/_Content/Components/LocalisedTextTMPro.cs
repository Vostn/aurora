﻿#if AURORA_TEXTMESHPRO

using UnityEngine;
using System.Globalization;
using Aurora.Unity;
using TMPro;

namespace Aurora.LocalisationSystem.Unity
{
	public class LocalisedTextTMPro : MonoBehaviour
	{
		[SerializeField]
		private TextMeshProUGUI textField;

		[SerializeField]
		private LocalisedStringKey stringKey;

		[SerializeField]
		private bool useActiveLanguage = true;

		[SerializeField]
		[Visibility(nameof(useActiveLanguage), MatchType.Equal, 0)]
		private LanguageCode languageOverride;

		private void Awake()
		{
			Localisation.OnActiveLanguageChanged += LanguageChanged;
			UpdateText();
		}

		private void LanguageChanged(Language language)
		{
			UpdateText();
		}

		private void OnDestroy()
		{
			Localisation.OnActiveLanguageChanged -= LanguageChanged;
		}

		private void UpdateText()
		{
			if (textField == null)
			{
				Debug.LogWarning($"{nameof(textField)} reference is null!");
				return;
			}

			CultureInfo activeCulture = Localisation.ActiveCulture;
			if (!useActiveLanguage)
				activeCulture = languageOverride;

			textField.text = Localisation.GetString(stringKey, activeCulture, true).ConvertToShaped();
			textField.isRightToLeftText = activeCulture.TextInfo.IsRightToLeft;
		}

		private void OnValidate()
		{
			UpdateText();
		}

		private void Reset()
		{
			textField = GetComponent<TextMeshProUGUI>();
		}
	}
}

#endif