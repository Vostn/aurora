﻿using UnityEngine;
using UnityEngine.UI;
using System.Globalization;
using Aurora.Unity;

namespace Aurora.LocalisationSystem.Unity
{
	public class LocalisedText : MonoBehaviour
	{
		[SerializeField]
		private Text textField;

		[SerializeField]
		private LocalisedStringKey stringKey;

		[SerializeField]
		private bool useActiveLanguage = true;

		[SerializeField]
		[Visibility(nameof(useActiveLanguage), MatchType.Equal, 0)]
		private LanguageCode languageOverride;

		private void Awake()
		{
			Localisation.OnActiveLanguageChanged += LanguageChanged;
			UpdateText();
		}

		private void LanguageChanged(Language language)
		{
			UpdateText();
		}

		private void OnDestroy()
		{
			Localisation.OnActiveLanguageChanged -= LanguageChanged;
		}

		private void UpdateText()
		{
			if (textField == null)
			{
				Debug.LogWarning($"{nameof(textField)} reference is null!");
				return;
			}

			CultureInfo activeCulture = Localisation.ActiveCulture;
			if (!useActiveLanguage)
				activeCulture = languageOverride;

			string text = Localisation.GetString(stringKey, activeCulture, true).ConvertToShaped();
			if (activeCulture.TextInfo.IsRightToLeft)
				text = text.ReverseDirection();

			textField.text = text;
		}

		private void OnValidate()
		{
			UpdateText();
		}

		private void Reset()
		{
			textField = GetComponent<Text>();
		}
	}
}