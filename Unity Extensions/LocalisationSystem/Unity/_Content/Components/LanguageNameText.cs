﻿using UnityEngine;
using System.Globalization;
using Aurora.Unity;
using UnityEngine.UI;

namespace Aurora.LocalisationSystem.Unity
{
	public class LanguageNameText : MonoBehaviour
	{
		[SerializeField]
		private Text textField;

		[SerializeField]
		private bool useActiveLanguage = true;

		[SerializeField]
		[Visibility(nameof(useActiveLanguage), MatchType.Equal, 0)]
		private LanguageCode languageOverride;

		private void Awake()
		{
			Localisation.OnActiveLanguageChanged += LanguageChanged;
			UpdateText();
		}

		private void LanguageChanged(Language newLanguage)
		{
			UpdateText();
		}

		private void OnDestroy()
		{
			Localisation.OnActiveLanguageChanged -= LanguageChanged;
		}

		private void UpdateText()
		{
			if (textField == null)
			{
				Debug.LogWarning($"{nameof(textField)} reference is null!");
				return;
			}

			CultureInfo activeCulture = Localisation.ActiveCulture;
			if (!useActiveLanguage)
				activeCulture = languageOverride;

			string text = activeCulture.NativeName.ConvertToShaped();
			if (activeCulture.TextInfo.IsRightToLeft)
				text = text.ReverseDirection();

			textField.text = text;
		}

		private void OnValidate()
		{
			UpdateText();
		}

		private void Reset()
		{
			textField = GetComponent<Text>();
		}
	}
}