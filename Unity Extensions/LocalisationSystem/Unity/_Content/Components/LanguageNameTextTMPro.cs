﻿#if AURORA_TEXTMESHPRO

using UnityEngine;
using System.Globalization;
using Aurora.Unity;
using TMPro;

namespace Aurora.LocalisationSystem.Unity
{
	public class LanguageNameTextTMPro : MonoBehaviour
	{
		[SerializeField]
		private TextMeshProUGUI textField;

		[SerializeField]
		private bool useActiveLanguage = true;

		[SerializeField]
		[Visibility(nameof(useActiveLanguage), MatchType.Equal, 0)]
		private LanguageCode languageOverride;

		private void Awake()
		{
			Localisation.OnActiveLanguageChanged += LanguageChanged;
			UpdateText();
		}

		private void LanguageChanged(Language newLanguage)
		{
			UpdateText();
		}

		private void OnDestroy()
		{
			Localisation.OnActiveLanguageChanged -= LanguageChanged;
		}

		private void UpdateText()
		{
			if (textField == null)
			{
				Debug.LogWarning($"{nameof(textField)} reference is null!");
				return;
			}

			CultureInfo activeCulture = Localisation.ActiveCulture;
			if (!useActiveLanguage)
				activeCulture = languageOverride;

			textField.text = activeCulture.NativeName.ConvertToShaped();
			textField.isRightToLeftText = activeCulture.TextInfo.IsRightToLeft;
		}

		private void OnValidate()
		{
			UpdateText();
		}

		private void Reset()
		{
			textField = GetComponent<TextMeshProUGUI>();
		}
	}
}

#endif