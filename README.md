# Aurora

Aurora is a library of tools, designed primarily for game development in the context of Unity.  
It contains many tools, some of which are usable in a pure C# context.

## Features

This is a non exhaustive list of some of the major features of Aurora:

### [Console](./README_CONSOLE.md) (C#, with Unity extensions)

- Scans code base for methods and properties from which to create commands
- Ability to filter commands by safety level, defined with an optional attribute (developer only, user exposed, etc) 
- Optional attribute to define descriptions of commands
- Predictive fuzzy search input
- Command recall
- Alias support
- Optional Unity UGUI interface
 
### [Deferred Decals](./README_DECALS.md) (Unity Only)

- Depth projected cube decals
- Optional directional projection
- Optional alpha blending or alpha testing
- Ability to modify almost any buffer, and in any combination (Albedo/Spec, Albedo/Metal, Smoothness, Emission, Normal)
- Detailed and informative material editor
- Supports Instancing
- Supports ambient light and reflection probes

### Data Persistence (C#, with Unity extensions)

- Can store and merge data from many sources
- Supports storage of data on Playfab and Steam via their Unity APIs
- Supports local device identification, discrimination, and data storage
- Supports value merging of summed data across any number of devices, regardless of time or location of creation or merging
- Replicates to all connected services and storage options

### [Colour](./README_COLOUR.md) (C#, with Unity extensions)

- Robust colour definition and conversion system supporting all modern colour gamuts and colour models
- Supported Gamuts: sRGB, AdobeRGB, P3DCI, P3D50, P3D65, DisplayP3, Rec2020, AdobeWideGamut
- Supported Models: RGB, HSL, HSV, XYZ, xyY, CIE LAB, CIE LCH
- Unity Editor based dynamic-conversion picker, with options for all supported models and gamuts

### Localisation (C#, with Unity Extensions)

- Loads language and localisation definitions from a simple file format
- Easily and dynamically change active langauge
- Simple and easy querying of localisation strings
- Optional Unity component for filtering and selecting localised strings, and dynamically changing text components based on active language

### Time Management (C#, with Unity Extensions)

- Detailed timing information, at several levels (real-time, application time, game time)
- Time Hierarchy system, for inheriting time scales and time "locations"
- Integrates with Unity time system
- Integrates with Unity components such as Rigidbody and Animator
- TimeBehaviour super-component for Unity scripts to get detailed time information about the local game object

### Custom Collections (C#)

- Heap (efficient tree-based priority queue)
- ListPath and QueuePath (lazy cached evaluators of the physical length of vector collections)

### Numerous Miscellaneous Features

- Generic Vector and mathematics interfaces, with vector definitions for most standard value types
- Math static helper class to provide options that System.Math and Unity.Mathf do not provide
- Fixed point value type, with most common operators, Trig Math extensions, and noise value generator
- Object Pooling helper classes (Both for Unity, and C# objects)
- Random value generation class, with much more options for generating values, and returning better random results than Unity and C# Random classes
- Scheduler for deferred execution, with lazy cached timers
- Event-Action Unity components for designer-accessible fast prototyping of game behaviours
- Wide array of stackable property attributes, for making complex inspector behaviours and designs, without writing editors
- Numerous helper components for simple behaviours, common game patterns, and improved prototyping
- Numerous extensions to Unity classes, from useful shortcuts in scripts, to extended component behaviours
- And much, *much* more
