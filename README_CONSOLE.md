# Console

The Console System consists of two parts: The **[Core](./Assets/Aurora/Core/ConsoleSystem/_Content/Scripts)** and the **[Unity Extension](./Assets/Aurora/Unity Extensions/ConsoleSystem/Unity/_Content)**.

The main part of the Core is the **[Console.cs](./Assets/Aurora/Core/ConsoleSystem/_Content/Scripts/Statics/Console.cs)** script. It manages the overarching collection and storage of commands (with the support and direction of a number of other types and attributes in the Core), as well as the submission and prediction of commands. Commands are collected via reflection, and attributes can provide commands with additional metadata for the console to utilise.

The Unity Extension portion provides **[ConsoleInterface.cs](./Assets/Aurora/Unity Extensions/ConsoleSystem/Unity/_Content/Scripts/Components/ConsoleInterface.cs)**, which is a GUI interface that allows a user to interact with the core console; input commands, select command predictions, review command history, and review information in the console.

Both scripts are of course supported by a number of other data types and attributes, to encapsulate the more complex backends required for accessing and storing command information and running prediction logic.