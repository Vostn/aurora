﻿namespace Aurora.TimeSystem
{
	public static class RealTimeTracker
	{
		private static readonly TimeStepper timeStepper = new TimeStepper();
		public static TimeTransform RealTimeTransform { get; private set; } = new TimeTransform();

		static RealTimeTracker()
		{
			timeStepper.Unpause();
		}

		public static void IncrementStep()
		{
			timeStepper.IncrementStep();
			RealTimeTransform.IncrementStep(timeStepper.DeltaTime);
		}
	}
}