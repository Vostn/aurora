﻿using System.Diagnostics;

namespace Aurora.TimeSystem
{
	public class TimeStepper
	{
		/// <summary>
		/// Keeps track of the real time that has elapsed since this instance was created.
		/// </summary>
		protected readonly Stopwatch stopwatch = new Stopwatch();

		/// <summary>
		/// Total elapsed time. Independent of steps, so the value can change from call to call.
		/// </summary>
		public double Time { get { return stopwatch.Elapsed.TotalSeconds; } }

		/// <summary>
		/// The value of Time when the last step occurred.
		/// </summary>
		public double StepTime { get; private set; }

		/// <summary>
		/// Duration of the previous step.
		/// </summary>
		public double DeltaTime { get; private set; }

		/// <summary>
		/// Total number of full steps that have occurred.
		/// </summary>
		public long TotalSteps { get; private set; }

		public event System.Action OnIncrementStep;

		/// <summary>
		/// Creates a new instance of TimeStepper. Internal timer starts in the paused state, call Unpause to begin timing.
		/// </summary>
		public TimeStepper()
		{
		}

		/// <summary>
		/// Increment to the next step, using the internal timer to automatically determine the step duration.
		/// </summary>
		public virtual void IncrementStep()
		{
			IncrementStep(stopwatch.Elapsed.TotalSeconds - StepTime);
		}

		/// <summary>
		/// Increment to the next step, with a custom step duration.
		/// </summary>
		/// <param name="deltaTime">The custom step duration to use.</param>
		public virtual void IncrementStep(double deltaTime)
		{
			StepTime += deltaTime;
			DeltaTime = deltaTime;
			++TotalSteps;
			OnIncrementStep?.Invoke();
		}

		/// <summary>
		/// Pause the internal timer.
		/// </summary>
		public void Pause()
		{
			stopwatch.Stop();
		}

		/// <summary>
		/// Unpause the internal timer.
		/// </summary>
		public void Unpause()
		{
			stopwatch.Start();
		}
	}	
}