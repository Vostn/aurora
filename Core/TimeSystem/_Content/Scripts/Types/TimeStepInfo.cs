﻿namespace Aurora.TimeSystem
{
	public class TimeStepInfo
	{
		/// <summary>
		/// Instance of TimeStepper to access with this instance of TimeStepInfo.
		/// </summary>
		private readonly TimeStepper timeStepper;

		/// <summary>
		/// Total elapsed time. Independent of steps, so the value can change from call to call.
		/// </summary>
		public double Time { get { return timeStepper.Time; } }

		/// <summary>
		/// The value of Time when the last step occurred.
		/// </summary>
		public double StepTime { get { return timeStepper.StepTime; } }

		/// <summary>
		/// Duration of the previous step.
		/// </summary>
		public double DeltaTime { get { return timeStepper.DeltaTime; } }

		/// <summary>
		/// Total number of full steps that have occurred.
		/// </summary>
		public double TotalSteps { get { return timeStepper.TotalSteps; } }

		/// <summary>
		/// Event called when the TimeStepper steps.
		/// </summary>
		public event System.Action OnIncrementStep;

		/// <summary>
		/// Private parameterless constructor for TimeStepInfo. TimeStepInfo requires an instance of a TimeStepper.
		/// </summary>
		private TimeStepInfo()
		{
		}

		/// <summary>
		/// Basic constructor for TImeStepInfo.
		/// </summary>
		/// <param name="timeStepper">The TimeStepper instance to base this TimeStepInfo instance on.</param>
		public TimeStepInfo(TimeStepper timeStepper)
		{
			this.timeStepper = timeStepper;
			timeStepper.OnIncrementStep += () => { OnIncrementStep?.Invoke(); } ;
		}
	}	
}