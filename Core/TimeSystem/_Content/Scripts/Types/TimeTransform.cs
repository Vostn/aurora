﻿using System.Collections.Generic;

namespace Aurora.TimeSystem
{
	public class TimeTransform
	{
		public TimeTransform(double initialDeltaTime = 0, double initialLocalTime = 0, double initialLocalTimeScale = 1)
		{
			AssertPositive(initialDeltaTime, nameof(initialDeltaTime));
			AssertPositive(initialLocalTime, nameof(initialLocalTime));
			AssertPositive(initialLocalTimeScale, nameof(initialLocalTimeScale));

			LocalTimeScale = initialLocalTimeScale;
			LocalTime = initialLocalTime;
			DeltaTime = initialDeltaTime;
		}

		private void AssertPositive(double value, string variableName)
		{
			const string positiveWarning = "must be positive!";
			if (value < 0)
				throw new System.ArgumentOutOfRangeException($"{variableName} {positiveWarning}");
		}

		public event System.Action OnChanged;

		public event System.Action OnIncrementStep;

		private TimeTransform parent;
		public TimeTransform Parent { get { return parent; } set { if (value != parent) { parent = value; TriggerChangedEvent(); } } }

		private double localTimeScale;
		public double LocalTimeScale { get { return localTimeScale; } set { if (localTimeScale != value) { localTimeScale = value; TriggerChangedEvent(); } } }
		public double TimeScale { get { return (Parent != null) ? (Parent.TimeScale * LocalTimeScale) : LocalTimeScale; } set { LocalTimeScale = (Parent != null) ? (value / Parent.TimeScale) : value; } }

		private double localTime;
		public double LocalTime { get { return localTime; } private set { if (localTime != value) { localTime = value; } } }
		public double Time { get { return (Parent != null) ? (Parent.Time + LocalTime) : LocalTime; } set { LocalTime = (Parent != null) ? (value - Parent.Time) : value; } }

		/// <summary>
		/// The duration of time covered in the current step.
		/// </summary>
		public double DeltaTime { get; private set; }

		public int StepCount { get; private set; }

		private readonly List<TimeTransform> children = new List<TimeTransform>();

		private void TriggerChangedEvent()
		{
			OnChanged?.Invoke();
			foreach (TimeTransform child in children)
				child.OnChanged?.Invoke();
		}

		public bool SetParent(TimeTransform newParent)
		{
			if (Parent == newParent)
				return true;
			if (newParent == this)
				return false;
			TimeTransform parentSample = newParent;
			while (parentSample != null)
			{
				if (parentSample == this)
					return false;
				parentSample = parentSample.Parent;
			}
			if (Parent != null)
				Parent.RemoveChild(this);
			Parent = newParent;
			if (Parent != null)
				Parent.AddChild(this);
			return true;
		}

		public void RemoveChild(TimeTransform child)
		{
			children.Remove(child);
		}

		public bool AddChild(TimeTransform child)
		{
			if (child == this)
				return false;
			if (child == null)
				return false;
			if (children.Contains(child))
				return true;
			if (child.SetParent(this))
				children.Add(child);
			return true;
		}

		public void IncrementStep(double realDeltaTime)
		{
			++StepCount;
			DeltaTime = realDeltaTime * TimeScale;
			Time += DeltaTime;
			for (int i = 0; i < children.Count; ++i)
				children[i].IncrementStep(realDeltaTime);
			OnIncrementStep?.Invoke();
		}
	}
}