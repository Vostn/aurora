﻿using System.Collections.Generic;
using Aurora.Diagnostics;

namespace Aurora.PersistenceSystem
{
	public static class PersistenceManager
	{
		/// <summary>
		/// Collection of all data to be persisted.
		/// </summary>
		private static Dictionary<string, PersistenceData> dataDictionary = new Dictionary<string, PersistenceData>();

		/// <summary>
		/// List of subscribers to modifications of particular data values.
		/// </summary>
		private static Dictionary<string, System.Action<PersistenceData.ChangeInfo>> changeCallbackDictionary = new Dictionary<string, System.Action<PersistenceData.ChangeInfo>>();

		/// <summary>
		/// Set of all known Device GUIDs.
		/// </summary>
		private static HashSet<string> allDeviceGUIDs = new HashSet<string>();

		/// <summary>
		/// List of all persistences that have been added to the PersistenceManager.
		/// </summary>
		private static List<IPersistence> allPersistences = new List<IPersistence>();

		/// <summary>
		/// Key used to retrieve this device's Device GUID.
		/// </summary>
		private const string deviceGUIDKey = "DeviceGUID";

		/// <summary>
		/// The known constant length of a GUID string.
		/// </summary>
		private const int guidLength = 36;

		static PersistenceManager()
		{
			string deviceGUID = System.Guid.NewGuid().ToString();
			SetData(deviceGUIDKey, new PersistenceData(System.DateTime.UtcNow.Ticks, deviceGUID, DataType.None, MergeType.Oldest, DataFlags.OfflineOnly));
		}

		public static void Finalise()
		{
			foreach (IPersistence persistence in allPersistences)
				persistence.Finalise();
		}

		public static void AddChangeCallback(string key, System.Action<PersistenceData.ChangeInfo> callback)
		{
			System.Action<PersistenceData.ChangeInfo> targetDelegate;
			if (changeCallbackDictionary.TryGetValue(key, out targetDelegate))
			{
				targetDelegate += callback;
				changeCallbackDictionary[key] = targetDelegate;
			}
			else
				changeCallbackDictionary.Add(key, callback);
		}

		public static void RemoveChangeCallback(string key, System.Action<PersistenceData.ChangeInfo> callback)
		{
			System.Action<PersistenceData.ChangeInfo> targetDelegate;
			if (changeCallbackDictionary.TryGetValue(key, out targetDelegate))
			{
				targetDelegate -= callback;
				changeCallbackDictionary[key] = targetDelegate;
			}
		}

		public static void AddPersistence(IPersistence persistence)
		{
			if (!allPersistences.Contains(persistence))
			{
				allPersistences.Add(persistence);
				persistence.LoadData(SetData);
				foreach (KeyValuePair<string, PersistenceData> keyValuePair in dataDictionary)
				{
					PersistenceData data = keyValuePair.Value;
					if (data.DataFlags.HasFlag(DataFlags.DontPersist))
						continue;
					if (data.DataFlags.HasFlag(DataFlags.OfflineOnly) && persistence.Online)
						continue;
					persistence.ChangeData(keyValuePair.Key, keyValuePair.Value);
				}
			}
		}

		private static void ChangeData(string key, PersistenceData newData, PersistenceData oldData)
		{
			if (dataDictionary.ContainsKey(key))
				dataDictionary[key] = newData;
			else
				dataDictionary.Add(key, newData);

			foreach (IPersistence persistence in allPersistences)
			{
				PersistenceData data = dataDictionary[key];
				if (data.DataFlags.HasFlag(DataFlags.DontPersist))
					continue;
				if (data.DataFlags.HasFlag(DataFlags.OfflineOnly) && persistence.Online)
					continue;
				persistence.ChangeData(key, newData);
			}

			System.Action<PersistenceData.ChangeInfo> targetDelegate;
			if (changeCallbackDictionary.TryGetValue(key, out targetDelegate))
			{
				PersistenceData.ChangeInfo changeInfo = new PersistenceData.ChangeInfo(oldData, newData);
				targetDelegate.Invoke(changeInfo);
			}
		}

		public static void SetData(string key, PersistenceData incomingData)
		{
			PersistenceData oldGetValue = default(PersistenceData);
			TryGetData(key, out oldGetValue);
			PersistenceData existingData = default(PersistenceData);

			if (incomingData.DataFlags.HasFlag(DataFlags.DeviceSpecific))
			{
				string sourceGUID = key.Substring(key.Length - guidLength, guidLength);
				allDeviceGUIDs.Add(sourceGUID);
			}

			if (dataDictionary.TryGetValue(key, out existingData))
			{
				PersistenceData newestData = PersistenceData.PickNewest(incomingData, existingData);
				PersistenceData oldestData = (incomingData as System.IEquatable<PersistenceData>).Equals(newestData) ? existingData : incomingData;

				if (existingData.MergeType == MergeType.Sum && incomingData.MergeType != MergeType.Sum)
				{
					foreach (string guid in allDeviceGUIDs)
					{
						string targetGUIDKey = $"{key}{guid}";
						dataDictionary.Remove(targetGUIDKey);
						foreach (IPersistence persistence in allPersistences)
							persistence.RemoveData(targetGUIDKey);
					}
				}

				if (newestData.MergeType == MergeType.Largest)
				{
					if (TryCompareData(incomingData, oldGetValue) > 0)
						ChangeData(key, new PersistenceData(incomingData.TimeStamp, incomingData.Data, incomingData.DataType, incomingData.MergeType, incomingData.DataFlags), oldGetValue);
					else
						ChangeData(key, new PersistenceData(oldGetValue.TimeStamp, oldGetValue.Data, incomingData.DataType, incomingData.MergeType, incomingData.DataFlags), oldGetValue);
				}
				else if (incomingData.MergeType == MergeType.Newest)
					ChangeData(key, new PersistenceData(newestData.TimeStamp, newestData.Data, incomingData.DataType, incomingData.MergeType, incomingData.DataFlags), oldGetValue);
				else if (incomingData.MergeType == MergeType.Oldest)
					ChangeData(key, new PersistenceData(oldestData.TimeStamp, oldestData.Data, incomingData.DataType, incomingData.MergeType, incomingData.DataFlags), oldGetValue);
				else if (incomingData.MergeType == MergeType.Sum)
					ChangeData(key, new PersistenceData(incomingData.TimeStamp, SumData(incomingData, oldGetValue).ToString(), incomingData.DataType, incomingData.MergeType, incomingData.DataFlags | DataFlags.DontPersist), oldGetValue);
				else
					throw new System.NotImplementedException();
			}
			else
			{
				if (incomingData.MergeType == MergeType.Sum)
					ChangeData(key, new PersistenceData(incomingData.TimeStamp, incomingData.Data, incomingData.DataType, incomingData.MergeType, incomingData.DataFlags | DataFlags.DontPersist), oldGetValue);
				else
					ChangeData(key, incomingData, existingData);
			}
			if (incomingData.MergeType == MergeType.Sum)
			{
				PersistenceData selfSumMirrorData = default(PersistenceData);
				string dataValue = incomingData.Data;
				string selfGUIDKey = $"{key}{GetData(deviceGUIDKey).Data}";
				if (TryGetData(selfGUIDKey, out selfSumMirrorData))
					dataValue = SumData(incomingData, selfSumMirrorData).ToString();
				SetData(selfGUIDKey, new PersistenceData(incomingData.TimeStamp, dataValue, incomingData.DataType, MergeType.Largest, incomingData.DataFlags | DataFlags.DeviceSpecific));
			}
		}

		public static bool DoesDataExist(string key)
		{
			return dataDictionary.ContainsKey(key);
		}

		public static PersistenceData GetData(string key)
		{
			PersistenceData data = default(PersistenceData);
			bool success = TryGetData(key, out data);
			if (success)
				return data;
			throw new System.Exception($"Data with key \"{key}\" doesn't exist!");
		}

		public static bool TryGetData(string key, out PersistenceData output)
		{
			output = default(PersistenceData);
			if (dataDictionary.TryGetValue(key, out output))
			{
				if (output.MergeType == MergeType.Sum)
				{
					double sumValue = 0;
					long latestTimeStamp = output.TimeStamp;
					foreach (string guid in allDeviceGUIDs)
					{
						PersistenceData deviceData = default(PersistenceData);
						if (dataDictionary.TryGetValue($"{key}{guid}", out deviceData))
						{
							latestTimeStamp = System.Math.Max(latestTimeStamp, deviceData.TimeStamp);
							double targetToSum = 0;
							if (!double.TryParse(deviceData.Data, out targetToSum))
							{
								Debug.LogError("Value is not numeric!");
								return false;
							}
							sumValue += targetToSum;
						}
					}
					output = new PersistenceData(latestTimeStamp, sumValue.ToString(), output.DataType, output.MergeType, output.DataFlags);
				}
				return true;
			}
			return false;
		}

		private static int TryCompareData(PersistenceData a, PersistenceData b)
		{
			double parseResultA;
			double parseResultB;
			if (double.TryParse(a.Data, out parseResultA) && double.TryParse(b.Data, out parseResultB))
				return parseResultA.CompareTo(parseResultB);
			Debug.LogError($"One of the values is not a numeric value! ({a.Data}),({b.Data})");
			return 0;
		}

		private static double SumData(PersistenceData a, PersistenceData b)
		{
			double parseResultA = 0;
			double parseResultB = 0;
			if (!(double.TryParse(a.Data, out parseResultA) && double.TryParse(b.Data, out parseResultB)))
				Debug.LogError($"One of the values is not a numeric value! ({a.Data}),({b.Data})");
			return parseResultA + parseResultB;
		}
	}
}