﻿namespace Aurora.PersistenceSystem
{
	[System.Serializable]
	public struct PersistenceData : System.IEquatable<PersistenceData>
	{
		public DataType DataType { get; private set; }
		public MergeType MergeType { get; private set; }
		public DataFlags DataFlags { get; private set; }
		public long TimeStamp { get; private set; }
		public string Data { get; private set; }

		public PersistenceData(long timeStamp, string data, DataType dataType = DataType.None, MergeType mergeType = MergeType.Newest, DataFlags dataFlags = DataFlags.None)
		{
			DataType = dataType;
			MergeType = mergeType;
			DataFlags = dataFlags;
			TimeStamp = timeStamp;
			Data = data;
		}

		public static PersistenceData PickNewest(PersistenceData a, PersistenceData b)
		{
			if (a.TimeStamp > b.TimeStamp)
				return a;
			return b;
		}

		public bool Equals(PersistenceData other)
		{
			if (DataType != other.DataType)
				return false;
			if (MergeType != other.MergeType)
				return false;
			if (DataFlags != other.DataFlags)
				return false;
			if (TimeStamp != other.TimeStamp)
				return false;
			if (Data != other.Data)
				return false;
			return true;
		}

		public override string ToString()
		{
			return $"{DataType},{MergeType},{DataFlags},{TimeStamp},{Data}";
		}

		public static PersistenceData Parse(string s)
		{
			PersistenceData output = default(PersistenceData);
			if (!TryParse(s, out output))
				throw new System.IO.InvalidDataException("The string is not a valid PersistenceData representation!");
			return output;
		}

		public static bool TryParse(string s, out PersistenceData output)
		{
			output = default(PersistenceData);

			int commaIndex = s.IndexOf(',');
			if (commaIndex == -1)
				return false;
			string dataTypeString = s.Substring(0, commaIndex);
			s = s.Substring(commaIndex + 1);

			commaIndex = s.IndexOf(',');
			if (commaIndex == -1)
				return false;
			string mergeTypeString = s.Substring(0, commaIndex);
			s = s.Substring(commaIndex + 1);

			commaIndex = s.IndexOf(',');
			if (commaIndex == -1)
				return false;
			string dataFlagsString = s.Substring(0, commaIndex);
			s = s.Substring(commaIndex + 1);

			commaIndex = s.IndexOf(',');
			if (commaIndex == -1)
				return false;
			string timeStampString = s.Substring(0, commaIndex);
			s = s.Substring(commaIndex + 1);

			string data = s;

			DataType dataType;
			MergeType mergeType;
			DataFlags dataFlags;

			if (!System.Enum.TryParse(dataTypeString, out dataType))
				return false;
			if (!System.Enum.TryParse(mergeTypeString, out mergeType))
				return false;
			if (!System.Enum.TryParse(dataFlagsString, out dataFlags))
				return false;

			long timeStamp;

			if (!long.TryParse(timeStampString, out timeStamp))
				return false;

			output = new PersistenceData(timeStamp, data, dataType, mergeType, dataFlags);
			return true;
		}

		public struct ChangeInfo
		{
			public PersistenceData OldData { get; private set; }
			public PersistenceData NewData { get; private set; }

			public ChangeInfo(PersistenceData oldData, PersistenceData newData)
			{
				OldData = oldData;
				NewData = newData;
			}
		}
	}
}