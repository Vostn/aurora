﻿using System;
using System.IO;
using Aurora.Diagnostics;
using System.Collections.Generic;

namespace Aurora.PersistenceSystem
{
	public sealed class LocalPersistence : IPersistence
	{
		private readonly Dictionary<string, PersistenceData> dataDictionary = new Dictionary<string, PersistenceData>();

		private readonly string filePath;

		private LocalPersistence()
		{
			throw new NotSupportedException();
		}

		public LocalPersistence(string filePath)
		{
			Directory.CreateDirectory(Path.GetDirectoryName(filePath));
			this.filePath = filePath;
		}

		void IPersistence.Finalise()
		{
			WriteToFile();
		}

		bool IPersistence.Online => false;

		private void ReadFromFile(Action<string, PersistenceData> dataLoadedCallback)
		{
			try
			{
				using (BinaryReader reader = new BinaryReader(File.Open(filePath, FileMode.OpenOrCreate)))
				{
					while (reader.BaseStream.Position < reader.BaseStream.Length)
					{
						long keyBeginning = reader.BaseStream.Position;
						string key = reader.ReadString();
						PersistenceData data;
						long dataBeginning = reader.BaseStream.Position;
						if (PersistenceData.TryParse(reader.ReadString(), out data))
						{
							dataDictionary.Add(key, data);
							dataLoadedCallback.Invoke(key, data);
						}
						else
							Debug.LogError($"The string is not a valid PersistenceData representation! (Key begins at character {keyBeginning}, Value begins at character {dataBeginning})");
					}
				}
			}
			catch (Exception e)
			{
				throw new Exception("File read by LocalPersistence failed!", e);
			}
		}

		private void WriteToFile()
		{
			try
			{
				using (BinaryWriter writer = new BinaryWriter(File.Open(filePath, FileMode.OpenOrCreate)))
				{
					foreach (var kvp in dataDictionary)
					{
						writer.Write(kvp.Key);
						writer.Write(kvp.Value.ToString());
					}
				}
			}
			catch (Exception e)
			{
				throw new Exception("File write by LocalPersistence failed!", e);
			}
		}

		void IPersistence.ChangeData(string key, PersistenceData data)
		{
			if (dataDictionary.ContainsKey(key))
				dataDictionary[key] = data;
			else
				dataDictionary.Add(key, data);
		}

		void IPersistence.LoadData(Action<string, PersistenceData> dataLoadedCallback)
		{
			ReadFromFile(dataLoadedCallback);
		}

		void IPersistence.RemoveData(string key)
		{
			dataDictionary.Remove(key);
		}
	}
}