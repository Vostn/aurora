﻿namespace Aurora.PersistenceSystem
{
	/// <summary>
	/// Any special settings about the data that the Persistence should be aware of and respect.
	/// </summary>
	[System.Flags]
	public enum DataFlags
	{
		None = 0,

		/// <summary>
		/// This value should not be synced to an Online Persistence.
		/// </summary>
		OfflineOnly = 1 << 0,

		/// <summary>
		/// This value comes from a specific device, and the key is suffixed with the device GUID.
		/// </summary>
		DeviceSpecific = 1 << 1,

		/// <summary>
		/// This data will not persist beyond the current session.
		/// </summary>
		DontPersist = 1 << 2,
	}
}