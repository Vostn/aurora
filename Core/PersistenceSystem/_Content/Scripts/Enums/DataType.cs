﻿namespace Aurora.PersistenceSystem
{
	//Defines if the Persistence should attempt to perform some Persistence specific action with the given data.
	public enum DataType : byte
	{
		//No action should be taken for this value beyond saving and restoring.
		None = 0,

		//This value should be posted to an Achivement Completion API.
		AchievementCompletion = 1,

		//This value should be posted to an Achivement Progress AP.
		AchievementProgress = 2,

		//This value should be posted to a Leaderboard.
		Leaderboard = 3,
	}
}