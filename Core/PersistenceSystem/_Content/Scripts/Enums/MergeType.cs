﻿namespace Aurora.PersistenceSystem
{
	/// <summary>
	/// How data value conflicts should be handled 
	/// </summary>
	public enum MergeType : byte
	{
		/// <summary>
		/// Uses only the data that has the larger timestamp.
		/// </summary>
		Newest = 0,

		/// <summary>
		/// Uses only the data that has the smaller timestamp.
		/// </summary>
		Oldest = 1,

		/// <summary>
		/// Uses only the data that has the largest numeric value.
		/// NOTE: Only works on numeric types.
		/// </summary>
		Largest = 2,

		/// <summary>
		/// Combines the numeric value of both data objects.
		/// NOTE: Only works on numeric types.
		/// </summary>
		Sum = 3,
	}
}