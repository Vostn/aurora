﻿namespace Aurora.PersistenceSystem
{
	public interface IPersistence
	{
		bool Online { get; }

		void ChangeData(string key, PersistenceData data);
		void RemoveData(string key);

		void Finalise();

		void LoadData(System.Action<string, PersistenceData> dataLoadedCallback);
	}
}