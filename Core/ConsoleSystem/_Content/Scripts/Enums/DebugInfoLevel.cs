﻿namespace Aurora.ConsoleSystem
{
	public enum DebugInfoLevel
	{
		NoOutput = 0,
		FirstLineOnly = 1,
		ErrorStackTrace = 2,
		AllStackTrace = 3,
	}
}