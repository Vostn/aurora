﻿namespace Aurora.ConsoleSystem
{
	public enum CommandSafetyLevel
	{
		PlayerExposed = 3,
		PlayerSafe = 2,
		DeveloperSafe = 1,
		Unsafe = 0,
	}
}