﻿namespace Aurora.ConsoleSystem
{
	public enum TextLogItemType
	{
		Warning,
		Error,
		Information,
		InputEcho,
		HelpHeader,
	}
}