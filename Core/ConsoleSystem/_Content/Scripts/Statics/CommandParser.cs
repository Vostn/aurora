﻿using System.Collections.Generic;

namespace Aurora.ConsoleSystem
{
	internal static class CommandParser
	{
		public static List<CommandCall> ParseCommands(string source)
		{
			List<CommandCall> commandCalls = new List<CommandCall>();
			List<CommandEntrySegment> entrySegments = new List<CommandEntrySegment>();
			bool argument = false;
			int depth = 0;
			int sectionStartIndex = 0;
			bool escape = false;
			for (int i = 0; i < source.Length; ++i)
			{
				char character = source[i];
				if (character == '\\')
				{
					source = source.Remove(i);
					escape = true;
					continue;
				}
				if (!escape)
				{
					if (character == '(')
					{
						if (depth == 0 && i > sectionStartIndex)
						{
							string content = source.Substring(sectionStartIndex, i - sectionStartIndex);
							entrySegments.Add(new CommandEntrySegment(content, argument));
							if (!argument)
								argument = true;
							sectionStartIndex = i + 1;
						}
						else if (depth == 0 && i == sectionStartIndex)
						{
							sectionStartIndex++;
						}
						depth++;
						continue;
					}
					else if (character == ')' && depth != 0)
					{
						if (depth == 1)
						{
							string content = source.Substring(sectionStartIndex, i - sectionStartIndex);
							entrySegments.Add(new CommandEntrySegment(content, argument));
							if (!argument)
								argument = true;
							sectionStartIndex = i + 1;
						}
						depth--;
						continue;
					}
					else if (character == ' ' && depth == 0)
					{
						if (sectionStartIndex != i)
						{
							string content = source.Substring(sectionStartIndex, i - sectionStartIndex);
								entrySegments.Add(new CommandEntrySegment(content, argument));
							if (!argument)
								argument = true;
						}
						sectionStartIndex = i + 1;
						continue;
					}
					else if (character == ';' && depth == 0)
					{
						if (sectionStartIndex != i)
						{
							string content = source.Substring(sectionStartIndex, i - sectionStartIndex);
							entrySegments.Add(new CommandEntrySegment(content, argument));
						}
						argument = false;
						sectionStartIndex = i + 1;
						continue;
					}
				}
				if (i == source.Length - 1)
				{
					string content = source.Substring(sectionStartIndex, (i + 1) - sectionStartIndex);
					entrySegments.Add(new CommandEntrySegment(content, argument));
					break;
				}
				escape = false;
			}
			for (int i = 0; i < entrySegments.Count;)
			{
				if (!entrySegments[i].IsArgument)
				{
					List<string> args = new List<string>();
					string identifier = entrySegments[i].Content;
					++i;
					while (i < entrySegments.Count && entrySegments[i].IsArgument) //-V3022
					{
						args.Add(entrySegments[i].Content);
						++i;
					}
					commandCalls.Add(new CommandCall(identifier, args));
					args.Clear();
				}
			}
			return commandCalls;
		}

		private class CommandEntrySegment
		{
			public string Content { get; private set; }
			public bool IsArgument { get; private set; }

			public CommandEntrySegment(string content, bool argument)
			{
				Content = content;
				IsArgument = argument;
			}

			private CommandEntrySegment() { }
		}
	}
}