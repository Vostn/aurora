﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace Aurora.ConsoleSystem
{
	/// <summary>
	/// Raw console logic class.
	/// </summary>
	public static class Console
	{
		private static List<Command> allCommands = new List<Command>();

		private static List<string> submittedCommands = new List<string>();
		private static DebugInfoLevel debugInfoLevel = DebugInfoLevel.FirstLineOnly;
		private static CommandSafetyLevel commandSafetyLevel = CommandSafetyLevel.PlayerExposed;
		public static CommandInput CurrentUserInput { get; private set; }
		internal static ReadOnlyCollection<Command> AllCommands { get; private set; } = allCommands.AsReadOnly();
		internal static ReadOnlyCollection<string> SubmittedCommands { get; private set; } = submittedCommands.AsReadOnly();

		[CommandSafetyLevel(CommandSafetyLevel.PlayerSafe)]
		[CommandDescription("Amount of detail displayed for logged debug lines.")]
		public static DebugInfoLevel DebugInfoLevel
		{
			get { return debugInfoLevel; }
			set { debugInfoLevel = (DebugInfoLevel)Math.Clamp((int)value, 0, 3); }
		}


		[CommandSafetyLevel(CommandSafetyLevel.PlayerExposed)]
		[CommandDescription("Commands lower than this safety level will not appear or be callable.")]
		public static CommandSafetyLevel CommandSafetyLevel
		{
			get { return commandSafetyLevel; }
			set
			{
#if !DEBUG
				if(value < CommandSafetyLevel.PlayerSafe)
					PrintError("CommandSafetyLevel cannot be lowered below PlayerSafe in a release build!");
				value = (CommandSafetyLevel)Math.Clamp((int)value, 2, 3);
#endif
				commandSafetyLevel = (CommandSafetyLevel)Math.Clamp((int)value, 0, 3);
			}
		}

		static Console()
		{
			AddCommands(Assembly.GetExecutingAssembly());
			CurrentUserInput = new CommandInput();
#if DEBUG
			DebugInfoLevel = DebugInfoLevel.ErrorStackTrace;
			CommandSafetyLevel = CommandSafetyLevel.DeveloperSafe;
#endif
		}

		/// <summary>
		/// This event is called when a new entry is logged to the console. This event can be called from any thread, so subscribers to this event must be thread safe.
		/// </summary>
		public static event System.Action<TextLogItem> NewEntryLogged;

		[CommandSafetyLevel(CommandSafetyLevel.PlayerSafe)]
		public static void PrintText(object message)
		{
			lock (NewEntryLogged)
				NewEntryLogged?.Invoke(new TextLogItem(message.ToStringNullAware(), TextLogItemType.Information));
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerSafe)]
		public static void PrintWarning(object message)
		{
			lock (NewEntryLogged)
				NewEntryLogged?.Invoke(new TextLogItem(message.ToStringNullAware(), TextLogItemType.Warning));
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerSafe)]
		public static void PrintError(object message)
		{
			lock (NewEntryLogged)
				NewEntryLogged?.Invoke(new TextLogItem(message.ToStringNullAware(), TextLogItemType.Error));
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerSafe)]
		public static string SanatiseCommandString(string value)
		{
			return Regex.Replace(value, "[\a\b\t\r\v\f\n]*", string.Empty);
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerSafe)]
		public static void PrintCommandHelp(object message)
		{
			lock (NewEntryLogged)
				NewEntryLogged?.Invoke(new TextLogItem(message.ToStringNullAware(), TextLogItemType.HelpHeader));
		}

		[CommandSafetyLevel(CommandSafetyLevel.DeveloperSafe)]
		public static void SubmitCommand(string commandString, bool echoCommand = true, bool addToHistory = true)
		{
			commandString = SanatiseCommandString(commandString);
			List<CommandCall> calls = CommandParser.ParseCommands(commandString);
			if (calls.Count > 0)
			{
				if (echoCommand)
					PrintCommandEcho(commandString);
				foreach (CommandCall call in calls)
				{
					List<Command> matchingCommands = FindMatchingCommands(call);

					if (matchingCommands.Count > 1)
					{
						PrintError(call.Identifier);
						PrintError("Multiple commands match the given signature!");
						foreach (Command command in matchingCommands)
							PrintError(command.ToString());
					}
					else if (matchingCommands.Count == 1)
					{
						object obj = matchingCommands[0].Invoke(call.Arguments);
						if (!matchingCommands[0].ReturnsVoid)
							PrintText(obj != null ? obj.ToString() : "Null");
					}
					else
						PrintError($"This console call does not match any known commands!");
				}
				if (addToHistory)
					submittedCommands.Add(commandString);
			}
			CurrentUserInput.Text = "";
			CurrentUserInput.CaretIndex = 0;
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerExposed)]
		public static void GetCommandHelp(string commandIdentifier = null)
		{
			if (commandIdentifier == null)
			{
				PrintCommandHelp("Include a command as a parameter to the call to GetCommandHelp to get help on a specific command!");
				return;
			}
			List<Command> commands = FindMatchingCommands(new CommandCall(commandIdentifier, new string[0]));
			if (commands.Count == 0)
				PrintWarning($"No commands match the given command \"{commandIdentifier}\"");
			foreach (Command command in commands)
				command.PrintCommandHelp();
		}

		public static void AddCommands(Assembly targetAssembly)
		{
			System.Type[] types = targetAssembly.GetTypes();
			foreach (System.Type type in types)
			{
				MethodInfo[] methods = type.GetMethods(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
				foreach (MethodInfo method in methods)
				{
					//Ignore if get or set method
					if (method.Name.StartsWith("get_"))
						continue;
					if (method.Name.StartsWith("set_"))
						continue;

					//Check if parameters are valid
					if (!MethodCommand.IsMethodValid(method))
						continue;

					CommandSafetyLevel safetyLevel = 0;
					CommandSafetyLevelAttribute[] safetyAttributes = (CommandSafetyLevelAttribute[])method.GetCustomAttributes(typeof(CommandSafetyLevelAttribute), false);
					if (safetyAttributes.Length > 0)
						safetyLevel = safetyAttributes[0].SafetyLevel;

					MethodCommand newCommand = new MethodCommand(method, safetyLevel);

					allCommands.Add(newCommand);
				}

				PropertyInfo[] properties = type.GetProperties(BindingFlags.Static | BindingFlags.NonPublic | BindingFlags.Public);
				foreach (PropertyInfo property in properties)
				{
					//Check if parameters are valid
					if (!PropertyCommand.IsPropertyValid(property))
						continue;

					CommandSafetyLevel safetyLevel = 0;
					CommandSafetyLevelAttribute[] safetyAttributes = (CommandSafetyLevelAttribute[])property.GetCustomAttributes(typeof(CommandSafetyLevelAttribute), false);
					if (safetyAttributes.Length > 0)
						safetyLevel = safetyAttributes[0].SafetyLevel;

					PropertyCommand newCommand = new PropertyCommand(property, safetyLevel);

					allCommands.Add(newCommand);
				}
			}
		}

		[CommandSafetyLevel(CommandSafetyLevel.DeveloperSafe)]
		private static void PrintCommandEcho(string text)
		{
			lock (NewEntryLogged)
				NewEntryLogged?.Invoke(new TextLogItem(text, TextLogItemType.InputEcho));
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerExposed)]
		private static void Alias(string aliasString, string commandString)
		{
			foreach (Command command in allCommands)
			{
				if (command.FullIdentifier == aliasString)
				{
					if (command is AliasCommand)
					{
						Dealias(aliasString);
						break;
					}
					else
					{
						PrintError($"Command already exists with the identifier \"{aliasString}\"!");
						return;
					}
				}
			}
			allCommands.Add(new AliasCommand(aliasString, commandString));
			PrintText($"Alias \"{aliasString}\" -> \"{commandString}\" registered!");
		}

		[CommandSafetyLevel(CommandSafetyLevel.PlayerExposed)]
		private static void Dealias(string aliasString)
		{
			foreach (Command command in allCommands)
			{
				AliasCommand aliasCommand = command as AliasCommand;
				if (aliasCommand == null)
					continue;
				if (command.FullIdentifier == aliasString)
				{
					allCommands.Remove(command);
					PrintText($"Alias \"{aliasCommand.FullIdentifier}\" -> \"{aliasCommand.AliasedString}\" de-registered!");
					return;
				}
			}
			PrintText("No alias exists with the identifier \"" + aliasString + "\"!");
		}

		internal static List<Command> FindMatchingCommands(CommandCall call)
		{
			List<Command> commandList = new List<Command>();

			for (int i = 0; i < allCommands.Count; ++i)
			{
				Command command = allCommands[i];
				if (command.SafetyLevel >= CommandSafetyLevel && command.FullIdentifier == call.Identifier)
					commandList.Add(allCommands[i]);
			}

			return commandList;
		}
	}
}