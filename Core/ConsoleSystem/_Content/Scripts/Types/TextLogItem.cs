﻿namespace Aurora.ConsoleSystem
{
	public sealed class TextLogItem
	{
		public string Text { get; private set; }
		public TextLogItemType Type { get; private set; }

		public TextLogItem(string text, TextLogItemType type)
		{
			Text = text;
			Type = type;
		}
	}
}