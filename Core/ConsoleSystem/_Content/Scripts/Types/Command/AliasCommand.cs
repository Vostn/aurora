﻿using System.Collections.Generic;
using System.Text;

namespace Aurora.ConsoleSystem
{
	internal sealed class AliasCommand : Command
	{
		public string AliasedString { get; private set; }

		public AliasCommand(string identifier, string aliasedString) : base(identifier, CommandSafetyLevel.PlayerExposed, true)
		{
			AliasedString = aliasedString;
		}
		
		public override object Invoke(IEnumerable<string> arguments)
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(AliasedString);
			foreach (string argument in arguments)
			{
				builder.Append(" (");
				builder.Append(argument);
				builder.Append(")");
			}
			Console.SubmitCommand(builder.ToString(),true, false);
			return null;
		}
		
		public string ToDetailedString()
		{
			return $"({FullIdentifier}) -> ({AliasedString})";
		}

		public override string ToString()
		{
			return AliasedString;
		}

		public override void PrintCommandHelp()
		{
			List<Command> commands = Console.FindMatchingCommands(new CommandCall(AliasedString, new string[0]));
			foreach (Command command in commands)
				command.PrintCommandHelp();
		}
	}
}
