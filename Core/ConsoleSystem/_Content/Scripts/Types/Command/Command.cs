﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace Aurora.ConsoleSystem
{
	public abstract class Command
	{
		private readonly List<string> identifiersInternal;
		public CommandSafetyLevel SafetyLevel { get; private set; }

		public ReadOnlyCollection<string> Identifiers { get; private set; }
		public string FullIdentifier { get; private set; }
		public bool ReturnsVoid { get; private set; }

		protected Command(string fullIdentifier, CommandSafetyLevel safetyLevel, bool returnsVoid)
		{
			ReturnsVoid = returnsVoid;
			FullIdentifier = fullIdentifier;
			identifiersInternal = fullIdentifier.Split('.').ToList();
			Identifiers = identifiersInternal.AsReadOnly();
			SafetyLevel = safetyLevel;
		}

		public abstract void PrintCommandHelp();

		public static bool IsArgumentConversionValid(string argument, System.Type parameterType)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(parameterType);
			try { converter.ConvertFrom(argument); }
			catch { return false; }
			return true;
		}

		protected static string EnumDetailString(System.Type enumType)
		{
			if (!enumType.IsEnum)
				throw new System.ArgumentException($"Argument {nameof(enumType)} must be an enum type!");

			StringBuilder stringBuilder = new StringBuilder();
			string[] enumNames = enumType.GetEnumNames();
			System.Array enumValues = enumType.GetEnumValues();
			for(int j = 0;j<enumNames.Length;++j)
			{
				stringBuilder.Append($"{(int)enumValues.GetValue(j)} - {enumNames[j]}");
				if (j != enumNames.Length - 1)
					stringBuilder.Append(", ");
			}
			return stringBuilder.ToString();
		}
		
		public abstract object Invoke(IEnumerable<string> arguments);
		
		public abstract override string ToString();
	}
}