﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Aurora.ConsoleSystem
{
	internal sealed class MethodCommand : Command
	{
		private readonly MethodInfo method;

		public MethodCommand(MethodInfo method, CommandSafetyLevel safetyLevel) : base($"{method.DeclaringType}.{method.Name}", safetyLevel, method.ReturnType == typeof(void))
		{ 
			this.method = method;
			if (!IsMethodValid(method))
				throw new System.ArgumentException($"Method passed to constructor for MethodConsoleCommand is not valid. Use {GetType()}.{nameof(IsMethodValid)} to verify methods before passing to the constructor.", "method");
		}

		public static bool IsMethodValid(MethodInfo method)
		{
			ParameterInfo[] parameters = method.GetParameters();
			foreach (ParameterInfo parameter in parameters)
			{
				TypeConverter converter = TypeDescriptor.GetConverter(parameter.ParameterType);

				if (parameter.ParameterType == typeof(object))
					continue;
				if (!converter.CanConvertFrom(typeof(string)))
					return false;
			}
			return true;
		}
		
		public override object Invoke(IEnumerable<string> arguments)
		{
			ParameterInfo[] methodParameters = method.GetParameters();
			int index = 0;
			foreach (string argument in arguments)
			{
				if (methodParameters.Length <= index)
					break;
				System.Type parameterType = methodParameters[index].ParameterType;
				if (!IsArgumentConversionValid(argument, parameterType))
				{
					Console.PrintError($"Argument {index + 1} ({argument}) is an invalid format for required type ({parameterType})!");
					return null;
				}
				++index;
			}
			
			object[] convertedArguments = ConvertArguments(arguments.ToList());
			if (convertedArguments != null)
			{
				object returnObject = method.Invoke(null, convertedArguments);
				if (method.ReturnType != typeof(void))
					return returnObject;
				return null;
			}
			else
				return null;
		}

		public override string ToString()
		{
			StringBuilder builder = new StringBuilder();
			builder.Append(FullIdentifier);
			foreach (ParameterInfo parameter in method.GetParameters())
			{
				builder.Append(' ');
				builder.Append(parameter.ParameterType);
			}
			return builder.ToString();
		}

		private object[] ConvertArguments(IEnumerable<string> argumentStrings)
		{
			ParameterInfo[] parameters = method.GetParameters();
			int requiredParameters = parameters.Count(x => !x.IsOptional);
			if (requiredParameters > argumentStrings.Count())
			{
				Console.PrintError($"Not enough parameters for command \"{FullIdentifier}\"!");
				PrintCommandHelp();
				return null;
			}
			List<object> convertedParameters = new List<object>();

			int i = 0;
			foreach (string parameter in argumentStrings)
			{
				if (i == parameters.Length)
				{
					Console.PrintWarning($"Too many parameters for command \"{FullIdentifier}\"!");
					break;
				}

				TypeConverter converter = TypeDescriptor.GetConverter(parameters[i].ParameterType);
				if (parameters[i].ParameterType == typeof(object))
					convertedParameters.Add(parameter);
				else
					convertedParameters.Add(converter.ConvertFrom(parameter));
				++i;
			}
			while(convertedParameters.Count < parameters.Length)
			{
				convertedParameters.Add(parameters[convertedParameters.Count].DefaultValue);
			}
			return convertedParameters.ToArray();
		}

		public override void PrintCommandHelp()
		{
			//Print Full Command Identifier
			Console.PrintCommandHelp($"Full Identifier: {FullIdentifier}");

			//Print description, if any
			CommandDescriptionAttribute attribute = method.GetCustomAttribute<CommandDescriptionAttribute>();
			string description = "No Description";
			if (attribute != null)
				description = attribute.CommandDescription;
			Console.PrintCommandHelp($"Description: {description}");

			//Print all parameters
			ParameterInfo[] paramInfo = method.GetParameters();
			for (int i = 0; i < paramInfo.Length; ++i)
			{
				Console.PrintCommandHelp($"\tParameter {i + 1}: \"{paramInfo[i].Name}\" Type: \"{paramInfo[i].ParameterType}\"");
				if(paramInfo[i].ParameterType.IsEnum)
					Console.PrintCommandHelp($"\t\tPossible Values for {paramInfo[i].ParameterType}: {EnumDetailString(paramInfo[i].ParameterType)}");
			}

			//Print return type
			Console.PrintCommandHelp($"\tReturn Type: \"{method.ReturnType}\"");
		}
	}
}