﻿using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace Aurora.ConsoleSystem
{
	internal sealed class PropertyCommand : Command
	{
		private readonly  PropertyInfo property;

		public PropertyCommand(PropertyInfo property, CommandSafetyLevel safetyLevel) : base($"{property.DeclaringType}.{property.Name}", safetyLevel, false)
		{
			this.property = property;
			if (!IsPropertyValid(property))
				throw new System.ArgumentException($"Property passed to constructor for PropertyConsoleCommand is not valid. Use {GetType()}.{nameof(IsPropertyValid)} to verify properties before passing to the constructor.", "property");
		}

		public static bool IsPropertyValid(PropertyInfo property)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(property.PropertyType);
			if (property.PropertyType == typeof(object))
				return true;
			return converter.CanConvertFrom(typeof(string));
		}

		public override string ToString()
		{
			return $"{FullIdentifier} {property.PropertyType}";
		}

		public override object Invoke(IEnumerable<string> arguments)
		{
			object returnObject = null;

			if (arguments.Any())
			{
				string argument = arguments.First();
				if (IsArgumentConversionValid(argument, property.PropertyType))
				{
					object convertedArgument = ConvertArgument(argument);
					if (convertedArgument != null)
					{
						MethodInfo info = property.GetSetMethod();
						if (info == null)
							Console.PrintWarning("This property is read only!");
						else
							info.Invoke(null, new object[] { convertedArgument });
					}
				}
				else
					Console.PrintError($"Argument 1 is an invalid format for required type ({property.PropertyType}) ({argument})");
			}

			returnObject = property.GetGetMethod().Invoke(null, null);
			return returnObject;
		}

		private object ConvertArgument(string argument)
		{
			if (property.PropertyType == typeof(object))
				return argument;

			TypeConverter converter = TypeDescriptor.GetConverter(property.PropertyType);
			return converter.ConvertFrom(argument);
		}

		public override void PrintCommandHelp()
		{
			//Print Full Command Identifier
			Console.PrintCommandHelp($"{FullIdentifier}");

			//Print description, if any
			CommandDescriptionAttribute attribute = property.GetCustomAttribute<CommandDescriptionAttribute>();
			string description = "No Description";
			if (attribute != null)
				description = attribute.CommandDescription;
			Console.PrintCommandHelp($"Description: {description}");

			//Print return type.
			Console.PrintCommandHelp($"\tAccepts and Returns values of type \"{property.PropertyType}\"");
			if(property.PropertyType.IsEnum)
				Console.PrintCommandHelp($"\t\tPossible Values for {property.PropertyType}: {EnumDetailString(property.PropertyType)}");
		}
	}
}