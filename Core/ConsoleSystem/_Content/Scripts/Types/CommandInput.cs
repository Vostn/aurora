﻿using System.Collections.Generic;
using System.Text.RegularExpressions;
using System.Text;

namespace Aurora.ConsoleSystem
{
	public sealed class CommandInput
	{
		private string text;
		private int caretIndex;

		private readonly List<CommandInputContextBase> contexts = new List<CommandInputContextBase>();

		//Currently unused, may  be used in the future to dynamically colour malformed commands.
		private List<CommandCall> parsedCommands = new List<CommandCall>();

		public CommandInputContext SelectedContext { get; private set; }

		public string Text
		{
			get
			{
				StringBuilder stringBuilder = new StringBuilder(); 
				foreach(CommandInputContextBase context in contexts)
				{
					stringBuilder.Append(context.CurrentText);
				}
				text = stringBuilder.ToString();
				return text;
			}
			set
			{
				if (Text != value)
				{
					text = value;
					parsedCommands = CommandParser.ParseCommands(text);
					RefreshContexts();
					RecalculateSelectedContext();
				}
			}
		}

		public int CaretIndex
		{
			get { return caretIndex; }
			set
			{
				caretIndex = value;
				RecalculateSelectedContext();
			}
		}

		
		
		public CommandInput()
		{
			Text = "";
			CaretIndex = 0;
		}

		private void RecalculateSelectedContext()
		{
			int index = 0;
			foreach (CommandInputContextBase context in contexts)
			{
				if (context is CommandInputContextSeparator)
				{
					++index;
				}
				else 
				{
					if (index <= CaretIndex && index + context.CurrentText.Length > CaretIndex - 1)
					{
						SelectedContext = context as CommandInputContext;
						return;
					}
					else
					{
						index += context.CurrentText.Length;
					}
				}
			}
			SelectedContext = null;
		}
		
		private void RefreshContexts()
		{
			//First, check what index range has been modified.
			//We do this by checking from the start of the string to the first change, and from the back of the string to the last change,
			//comparing the text against the existing contexts. Any contexts that occur before a change are considered still valid,
			//and any that occur after are considered invalid.
			//In a typical typing scenario, all changes are together, but a copy paste can lead to non contiguous changes.
			//In this situation, dropping all contexts in that changed area is preferable, as it can be unclear where a potentially valid context should go,
			//and it would be unwise to assume a context where the user expects no context transferral.

			//Initialise lists of still-valid contexts.
			List<CommandInputContextBase> fromStartContexts = new List<CommandInputContextBase>();
			List<CommandInputContextBase> fromEndContexts = new List<CommandInputContextBase>();

			//Create our index tracking variables
			int firstChangeIndex = 0;
			int lastChangeIndex = text.Length - 1;
			
			//First, check from start->change contexts, and track the index of the first change.
			int currentTestIndex = 0;
			for (int i = 0;i< contexts.Count;++i)
			{
				CommandInputContextBase context = contexts[i];
				if (currentTestIndex + context.CurrentText.Length >= text.Length)
					break;
				bool thisIsSeparatorContext = context is CommandInputContextSeparator;
				bool nextCharIsSeparator = Regex.Match(text[currentTestIndex + context.CurrentText.Length].ToString(), "[() ;]{1}").Success;
				if (!thisIsSeparatorContext && !nextCharIsSeparator)
					break;
				
				fromStartContexts.Add(context);
				currentTestIndex += context.CurrentText.Length;
				firstChangeIndex = currentTestIndex;
			}
			
			//Now check the contexts from the back to the last change.
			currentTestIndex = text.Length;
			for (int i = contexts.Count-1; i >=0; --i)
			{
				//If context text matches the text of this input at the current position, it is still valid, and therefore added.
				CommandInputContextBase context = contexts[i];
				if (currentTestIndex >= text.Length)
					break;
				bool thisIsSeparatorContext = context is CommandInputContextSeparator;
				bool nextCharIsSeparator = Regex.Match(text[currentTestIndex - context.CurrentText.Length].ToString(), "[() ;]{1}").Success;
				if (!thisIsSeparatorContext && !nextCharIsSeparator)
					break;
				
				fromEndContexts.Add(context);
				currentTestIndex -= context.CurrentText.Length;
				lastChangeIndex = currentTestIndex-1;
			}
			//Clear current context list, and add in the contexts at the start that are still valid.
			contexts.Clear();
			contexts.AddRange(fromStartContexts);
			//Going from our first change index to our last, rebuild all contexts.
			for (int i = firstChangeIndex; i <= lastChangeIndex; ++i)
			{
				//If the current character is a context separator, add a separator context.
				Match match = Regex.Match(text.Substring(i, (lastChangeIndex + 1) - i), "[() ;]{1}");
				if (match.Success && match.Index == 0)
				{
					contexts.Add(new CommandInputContextSeparator(text[match.Index+i].ToString(), match.Index));
				}
				//If not a context separator, create a new standard context.
				else
				{
					int length = 0;
					if (match.Success)
						length = match.Index + 1;
					else
						length = (lastChangeIndex + 1) - i;
					string contextText = text.Substring(i, length);
					CommandInputContext newContext = new CommandInputContext(contextText, i);
					contexts.Add(newContext);
					i += contextText.Length - 1;
				}
			}

			//Add in contexts that are still valid at the end of the text.
			for (int i = fromEndContexts.Count - 1; i >= 0; --i)
				contexts.Add(fromEndContexts[i]);

			//If we have no contexts, then we have to add a single empty context, so that command history can still be accessed.
			if (contexts.Count == 0)
				contexts.Add(new CommandInputContext("", 0));
		}
	}
}
