﻿namespace Aurora.ConsoleSystem
{
	public abstract class CommandInputContextBase
	{
		public string CurrentText { get; protected set; }
		public int StartIndex { get; private set; }

		protected CommandInputContextBase(string text, int startIndex)
		{
			CurrentText = text;
			StartIndex = startIndex;
		}
	}
}