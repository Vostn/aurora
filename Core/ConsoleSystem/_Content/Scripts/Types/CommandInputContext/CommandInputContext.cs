﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text.RegularExpressions;

namespace Aurora.ConsoleSystem
{
	public sealed class CommandInputContext : CommandInputContextBase
	{
		private static Func<string, string, bool>[] matchFunctions = new Func<string, string, bool>[4];

		private int selectionIndex = 0;
		private readonly List<Command> predictedCommands = new List<Command>();
		private readonly List<string> filteredHistoricCommands = new List<string>();
		public string OriginalText { get; private set; }
		public ReadOnlyCollection<Command> PredictedCommands { get; private set; }
		public ReadOnlyCollection<string> FilteredHistoricCommands { get; private set; }

		public int SelectionIndex
		{
			get
			{
				return selectionIndex;
			}
			set
			{
				selectionIndex = value;
				RecalculateCurrentText();
			}
		}

		static CommandInputContext()
		{
			matchFunctions[0] = (x, y) => (x[0].ToString() + Regex.Replace(x.Substring(1, x.Length - 1), "[^A-Z0-9]", "")).ToUpper().StartsWith(y.ToUpper());
			matchFunctions[1] = (x, y) => x.ToUpper().StartsWith(y.ToUpper());
			matchFunctions[2] = (x, y) => (x[0].ToString() + Regex.Replace(x.Substring(1, x.Length - 1), "[^A-Z0-9]", "")).ToUpper().Contains(y.ToUpper());
			matchFunctions[3] = (x, y) => x.ToUpper().Contains(y.ToUpper());
		}

		public CommandInputContext(string text, int startIndex) : base(text, startIndex)
		{
			OriginalText = text;
			SelectionIndex = 0;

			PredictedCommands = predictedCommands.AsReadOnly();
			FilteredHistoricCommands = filteredHistoricCommands.AsReadOnly();

			RebuildCollections();

			RecalculateCurrentText();
		}

		private void RebuildCollections()
		{
			RebuildPredictedCommands();
			RebuildFilteredHistoricCommands();
		}

		private void RebuildPredictedCommands()
		{
			predictedCommands.Clear();

			if (OriginalText.Length <= 0)
				return;
			string[] identifiers = OriginalText.Split(new char[] { '.' }, StringSplitOptions.None);
			for (int i = 0; i < identifiers.Length; ++i)
				identifiers[i] = identifiers[i].ToUpper();

			Dictionary<string, CommandPriority> matchesDict = new Dictionary<string, CommandPriority>();

			foreach (Command command in Console.AllCommands)
			{
				if (command.SafetyLevel < Console.CommandSafetyLevel)
					continue;
				if (matchesDict.ContainsKey(command.FullIdentifier))
					continue;
				if (command.Identifiers.Count < identifiers.Length)
					continue;

				//Match Order:
				//(Ordered by closeness to end)->(ordered by type (Starts with)->(matches capitals/numerics)->(contains))->(ordered by match count)->(ordered alphabetically, starting with last identifier, moving to front)

				int firstMatchIndex = int.MaxValue;
				int matchCount = 0;
				int matchType = -1;
				int testStartIndex = 1;
				for (int j = 0; j < matchFunctions.Length; ++j)
				{
					int k = 1;
					for (int i = testStartIndex; i <= command.Identifiers.Count; ++i)
					{
						if (k > identifiers.Length)
							break;
						if(identifiers.Length > command.Identifiers.Count + (k - i))
						{
							firstMatchIndex = int.MaxValue;
							matchCount = 0;
							matchType = -1;
							break;
						}
						string commandIdent = command.Identifiers[command.Identifiers.Count - i];
						string searchIdent = identifiers[identifiers.Length - k];

						if (matchFunctions[j](commandIdent, searchIdent))
						{
							firstMatchIndex = System.Math.Min(firstMatchIndex, i - 1);
							matchType = j;
							++matchCount;
							++k;
						}
						else if (firstMatchIndex != int.MaxValue)
						{
							firstMatchIndex = int.MaxValue;
							matchCount = 0;
							matchType = -1;
							break;
						}
					}
					if (matchType != -1)
						break;
					else
					{
						if (testStartIndex < command.Identifiers.Count)
						{
							++testStartIndex;
							--j;
						}
						else
							testStartIndex = 1;
					}
				}
				if (matchType == -1)
					continue;

				int priority = (firstMatchIndex << 16) + (matchType << 8) + (matchCount);
				matchesDict.Add(command.FullIdentifier, new CommandPriority(priority, command));
			}
			
			List<CommandPriority> startsWithMatchesList = new List<CommandPriority>(matchesDict.Values);
			startsWithMatchesList.Sort();
			foreach (CommandPriority command in startsWithMatchesList)
				predictedCommands.Add(command.Command);
		}

		private void RebuildFilteredHistoricCommands()
		{
			filteredHistoricCommands.Clear();
			if (OriginalText.Length > 0)
			{
				foreach (string historicCommandCall in Console.SubmittedCommands)
				{
					if (historicCommandCall.ToUpper().Contains(OriginalText.ToUpper()))
						filteredHistoricCommands.Add(historicCommandCall);
				}
			}
			else
			{
				filteredHistoricCommands.AddRange(Console.SubmittedCommands);
			}
		}

		private void RecalculateCurrentText()
		{
			if (SelectionIndex > 0)
			{
				if (SelectionIndex > PredictedCommands.Count)
					SelectionIndex = PredictedCommands.Count;
				if (SelectionIndex > 0)
					CurrentText = PredictedCommands[SelectionIndex - 1].FullIdentifier;
			}
			if (SelectionIndex < 0)
			{
				if (-SelectionIndex > FilteredHistoricCommands.Count)
					SelectionIndex = -FilteredHistoricCommands.Count;
				if (SelectionIndex < 0)
					CurrentText = FilteredHistoricCommands[FilteredHistoricCommands.Count + SelectionIndex];
			}
			if (SelectionIndex == 0)
				CurrentText = OriginalText;
		}

		private class CommandPriority : System.IComparable<CommandPriority>
		{
			public int PriorityLevel { get; private set; }
			public Command Command { get; private set; }

			public CommandPriority(int priorityLevel, Command command)
			{
				Command = command;
				PriorityLevel = priorityLevel;
			}

			int IComparable<CommandPriority>.CompareTo(CommandPriority other)
			{
				int value = PriorityLevel.CompareTo(other.PriorityLevel);
				if (value != 0)
					return value;
				for (int i = 1; i <= Command.Identifiers.Count && i <= other.Command.Identifiers.Count; ++i)
				{
					value = Command.Identifiers[Command.Identifiers.Count - i].CompareTo(other.Command.Identifiers[other.Command.Identifiers.Count - i]);
					if (value != 0)
						return value;
				}
				return 0;
			}
		}
	}
}