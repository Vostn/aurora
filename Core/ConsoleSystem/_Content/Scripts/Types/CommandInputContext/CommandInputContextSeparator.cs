﻿namespace Aurora.ConsoleSystem
{
	public sealed class CommandInputContextSeparator : CommandInputContextBase
	{
		public CommandInputContextSeparator(string text, int startIndex) : base(text, startIndex)
		{
		}
	}
}