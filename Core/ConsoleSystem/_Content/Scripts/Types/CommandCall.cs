﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Aurora.ConsoleSystem
{
	internal sealed class CommandCall
	{
		private List<string> arguments = new List<string>();
		public string Identifier { get; private set; }
		public ReadOnlyCollection<string> Arguments { get; private set; }

		public CommandCall(string identifier, IEnumerable<string> arguments)
		{
			Identifier = identifier;
			this.arguments.AddRange(arguments);
			Arguments = this.arguments.AsReadOnly();
		}

		private CommandCall()
		{
		}
	}
}