﻿using System;

namespace Aurora.ConsoleSystem
{
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
	public sealed class CommandSafetyLevelAttribute : Attribute
	{
		public CommandSafetyLevel SafetyLevel { get; private set; }

		public CommandSafetyLevelAttribute(CommandSafetyLevel safetyLevel)
		{
			this.SafetyLevel = safetyLevel;
		}
	}
}