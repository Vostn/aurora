﻿using System;

namespace Aurora.ConsoleSystem
{
	[AttributeUsage(AttributeTargets.Method | AttributeTargets.Property, AllowMultiple = false, Inherited = false)]
	public sealed class CommandDescriptionAttribute : Attribute
	{
		public string CommandDescription { get; private set; }

		public CommandDescriptionAttribute(string commandDescription)
		{
			CommandDescription = commandDescription;
		}
	}
}