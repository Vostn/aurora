﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Aurora.LocalisationSystem
{
	public static class CultureInfoExt
	{
		private static IOrderedEnumerable<CultureInfo> InheritenceTree(this CultureInfo culture)
		{
			IEnumerable<CultureInfo> cultures = new List<CultureInfo>();
			cultures = cultures.Union(Enumerable.Repeat(culture, 1));
			if (culture != CultureInfo.InvariantCulture)
				cultures = cultures.Union(culture.Parent.InheritenceTree());
			return cultures.OrderBy(x => x.Depth());
		}

		public static IOrderedEnumerable<CultureInfo> AllParentCultures(this IEnumerable<CultureInfo> cultures)
		{
			return cultures.SelectMany(x => x.InheritenceTree()).Distinct().OrderBy(x => x.Depth());
		}

		public static IOrderedEnumerable<CultureInfo> CommonCultures(this IEnumerable<CultureInfo> cultures)
		{
			return cultures.AllParentCultures().Where(x => x.IsParentOfAll(cultures)).OrderBy(x => x.Depth());
		}

		public static CultureInfo LangaugeDefault(this IEnumerable<CultureInfo> cultures)
		{
			string langaugeCode = cultures.First().TwoLetterISOLanguageName;
			return new CultureInfo(langaugeCode);
		}

		public static bool ContainsLangaugeDefault(this IEnumerable<CultureInfo> cultures)
		{
			return cultures.Contains(CultureInfo.CreateSpecificCulture(cultures.First().TwoLetterISOLanguageName));
		}

		public static int Depth(this CultureInfo culture)
		{
			int depth = culture == CultureInfo.InvariantCulture? 0 : 1;
			while(culture.Parent != CultureInfo.InvariantCulture)
			{
				++depth;
				culture = culture.Parent;
			}
			return depth;
		}

		public static bool IsChildOf(this CultureInfo culture, CultureInfo parent)
		{
			return culture.InheritenceTree().Contains(parent);
		}

		public static bool IsChildOfAny(this CultureInfo culture, IEnumerable<CultureInfo> parents)
		{
			return parents.Any(x => x.IsParentOf(culture));
		}

		public static bool IsParentOfAll(this CultureInfo culture, IEnumerable<CultureInfo> children)
		{
			return children.All(x => x.IsChildOf(culture));
		}

		public static bool IsParentOf(this CultureInfo culture, CultureInfo child)
		{
			return child.IsChildOf(culture);
		}
	}
}
