﻿using Aurora.Diagnostics;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Aurora.LocalisationSystem
{
	public class Language
	{
		/// <summary>
		/// Language strings keyed by unique string key.
		/// </summary>
		private readonly Dictionary<string, string> strings;

		public readonly IReadOnlyList<CultureInfo> AssociatedCultures;

		private Language()
		{
		}

		public Language(IEnumerable<CultureInfo> associatedCultures, IDictionary<string, string> strings)
		{
			AssociatedCultures = associatedCultures.ToArray();
			this.strings = new Dictionary<string, string>(strings);
		}

		public IEnumerable<string> GetAllKeys()
		{
			return strings.Keys;
		}

		public bool HasString(string key)
		{
			return strings.ContainsKey(key);
		}

		public bool TryGetString(string key, out string stringResult)
		{
			stringResult = null;

			if (!HasString(key))
				return false;
			stringResult = GetString(key);
			return true;
		}

		public string GetString(string key)
		{
			if (!HasString(key))
				return Localisation.StringNotInLanguageError(key, this);
			return strings[key];
		}

		public static Language CreateFromFile(string languageFileContents)
		{
			languageFileContents = languageFileContents.Replace("\r\n", "\n");
			string[] lines = languageFileContents.Split('\n');

			string cultureCodesLine = lines[0].Replace('\t', ' ');
			string[] cultureCodes = cultureCodesLine.Split(' ');

			// Find all cultures specified in the file
			List<CultureInfo> associatedCultures = new List<CultureInfo>();
			foreach (string code in cultureCodes)
			{
				CultureInfo cultureInfo = Localisation.GetCultureFromCultureCode(code);
				if (cultureInfo == null)
					continue;

				associatedCultures.Add(cultureInfo);
			}

			// Ensure that at least one culture code exists
			if (!associatedCultures.Any())
			{
				Debug.LogError($"Language has no associated cultures! Language was not loaded!");
				return null;
			}

			// Find the common neutral cultures of this language.
			IEnumerable<CultureInfo> commonCultures = associatedCultures.CommonCultures();

			// Ensure that the language contains at least one common culture.
			if (!commonCultures.Any())
			{
				Debug.LogError($"Language contains multiple conflicting cultures! Language was not loaded! (Cultures: {string.Join(", ",associatedCultures)})");
				return null;
			}

			// Load all string keys from the file.
			Dictionary<string, string> stringDictionary = new Dictionary<string, string>();
			for (int i = 1; i < lines.Length; ++i)
			{
				string line = lines[i];
				if (string.IsNullOrWhiteSpace(line))
					continue;
				string[] lineParts = lines[i].Split('\t');
				if (lineParts.Length != 2)
				{
					Debug.LogError($"Language file had a malformed string key line! (String key line: {line}) (Cultures: {string.Join(", ",associatedCultures)})");
					continue;
				}
				stringDictionary.Add(lineParts[0], lineParts[1]);
			}

			// Build and return the loaded language
			return new Language(associatedCultures, stringDictionary);
		}
	}
}