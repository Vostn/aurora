﻿using System.Text;

namespace Aurora.LocalisationSystem
{
	public static partial class TextShaper
	{
		public static string ConvertToShaped(this string original)
		{
			foreach(var combinationDefintion in glyphCombinationDefinitions)
			{
				original = original.Replace(combinationDefintion.CombinationSource, combinationDefintion.CombinationResult);
			}

			StringBuilder builder = new StringBuilder();

			char? previousCharacter = null;
			for(int i = 0;i < original.Length;++i)
			{
				char currentCharacter = original[i];
				if (char.IsWhiteSpace(currentCharacter))
				{
					builder.Append(currentCharacter);
					continue;
				}

				GlyphShapeDefinition definition;

				if (!glyphShapeDefinitions.TryGetValue(currentCharacter, out definition))
				{
					builder.Append(currentCharacter);
					previousCharacter = null;
					continue;
				}

				char nextCharacter = ' ';
				if (i < original.Length - 1)
					nextCharacter = original[i+1];

				char usedCharacter;

				bool nextIsWhitespace = char.IsWhiteSpace(nextCharacter);

				if (previousCharacter == null && !nextIsWhitespace)
					usedCharacter = definition.Beginning;
				else if (previousCharacter != null && nextIsWhitespace)
					usedCharacter = definition.End;
				else if (previousCharacter != null)
					usedCharacter = definition.Middle;
				else
					usedCharacter = definition.Isolated;

				builder.Append(usedCharacter);

				if ((usedCharacter == definition.End || usedCharacter == definition.Isolated || usedCharacter == definition.General) && (definition.General != definition.Isolated))
					previousCharacter = null;
				else
					previousCharacter = currentCharacter;
			}

			return builder.ToString();
		}
	}
}