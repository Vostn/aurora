﻿using System.Collections.Generic;

namespace Aurora.LocalisationSystem
{
    public static partial class TextShaper
    {
		private struct GlyphShapeDefinition
		{
			public char General { get; }
			public char Isolated { get; }
			public char End { get; }
			public char Middle { get; }
			public char Beginning { get; }

			public GlyphShapeDefinition(char general, char isolated, char end, char middle, char beginning)
			{
				General = general;
				Isolated = isolated;
				End = end;
				Middle = middle;
				Beginning = beginning;
			}
		}

		private struct GlyphCombinationDefinition
		{
			public string CombinationSource { get; }
			public string CombinationResult { get; }

			public GlyphCombinationDefinition (string combinationSource, string combinationResult)
			{
				CombinationSource = combinationSource;
				CombinationResult = combinationResult;
			}
		}

		static TextShaper()
		{
			AddArabicDefinitions();
		}

		private static void AddArabicDefinitions()
		{
			AddDefinition(new GlyphShapeDefinition('\u0622', '\uFE81', '\uFE82', '\uFE81', '\uFE81'));

			AddDefinition(new GlyphShapeDefinition('\u0626', '\uFE89', '\uFE8A', '\uFE8C', '\uFE8B'));
			AddDefinition(new GlyphShapeDefinition('\u0627', '\uFE8D', '\uFE8E', '\uFE8E', '\uFE8D'));
			AddDefinition(new GlyphShapeDefinition('\u0628', '\uFE8F', '\uFE90', '\uFE92', '\uFE91'));
			AddDefinition(new GlyphShapeDefinition('\u0629', '\uFE93', '\uFE94', '\uFE94', '\uFE93'));
			
			AddDefinition(new GlyphShapeDefinition('\u062A', '\uFE95', '\uFE96', '\uFE98', '\uFE97'));
			AddDefinition(new GlyphShapeDefinition('\u062B', '\uFE99', '\uFE9A', '\uFE9C', '\uFE9B'));
			AddDefinition(new GlyphShapeDefinition('\u062C', '\uFE9D', '\uFE9E', '\uFEA0', '\uFE9F'));
			AddDefinition(new GlyphShapeDefinition('\u062D', '\uFEA1', '\uFEA2', '\uFEA4', '\uFEA3'));
			AddDefinition(new GlyphShapeDefinition('\u062E', '\uFEA5', '\uFEA6', '\uFEA8', '\uFEA7'));
			AddDefinition(new GlyphShapeDefinition('\u062F', '\uFEA9', '\uFEAA', '\uFEAA', '\uFEA9'));
			AddDefinition(new GlyphShapeDefinition('\u0630', '\uFEAB', '\uFEAC', '\uFEAC', '\uFEAB'));
			AddDefinition(new GlyphShapeDefinition('\u0631', '\uFEAD', '\uFEAE', '\uFEAE', '\uFEAD'));
			AddDefinition(new GlyphShapeDefinition('\u0632', '\uFEAF', '\uFEB0', '\uFEB0', '\uFEAF'));
			AddDefinition(new GlyphShapeDefinition('\u0633', '\uFEB1', '\uFEB2', '\uFEB4', '\uFEB3'));
			AddDefinition(new GlyphShapeDefinition('\u0634', '\uFEB5', '\uFEB6', '\uFEB8', '\uFEB7'));
			AddDefinition(new GlyphShapeDefinition('\u0635', '\uFEB9', '\uFEBA', '\uFEBC', '\uFEBB'));
			AddDefinition(new GlyphShapeDefinition('\u0636', '\uFEBD', '\uFEBE', '\uFEC0', '\uFEBF'));
			AddDefinition(new GlyphShapeDefinition('\u0637', '\uFEC1', '\uFEC2', '\uFEC4', '\uFEC3'));
			AddDefinition(new GlyphShapeDefinition('\u0638', '\uFEC5', '\uFEC6', '\uFEC8', '\uFEC7'));
			AddDefinition(new GlyphShapeDefinition('\u0639', '\uFEC9', '\uFECA', '\uFECC', '\uFECB'));
			AddDefinition(new GlyphShapeDefinition('\u063A', '\uFECD', '\uFECE', '\uFED0', '\uFECF'));

			AddDefinition(new GlyphShapeDefinition('\u0641', '\uFED1', '\uFED2', '\uFED4', '\uFED3'));
			AddDefinition(new GlyphShapeDefinition('\u0642', '\uFED5', '\uFED6', '\uFED8', '\uFED7'));

			AddDefinition(new GlyphShapeDefinition('\u0644', '\uFEDD', '\uFEDE', '\uFEE0', '\uFEDF'));
			AddDefinition(new GlyphShapeDefinition('\u0645', '\uFEE1', '\uFEE2', '\uFEE4', '\uFEE3'));
			AddDefinition(new GlyphShapeDefinition('\u0646', '\uFEE5', '\uFEE6', '\uFEE8', '\uFEE7'));
			AddDefinition(new GlyphShapeDefinition('\u0647', '\uFEE9', '\uFEEA', '\uFEEC', '\uFEEB'));
			AddDefinition(new GlyphShapeDefinition('\u0648', '\uFEED', '\uFEEE', '\uFEEE', '\uFEED'));
			AddDefinition(new GlyphShapeDefinition('\u0649', '\uFEEF', '\uFEF0', '\uFEF0', '\uFEEF'));

			AddDefinition(new GlyphShapeDefinition('\u064A', '\uFEF1', '\uFEF2', '\uFEF4', '\uFEF3'));

			AddDefinition(new GlyphShapeDefinition('\u064E', '\u064E', '\u064E', '\u064E', '\u064E'));
			AddDefinition(new GlyphShapeDefinition('\u064F', '\u064F', '\u064F', '\u064F', '\u064F'));

			AddDefinition(new GlyphShapeDefinition('\u0651', '\u0651', '\u0651', '\u0651', '\u0651'));

			AddDefinition(new GlyphShapeDefinition('\uFC60', '\uFC60', '\uFC60', '\uFC60', '\uFC60'));

			AddDefinition(new GlyphCombinationDefinition("\u0644\u0625", "\uFEF9"));
			AddDefinition(new GlyphCombinationDefinition("\u0651\u064E", "\uFC60"));
		}

		private static void AddDefinition(GlyphShapeDefinition definition)
		{
			glyphShapeDefinitions.Add(definition.General, definition);
		}

		private static void AddDefinition(GlyphCombinationDefinition definition)
		{
			glyphCombinationDefinitions.Add(definition);
		}

		private readonly static Dictionary<char, GlyphShapeDefinition> glyphShapeDefinitions = new Dictionary<char, GlyphShapeDefinition>();
		private readonly static List<GlyphCombinationDefinition> glyphCombinationDefinitions = new List<GlyphCombinationDefinition>();
    }
}