﻿using System.Collections.Generic;
using Aurora.Diagnostics;
using System.Linq;
using System.Globalization;
using System;
using System.Reflection;

namespace Aurora.LocalisationSystem
{
	public static class Localisation
	{
		private const string localisationErrorString = "==LOCALISATION_ERROR==";

		/// <summary>
		/// All loaded languages, keyed by CultureInfo
		/// </summary>
		private static Dictionary<CultureInfo, Language> languages = new Dictionary<CultureInfo, Language>();

		private static Dictionary<CultureInfo, Language> fallbackLanguages = new Dictionary<CultureInfo, Language>();

		private  static readonly List<CultureInfo> blockedCultures = new List<CultureInfo>();

		public static event Action<CultureInfo> OnActiveCultureChanged;
		public static event Action<Language> OnActiveLanguageChanged;

		public static Language ActiveLanguage { get; private set; }
		public static CultureInfo ActiveCulture { get; private set; }

		public static void SetActiveCulture(CultureInfo newCulture)
		{
			newCulture = GetBestFitCulture(newCulture);

			// If the chosen culture and none of it's fall backs have a configured language, return
			if (!HasLanguage(newCulture))
				return;

			// Don't update anything if the culture hasn't changed.
			if (newCulture == ActiveCulture)
				return;
			
			// Make sure we update our culture.
			ActiveCulture = CultureInfo.CurrentCulture = CultureInfo.CurrentUICulture = CultureInfo.DefaultThreadCurrentCulture = CultureInfo.DefaultThreadCurrentUICulture = newCulture;
			// Alert subscribers to a new culture.
			OnActiveCultureChanged?.Invoke(newCulture);

			// Check the language we will be loading
			Language configuredLanguage = GetLanguage(newCulture);

			// If it is a new language, change it and alert subscribers.
			if (ActiveLanguage != configuredLanguage)
			{
				ActiveLanguage = configuredLanguage;
				OnActiveLanguageChanged?.Invoke(ActiveLanguage);
			}
		}

		public static bool HasLanguage(CultureInfo culture, bool allowFallback = true)
		{
			if (culture == null)
				return NullArgumentError<bool, CultureInfo>(nameof(culture));
			if (allowFallback)
				culture = GetBestFitCulture(culture);
			return languages.ContainsKey(culture) || fallbackLanguages.ContainsKey(culture);
		}

		public static bool TryGetLanguage(CultureInfo culture, out Language language, bool allowFallback = true)
		{
			language = null;

			if (culture == null)
				return NullArgumentError<bool, CultureInfo>(nameof(culture));

			if (allowFallback)
				culture = GetBestFitCulture(culture);
			if (!HasLanguage(culture, allowFallback))
				return false;
			language = GetLanguage(culture, allowFallback);
			return true;
		}

		public static Language GetLanguage(CultureInfo culture, bool allowFallback = true)
		{
			if (culture == null)
				return NullArgumentError<Language, CultureInfo>(nameof(culture));

			if (allowFallback)
				culture = GetBestFitCulture(culture);
			if(languages.ContainsKey(culture))
				return languages[culture];
			return fallbackLanguages[culture];
		}

		public static CultureInfo GetBestFitCulture(CultureInfo culture)
		{
			if (culture == null)
				return NullArgumentError<CultureInfo, CultureInfo>(nameof(culture));

			while (!HasLanguage(culture, false))
			{
				if (culture == culture.Parent)
					break;
				culture = culture.Parent;
			}
			return culture;
		}

		public static bool HasString(string stringKey, CultureInfo culture, bool allowFallback = true)
		{
			if (culture == null)
				return NullArgumentError<bool, CultureInfo>(nameof(culture));
			if (stringKey == null)
				return NullArgumentError<bool, string>(nameof(stringKey));

			if (!HasLanguage(culture, allowFallback))
				return false;
			Language langauge = GetLanguage(culture, allowFallback);
			return langauge.HasString(stringKey);
		}

		public static bool TryGetString(string stringKey, CultureInfo culture, out string langaugeString, bool allowFallback = true)
		{
			langaugeString = localisationErrorString;

			if (culture == null)
				return NullArgumentError<bool, CultureInfo>(nameof(culture));
			if (stringKey == null)
				return NullArgumentError<bool, string>(nameof(stringKey));

			if (!HasLanguage(culture, allowFallback))
				return false;
			Language langauge = GetLanguage(culture, allowFallback);
			if (!langauge.HasString(stringKey))
				return false;
			langaugeString = langauge.GetString(stringKey);
			return true;
		}

		public static string GetString(string stringKey, CultureInfo culture, bool allowFallback = true)
		{
			if (culture == null)
				return NullArgumentError<string, CultureInfo>(nameof(culture), localisationErrorString);
			if (stringKey == null)
				return NullArgumentError<string, string>(nameof(stringKey), localisationErrorString);

			if (!HasLanguage(culture, allowFallback))
				return LanguageNotLoadedError(culture);
			Language langauge = GetLanguage(culture, allowFallback);
			if (!langauge.HasString(stringKey))
				return localisationErrorString;
			return langauge.GetString(stringKey);
		}

		public static string GetString(string stringKey, bool allowFallback = true)
		{
			if (stringKey == null)
				return NullArgumentError<string, string>(nameof(stringKey), localisationErrorString);

			if (!HasLanguage(ActiveCulture, allowFallback))
				return LanguageNotLoadedError(ActiveCulture);
			Language langauge = GetLanguage(ActiveCulture, allowFallback);
			if (!langauge.HasString(stringKey))
				return localisationErrorString;
			return langauge.GetString(stringKey);
		}

		public static string[] GetAllStringKeys()
		{
			HashSet<string> allKeys = new HashSet<string>();
			foreach (Language language in languages.Values)
				foreach (string key in language.GetAllKeys())
					allKeys.Add(key);
			return allKeys.ToArray();
		}

		public static IEnumerable<Language> GetAllLoadedLanguages()
		{
			return languages.Values;
		}

		public static IEnumerable<string> GetAllCultureNamesNative()
		{
			return languages.Keys.Select(x => x.NativeName);
		}

		public static IEnumerable<string> GetAllCultureNamesEnglish()
		{
			return languages.Keys.Select(x => x.EnglishName);
		} 

		public static IEnumerable<CultureInfo> GetAllCultures()
		{
			return languages.Keys;
		} 

		public static void UnloadAllLangauges()
		{
			languages.Clear();
			blockedCultures.Clear();
		}

		public static CultureInfo GetCultureFromCultureCode(string cultureCode)
		{
			CultureInfo cultureInfo = null;
			try
			{
				cultureInfo = new CultureInfo(cultureCode);
			}
			catch
			{
				try
				{
					cultureInfo = new CultureInfo(cultureCode.Substring(0,cultureCode.IndexOf('-')));
				}
				catch(Exception e)
				{
					Debug.LogError($"Exception occurred when trying to creature a culture from culture code of {cultureCode}: {e}");
				}
			}
			return cultureInfo;
		}

		public static void RegisterLanguage(Language newLanguage)
		{
			// Find the common neutral cultures of this language.
			IEnumerable<CultureInfo> commonCultures = newLanguage.AssociatedCultures.CommonCultures();

			commonCultures = commonCultures.Except(blockedCultures);
			if (!commonCultures.Any())
			{
				Debug.LogError($"All of this languages's common cultures are already registered to other languages! Language was not registered! (Cultures: {string.Join(", ",newLanguage.AssociatedCultures)})");
				return;
			}

			CultureInfo largestCommonCulture = commonCultures.First();

			// If an existing language has the same largest common culture, we need to use each languages next largest common culture
			if (languages.ContainsKey(largestCommonCulture))
			{
				// Get the other language
				Language otherLanguage = languages[largestCommonCulture];

				// Get all the common cultures for each language
				List<CultureInfo> thisLangaugeCommonCultures = commonCultures.ToList();
				List<CultureInfo> otherLangaugeCommonCultures = otherLanguage.AssociatedCultures.CommonCultures().Except(blockedCultures).ToList();
				
				// Starting with their common culture
				CultureInfo thisLanguageReplacement = largestCommonCulture;
				CultureInfo otherLangaugeReplacement = largestCommonCulture;

				// If their current common options are the same
				while (otherLangaugeReplacement == thisLanguageReplacement)
				{ 
					// Remove that shared common from their available options
					thisLangaugeCommonCultures.Remove(thisLanguageReplacement);
					otherLangaugeCommonCultures.Remove(otherLangaugeReplacement);

					// Ensure that the shared culture spot is empty
					languages.Remove(thisLanguageReplacement);
					// Add the shared culture to the list of blocked cultures, so that another language cannot attempt to take the now empty place.
					blockedCultures.Add(largestCommonCulture);

					// And then use their next lowest depth option
					thisLanguageReplacement = thisLangaugeCommonCultures.First();
					otherLangaugeReplacement = otherLangaugeCommonCultures.First();
				}

				// Place both languages in their new place
				languages.Add(otherLangaugeReplacement, otherLanguage);
				languages.Add(thisLanguageReplacement, newLanguage);
			}
			// Otherwise just place the language using its common culture code.
			else
			{
				languages.Add(largestCommonCulture, newLanguage);
			}

			// If this language file contains the default culture for this language, add it to the location of the language's neutral culture as a fall back.
			if (newLanguage.AssociatedCultures.ContainsLangaugeDefault())
			{
				CultureInfo langaugeDefault = newLanguage.AssociatedCultures.LangaugeDefault();
				if (!fallbackLanguages.ContainsKey(langaugeDefault))
					fallbackLanguages.Add(langaugeDefault, newLanguage);
			}

			// If this language is en-US, add it to the invariant culture position as a fall back.
			if(newLanguage.AssociatedCultures.Contains(new CultureInfo("en-US")))
			{
				if (fallbackLanguages.ContainsKey(CultureInfo.InvariantCulture))
					fallbackLanguages.Remove(CultureInfo.InvariantCulture);
				fallbackLanguages.Add(CultureInfo.InvariantCulture, newLanguage);
			}
		}

		public static string LanguageNotLoadedError(CultureInfo culture)
		{
			Debug.LogError($"Language for culture '{culture.Name}' has not been loaded!");
			return localisationErrorString;
		}

		public static string StringNotInLanguageError(string stringKey, Language language)
		{
			Debug.LogError($"Language string key '{stringKey}' doesn't exist in this language! ({language.AssociatedCultures.First().Name})");
			return localisationErrorString;
		}

		private static T NullArgumentError<T, U>(string argumentName, T returnValue = default)
		{
			Debug.LogError($"Argument \"{argumentName}\" of type \"{typeof(U)}\" was null!");
			return returnValue;
		}
	}
}