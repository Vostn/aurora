﻿namespace Aurora
{
	public enum NullState
	{
		NotNull,
		FakeNull,
		TrueNull,
	}
}