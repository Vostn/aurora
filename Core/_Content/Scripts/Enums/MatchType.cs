﻿namespace Aurora
{
	public enum MatchType
	{
		/// <summary>
		/// Value must contain the comparison flag.
		/// </summary>
		HasFlag,

		/// <summary>
		/// Value must not contain the comparison flag.
		/// </summary>
		NotHasFlag,

		/// <summary>
		/// Value must be exactly the comparison value.
		/// </summary>
		Equal,

		/// <summary>
		/// Value must not match the comparison value.
		/// </summary>
		NotEqual,

		/// <summary>
		/// Value must be greater than the comparison value.
		/// </summary>
		Greater,

		/// <summary>
		/// Value must be greater than or equal to the comparison value.
		/// </summary>
		GreaterOrEqual,

		/// <summary>
		/// Value must be less than the comparison value.
		/// </summary>
		Less,

		/// <summary>
		/// Value must be less than or equal to the comparison value.
		/// </summary>
		LessOrEqual,
	}
}