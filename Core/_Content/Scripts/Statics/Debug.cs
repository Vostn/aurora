﻿namespace Aurora.Diagnostics
{
	public static class Debug
	{
		public static void Log(object message)
		{
#if UNITY_2017_1_OR_NEWER
			UnityEngine.Debug.Log(message);
#else
			ConsoleSystem.Console.PrintText(message);
			System.Diagnostics.Trace.TraceInformation(message.ToStringNullAware());
#endif
		}

#if UNITY_2017_1_OR_NEWER

		public static void Log(object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.Log(message, context);
		}

#endif

		public static void LogError(object message)
		{
#if UNITY_2017_1_OR_NEWER
			UnityEngine.Debug.LogError(message);
#else
			ConsoleSystem.Console.PrintError(message);
			System.Diagnostics.Trace.TraceError(message.ToStringNullAware());
#endif
		}

#if UNITY_2017_1_OR_NEWER

		public static void LogError(object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.LogError(message, context);
		}

#endif

		public static void LogException(System.Exception exception)
		{
#if UNITY_2017_1_OR_NEWER
			UnityEngine.Debug.LogException(exception);
#else
			string message = $"{exception.ToString()} - {exception.Message}";
			ConsoleSystem.Console.PrintError(message);
			System.Diagnostics.Trace.TraceError(message);
#endif
		}

#if UNITY_2017_1_OR_NEWER

		public static void LogException(System.Exception exception, UnityEngine.Object context)
		{
			UnityEngine.Debug.LogException(exception, context);
		}

#endif

		public static void LogWarning(object message)
		{
#if UNITY_2017_1_OR_NEWER
			UnityEngine.Debug.LogWarning(message);
#else
			ConsoleSystem.Console.PrintError(message);
			System.Diagnostics.Trace.TraceWarning(message.ToStringNullAware());
#endif
		}

#if UNITY_2017_1_OR_NEWER

		public static void LogWarning(object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.LogWarning(message, context);
		}

#endif

		public static void Assert(bool condition, object message)
		{
#if UNITY_2017_1_OR_NEWER
			UnityEngine.Debug.Assert(condition, message);
#else
			if(!condition)
				ConsoleSystem.Console.PrintError(message);
			System.Diagnostics.Debug.Assert(condition, message.ToStringNullAware());
#endif
		}

#if UNITY_2017_1_OR_NEWER

		public static void Assert(bool condition, object message, UnityEngine.Object context)
		{
			UnityEngine.Debug.Assert(condition, message, context);
		}

#endif
	}
}