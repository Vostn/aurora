﻿using Aurora.Collections;

namespace Aurora
{
	public static class AverageSpline<T> where T : struct, IVector<T>
	{
		public class Traveller : ITraveller
		{
			public Traveller()
			{
				(this as ITraveller).PositionIndex = 1;
				(this as ITraveller).TravelledKeyframeDistance = 0;
			}

			int ITraveller.PositionIndex { get; set; }
			double ITraveller.TravelledKeyframeDistance { get; set; }
		}

		private interface ITraveller
		{
			int PositionIndex { get; set; }
			double TravelledKeyframeDistance { get; set; }
		}

		public static T SamplePosition(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped)
		{
			Traveller traveller = new Traveller();
			return SamplePosition(splinePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
		}

		public static T SamplePosition(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped, ref Traveller traveller)
		{
			ITraveller castTraveller = traveller as ITraveller;
			return SamplePosition(splinePath, averagingRange, sampleLocationOnSpline, looped, ref castTraveller);
		}

		private static T SamplePosition(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped, ref ITraveller traveller)
		{
			if (splinePath == null)
				throw new System.ArgumentNullException($"{nameof(splinePath)} argument is null!");
			if (splinePath.NodeCount == 0)
				throw new System.ArgumentException($"{nameof(splinePath)} argument is empty!");
			if (splinePath.TotalLength == 0)
				return splinePath[0];
			if (splinePath.NodeCount == 1)
				return splinePath[0];

			T pointSum = default(T);

			System.Func<int, int, int, int> indexFunc;
			if (looped)
				indexFunc = Math.Repeat;
			else
				indexFunc = Math.Clamp;

			double travelledKeyframeDistance = traveller.TravelledKeyframeDistance;
			double travelledSampleDistance = 0;
			double currentSampleLocation = sampleLocationOnSpline;
			
			double averagingDistance = averagingRange * 0.5;

			int maxNodeIndex = splinePath.NodeCount - 1;

			int positionIndex = traveller.PositionIndex;
			int previousPointIndex = Math.Repeat(positionIndex - 1, 0, maxNodeIndex);
			double keyframeGap = splinePath.DistanceToNextNode(previousPointIndex);

			void MoveTravellerToSampleLocation()
			{
				while (currentSampleLocation < travelledKeyframeDistance)
				{
					positionIndex = positionIndex - 1;
					previousPointIndex = Math.Repeat(positionIndex - 1, 0, maxNodeIndex);
					keyframeGap = splinePath.DistanceToNextNode(previousPointIndex);
					travelledKeyframeDistance -= keyframeGap;
				}

				while (currentSampleLocation >= travelledKeyframeDistance + keyframeGap)
				{
					travelledKeyframeDistance += keyframeGap;
					positionIndex = positionIndex + 1;
					previousPointIndex = Math.Repeat(positionIndex - 1, 0, maxNodeIndex);
					keyframeGap = splinePath.DistanceToNextNode(previousPointIndex);
				}
			}

			T samplePoint;

			while (travelledSampleDistance < averagingRange || averagingRange == 0)
			{
				MoveTravellerToSampleLocation();

				double sampleWeight = 1;
				double mergedSampleLocation;
				double remainingDistanceInKeyframe = (travelledKeyframeDistance + keyframeGap) - currentSampleLocation;
				if (remainingDistanceInKeyframe + travelledSampleDistance < averagingRange)
				{
					mergedSampleLocation = currentSampleLocation + (remainingDistanceInKeyframe * 0.5);
					travelledSampleDistance += remainingDistanceInKeyframe;
					currentSampleLocation += remainingDistanceInKeyframe;
					sampleWeight = remainingDistanceInKeyframe;
				}
				else
				{
					double remainingSampleRange = averagingRange - travelledSampleDistance;
					mergedSampleLocation = currentSampleLocation + (remainingSampleRange * 0.5);
					currentSampleLocation = averagingDistance;
					travelledSampleDistance = averagingRange;
					if (averagingRange != 0)
						sampleWeight = remainingSampleRange;
				}

				T nextPointLocation = splinePath[indexFunc(positionIndex, 0, maxNodeIndex)];
				T previousPointLocation = splinePath[indexFunc(positionIndex - 1, 0, maxNodeIndex)];
				samplePoint = previousPointLocation.Interpolant(nextPointLocation, (mergedSampleLocation - travelledKeyframeDistance) / keyframeGap);

				pointSum.Add(samplePoint.Product(sampleWeight));
				if (averagingRange == 0)
					break;
			}
			traveller.PositionIndex = positionIndex;
			traveller.TravelledKeyframeDistance = travelledKeyframeDistance;
			return pointSum.Quotient(averagingRange != 0 ? averagingRange : 1);
		}

		public static T SampleTangent(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped)
		{
			Traveller traveller = new Traveller();
			return SampleTangent(splinePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
		}

		public static T SampleTangent(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped, ref Traveller traveller)
		{
			ITraveller castTraveller = traveller as ITraveller;
			return SampleTangent(splinePath, averagingRange, sampleLocationOnSpline, looped, ref castTraveller);
		}

		private static T SampleTangent(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped, ref ITraveller traveller)
		{
			double difference = 0.00001;
			T firstPosition = SamplePosition(splinePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
			T secondPosition = SamplePosition(splinePath, averagingRange, sampleLocationOnSpline + difference, looped, ref traveller);
			return firstPosition.Difference(secondPosition).Normalised;
		}

		public static Tangency<T> SampleTangency(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped)
		{
			Traveller traveller = new Traveller();
			return SampleTangency(splinePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
		}

		public static Tangency<T> SampleTangency(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped, ref Traveller traveller)
		{
			ITraveller castTraveller = traveller as ITraveller;
			return SampleTangency(splinePath, averagingRange, sampleLocationOnSpline, looped, ref castTraveller);
		}

		private static Tangency<T> SampleTangency(IReadOnlyPath<T> splinePath, double averagingRange, double sampleLocationOnSpline, bool looped, ref ITraveller traveller)
		{
			double difference = 0.00001;
			T firstPosition = SamplePosition(splinePath, averagingRange, sampleLocationOnSpline, looped, ref traveller);
			T secondPosition = SamplePosition(splinePath, averagingRange, sampleLocationOnSpline + difference, looped, ref traveller);
			T tangent = firstPosition.Difference(secondPosition).Normalised;
			return new Tangency<T>(firstPosition, tangent);
		}

		public static ListPath<T> Integrate(IReadOnlyPath<T> splinePath, double averagingRange, double nodeAddAngle, bool looped)
		{
			if (splinePath == null)
				throw new System.ArgumentNullException($"{nameof(splinePath)} argument is null!");

			ListPath<T> path = new ListPath<T>();

			if (splinePath.NodeCount < 2)
			{
				if (splinePath.NodeCount == 1)
					path.AddNode(splinePath[0]);
				return path;
			}
			else
			{
				double sampleLocationOnSpline = 0;
				double splineLength = splinePath.TotalLength;
				if (looped)
					splineLength = splinePath.TotalLengthLooped;
				while (sampleLocationOnSpline < splineLength)
				{
					T newNode = SamplePosition(splinePath, averagingRange, sampleLocationOnSpline, looped);
					if (path[path.NodeCount - 1].Difference(path[path.NodeCount - 2]).Angle(newNode.Difference(path[path.NodeCount - 1])) > nodeAddAngle)
						path.AddNode(newNode);
					sampleLocationOnSpline += 0.01f;
				}
				return path;
			}
		}
	}
}