﻿using System.Collections.Generic;

namespace Aurora
{
	public static class ObjectPool<T, U> where T : IPooledObject<U>, new()
	{
		private static Stack<T> pool = new Stack<T>();

		public static T Withdraw(U arg)
		{
			if (pool.Count <= 0)
				pool.Push(new T());
			T withdrawTarget = pool.Pop();
			withdrawTarget.OnWithdraw(arg);
			return withdrawTarget;
		}

		public static void Deposit(T target)
		{
			pool.Push(target);
			target.OnDeposit();
		}
	}

	public static class ObjectPool<T> where T : IPooledObject, new()
	{
		private static Stack<T> pool = new Stack<T>();

		public static T Withdraw()
		{
			if (pool.Count <= 0)
				pool.Push(new T());
			T withdrawTarget = pool.Pop();
			withdrawTarget.OnWithdraw();
			return withdrawTarget;
		}

		public static void Deposit(T target)
		{
			pool.Push(target);
			target.OnDeposit();
		}
	}
}