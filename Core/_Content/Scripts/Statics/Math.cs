﻿namespace Aurora
{
	public static partial class Math
	{
		public const double PI = 3.141592653589793238462643383279502884197169399375105820974944;
		public const double PI2 = 6.283185307179586476925286766559005768394338798750211641949889;

		public const double RadToDeg = 57.295779513082320876798154814105;
		public const double DegToRad = 0.01745329251994329576923690768489;

		public const double MilliTauToDeg = 0.36f;
		public const double DegToMilliTau = 2.777777777777777777777777777777777777777777777777777777777778;

		public const double MilliTauToRad = 0.006283185307179586476925286766559005768394338798750211641949;
		public const double RadToMilliTau = 159.1549430918953357688837633725143620344596457404564487476673;

		public const double TauToDeg = 360.0f;
		public const double DegToTau = 0.0027777777777777777777777777777777777777777777777777777777778;

		public const double TauToRad = 6.283185307179586476925286766559005768394338798750211641949889;
		public const double RadToTau = 0.1591549430918953357688837633725143620344596457404564487476673;

		public static byte Clamp(byte s, byte min, byte max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static sbyte Clamp(sbyte s, sbyte min, sbyte max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static short Clamp(short s, short min, short max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static ushort Clamp(ushort s, ushort min, ushort max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static int Clamp(int s, int min, int max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static uint Clamp(uint s, uint min, uint max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}
		
		public static double Clamp(double s, double min, double max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static long Clamp(long s, long min, long max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static ulong Clamp(ulong s, ulong min, ulong max)
		{
			if (s < min)
				return min;
			if (s > max)
				return max;
			return s;
		}

		public static byte Repeat(byte s, byte min, byte max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			byte range = (byte)((max - min) + 1);
			return (byte)((((s - min) % range) + range) % range + min);
		}

		public static sbyte Repeat(sbyte s, sbyte min, sbyte max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			sbyte range = (sbyte)((max - min) + 1);
			return (sbyte)((((s - min) % range) + range) % range + min);
		}

		public static short Repeat(short s, short min, short max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			short range = (short)((max - min) + 1);
			return (short)((((s - min) % range) + range) % range + min);
		}

		public static ushort Repeat(ushort s, ushort min, ushort max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			ushort range = (ushort)((max - min) + 1);
			return (ushort)((((s - min) % range) + range) % range + min);
		}

		public static int Repeat(int s, int min, int max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			int range = ((max - min) + 1);
			return (((s - min) % range) + range) % range + min;
		}

		public static uint Repeat(uint s, uint min, uint max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			uint range = ((max - min) + 1);
			return (((s - min) % range) + range) % range + min;
		}

		public static double Repeat(double s, double min, double max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			double range = max - min;
			return (((s - min) % range) + range) % range + min;
		}

		public static long Repeat(long s, long min, long max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			long range = ((max - min) + 1);
			return (((s - min) % range) + range) % range + min;
		}

		public static ulong Repeat(ulong s, ulong min, ulong max)
		{
			if (min > max)
				SwapValues(ref min, ref max);
			ulong range = ((max - min) + 1);
			return (((s - min) % range) + range) % range + min;
		}

		public static int FloorToInt(double value)
		{
			if (value > 0)
				return (int)value;
			else if (value < 0)
				return ((int)value) - 1;
			else
				return 0;
		}

		public static int CeilToInt(double value)
		{
			if (value < 0)
				return (int)value;
			else if (value > 0)
				return ((int)value) + 1;
			else
				return 0;
		}

		public static int RoundToInt(double value)
		{
			if (value < 0)
				return (int)(value - 0.5);
			else if (value > 0)
				return (int)(value + 0.5);
			else
				return 0;
		}

		public static void SwapValues<T>(ref T minimum, ref T maximum)
		{
			T oldMinimum = minimum;
			minimum = maximum;
			maximum = oldMinimum;
		}

		public static double Lerp(double a, double b, double t)
		{
			return a + ((b - a) * t);
		}

		public static double Max(params double[] values)
		{
			double max = values[0];
			for (int i = 1; i < values.Length; ++i)
				max = System.Math.Max(max, values[i]);
			return max;
		}

		public static double Min(params double[] values)
		{
			double min = values[0];
			for (int i = 1; i < values.Length; ++i)
				min = System.Math.Min(values[i], min);
			return min;
		}

		public static Fixed Sin(Fixed radians)
		{
			radians = Fixed.Wrap(radians, 0, Fixed.PI2);
			int segmentCount = sineValues.Length;
			Fixed segment = ((radians * segmentCount) / (Fixed.PI2));
			int lower = (int)segment;
			int upper = lower + 1;
			if (upper == segmentCount)
				upper = 0;
			Fixed lerpValue = segment - lower;
			Fixed lowerValue = Fixed.FromRawValue(sineValues[lower]);
			Fixed upperValue = Fixed.FromRawValue(sineValues[upper]);
			Fixed finalValue = Fixed.Lerp(lowerValue, upperValue, lerpValue);
			return finalValue;
		}

		public static Fixed Cos(Fixed radians)
		{
			return Sin(radians + (Fixed.PI / 2));
		}

		public static Fixed Acos(Fixed value)
		{
			value = Fixed.Clamp(value, -1, 1);
			int segmentCount = acosValues.Length;
			Fixed segment = ((value + 1) / 2) * segmentCount;
			int lower = (int)segment;
			int upper = lower + 1;
			Fixed lerpValue = segment - lower;
			Fixed lowerValue = Fixed.FromRawValue(acosValues[lower]);
			Fixed upperValue = 0;
			if (upper <= segmentCount)
				upperValue = Fixed.FromRawValue(acosValues[upper]);
			Fixed finalValue = Fixed.Lerp(lowerValue, upperValue, lerpValue);
			return finalValue;
		}
	}
}