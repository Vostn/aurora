﻿using Aurora.Collections;
using Aurora.TimeSystem;

namespace Aurora
{
	public static class Scheduler
	{
		private static Heap<double, ScheduledAction> heap = new Heap<double, ScheduledAction>();

#if UNITY_2017_1_OR_NEWER

		[UnityEngine.RuntimeInitializeOnLoadMethod(UnityEngine.RuntimeInitializeLoadType.BeforeSceneLoad)]
		private static void UnityInitialise()
		{
			RealTimeTracker.RealTimeTransform.OnIncrementStep += Update;
		}

		private static void Update()
		{
			DequeueActions();
		}

#endif

		public static void Schedule(Hourglass hourglass, System.Action action)
		{
			ScheduledAction scheduledAction = new ScheduledAction(hourglass, action);
			Insert(scheduledAction);
		}

		private static void Insert(ScheduledAction scheduledAction)
		{
			double realTimeHourglassEnd = RealTimeTracker.RealTimeTransform.Time + ((scheduledAction.Hourglass.ExpiryTime - scheduledAction.Hourglass.TimeTransform.Time) / scheduledAction.Hourglass.TimeTransform.TimeScale);
			heap.Push(realTimeHourglassEnd, scheduledAction);
		}

		public static void DequeueActions()
		{
			while (heap.Count > 0 && heap.Peek().Key < RealTimeTracker.RealTimeTransform.Time)
			{
				ScheduledAction scheduledAction = heap.Pop().Element;
				if (!scheduledAction.Valid)
					continue;
				if (scheduledAction.Hourglass.Expired)
				{
					scheduledAction.Valid = false;
					scheduledAction.Action.Invoke();
				}
				else
					Insert(scheduledAction);
			}
		}

		private struct ScheduledAction
		{
			public Hourglass Hourglass { get; private set; }
			public System.Action Action { get; private set; }
			public bool Valid { get; set; }

			public ScheduledAction(Hourglass hourglass, System.Action action)
			{
				Hourglass = hourglass;
				Action = action;
				Valid = true;
				hourglass.OnChanged += TriggerChangeEvent;
			}

			private void TriggerChangeEvent()
			{
				Valid = false;
				Schedule(Hourglass, Action);
			}
		}
	}
}