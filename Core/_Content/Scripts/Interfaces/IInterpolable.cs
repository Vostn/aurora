﻿namespace Aurora
{
	/// <summary>
	/// Indicates that a data type can be interpolated.
	/// </summary>
	/// <typeparam name="T">The type of the implementer.</typeparam>
	public interface IInterpolable<T> where T: struct, IInterpolable<T>
	{
		T Interpolant(T o2, double t);
	}
}