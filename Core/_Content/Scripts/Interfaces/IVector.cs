﻿namespace Aurora
{
	/// <summary>
	/// Indicates that this type is a vector, and fulfills the common operators of vectors.
	/// </summary>
	/// <typeparam name="T">The type of the implementor.</typeparam>
	public interface IVector<T> : IArithmetic<T>, ILerpable<T>, IAnglable<T> where T : struct, IVector<T>
	{
		void Multiply(double v);

		void Divide(double v);

		T Product(double v);

		T Quotient(double v);

		T Value { get; }

		double Magnitude { get; }

		T Normalised { get; }
	}
}