﻿namespace Aurora
{
	/// <summary>
	/// Indicates that this type can have a geometric angle between itself and other instance of this type.
	/// </summary>
	/// <typeparam name="T">The type of the implementer.</typeparam>
	public interface IAnglable<T>
	{
		double Angle(T o);
	}
}