﻿namespace Aurora
{
	/// <summary>
	/// Indicates that this type can perform common mathematical operations.
	/// </summary>
	/// <typeparam name="T">The type of the implementor.</typeparam>
	public interface IArithmetic<T> where T : struct
	{
		void Add(T v);

		void Subtract(T v);

		void Multiply(T v);

		void Divide(T v);

		T Sum(T v);

		T Difference(T v);

		T Product(T v);

		T Quotient(T v);

		T Negative { get; }
	}
}