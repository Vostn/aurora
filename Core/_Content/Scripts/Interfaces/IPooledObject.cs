﻿namespace Aurora
{
	public interface IPooledObject<T>
	{
		void OnDeposit();

		void OnWithdraw(T arg);
	}

	public interface IPooledObject
	{
		void OnDeposit();

		void OnWithdraw();
	}
}