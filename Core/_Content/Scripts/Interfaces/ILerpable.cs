﻿namespace Aurora
{
	/// <summary>
	/// Indicates that this type can be linearly interpolated.
	/// </summary>
	/// <typeparam name="T">The type of the implementer.</typeparam>
	public interface ILerpable<T> : IInterpolable<T> where T : struct, ILerpable<T>, IInterpolable<T>
	{
		T Lerpolant(T o2, double t);
	}
}