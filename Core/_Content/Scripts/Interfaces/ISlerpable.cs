﻿namespace Aurora
{
	/// <summary>
	/// Indicates that this type can be spherically interpolated.
	/// </summary>
	/// <typeparam name="T">The type of the implementer.</typeparam>
	public interface ISlerpable<T> where T: struct, ISlerpable<T>
	{
		T Slerpolant(T o2, double t);
	}
}