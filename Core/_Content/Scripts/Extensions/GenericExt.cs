﻿using System.ComponentModel;

namespace Aurora
{
	public static class GenericExt
	{
		/// <summary>
		/// Parse a value from a string. NOTE: Only works if the generic type implements a TypeConverter!
		/// </summary>
		/// <typeparam name="T">Type to parse the string to.</typeparam>
		/// <param name="stringData">String to parse.</param>
		/// <returns>Returns the parsed string.</returns>
		public static T Parse<T>(this string stringData)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
			if (converter == null)
				throw new System.NotSupportedException($"No converter defined for type of \"{typeof(T)}\"!");
			return (T)converter.ConvertFromString(stringData);
		}

		/// <summary>
		/// Attempt to parse a value from a string. NOTE: Only works if the generic type implements a TypeConverter!
		/// </summary>
		/// <typeparam name="T">Type to parse the string to.</typeparam>
		/// <param name="stringData">String to attempt to parse.</param>
		/// <param name="target">Instance to output the parsed value into.</param>
		/// <returns>Whether the parse succeeded or not.</returns>
		public static bool TryParse<T>(this string stringData, out T target)
		{
			TypeConverter converter = TypeDescriptor.GetConverter(typeof(T));
			if (converter == null)
			{
				target = default(T);
				return false;
			}
			try
			{
				object value = converter.ConvertFromString(stringData);
				target = (T)value;
				return true;
			}
			catch
			{
				target = default(T);
				return false;
			}
		}
	}
}