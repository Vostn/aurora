﻿using System;
using System.Collections.Generic;

namespace Aurora
{
	public static class IEnumerableExt
	{
		public static void ForEach<T>(this IEnumerable<T> collection, Action<T> method)
		{
			foreach (var item in collection)
				method?.Invoke(item);
		}
	}
}