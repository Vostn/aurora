﻿using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Aurora
{
	public static class StringExt
	{
		static readonly Dictionary<string, string> bracketPairs = new Dictionary<string, string> 
		{ 
			{ "[", "]" },
			{ "]", "[" },
			{ ")", "(" },
			{ "(", ")" },
		};

		/// <summary>
		/// Get all grapheme clusters of a string.
		/// </summary>
		/// <param name="s">The string to get grapheme clusters from.</param>
		/// <returns>Returns an enumerable of all graphemes in the string.</returns>
		public static IEnumerable<string> Graphemes(this string s)
		{
			var enumerator = StringInfo.GetTextElementEnumerator(s);
			while (enumerator.MoveNext())
				yield return (string)enumerator.Current;
		}

		/// <summary>
		/// Reverses the given string, accounting for high-unicode characters and brackets.
		/// </summary>
		/// <param name="s">The string to reverse.</param>
		/// <returns>Returns the reversed string.</returns>
		public static string ReverseDirection(this string s)
		{
			s = string.Join("", s.Graphemes().Reverse());
			s = s.InvertGroupingSymbols();
			return s;
		}

		/// <summary>
		/// Inverts all grouping symbols within a string.
		/// </summary>
		/// <param name="s">The string for which to invert grouping symbols.</param>
		/// <returns>Returns the given string with inverted grouping symbols.</returns>
		public static string InvertGroupingSymbols(this string s)
		{
			StringBuilder sb = new StringBuilder(s.Length);
			foreach (string grapheme in s.Graphemes())
			{
				string newGrapheme;
				bool contains = bracketPairs.TryGetValue(grapheme, out newGrapheme);
				sb.Append(contains ? newGrapheme : grapheme);
			}
			return sb.ToString();
		}
	}
}
