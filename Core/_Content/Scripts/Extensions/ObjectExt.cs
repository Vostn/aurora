﻿namespace Aurora
{
	public static class ObjectExt
	{
		/// <summary>
		/// This method functions regardless of the null state of the object it is called on, as it is a null checking extension method.
		/// </summary>
		/// <param name="target">The object to check the NullState for.</param>
		/// <returns>The NullState of the targeted object.</returns>
		public static NullState GetNullState(this object target)
		{
			bool? nullState = target?.Equals(null);
			if (nullState == null)
				return NullState.TrueNull;
			else if (nullState == true || target == null)
				return NullState.FakeNull;
			return NullState.NotNull;
		}
		
		/// <summary>
		/// Identical to ToString(), but returns "Null" if the object is null.
		/// </summary>
		/// <param name="target"></param>
		/// <returns></returns>
		public static string ToStringNullAware(this object target)
		{
			if(target == null)
				return "Null";
			return target.ToString();
		}
	}
}