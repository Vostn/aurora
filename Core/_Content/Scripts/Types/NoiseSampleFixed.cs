﻿namespace Aurora
{
	public struct NoiseSampleFixed
	{
		public Fixed value;
		public Fixed3 derivative;

		public static NoiseSampleFixed operator +(NoiseSampleFixed a, Fixed b)
		{
			a.value += b;
			return a;
		}

		public static NoiseSampleFixed operator +(Fixed a, NoiseSampleFixed b)
		{
			b.value += a;
			return b;
		}

		public static NoiseSampleFixed operator +(NoiseSampleFixed a, NoiseSampleFixed b)
		{
			a.value += b.value;
			a.derivative += b.derivative;
			return a;
		}

		public static NoiseSampleFixed operator -(NoiseSampleFixed a, Fixed b)
		{
			a.value -= b;
			return a;
		}

		public static NoiseSampleFixed operator -(Fixed a, NoiseSampleFixed b)
		{
			b.value = a - b.value;
			b.derivative = -b.derivative;
			return b;
		}

		public static NoiseSampleFixed operator -(NoiseSampleFixed a, NoiseSampleFixed b)
		{
			a.value -= b.value;
			a.derivative -= b.derivative;
			return a;
		}

		public static NoiseSampleFixed operator *(NoiseSampleFixed a, Fixed b)
		{
			a.value *= b;
			a.derivative *= b;
			return a;
		}

		public static NoiseSampleFixed operator /(NoiseSampleFixed a, Fixed b)
		{
			a.value /= b;
			a.derivative /= b;
			return a;
		}

		public static NoiseSampleFixed operator *(Fixed a, NoiseSampleFixed b)
		{
			b.value *= a;
			b.derivative *= a;
			return b;
		}

		public static NoiseSampleFixed operator *(NoiseSampleFixed a, NoiseSampleFixed b)
		{
			a.derivative = a.derivative * b.value + b.derivative * a.value;
			a.value *= b.value;
			return a;
		}
	}
}