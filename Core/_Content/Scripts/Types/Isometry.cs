﻿namespace Aurora
{
	public struct Isometry : IInterpolable<Isometry>, IAnglable<Isometry>
	{
		public Double3 Position { get; set; }
		public Quaternion Rotation { get; set; }

		public Isometry(Double3 position, Quaternion rotation)
		{
			Position = position;
			Rotation = rotation;
		}

		public Isometry(Double3 position)
		{
			Position = position;
			Rotation = Quaternion.identity;
		}

		public Isometry(Quaternion rotation)
		{
			Position = Double3.Uniform(0);
			Rotation = rotation;
		}

		public static double Angle(Isometry a, Isometry b)
		{
			return Quaternion.Angle(a.Rotation, b.Rotation);
		}

		double IAnglable<Isometry>.Angle(Isometry other)
		{
			return Angle(this, other);
		}

		public static Isometry Interpolate(Isometry a, Isometry b, double t)
		{
			Double3 position = Double3.Lerp(a.Position, b.Position, t);
			Quaternion rotation = Quaternion.Slerp(a.Rotation, b.Rotation, t);
			return new Isometry(position, rotation);
		}

		Isometry IInterpolable<Isometry>.Interpolant(Isometry o2, double t)
		{
			return Interpolate(this, o2, t);
		}
	}
}