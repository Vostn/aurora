﻿namespace Aurora
{
	public struct Tangency<T> where T : struct, IVector<T>
	{
		public T Position { get; private set; }
		public T Tangent { get; private set; }

		public Tangency(T position, T tangent)
		{
			Position = position;
			Tangent = tangent;
		}
	}
}
