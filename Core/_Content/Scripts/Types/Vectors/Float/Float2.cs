﻿namespace Aurora
{
	[System.Serializable]
	public struct Float2
	{
		public float x, y;

		public Float2(Float2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Float2(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Float2 v = (Float2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Float2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Float2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(Float2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Float2(UnityEngine.Vector4 v)
		{
			return new Float2(v.x, v.y);
		}

		public static explicit operator Float2(UnityEngine.Vector3 v)
		{
			return new Float2(v.x, v.y);
		}

		public static implicit operator Float2(UnityEngine.Vector2 v)
		{
			return new Float2(v.x, v.y);
		}

		public static implicit operator UnityEngine.Color(Float2 v)
		{
			return new UnityEngine.Color(v.x, v.y, 0);
		}

		public static explicit operator Float2(UnityEngine.Color c)
		{
			return new Float2(c.r, c.g);
		}

#endif

		public static Float2 operator +(Float2 v1, Float2 v2)
		{
			return new Float2(v1.x + v2.x, v1.y + v2.y);
		}

		public static Float2 operator -(Float2 v1, Float2 v2)
		{
			return new Float2(v1.x - v2.x, v1.y - v2.y);
		}

		public static Float2 operator *(Float2 v1, Float2 v2)
		{
			return new Float2(v1.x * v2.x, v1.y * v2.y);
		}

		public static Float2 operator /(Float2 v1, Float2 v2)
		{
			return new Float2(v1.x / v2.x, v1.y / v2.y);
		}

		public static Float2 operator *(Float2 v1, double s)
		{
			return new Float2((float)(v1.x * s), (float)(v1.y * s));
		}

		public static Float2 operator /(Float2 v1, double s)
		{
			return new Float2((float)(v1.x / s), (float)(v1.y / s));
		}

		public static Float2 Uniform(float s)
		{
			return new Float2(s, s);
		}

		public float this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Float2 Pow(Float2 v, double p)
		{
			return new Float2((float)System.Math.Pow(v.x, p), (float)System.Math.Pow(v.y, p));
		}

		public static Float2 Clamp(Float2 v, float min, float max)
		{
			return new Float2((float)Math.Clamp(v.x, min, max), (float)Math.Clamp(v.y, min, max));
		}

		public static Float2 Clamp(Float2 v, Float2 min, Float2 max)
		{
			return new Float2((float)Math.Clamp(v.x, min.x, max.x), (float)Math.Clamp(v.y, min.y, max.y));
		}

		public static Float2 Repeat(Float2 v, float min, float max)
		{
			return new Float2((float)Math.Repeat(v.x, min, max), (float)Math.Repeat(v.y, min, max));
		}

		public static Float2 Repeat(Float2 v, Float2 min, Float2 max)
		{
			return new Float2((float)Math.Repeat(v.x, min.x, max.x), (float)Math.Repeat(v.y, min.y, max.y));
		}

		public static Float2 Min(Float2 v1, Float2 v2)
		{
			return new Float2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static Float2 Max(Float2 v1, Float2 v2)
		{
			return new Float2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public float MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public float MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public float Magnitude
		{
			get { return (float)System.Math.Sqrt(SqrMagnitude); }
		}

		public float SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(Float2 v1, Float2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Float2 v1, Float2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Float2 left = new Float2(-1, 0);
		public static readonly Float2 right = new Float2(1, 0);
		public static readonly Float2 up = new Float2(0, 1);
		public static readonly Float2 down = new Float2(0, -1);
	}
}