﻿namespace Aurora
{
	[System.Serializable]
	public struct Float4
	{
		public float x, y, z, w;

		public Float4(Float4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Float4(float x, float y, float z = 0, float w = 0)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Float4 v = (Float4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Float4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(Float4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Float4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static implicit operator Float4(UnityEngine.Vector4 v)
		{
			return new Float4(v.x, v.y, v.z, v.w);
		}

		public static implicit operator Float4(UnityEngine.Vector3 v)
		{
			return new Float4(v.x, v.y, v.z, 0);
		}

		public static implicit operator Float4(UnityEngine.Vector2 v)
		{
			return new Float4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Color(Float4 v)
		{
			return new UnityEngine.Color(v.x, v.y, v.z, v.w);
		}

		public static implicit operator Float4(UnityEngine.Color c)
		{
			return new Float4(c.r, c.g, c.b, c.a);
		}

#endif

		public static Float4 operator +(Float4 v1, Float4 v2)
		{
			return new Float4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static Float4 operator -(Float4 v1, Float4 v2)
		{
			return new Float4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static Float4 operator *(Float4 v1, Float4 v2)
		{
			return new Float4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static Float4 operator /(Float4 v1, Float4 v2)
		{
			return new Float4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static Float4 operator *(Float4 v1, double s)
		{
			return new Float4((float)(v1.x * s), (float)(v1.y * s), (float)(v1.z * s), (float)(v1.w * s));
		}

		public static Float4 operator /(Float4 v1, double s)
		{
			return new Float4((float)(v1.x / s), (float)(v1.y / s), (float)(v1.z / s), (float)(v1.w / s));
		}

		public static Float4 Uniform(float s)
		{
			return new Float4(s, s, s, s);
		}

		public float this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Float4 Pow(Float4 v, double p)
		{
			return new Float4((float)System.Math.Pow(v.x, p), (float)System.Math.Pow(v.y, p), (float)System.Math.Pow(v.z, p), (float)System.Math.Pow(v.w, p));
		}

		public static Float4 Clamp(Float4 v, double min, double max)
		{
			return new Float4((float)Math.Clamp(v.x, min, max), (float)Math.Clamp(v.y, min, max), (float)Math.Clamp(v.z, min, max), (float)Math.Clamp(v.w, min, max));
		}

		public static Float4 Clamp(Float4 v, Float4 min, Float4 max)
		{
			return new Float4((float)Math.Clamp(v.x, min.x, max.x), (float)Math.Clamp(v.y, min.y, max.y), (float)Math.Clamp(v.z, min.z, max.z), (float)Math.Clamp(v.w, min.w, max.w));
		}

		public static Float4 Repeat(Float4 v, double min, double max)
		{
			return new Float4((float)Math.Repeat(v.x, min, max), (float)Math.Repeat(v.y, min, max), (float)Math.Repeat(v.z, min, max), (float)Math.Repeat(v.w, min, max));
		}

		public static Float4 Repeat(Float4 v, Float4 min, Float4 max)
		{
			return new Float4((float)Math.Repeat(v.x, min.x, max.x), (float)Math.Repeat(v.y, min.y, max.y), (float)Math.Repeat(v.z, min.z, max.z), (float)Math.Repeat(v.w, min.w, max.w));
		}

		public static Float4 Min(Float4 v1, Float4 v2)
		{
			return new Float4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static Float4 Max(Float4 v1, Float4 v2)
		{
			return new Float4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public float MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public float MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public float Magnitude
		{
			get { return (float)System.Math.Sqrt(SqrMagnitude); }
		}

		public float SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(Float4 v1, Float4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Float4 v1, Float4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Float4 left = new Float4(-1, 0, 0, 0);
		public static readonly Float4 right = new Float4(1, 0, 0, 0);
		public static readonly Float4 up = new Float4(0, 1, 0, 0);
		public static readonly Float4 down = new Float4(0, -1, 0, 0);
		public static readonly Float4 forward = new Float4(0, 0, 1, 0);
		public static readonly Float4 back = new Float4(0, 0, -1, 0);
		public static readonly Float4 ana = new Float4(0, 0, 0, 1);
		public static readonly Float4 kata = new Float4(0, 0, 0, -1);
	}
}