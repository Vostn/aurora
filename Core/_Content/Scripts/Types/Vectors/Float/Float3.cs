﻿using System.ComponentModel;

namespace Aurora
{
	[System.Serializable]
	[TypeConverter(typeof(Converters.Float3_Converter))]
	public struct Float3
	{
		public float x, y, z;

		public Float3(Float3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Float3(float x, float y, float z = 0)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Float3 v = (Float3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Float3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(Float3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Float3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Float3(UnityEngine.Vector4 v)
		{
			return new Float3(v.x, v.y, v.z);
		}

		public static implicit operator Float3(UnityEngine.Vector3 v)
		{
			return new Float3(v.x, v.y, v.z);
		}

		public static implicit operator Float3(UnityEngine.Vector2 v)
		{
			return new Float3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Color(Float3 v)
		{
			return new UnityEngine.Color(v.x, v.y, v.z);
		}

		public static explicit operator Float3(UnityEngine.Color c)
		{
			return new Float3(c.r, c.g, c.b);
		}

#endif

		public static Float3 operator +(Float3 v1, Float3 v2)
		{
			return new Float3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		public static Float3 operator -(Float3 v1, Float3 v2)
		{
			return new Float3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		public static Float3 operator *(Float3 v1, Float3 v2)
		{
			return new Float3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		public static Float3 operator /(Float3 v1, Float3 v2)
		{
			return new Float3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		public static Float3 operator *(Float3 v1, double s)
		{
			return new Float3((float)(v1.x * s), (float)(v1.y * s), (float)(v1.z * s));
		}

		public static Float3 operator /(Float3 v1, double s)
		{
			return new Float3((float)(v1.x / s), (float)(v1.y / s), (float)(v1.z / s));
		}

		public static Float3 Uniform(float s)
		{
			return new Float3(s, s, s);
		}

		public float this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Float3 Pow(Float3 v, double p)
		{
			return new Float3((float)System.Math.Pow(v.x, p), (float)System.Math.Pow(v.y, p), (float)System.Math.Pow(v.z, p));
		}

		public static Float3 Clamp(Float3 v, double min, double max)
		{
			return new Float3((float)Math.Clamp(v.x, min, max), (float)Math.Clamp(v.y, min, max), (float)Math.Clamp(v.z, min, max));
		}

		public static Float3 Clamp(Float3 v, Float3 min, Float3 max)
		{
			return new Float3((float)Math.Clamp(v.x, min.x, max.x), (float)Math.Clamp(v.y, min.y, max.y), (float)Math.Clamp(v.z, min.z, max.z));
		}

		public static Float3 Repeat(Float3 v, double min, double max)
		{
			return new Float3((float)Math.Repeat(v.x, min, max), (float)Math.Repeat(v.y, min, max), (float)Math.Repeat(v.z, min, max));
		}

		public static Float3 Repeat(Float3 v, Float3 min, Float3 max)
		{
			return new Float3((float)Math.Repeat(v.x, min.x, max.x), (float)Math.Repeat(v.y, min.y, max.y), (float)Math.Repeat(v.z, min.z, max.z));
		}

		public static Float3 Min(Float3 v1, Float3 v2)
		{
			return new Float3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static Float3 Max(Float3 v1, Float3 v2)
		{
			return new Float3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public float MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public float MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public float Magnitude
		{
			get { return (float)System.Math.Sqrt(SqrMagnitude); }
		}

		public float SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static Float3 Lerp(Float3 v1, Float3 v2, double t)
		{
			return v1 + ((v2 - v1) * t);
		}

		public static bool operator ==(Float3 v1, Float3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Float3 v1, Float3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Float3 left = new Float3(-1, 0, 0);
		public static readonly Float3 right = new Float3(1, 0, 0);
		public static readonly Float3 up = new Float3(0, 1, 0);
		public static readonly Float3 down = new Float3(0, -1, 0);
		public static readonly Float3 forward = new Float3(0, 0, 1);
		public static readonly Float3 back = new Float3(0, 0, -1);
	}
}