﻿namespace Aurora
{
	[System.Serializable]
	public struct SByte4
	{
		public sbyte x, y, z, w;

		public SByte4(SByte4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public SByte4(sbyte x, sbyte y, sbyte z, sbyte w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			SByte4 v = (SByte4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(SByte4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(SByte4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(SByte4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator SByte4(UnityEngine.Vector4 v)
		{
			return new SByte4((sbyte)v.x, (sbyte)v.y, (sbyte)v.z, (sbyte)v.w);
		}

		public static explicit operator SByte4(UnityEngine.Vector3 v)
		{
			return new SByte4((sbyte)v.x, (sbyte)v.y, (sbyte)v.z, 0);
		}

		public static explicit operator SByte4(UnityEngine.Vector2 v)
		{
			return new SByte4((sbyte)v.x, (sbyte)v.y, 0, 0);
		}

#endif

		public static SByte4 operator +(SByte4 v1, SByte4 v2)
		{
			return new SByte4((sbyte)(v1.x + v2.x), (sbyte)(v1.y + v2.y), (sbyte)(v1.z + v2.z), (sbyte)(v1.w + v2.w));
		}

		public static SByte4 operator -(SByte4 v1, SByte4 v2)
		{
			return new SByte4((sbyte)(v1.x - v2.x), (sbyte)(v1.y - v2.y), (sbyte)(v1.z - v2.z), (sbyte)(v1.w - v2.w));
		}

		public static SByte4 operator *(SByte4 v1, SByte4 v2)
		{
			return new SByte4((sbyte)(v1.x * v2.x), (sbyte)(v1.y * v2.y), (sbyte)(v1.z * v2.z), (sbyte)(v1.w * v2.w));
		}

		public static SByte4 operator /(SByte4 v1, SByte4 v2)
		{
			return new SByte4((sbyte)(v1.x / v2.x), (sbyte)(v1.y / v2.y), (sbyte)(v1.z / v2.z), (sbyte)(v1.w / v2.w));
		}

		public static SByte4 operator *(SByte4 v1, double s)
		{
			return new SByte4((sbyte)(v1.x * s), (sbyte)(v1.y * s), (sbyte)(v1.z * s), (sbyte)(v1.w * s));
		}

		public static SByte4 operator /(SByte4 v1, double s)
		{
			return new SByte4((sbyte)(v1.x / s), (sbyte)(v1.y / s), (sbyte)(v1.z / s), (sbyte)(v1.w / s));
		}

		public static SByte4 Uniform(sbyte s)
		{
			return new SByte4(s, s, s, s);
		}

		public sbyte this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static SByte4 Clamp(SByte4 v, sbyte min, sbyte max)
		{
			return new SByte4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static SByte4 Clamp(SByte4 v, SByte4 min, SByte4 max)
		{
			return new SByte4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static SByte4 Repeat(SByte4 v, sbyte min, sbyte max)
		{
			return new SByte4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static SByte4 Repeat(SByte4 v, SByte4 min, SByte4 max)
		{
			return new SByte4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static SByte4 Min(SByte4 v1, SByte4 v2)
		{
			return new SByte4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static SByte4 Max(SByte4 v1, SByte4 v2)
		{
			return new SByte4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public sbyte MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public sbyte MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(SByte4 v1, SByte4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(SByte4 v1, SByte4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly SByte4 right = new SByte4(1, 0, 0, 0);
		public static readonly SByte4 up = new SByte4(0, 1, 0, 0);
		public static readonly SByte4 forward = new SByte4(0, 0, 1, 0);
		public static readonly SByte4 ana = new SByte4(0, 0, 0, 1);
	}
}