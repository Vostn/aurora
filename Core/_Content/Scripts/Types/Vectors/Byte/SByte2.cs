﻿namespace Aurora
{
	[System.Serializable]
	public struct SByte2
	{
		public sbyte x, y;

		public SByte2(SByte2 v)
		{
			x = v.x;
			y = v.y;
		}

		public SByte2(sbyte x, sbyte y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Byte2 v = (Byte2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(SByte2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(SByte2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(SByte2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator SByte2(UnityEngine.Vector4 v)
		{
			return new SByte2((sbyte)v.x, (sbyte)v.y);
		}

		public static explicit operator SByte2(UnityEngine.Vector3 v)
		{
			return new SByte2((sbyte)v.x, (sbyte)v.y);
		}

		public static explicit operator SByte2(UnityEngine.Vector2 v)
		{
			return new SByte2((sbyte)v.x, (sbyte)v.y);
		}

#endif

		public static SByte2 operator +(SByte2 v1, SByte2 v2)
		{
			return new SByte2((sbyte)(v1.x + v2.x), (sbyte)(v1.y + v2.y));
		}

		public static SByte2 operator -(SByte2 v1, SByte2 v2)
		{
			return new SByte2((sbyte)(v1.x - v2.x), (sbyte)(v1.y - v2.y));
		}

		public static SByte2 operator *(SByte2 v1, SByte2 v2)
		{
			return new SByte2((sbyte)(v1.x * v2.x), (sbyte)(v1.y * v2.y));
		}

		public static SByte2 operator /(SByte2 v1, SByte2 v2)
		{
			return new SByte2((sbyte)(v1.x / v2.x), (sbyte)(v1.y / v2.y));
		}

		public static SByte2 operator *(SByte2 v1, double s)
		{
			return new SByte2((sbyte)(v1.x * s), (sbyte)(v1.y * s));
		}

		public static SByte2 operator /(SByte2 v1, double s)
		{
			return new SByte2((sbyte)(v1.x / s), (sbyte)(v1.y / s));
		}

		public static SByte2 Uniform(sbyte s)
		{
			return new SByte2(s, s);
		}

		public sbyte this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static SByte2 Clamp(SByte2 v, sbyte min, sbyte max)
		{
			return new SByte2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static SByte2 Clamp(SByte2 v, SByte2 min, SByte2 max)
		{
			return new SByte2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static SByte2 Repeat(SByte2 v, sbyte min, sbyte max)
		{
			return new SByte2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static SByte2 Repeat(SByte2 v, SByte2 min, SByte2 max)
		{
			return new SByte2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static SByte2 Min(SByte2 v1, SByte2 v2)
		{
			return new SByte2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static SByte2 Max(SByte2 v1, SByte2 v2)
		{
			return new SByte2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public sbyte MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public sbyte MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(SByte2 v1, SByte2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(SByte2 v1, SByte2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly SByte2 right = new SByte2(1, 0);
		public static readonly SByte2 up = new SByte2(0, 1);
	}
}