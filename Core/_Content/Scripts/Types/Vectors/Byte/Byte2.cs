﻿namespace Aurora
{
	[System.Serializable]
	public struct Byte2
	{
		public byte x, y;

		public Byte2(Byte2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Byte2(byte x, byte y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Byte2 v = (Byte2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Byte2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Byte2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(Byte2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Byte2(UnityEngine.Vector4 v)
		{
			return new Byte2((byte)v.x, (byte)v.y);
		}

		public static explicit operator Byte2(UnityEngine.Vector3 v)
		{
			return new Byte2((byte)v.x, (byte)v.y);
		}

		public static explicit operator Byte2(UnityEngine.Vector2 v)
		{
			return new Byte2((byte)v.x, (byte)v.y);
		}

#endif

		public static Byte2 operator +(Byte2 v1, Byte2 v2)
		{
			return new Byte2((byte)(v1.x + v2.x), (byte)(v1.y + v2.y));
		}

		public static Byte2 operator -(Byte2 v1, Byte2 v2)
		{
			return new Byte2((byte)(v1.x - v2.x), (byte)(v1.y - v2.y));
		}

		public static Byte2 operator *(Byte2 v1, Byte2 v2)
		{
			return new Byte2((byte)(v1.x * v2.x), (byte)(v1.y * v2.y));
		}

		public static Byte2 operator /(Byte2 v1, Byte2 v2)
		{
			return new Byte2((byte)(v1.x / v2.x), (byte)(v1.y / v2.y));
		}

		public static Byte2 operator *(Byte2 v1, double s)
		{
			return new Byte2((byte)(v1.x * s), (byte)(v1.y * s));
		}

		public static Byte2 operator /(Byte2 v1, double s)
		{
			return new Byte2((byte)(v1.x / s), (byte)(v1.y / s));
		}

		public static Byte2 Uniform(byte s)
		{
			return new Byte2(s, s);
		}

		public byte this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Byte2 Clamp(Byte2 v, byte min, byte max)
		{
			return new Byte2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static Byte2 Clamp(Byte2 v, Byte2 min, Byte2 max)
		{
			return new Byte2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static Byte2 Repeat(Byte2 v, byte min, byte max)
		{
			return new Byte2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static Byte2 Repeat(Byte2 v, Byte2 min, Byte2 max)
		{
			return new Byte2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static Byte2 Min(Byte2 v1, Byte2 v2)
		{
			return new Byte2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static Byte2 Max(Byte2 v1, Byte2 v2)
		{
			return new Byte2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public byte MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public byte MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(Byte2 v1, Byte2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Byte2 v1, Byte2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Byte2 right = new Byte2(1, 0);
		public static readonly Byte2 up = new Byte2(0, 1);
	}
}