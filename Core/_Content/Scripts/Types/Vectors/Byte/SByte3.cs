﻿namespace Aurora
{
	[System.Serializable]
	public struct SByte3
	{
		public sbyte x, y, z;

		public SByte3(SByte3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public SByte3(sbyte x, sbyte y, sbyte z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			SByte3 v = (SByte3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(SByte3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(SByte3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(SByte3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator SByte3(UnityEngine.Vector4 v)
		{
			return new SByte3((sbyte)v.x, (sbyte)v.y, (sbyte)v.z);
		}

		public static explicit operator SByte3(UnityEngine.Vector3 v)
		{
			return new SByte3((sbyte)v.x, (sbyte)v.y, (sbyte)v.z);
		}

		public static explicit operator SByte3(UnityEngine.Vector2 v)
		{
			return new SByte3((sbyte)v.x, (sbyte)v.y, 0);
		}

#endif

		public static SByte3 operator +(SByte3 v1, SByte3 v2)
		{
			return new SByte3((sbyte)(v1.x + v2.x), (sbyte)(v1.y + v2.y), (sbyte)(v1.z + v2.z));
		}

		public static SByte3 operator -(SByte3 v1, SByte3 v2)
		{
			return new SByte3((sbyte)(v1.x - v2.x), (sbyte)(v1.y - v2.y), (sbyte)(v1.z - v2.z));
		}

		public static SByte3 operator *(SByte3 v1, SByte3 v2)
		{
			return new SByte3((sbyte)(v1.x * v2.x), (sbyte)(v1.y * v2.y), (sbyte)(v1.z * v2.z));
		}

		public static SByte3 operator /(SByte3 v1, SByte3 v2)
		{
			return new SByte3((sbyte)(v1.x / v2.x), (sbyte)(v1.y / v2.y), (sbyte)(v1.z / v2.z));
		}

		public static SByte3 operator *(SByte3 v1, double s)
		{
			return new SByte3((sbyte)(v1.x * s), (sbyte)(v1.y * s), (sbyte)(v1.z * s));
		}

		public static SByte3 operator /(SByte3 v1, double s)
		{
			return new SByte3((sbyte)(v1.x / s), (sbyte)(v1.y / s), (sbyte)(v1.z / s));
		}

		public static SByte3 Uniform(sbyte s)
		{
			return new SByte3(s, s, s);
		}

		public sbyte this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static SByte3 Clamp(SByte3 v, sbyte min, sbyte max)
		{
			return new SByte3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static SByte3 Clamp(SByte3 v, SByte3 min, SByte3 max)
		{
			return new SByte3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static SByte3 Repeat(SByte3 v, sbyte min, sbyte max)
		{
			return new SByte3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static SByte3 Repeat(SByte3 v, SByte3 min, SByte3 max)
		{
			return new SByte3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static SByte3 Min(SByte3 v1, SByte3 v2)
		{
			return new SByte3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static SByte3 Max(SByte3 v1, SByte3 v2)
		{
			return new SByte3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public sbyte MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public sbyte MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(SByte3 v1, SByte3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(SByte3 v1, SByte3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly SByte3 right = new SByte3(1, 0, 0);
		public static readonly SByte3 up = new SByte3(0, 1, 0);
		public static readonly SByte3 forward = new SByte3(0, 0, 1);
	}
}