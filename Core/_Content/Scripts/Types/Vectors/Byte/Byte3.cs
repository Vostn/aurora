﻿namespace Aurora
{
	[System.Serializable]
	public struct Byte3
	{
		public byte x, y, z;

		public Byte3(Byte3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Byte3(byte x, byte y, byte z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Byte3 v = (Byte3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Byte3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(Byte3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Byte3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Byte3(UnityEngine.Vector4 v)
		{
			return new Byte3((byte)v.x, (byte)v.y, (byte)v.z);
		}

		public static explicit operator Byte3(UnityEngine.Vector3 v)
		{
			return new Byte3((byte)v.x, (byte)v.y, (byte)v.z);
		}

		public static explicit operator Byte3(UnityEngine.Vector2 v)
		{
			return new Byte3((byte)v.x, (byte)v.y, 0);
		}

#endif

		public static Byte3 operator +(Byte3 v1, Byte3 v2)
		{
			return new Byte3((byte)(v1.x + v2.x), (byte)(v1.y + v2.y), (byte)(v1.z + v2.z));
		}

		public static Byte3 operator -(Byte3 v1, Byte3 v2)
		{
			return new Byte3((byte)(v1.x - v2.x), (byte)(v1.y - v2.y), (byte)(v1.z - v2.z));
		}

		public static Byte3 operator *(Byte3 v1, Byte3 v2)
		{
			return new Byte3((byte)(v1.x * v2.x), (byte)(v1.y * v2.y), (byte)(v1.z * v2.z));
		}

		public static Byte3 operator /(Byte3 v1, Byte3 v2)
		{
			return new Byte3((byte)(v1.x / v2.x), (byte)(v1.y / v2.y), (byte)(v1.z / v2.z));
		}

		public static Byte3 operator *(Byte3 v1, double s)
		{
			return new Byte3((byte)(v1.x * s), (byte)(v1.y * s), (byte)(v1.z * s));
		}

		public static Byte3 operator /(Byte3 v1, double s)
		{
			return new Byte3((byte)(v1.x / s), (byte)(v1.y / s), (byte)(v1.z / s));
		}

		public static Byte3 Uniform(byte s)
		{
			return new Byte3(s, s, s);
		}

		public byte this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Byte3 Clamp(Byte3 v, byte min, byte max)
		{
			return new Byte3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static Byte3 Clamp(Byte3 v, Byte3 min, Byte3 max)
		{
			return new Byte3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static Byte3 Repeat(Byte3 v, byte min, byte max)
		{
			return new Byte3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static Byte3 Repeat(Byte3 v, Byte3 min, Byte3 max)
		{
			return new Byte3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static Byte3 Min(Byte3 v1, Byte3 v2)
		{
			return new Byte3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static Byte3 Max(Byte3 v1, Byte3 v2)
		{
			return new Byte3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public byte MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public byte MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(Byte3 v1, Byte3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Byte3 v1, Byte3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Byte3 right = new Byte3(1, 0, 0);
		public static readonly Byte3 up = new Byte3(0, 1, 0);
		public static readonly Byte3 forward = new Byte3(0, 0, 1);
	}
}