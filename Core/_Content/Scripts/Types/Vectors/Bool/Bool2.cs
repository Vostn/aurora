﻿namespace Aurora
{
	[System.Serializable]
	public struct Bool2
	{
		public bool x, y;

		public Bool2(Bool2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Bool2(bool x, bool y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Bool2 v = (Bool2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Bool2 v)
		{
			return new UnityEngine.Vector4(v.x ? 1 : 0, v.y ? 1 : 0, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Bool2 v)
		{
			return new UnityEngine.Vector3(v.x ? 1 : 0, v.y ? 1 : 0, 0);
		}

		public static implicit operator UnityEngine.Vector2(Bool2 v)
		{
			return new UnityEngine.Vector2(v.x ? 1 : 0, v.y ? 1 : 0);
		}

#endif

		public static Bool2 operator &(Bool2 v1, Bool2 v2)
		{
			return new Bool2(v1.x & v2.x, v1.y & v2.y);
		}

		public static Bool2 operator |(Bool2 v1, Bool2 v2)
		{
			return new Bool2(v1.x | v2.x, v1.y | v2.y);
		}

		public static Bool2 operator ^(Bool2 v1, Bool2 v2)
		{
			return new Bool2(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		public static Bool2 operator !(Bool2 v)
		{
			return new Bool2(!v.x, !v.y);
		}

		public static Bool2 Uniform(bool s)
		{
			return new Bool2(s, s);
		}

		public bool this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static bool operator ==(Bool2 v1, Bool2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Bool2 v1, Bool2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Bool2 right = new Bool2(true, false);
		public static readonly Bool2 up = new Bool2(false, true);
	}
}