﻿namespace Aurora
{
	[System.Serializable]
	public struct Bool4
	{
		public bool x, y, z, w;

		public Bool4(Bool4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Bool4(bool x, bool y, bool z = false, bool w = false)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Bool4 v = (Bool4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Bool4 v)
		{
			return new UnityEngine.Vector4(v.x ? 1 : 0, v.y ? 1 : 0, v.z ? 1 : 0, v.w ? 1 : 0);
		}

		public static explicit operator UnityEngine.Vector3(Bool4 v)
		{
			return new UnityEngine.Vector3(v.x ? 1 : 0, v.y ? 1 : 0, v.z ? 1 : 0);
		}

		public static explicit operator UnityEngine.Vector2(Bool4 v)
		{
			return new UnityEngine.Vector2(v.x ? 1 : 0, v.y ? 1 : 0);
		}

#endif

		public static Bool4 operator &(Bool4 v1, Bool4 v2)
		{
			return new Bool4(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		public static Bool4 operator |(Bool4 v1, Bool4 v2)
		{
			return new Bool4(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		public static Bool4 operator ^(Bool4 v1, Bool4 v2)
		{
			return new Bool4(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		public static Bool4 operator !(Bool4 v)
		{
			return new Bool4(!v.x, !v.y, !v.z, !v.w);
		}

		public static Bool4 Uniform(bool s)
		{
			return new Bool4(s, s, s, s);
		}

		public bool this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static bool operator ==(Bool4 v1, Bool4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Bool4 v1, Bool4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Bool4 right = new Bool4(true, false, false, false);
		public static readonly Bool4 up = new Bool4(false, true, false, false);
		public static readonly Bool4 forward = new Bool4(false, false, true, false);
		public static readonly Bool4 ana = new Bool4(false, false, false, true);
	}
}