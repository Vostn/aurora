﻿namespace Aurora
{
	[System.Serializable]
	public struct Bool3
	{
		public bool x, y, z;

		public Bool3(Bool3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Bool3(bool x, bool y, bool z = false)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Bool3 v = (Bool3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Bool3 v)
		{
			return new UnityEngine.Vector4(v.x ? 1 : 0, v.y ? 1 : 0, v.z ? 1 : 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Bool3 v)
		{
			return new UnityEngine.Vector3(v.x ? 1 : 0, v.y ? 1 : 0, v.z ? 1 : 0);
		}

		public static explicit operator UnityEngine.Vector2(Bool3 v)
		{
			return new UnityEngine.Vector2(v.x ? 1 : 0, v.y ? 1 : 0);
		}

#endif

		public static Bool3 operator &(Bool3 v1, Bool3 v2)
		{
			return new Bool3(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		public static Bool3 operator |(Bool3 v1, Bool3 v2)
		{
			return new Bool3(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		public static Bool3 operator ^(Bool3 v1, Bool3 v2)
		{
			return new Bool3(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		public static Bool3 operator !(Bool3 v)
		{
			return new Bool3(!v.x, !v.y, !v.z);
		}

		public static Bool3 Uniform(bool s)
		{
			return new Bool3(s, s, s);
		}

		public bool this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static bool operator ==(Bool3 v1, Bool3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Bool3 v1, Bool3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Bool3 right = new Bool3(true, false, false);
		public static readonly Bool3 up = new Bool3(false, true, false);
		public static readonly Bool3 forward = new Bool3(false, false, true);
	}
}