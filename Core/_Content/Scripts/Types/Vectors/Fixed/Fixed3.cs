﻿namespace Aurora
{
	[System.Serializable]
	public struct Fixed3
	{
		public Fixed x, y, z;

		public Fixed3(Fixed3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Fixed3(Fixed x, Fixed y)
		{
			this.x = x;
			this.y = y;
			this.z = 0;
		}

		public Fixed3(Fixed x, Fixed y, Fixed z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Fixed3 v = (Fixed3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector2(Fixed3 v)
		{
			return new UnityEngine.Vector2((float)v.x, (float)v.y);
		}

		public static explicit operator UnityEngine.Vector3(Fixed3 v)
		{
			return new UnityEngine.Vector3((float)v.x, (float)v.y, (float)v.z);
		}

		public static explicit operator UnityEngine.Vector4(Fixed3 v)
		{
			return new UnityEngine.Vector4((float)v.x, (float)v.y, (float)v.z);
		}

		public static explicit operator Fixed3(UnityEngine.Vector2 v)
		{
			return new Fixed3((Fixed)v.x, (Fixed)v.y, 0);
		}

		public static explicit operator Fixed3(UnityEngine.Vector3 v)
		{
			return new Fixed3((Fixed)v.x, (Fixed)v.y, (Fixed)v.z);
		}

		public static explicit operator Fixed3(UnityEngine.Vector4 v)
		{
			return new Fixed3((Fixed)v.x, (Fixed)v.y, (Fixed)v.z);
		}

#endif

		public static explicit operator Fixed2(Fixed3 v)
		{
			return new Fixed2(v.x, v.y);
		}

		public static implicit operator Fixed4(Fixed3 v)
		{
			return new Fixed4(v.x, v.y, v.z);
		}

		public static Fixed3 operator +(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		public static Fixed3 operator -(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		public static Fixed3 operator -(Fixed3 v)
		{
			return new Fixed3(-v.x, -v.y, -v.z);
		}

		public static Fixed3 operator *(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		public static Fixed3 operator /(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		public static Fixed3 operator *(Fixed3 v1, Fixed s)
		{
			return new Fixed3(v1.x * s, v1.y * s, v1.z * s);
		}

		public static Fixed3 operator /(Fixed3 v1, Fixed s)
		{
			return new Fixed3(v1.x / s, v1.y / s, v1.z / s);
		}

		public static Fixed3 operator &(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		public static Fixed3 operator |(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		public static Fixed3 operator ^(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		public static Fixed3 operator ~(Fixed3 v)
		{
			return new Fixed3(~v.x, ~v.y, ~v.z);
		}

		public static Fixed3 Uniform(Fixed s)
		{
			return new Fixed3(s, s, s);
		}

		public Fixed this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Fixed3 Clamp(Fixed3 v, Fixed min, Fixed max)
		{
			return new Fixed3(Fixed.Clamp(v.x, min, max), Fixed.Clamp(v.y, min, max), Fixed.Clamp(v.z, min, max));
		}

		public static Fixed3 Clamp(Fixed3 v, Fixed3 min, Fixed3 max)
		{
			return new Fixed3(Fixed.Clamp(v.x, min.x, max.x), Fixed.Clamp(v.y, min.y, max.y), Fixed.Clamp(v.z, min.z, max.z));
		}

		public static Fixed3 Repeat(Fixed3 v, Fixed min, Fixed max)
		{
			return new Fixed3(Fixed.Repeat(v.x, min, max), Fixed.Repeat(v.y, min, max), Fixed.Repeat(v.z, min, max));
		}

		public static Fixed3 Repeat(Fixed3 v, Fixed3 min, Fixed3 max)
		{
			return new Fixed3(Fixed.Repeat(v.x, min.x, max.x), Fixed.Repeat(v.y, min.y, max.y), Fixed.Repeat(v.z, min.z, max.z));
		}

		public static Fixed3 Min(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(Fixed.Min(v1.x, v2.x), Fixed.Min(v1.y, v2.y), Fixed.Min(v1.z, v2.z));
		}

		public static Fixed3 Max(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(Fixed.Max(v1.x, v2.x), Fixed.Max(v1.y, v2.y), Fixed.Max(v1.z, v2.z));
		}

		public Fixed MaxValue
		{
			get
			{
				Fixed maxValue = x;
				if (y > maxValue)
					maxValue = y;
				if (z > maxValue)
					maxValue = z;
				return maxValue;
			}
		}

		public Fixed MinValue
		{
			get
			{
				Fixed maxValue = x;
				if (y < maxValue)
					maxValue = y;
				if (z < maxValue)
					maxValue = z;
				return maxValue;
			}
		}

		/// <summary>
		/// Returns this vector, normalised to a length of 1 unit.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public Fixed3 Normalised
		{
			get { return this / Magnitude; }
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public void Normalise()
		{
			this /= Magnitude;
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Slower than Normalised, and result is not accurate.
		/// </summary>
		public Fixed3 NormalisedHypot
		{
			get { return this / MagnitudeHypot; }
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Slower than Normalise, and result is not accurate.
		/// </summary>
		public void NormaliseHypot()
		{
			this /= MagnitudeHypot;
		}

		/// <summary>
		/// Returns the magnitude of this vector.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public Fixed Magnitude
		{
			get
			{
				return Fixed.Sqrt((x * x) + (y * y) + (z * z));
			}
		}

		/// <summary>
		/// Returns the magnitude of this vector.
		/// WARNING: Slower than Magnitude, and result is not accurate.
		/// </summary>
		public Fixed MagnitudeHypot
		{
			get
			{
				Fixed x = Fixed.Abs(this.x);
				Fixed y = Fixed.Abs(this.y);
				Fixed z = Fixed.Max(x, y);
				x = Fixed.Min(x, y) / z;
				x = z * Fixed.Sqrt(1 + (x * x));

				x = Fixed.Abs(x);
				y = Fixed.Abs(this.z);
				z = Fixed.Max(x, y);
				x = Fixed.Min(x, y) / z;
				return z * Fixed.Sqrt(1 + (x * x));
			}
		}

		public static Fixed3 Lerp(Fixed3 v1, Fixed3 v2, Fixed t)
		{
			return v1 + ((v2 - v1) * t);
		}

		public static Fixed Dot(Fixed3 v1, Fixed3 v2)
		{
			return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
		}

		public static Fixed3 Cross(Fixed3 v1, Fixed3 v2)
		{
			return new Fixed3(v1.y * v2.z - v1.z * v2.y, v1.z * v2.x - v1.x * v2.z, v1.x * v2.y - v1.y * v2.x);
		}

		public Fixed Angle(Fixed3 v1, Fixed3 v2)
		{
			return Math.Acos(Fixed.Clamp(Dot(v1.Normalised, v2.Normalised), -1, 1));
		}

		public static bool operator ==(Fixed3 v1, Fixed3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Fixed3 v1, Fixed3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Fixed3 left = new Fixed3(-1, 0, 0);
		public static readonly Fixed3 right = new Fixed3(1, 0, 0);
		public static readonly Fixed3 up = new Fixed3(0, 1, 0);
		public static readonly Fixed3 down = new Fixed3(0, -1, 0);
		public static readonly Fixed3 forward = new Fixed3(0, 0, 1);
		public static readonly Fixed3 back = new Fixed3(0, 0, -1);
	}
}