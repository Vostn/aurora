﻿namespace Aurora
{
	[System.Serializable]
	public struct Fixed4
	{
		public Fixed x, y, z, w;

		public Fixed4(Fixed4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Fixed4(Fixed x, Fixed y)
		{
			this.x = x;
			this.y = y;
			this.z = 0;
			this.w = 0;
		}

		public Fixed4(Fixed x, Fixed y, Fixed z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = 0;
		}

		public Fixed4(Fixed x, Fixed y, Fixed z, Fixed w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Fixed4 v = (Fixed4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector2(Fixed4 v)
		{
			return new UnityEngine.Vector2((float)v.x, (float)v.y);
		}

		public static explicit operator UnityEngine.Vector3(Fixed4 v)
		{
			return new UnityEngine.Vector3((float)v.x, (float)v.y, (float)v.z);
		}

		public static explicit operator UnityEngine.Vector4(Fixed4 v)
		{
			return new UnityEngine.Vector4((float)v.x, (float)v.y, (float)v.z, (float)v.w);
		}

		public static explicit operator Fixed4(UnityEngine.Vector2 v)
		{
			return new Fixed4((Fixed)v.x, (Fixed)v.y);
		}

		public static explicit operator Fixed4(UnityEngine.Vector3 v)
		{
			return new Fixed4((Fixed)v.x, (Fixed)v.y, (Fixed)v.z);
		}

		public static explicit operator Fixed4(UnityEngine.Vector4 v)
		{
			return new Fixed4((Fixed)v.x, (Fixed)v.y, (Fixed)v.z, (Fixed)v.w);
		}

#endif

		public static explicit operator Fixed2(Fixed4 v)
		{
			return new Fixed2(v.x, v.y);
		}

		public static explicit operator Fixed3(Fixed4 v)
		{
			return new Fixed3(v.x, v.y, v.z);
		}

		public static Fixed4 operator +(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static Fixed4 operator -(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static Fixed4 operator *(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static Fixed4 operator /(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static Fixed4 operator *(Fixed4 v1, Fixed s)
		{
			return new Fixed4(v1.x * s, v1.y * s, v1.z * s, v1.w * s);
		}

		public static Fixed4 operator /(Fixed4 v1, Fixed s)
		{
			return new Fixed4(v1.x / s, v1.y / s, v1.z / s, v1.w / s);
		}

		public static Fixed4 operator &(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		public static Fixed4 operator |(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		public static Fixed4 operator ^(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		public static Fixed4 operator ~(Fixed4 v)
		{
			return new Fixed4(~v.x, ~v.y, ~v.z, ~v.w);
		}

		public static Fixed4 Uniform(Fixed s)
		{
			return new Fixed4(s, s, s, s);
		}

		public Fixed this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Fixed4 Clamp(Fixed4 v, Fixed min, Fixed max)
		{
			return new Fixed4(Fixed.Clamp(v.x, min, max), Fixed.Clamp(v.y, min, max), Fixed.Clamp(v.z, min, max), Fixed.Clamp(v.w, min, max));
		}

		public static Fixed4 Clamp(Fixed4 v, Fixed4 min, Fixed4 max)
		{
			return new Fixed4(Fixed.Clamp(v.x, min.x, max.x), Fixed.Clamp(v.y, min.y, max.y), Fixed.Clamp(v.z, min.z, max.z), Fixed.Clamp(v.w, min.w, max.w));
		}

		public static Fixed4 Repeat(Fixed4 v, Fixed min, Fixed max)
		{
			return new Fixed4(Fixed.Repeat(v.x, min, max), Fixed.Repeat(v.y, min, max), Fixed.Repeat(v.z, min, max), Fixed.Repeat(v.w, min, max));
		}

		public static Fixed4 Repeat(Fixed4 v, Fixed4 min, Fixed4 max)
		{
			return new Fixed4(Fixed.Repeat(v.x, min.x, max.x), Fixed.Repeat(v.y, min.y, max.y), Fixed.Repeat(v.z, min.z, max.z), Fixed.Repeat(v.w, min.w, max.w));
		}

		public static Fixed4 Min(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(Fixed.Min(v1.x, v2.x), Fixed.Min(v1.y, v2.y), Fixed.Min(v1.z, v2.z), Fixed.Min(v1.w, v2.w));
		}

		public static Fixed4 Max(Fixed4 v1, Fixed4 v2)
		{
			return new Fixed4(Fixed.Max(v1.x, v2.x), Fixed.Max(v1.y, v2.y), Fixed.Max(v1.z, v2.z), Fixed.Max(v1.w, v2.w));
		}

		public Fixed MinVal
		{
			get { return Fixed.Min(x, Fixed.Min(y, Fixed.Min(z, w))); }
		}

		public Fixed MaxVal
		{
			get { return Fixed.Max(x, Fixed.Max(y, Fixed.Max(z, w))); }
		}
		
		/// <summary>
		/// Returns this vector, normalised to a length of 1 unit.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public Fixed4 Normalised
		{
			get { return this / Magnitude; }
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public void Normalise()
		{
			this /= Magnitude;
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Slower than Normalised, and result is not accurate.
		/// </summary>
		public Fixed4 NormalisedHypot
		{
			get { return this / MagnitudeHypot; }
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Slower than Normalise, and result is not accurate.
		/// </summary>
		public void NormaliseHypot()
		{
			this /= MagnitudeHypot;
		}

		/// <summary>
		/// Returns the magnitude of this vector.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public Fixed Magnitude
		{
			get
			{
				return Fixed.Sqrt((x * x) + (y * y) + (z * z) + (w * w));
			}
		}

		/// <summary>
		/// Returns the magnitude of this vector.
		/// WARNING: Slower than Magnitude, and result is not accurate.
		/// </summary>
		public Fixed MagnitudeHypot
		{
			get
			{
				Fixed x = Fixed.Abs(this.x);
				Fixed y = Fixed.Abs(this.y);
				Fixed z = Fixed.Max(x, y);
				x = Fixed.Min(x, y) / z;
				x = z * Fixed.Sqrt(1 + (x * x));

				x = Fixed.Abs(x);
				y = Fixed.Abs(this.z);
				z = Fixed.Max(x, y);
				x = Fixed.Min(x, y) / z;
				x = z * Fixed.Sqrt(1 + (x * x));

				x = Fixed.Abs(x);
				y = Fixed.Abs(this.w);
				z = Fixed.Max(x, y);
				x = Fixed.Min(x, y) / z;
				return z * Fixed.Sqrt(1 + (x * x));
			}
		}

		public static Fixed4 Lerp(Fixed4 v1, Fixed4 v2, Fixed t)
		{
			return v1 + ((v2 - v1) * t);
		}

		public Fixed Dot(Fixed4 v1, Fixed4 v2)
		{
			return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z + v1.w * v2.w;
		}

		public Fixed Angle(Fixed4 v1, Fixed4 v2)
		{
			return Math.Acos(Fixed.Clamp(Dot(v1.Normalised, v2.Normalised), -1, 1));
		}

		public static bool operator ==(Fixed4 v1, Fixed4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Fixed4 v1, Fixed4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Fixed4 left = new Fixed4(-1, 0, 0, 0);
		public static readonly Fixed4 right = new Fixed4(1, 0, 0, 0);
		public static readonly Fixed4 up = new Fixed4(0, 1, 0, 0);
		public static readonly Fixed4 down = new Fixed4(0, -1, 0, 0);
		public static readonly Fixed4 forward = new Fixed4(0, 0, 1, 0);
		public static readonly Fixed4 back = new Fixed4(0, 0, -1, 0);
		public static readonly Fixed4 ana = new Fixed4(0, 0, 0, 1);
		public static readonly Fixed4 kata = new Fixed4(0, 0, 0, -1);
	}
}