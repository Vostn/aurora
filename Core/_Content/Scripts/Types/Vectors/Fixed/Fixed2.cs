﻿namespace Aurora
{
	[System.Serializable]
	public struct Fixed2
	{
		public Fixed x, y;

		public Fixed2(Fixed2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Fixed2(Fixed x, Fixed y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Fixed2 v = (Fixed2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector2(Fixed2 v)
		{
			return new UnityEngine.Vector2((float)v.x, (float)v.y);
		}

		public static explicit operator UnityEngine.Vector3(Fixed2 v)
		{
			return new UnityEngine.Vector3((float)v.x, (float)v.y);
		}

		public static explicit operator UnityEngine.Vector4(Fixed2 v)
		{
			return new UnityEngine.Vector4((float)v.x, (float)v.y);
		}

		public static explicit operator Fixed2(UnityEngine.Vector2 v)
		{
			return new Fixed2((Fixed)v.x, (Fixed)v.y);
		}

		public static explicit operator Fixed2(UnityEngine.Vector3 v)
		{
			return new Fixed2((Fixed)v.x, (Fixed)v.y);
		}

		public static explicit operator Fixed2(UnityEngine.Vector4 v)
		{
			return new Fixed2((Fixed)v.x, (Fixed)v.y);
		}

#endif

		public static implicit operator Fixed3(Fixed2 v)
		{
			return new Fixed3(v.x, v.y);
		}

		public static implicit operator Fixed4(Fixed2 v)
		{
			return new Fixed4(v.x, v.y);
		}

		public static Fixed2 operator +(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x + v2.x, v1.y + v2.y);
		}

		public static Fixed2 operator -(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x - v2.x, v1.y - v2.y);
		}

		public static Fixed2 operator *(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x * v2.x, v1.y * v2.y);
		}

		public static Fixed2 operator /(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x / v2.x, v1.y / v2.y);
		}

		public static Fixed2 operator *(Fixed2 v1, Fixed s)
		{
			return new Fixed2(v1.x * s, v1.y * s);
		}

		public static Fixed2 operator /(Fixed2 v1, Fixed s)
		{
			return new Fixed2(v1.x / s, v1.y / s);
		}

		public static Fixed2 operator &(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x & v2.x, v1.y & v2.y);
		}

		public static Fixed2 operator |(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x | v2.x, v1.y | v2.y);
		}

		public static Fixed2 operator ^(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		public static Fixed2 operator ~(Fixed2 v)
		{
			return new Fixed2(~v.x, ~v.y);
		}

		public static Fixed2 Uniform(Fixed s)
		{
			return new Fixed2(s, s);
		}

		public Fixed this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Fixed2 Clamp(Fixed2 v, Fixed min, Fixed max)
		{
			return new Fixed2(Fixed.Clamp(v.x, min, max), Fixed.Clamp(v.y, min, max));
		}

		public static Fixed2 Clamp(Fixed2 v, Fixed2 min, Fixed2 max)
		{
			return new Fixed2(Fixed.Clamp(v.x, min.x, max.x), Fixed.Clamp(v.y, min.y, max.y));
		}

		public static Fixed2 Repeat(Fixed2 v, Fixed min, Fixed max)
		{
			return new Fixed2(Fixed.Repeat(v.x, min, max), Fixed.Repeat(v.y, min, max));
		}

		public static Fixed2 Repeat(Fixed2 v, Fixed2 min, Fixed2 max)
		{
			return new Fixed2(Fixed.Repeat(v.x, min.x, max.x), Fixed.Repeat(v.y, min.y, max.y));
		}

		public static Fixed2 Min(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(Fixed.Min(v1.x, v2.x), Fixed.Min(v1.y, v2.y));
		}

		public static Fixed2 Max(Fixed2 v1, Fixed2 v2)
		{
			return new Fixed2(Fixed.Max(v1.x, v2.x), Fixed.Max(v1.y, v2.y));
		}

		public Fixed MaxValue
		{
			get
			{
				Fixed maxValue = x;
				if (y > maxValue)
					maxValue = y;
				return maxValue;
			}
		}

		public Fixed MinValue
		{
			get
			{
				Fixed maxValue = x;
				if (y < maxValue)
					maxValue = y;
				return maxValue;
			}
		}

		/// <summary>
		/// Returns this vector, normalised to a length of 1 unit.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public Fixed2 Normalised
		{
			get { return this / Magnitude; }
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public void Normalise()
		{
			this /= Magnitude;
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Slower than Normalised, and result is not accurate.
		/// </summary>
		public Fixed2 NormalisedHypot
		{
			get { return this / MagnitudeHypot; }
		}

		/// <summary>
		/// Normalises this vector to a length of 1 unit.
		/// WARNING: Slower than Normalise, and result is not accurate.
		/// </summary>
		public void NormaliseHypot()
		{
			this /= MagnitudeHypot;
		}

		/// <summary>
		/// Returns the magnitude of this vector.
		/// WARNING: Returns incorrect result if length is greater than Sqrt(2^21) (~1448.15468)
		/// </summary>
		public Fixed Magnitude
		{
			get
			{
				return Fixed.Sqrt((x * x) + (y * y));
			}
		}

		/// <summary>
		/// Returns the magnitude of this vector.
		/// WARNING: Slower than Magnitude, and result is not accurate.
		/// </summary>
		public Fixed MagnitudeHypot
		{
			get
			{
				Fixed x = Fixed.Abs(this.x);
				Fixed y = Fixed.Abs(this.y);
				Fixed z = Fixed.Max(x, y);
				x = Fixed.Min(x, y) / z;
				return z * Fixed.Sqrt(1 + (x * x));
			}
		}

		public static Fixed2 Lerp(Fixed2 v1, Fixed2 v2, Fixed t)
		{
			return v1 + ((v2 - v1) * t);
		}

		public Fixed Dot(Fixed2 v1, Fixed2 v2)
		{
			return v1.x * v2.x + v1.y * v2.y;
		}

		public Fixed Angle(Fixed2 v1, Fixed2 v2)
		{
			return Math.Acos(Fixed.Clamp(Dot(v1.Normalised, v2.Normalised), -1, 1));
		}

		public static bool operator ==(Fixed2 v1, Fixed2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Fixed2 v1, Fixed2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Fixed2 left = new Fixed2(-1, 0);
		public static readonly Fixed2 right = new Fixed2(1, 0);
		public static readonly Fixed2 up = new Fixed2(0, 1);
		public static readonly Fixed2 down = new Fixed2(0, -1);
	}
}