﻿namespace Aurora
{
	[System.Serializable]
	public struct Short2
	{
		public short x, y;

		public Short2(Short2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Short2(short x, short y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Short2 v = (Short2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Short2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Short2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(Short2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Short2(UnityEngine.Vector4 v)
		{
			return new Short2((short)v.x, (short)v.y);
		}

		public static explicit operator Short2(UnityEngine.Vector3 v)
		{
			return new Short2((short)v.x, (short)v.y);
		}

		public static explicit operator Short2(UnityEngine.Vector2 v)
		{
			return new Short2((short)v.x, (short)v.y);
		}

#endif

		public static Short2 operator +(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x + v2.x), (short)(v1.y + v2.y));
		}

		public static Short2 operator -(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x - v2.x), (short)(v1.y - v2.y));
		}

		public static Short2 operator *(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x * v2.x), (short)(v1.y * v2.y));
		}

		public static Short2 operator /(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x / v2.x), (short)(v1.y / v2.y));
		}

		public static Short2 operator *(Short2 v1, double s)
		{
			return new Short2((short)(v1.x * s), (short)(v1.y * s));
		}

		public static Short2 operator /(Short2 v1, double s)
		{
			return new Short2((short)(v1.x / s), (short)(v1.y / s));
		}

		public static Short2 operator &(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x & v2.x), (short)(v1.y & v2.y));
		}

		public static Short2 operator |(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x | v2.x), (short)(v1.y | v2.y));
		}

		public static Short2 operator ^(Short2 v1, Short2 v2)
		{
			return new Short2((short)(v1.x ^ v2.x), (short)(v1.y ^ v2.y));
		}

		public static Short2 Uniform(short s)
		{
			return new Short2(s, s);
		}

		public short this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Short2 Clamp(Short2 v, short min, short max)
		{
			return new Short2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static Short2 Clamp(Short2 v, Short2 min, Short2 max)
		{
			return new Short2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static Short2 Repeat(Short2 v, short min, short max)
		{
			return new Short2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static Short2 Repeat(Short2 v, Short2 min, Short2 max)
		{
			return new Short2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static Short2 Min(Short2 v1, Short2 v2)
		{
			return new Short2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static Short2 Max(Short2 v1, Short2 v2)
		{
			return new Short2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public short MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public short MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(Short2 v1, Short2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Short2 v1, Short2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Short2 left = new Short2(-1, 0);
		public static readonly Short2 right = new Short2(1, 0);
		public static readonly Short2 up = new Short2(0, 1);
		public static readonly Short2 down = new Short2(0, -1);
	}
}