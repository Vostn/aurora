﻿namespace Aurora
{
	[System.Serializable]
	public struct UShort3
	{
		public ushort x, y, z;

		public UShort3(UShort3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public UShort3(ushort x, ushort y, ushort z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			UShort3 v = (UShort3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(UShort3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(UShort3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(UShort3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator UShort3(UnityEngine.Vector4 v)
		{
			return new UShort3((ushort)v.x, (ushort)v.y, (ushort)v.z);
		}

		public static explicit operator UShort3(UnityEngine.Vector3 v)
		{
			return new UShort3((ushort)v.x, (ushort)v.y, (ushort)v.z);
		}

		public static explicit operator UShort3(UnityEngine.Vector2 v)
		{
			return new UShort3((ushort)v.x, (ushort)v.y, 0);
		}

#endif

		public static UShort3 operator +(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x + v2.x), (ushort)(v1.y + v2.y), (ushort)(v1.z + v2.z));
		}

		public static UShort3 operator -(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x - v2.x), (ushort)(v1.y - v2.y), (ushort)(v1.z - v2.z));
		}

		public static UShort3 operator *(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x * v2.x), (ushort)(v1.y * v2.y), (ushort)(v1.z * v2.z));
		}

		public static UShort3 operator /(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x / v2.x), (ushort)(v1.y / v2.y), (ushort)(v1.z / v2.z));
		}

		public static UShort3 operator *(UShort3 v1, double s)
		{
			return new UShort3((ushort)(v1.x * s), (ushort)(v1.y * s), (ushort)(v1.z * s));
		}

		public static UShort3 operator /(UShort3 v1, double s)
		{
			return new UShort3((ushort)(v1.x / s), (ushort)(v1.y / s), (ushort)(v1.z / s));
		}

		public static UShort3 operator &(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x & v2.x), (ushort)(v1.y & v2.y), (ushort)(v1.z & v2.z));
		}

		public static UShort3 operator |(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x | v2.x), (ushort)(v1.y | v2.y), (ushort)(v1.z | v2.z));
		}

		public static UShort3 operator ^(UShort3 v1, UShort3 v2)
		{
			return new UShort3((ushort)(v1.x ^ v2.x), (ushort)(v1.y ^ v2.y), (ushort)(v1.z ^ v2.z));
		}

		public static UShort3 Uniform(ushort s)
		{
			return new UShort3(s, s, s);
		}

		public ushort this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static UShort3 Clamp(UShort3 v, ushort min, ushort max)
		{
			return new UShort3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static UShort3 Clamp(UShort3 v, UShort3 min, UShort3 max)
		{
			return new UShort3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static UShort3 Repeat(UShort3 v, ushort min, ushort max)
		{
			return new UShort3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static UShort3 Repeat(UShort3 v, UShort3 min, UShort3 max)
		{
			return new UShort3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static UShort3 Min(UShort3 v1, UShort3 v2)
		{
			return new UShort3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static UShort3 Max(UShort3 v1, UShort3 v2)
		{
			return new UShort3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public ushort MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public ushort MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(UShort3 v1, UShort3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(UShort3 v1, UShort3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly UShort3 right = new UShort3(1, 0, 0);
		public static readonly UShort3 up = new UShort3(0, 1, 0);
		public static readonly UShort3 forward = new UShort3(0, 0, 1);
	}
}