﻿namespace Aurora
{
	[System.Serializable]
	public struct UShort2
	{
		public ushort x, y;

		public UShort2(UShort2 v)
		{
			x = v.x;
			y = v.y;
		}

		public UShort2(ushort x, ushort y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			UShort2 v = (UShort2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(UShort2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(UShort2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(UShort2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator UShort2(UnityEngine.Vector4 v)
		{
			return new UShort2((ushort)v.x, (ushort)v.y);
		}

		public static explicit operator UShort2(UnityEngine.Vector3 v)
		{
			return new UShort2((ushort)v.x, (ushort)v.y);
		}

		public static explicit operator UShort2(UnityEngine.Vector2 v)
		{
			return new UShort2((ushort)v.x, (ushort)v.y);
		}

#endif

		public static UShort2 operator +(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x + v2.x), (ushort)(v1.y + v2.y));
		}

		public static UShort2 operator -(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x - v2.x), (ushort)(v1.y - v2.y));
		}

		public static UShort2 operator *(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x * v2.x), (ushort)(v1.y * v2.y));
		}

		public static UShort2 operator /(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x / v2.x), (ushort)(v1.y / v2.y));
		}

		public static UShort2 operator *(UShort2 v1, double s)
		{
			return new UShort2((ushort)(v1.x * s), (ushort)(v1.y * s));
		}

		public static UShort2 operator /(UShort2 v1, double s)
		{
			return new UShort2((ushort)(v1.x / s), (ushort)(v1.y / s));
		}

		public static UShort2 operator &(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x & v2.x), (ushort)(v1.y & v2.y));
		}

		public static UShort2 operator |(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x | v2.x), (ushort)(v1.y | v2.y));
		}

		public static UShort2 operator ^(UShort2 v1, UShort2 v2)
		{
			return new UShort2((ushort)(v1.x ^ v2.x), (ushort)(v1.y ^ v2.y));
		}

		public static UShort2 Uniform(ushort s)
		{
			return new UShort2(s, s);
		}

		public ushort this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static UShort2 Clamp(UShort2 v, ushort min, ushort max)
		{
			return new UShort2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static UShort2 Clamp(UShort2 v, UShort2 min, UShort2 max)
		{
			return new UShort2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static UShort2 Repeat(UShort2 v, ushort min, ushort max)
		{
			return new UShort2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static UShort2 Repeat(UShort2 v, UShort2 min, UShort2 max)
		{
			return new UShort2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static UShort2 Min(UShort2 v1, UShort2 v2)
		{
			return new UShort2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static UShort2 Max(UShort2 v1, UShort2 v2)
		{
			return new UShort2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public ushort MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public ushort MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(UShort2 v1, UShort2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(UShort2 v1, UShort2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly UShort2 right = new UShort2(1, 0);
		public static readonly UShort2 up = new UShort2(0, 1);
	}
}