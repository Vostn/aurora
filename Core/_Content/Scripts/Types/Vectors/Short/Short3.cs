﻿namespace Aurora
{
	[System.Serializable]
	public struct Short3
	{
		public short x, y, z;

		public Short3(Short3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Short3(short x, short y, short z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Short3 v = (Short3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Short3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(Short3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Short3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Short3(UnityEngine.Vector4 v)
		{
			return new Short3((short)v.x, (short)v.y, (short)v.z);
		}

		public static explicit operator Short3(UnityEngine.Vector3 v)
		{
			return new Short3((short)v.x, (short)v.y, (short)v.z);
		}

		public static explicit operator Short3(UnityEngine.Vector2 v)
		{
			return new Short3((short)v.x, (short)v.y, 0);
		}

#endif

		public static Short3 operator +(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x + v2.x), (short)(v1.y + v2.y), (short)(v1.z + v2.z));
		}

		public static Short3 operator -(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x - v2.x), (short)(v1.y - v2.y), (short)(v1.z - v2.z));
		}

		public static Short3 operator *(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x * v2.x), (short)(v1.y * v2.y), (short)(v1.z * v2.z));
		}

		public static Short3 operator /(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x / v2.x), (short)(v1.y / v2.y), (short)(v1.z / v2.z));
		}

		public static Short3 operator *(Short3 v1, double s)
		{
			return new Short3((short)(v1.x * s), (short)(v1.y * s), (short)(v1.z * s));
		}

		public static Short3 operator /(Short3 v1, double s)
		{
			return new Short3((short)(v1.x / s), (short)(v1.y / s), (short)(v1.z / s));
		}

		public static Short3 operator &(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x & v2.x), (short)(v1.y & v2.y), (short)(v1.z & v2.z));
		}

		public static Short3 operator |(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x | v2.x), (short)(v1.y | v2.y), (short)(v1.z | v2.z));
		}

		public static Short3 operator ^(Short3 v1, Short3 v2)
		{
			return new Short3((short)(v1.x ^ v2.x), (short)(v1.y ^ v2.y), (short)(v1.z ^ v2.z));
		}

		public static Short3 Uniform(short s)
		{
			return new Short3(s, s, s);
		}

		public short this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Short3 Clamp(Short3 v, short min, short max)
		{
			return new Short3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static Short3 Clamp(Short3 v, Short3 min, Short3 max)
		{
			return new Short3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static Short3 Repeat(Short3 v, short min, short max)
		{
			return new Short3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static Short3 Repeat(Short3 v, Short3 min, Short3 max)
		{
			return new Short3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static Short3 Min(Short3 v1, Short3 v2)
		{
			return new Short3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static Short3 Max(Short3 v1, Short3 v2)
		{
			return new Short3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public short MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public short MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(Short3 v1, Short3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Short3 v1, Short3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Short3 left = new Short3(-1, 0, 0);
		public static readonly Short3 right = new Short3(1, 0, 0);
		public static readonly Short3 up = new Short3(0, 1, 0);
		public static readonly Short3 down = new Short3(0, -1, 0);
		public static readonly Short3 forward = new Short3(0, 0, 1);
		public static readonly Short3 back = new Short3(0, 0, -1);
	}
}