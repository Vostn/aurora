﻿namespace Aurora
{
	[System.Serializable]
	public struct Short4
	{
		public short x, y, z, w;

		public Short4(Short4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Short4(short x, short y, short z, short w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Short4 v = (Short4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Short4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(Short4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Short4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Short4(UnityEngine.Vector4 v)
		{
			return new Short4((short)v.x, (short)v.y, (short)v.z, (short)v.w);
		}

		public static explicit operator Short4(UnityEngine.Vector3 v)
		{
			return new Short4((short)v.x, (short)v.y, (short)v.z, 0);
		}

		public static explicit operator Short4(UnityEngine.Vector2 v)
		{
			return new Short4((short)v.x, (short)v.y, 0, 0);
		}

#endif

		public static Short4 operator +(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x + v2.x), (short)(v1.y + v2.y), (short)(v1.z + v2.z), (short)(v1.w + v2.w));
		}

		public static Short4 operator -(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x - v2.x), (short)(v1.y - v2.y), (short)(v1.z - v2.z), (short)(v1.w - v2.w));
		}

		public static Short4 operator *(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x * v2.x), (short)(v1.y * v2.y), (short)(v1.z * v2.z), (short)(v1.w * v2.w));
		}

		public static Short4 operator /(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x / v2.x), (short)(v1.y / v2.y), (short)(v1.z / v2.z), (short)(v1.w / v2.w));
		}

		public static Short4 operator *(Short4 v1, double s)
		{
			return new Short4((short)(v1.x * s), (short)(v1.y * s), (short)(v1.z * s), (short)(v1.w * s));
		}

		public static Short4 operator /(Short4 v1, double s)
		{
			return new Short4((short)(v1.x / s), (short)(v1.y / s), (short)(v1.z / s), (short)(v1.w / s));
		}

		public static Short4 operator &(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x & v2.x), (short)(v1.y & v2.y), (short)(v1.z & v2.z), (short)(v1.w & v2.w));
		}

		public static Short4 operator |(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x | v2.x), (short)(v1.y | v2.y), (short)(v1.z | v2.z), (short)(v1.w | v2.w));
		}

		public static Short4 operator ^(Short4 v1, Short4 v2)
		{
			return new Short4((short)(v1.x ^ v2.x), (short)(v1.y ^ v2.y), (short)(v1.z ^ v2.z), (short)(v1.w ^ v2.w));
		}

		public static Short4 Uniform(short s)
		{
			return new Short4(s, s, s, s);
		}

		public short this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Short4 Clamp(Short4 v, short min, short max)
		{
			return new Short4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static Short4 Clamp(Short4 v, Short4 min, Short4 max)
		{
			return new Short4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static Short4 Repeat(Short4 v, short min, short max)
		{
			return new Short4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static Short4 Repeat(Short4 v, Short4 min, Short4 max)
		{
			return new Short4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static Short4 Min(Short4 v1, Short4 v2)
		{
			return new Short4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static Short4 Max(Short4 v1, Short4 v2)
		{
			return new Short4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public short MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public short MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(Short4 v1, Short4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Short4 v1, Short4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Short4 left = new Short4(-1, 0, 0, 0);
		public static readonly Short4 right = new Short4(1, 0, 0, 0);
		public static readonly Short4 up = new Short4(0, 1, 0, 0);
		public static readonly Short4 down = new Short4(0, -1, 0, 0);
		public static readonly Short4 forward = new Short4(0, 0, 1, 0);
		public static readonly Short4 back = new Short4(0, 0, -1, 0);
		public static readonly Short4 ana = new Short4(0, 0, 0, 1);
		public static readonly Short4 kata = new Short4(0, 0, 0, -1);
	}
}