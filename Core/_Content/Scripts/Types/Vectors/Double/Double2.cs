﻿namespace Aurora
{
	[System.Serializable]
	public struct Double2 : IVector<Double2>, ISlerpable<Double2>
	{
		public double x, y;

		public Double2(Double2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Double2(double x, double y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Double2 v = (Double2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector4(Double2 v)
		{
			return new UnityEngine.Vector4((float)v.x, (float)v.y, 0, 0);
		}

		public static explicit operator UnityEngine.Vector3(Double2 v)
		{
			return new UnityEngine.Vector3((float)v.x, (float)v.y, 0);
		}

		public static explicit operator UnityEngine.Vector2(Double2 v)
		{
			return new UnityEngine.Vector2((float)v.x, (float)v.y);
		}

		public static explicit operator Double2(UnityEngine.Vector4 v)
		{
			return new Double2(v.x, v.y);
		}

		public static explicit operator Double2(UnityEngine.Vector3 v)
		{
			return new Double2(v.x, v.y);
		}

		public static implicit operator Double2(UnityEngine.Vector2 v)
		{
			return new Double2(v.x, v.y);
		}

		public static explicit operator UnityEngine.Color(Double2 v)
		{
			return new UnityEngine.Color((float)v.x, (float)v.y, 0);
		}

		public static explicit operator Double2(UnityEngine.Color c)
		{
			return new Double2(c.r, c.g);
		}

#endif

		public Double2 Value => this;

		public static Double2 operator +(Double2 v1, Double2 v2)
		{
			return new Double2(v1.x + v2.x, v1.y + v2.y);
		}

		void IArithmetic<Double2>.Add(Double2 v)
		{
			this += v;
		}

		Double2 IArithmetic<Double2>.Sum(Double2 v)
		{
			return this + v;
		}

		public static Double2 operator -(Double2 v1, Double2 v2)
		{
			return new Double2(v1.x - v2.x, v1.y - v2.y);
		}

		void IArithmetic<Double2>.Subtract(Double2 v)
		{
			this -= v;
		}

		Double2 IArithmetic<Double2>.Difference(Double2 v)
		{
			return this - v;
		}

		public static Double2 operator -(Double2 v)
		{
			return new Double2(-v.x, -v.y);
		}

		Double2 IArithmetic<Double2>.Negative
		{
			get { return -this; }
		}

		public static Double2 operator *(Double2 v1, Double2 v2)
		{
			return new Double2(v1.x * v2.x, v1.y * v2.y);
		}

		void IArithmetic<Double2>.Multiply(Double2 v)
		{
			this *= v;
		}

		Double2 IArithmetic<Double2>.Product(Double2 v)
		{
			return this * v;
		}

		public static Double2 operator /(Double2 v1, Double2 v2)
		{
			return new Double2(v1.x / v2.x, v1.y / v2.y);
		}

		void IArithmetic<Double2>.Divide(Double2 v)
		{
			this /= v;
		}

		Double2 IArithmetic<Double2>.Quotient(Double2 v)
		{
			return this / v;
		}

		public static Double2 operator *(Double2 v1, double s)
		{
			return new Double2(v1.x * s, v1.y * s);
		}

		void IVector<Double2>.Multiply(double v)
		{
			this *= v;
		}

		Double2 IVector<Double2>.Product(double v)
		{
			return this * v;
		}

		public static Double2 operator /(Double2 v1, double s)
		{
			return new Double2(v1.x / s, v1.y / s);
		}

		void IVector<Double2>.Divide(double v)
		{
			this /= v;
		}

		Double2 IVector<Double2>.Quotient(double v)
		{
			return this / v;
		}

		public static Double2 Uniform(double s)
		{
			return new Double2(s, s);
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Double2 Pow(Double2 v, double p)
		{
			return new Double2(System.Math.Pow(v.x, p), System.Math.Pow(v.y, p));
		}

		public static Double2 Clamp(Double2 v, double min, double max)
		{
			return new Double2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static Double2 Clamp(Double2 v, Double2 min, Double2 max)
		{
			return new Double2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static Double2 Repeat(Double2 v, byte min, byte max)
		{
			return new Double2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static Double2 Repeat(Double2 v, Double2 min, Double2 max)
		{
			return new Double2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static Double2 Min(Double2 v1, Double2 v2)
		{
			return new Double2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static Double2 Max(Double2 v1, Double2 v2)
		{
			return new Double2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public double MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public double MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public Double2 Normalised
		{
			get { return this / Magnitude; }
		}

		public static double Dot(Double2 v1, Double2 v2)
		{
			return v1.x * v2.x + v1.y * v2.y;
		}

		public static Double2 Lerp(Double2 v1, Double2 v2, double t)
		{
			return v1 + ((v2 - v1) * t);
		}

		Double2 ILerpable<Double2>.Lerpolant(Double2 v2, double t)
		{
			return Lerp(this, v2, t);
		}

		Double2 IInterpolable<Double2>.Interpolant(Double2 v2, double t)
		{
			return Lerp(this, v2, t);
		}

		public static Double2 Slerp(Double2 v1, Double2 v2, double t)
		{
			double dot = Math.Clamp(Dot(v1, v2), -1, 1);
			double theta = System.Math.Acos(dot) * t;
			Double2 relativeVec = (v1 - v2 * dot);
			return (v1 * System.Math.Cos(theta)) + (relativeVec * System.Math.Sin(theta));
		}

		Double2 ISlerpable<Double2>.Slerpolant(Double2 v2, double t)
		{
			return Slerp(this, v2, t);
		}

		public double Angle(Double2 v)
		{
			return System.Math.Atan2(v.y - y, v.x - x) * Math.RadToDeg;
		}

		double IAnglable<Double2>.Angle(Double2 other)
		{
			return Angle(other);
		}

		public static bool operator ==(Double2 v1, Double2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Double2 v1, Double2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Double2 left = new Double2(-1, 0);
		public static readonly Double2 right = new Double2(1, 0);
		public static readonly Double2 up = new Double2(0, 1);
		public static readonly Double2 down = new Double2(0, -1);
	}
}