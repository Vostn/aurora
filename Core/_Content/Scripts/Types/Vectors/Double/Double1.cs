﻿namespace Aurora
{
	[System.Serializable]
	public struct Double1 : IVector<Double1>
	{
		public double x;
		
		public Double1(Double1 v)
		{
			x = v.x;
		}

		public Double1(double v)
		{
			x = v;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Double1 v = (Double1)obj;
			return v.x == x;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector4(Double1 v)
		{
			return new UnityEngine.Vector4((float)v.x, 0, 0, 0);
		}

		public static explicit operator UnityEngine.Vector3(Double1 v)
		{
			return new UnityEngine.Vector3((float)v.x, 0, 0);
		}

		public static explicit operator UnityEngine.Vector2(Double1 v)
		{
			return new UnityEngine.Vector2((float)v.x, 0);
		}

		public static explicit operator Double1(UnityEngine.Vector4 v)
		{
			return v.x;
		}

		public static explicit operator Double1(UnityEngine.Vector3 v)
		{
			return v.x;
		}

		public static explicit operator Double1(UnityEngine.Vector2 v)
		{
			return v.x;
		}

#endif

		public Double1 Value => this;

		public static implicit operator double(Double1 v)
		{
			return v.x;
		}

		public static implicit operator Double1(double v)
		{
			return new Double1(v);
		}

		public static Double1 operator +(Double1 v1, Double1 v2)
		{
			return new Double1(v1.x + v2.x);
		}

		void IArithmetic<Double1>.Add(Double1 v)
		{
			this += v;
		}

		Double1 IArithmetic<Double1>.Sum(Double1 v)
		{
			return this + v;
		}

		public static Double1 operator -(Double1 v1, Double1 v2)
		{
			return new Double1(v1.x - v2.x);
		}

		void IArithmetic<Double1>.Subtract(Double1 v)
		{
			this -= v;
		}

		Double1 IArithmetic<Double1>.Difference(Double1 v)
		{
			return this - v;
		}

		public static Double1 operator -(Double1 v)
		{
			return new Double1(-v.x);
		}

		Double1 IArithmetic<Double1>.Negative
		{
			get { return -this; }
		}

		public static Double1 operator *(Double1 v1, Double1 v2)
		{
			return new Double1(v1.x * v2.x);
		}

		void IArithmetic<Double1>.Multiply(Double1 v)
		{
			this *= v;
		}

		Double1 IArithmetic<Double1>.Product(Double1 v)
		{
			return this * v;
		}

		public static Double1 operator /(Double1 v1, Double1 v2)
		{
			return new Double1(v1.x / v2.x);
		}

		void IArithmetic<Double1>.Divide(Double1 v)
		{
			this /= v;
		}

		Double1 IArithmetic<Double1>.Quotient(Double1 v)
		{
			return this / v;
		}

		public static Double1 operator *(Double1 v1, double s)
		{
			return new Double1(v1.x * s);
		}

		void IVector<Double1>.Multiply(double v)
		{
			this *= v;
		}

		Double1 IVector<Double1>.Product(double v)
		{
			return this * v;
		}

		public static Double1 operator /(Double1 v1, double s)
		{
			return new Double1(v1.x / s);
		}

		void IVector<Double1>.Divide(double v)
		{
			this /= v;
		}

		Double1 IVector<Double1>.Quotient(double v)
		{
			return this / v;
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;
						
					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Double1 Pow(Double1 v, double p)
		{
			return new Double1(System.Math.Pow(v.x, p));
		}

		public static Double1 Clamp(Double1 v, double min, double max)
		{
			return new Double1(Math.Clamp(v.x, min, max));
		}

		public static Double1 Clamp(Double1 v, Double1 min, Double1 max)
		{
			return new Double1(Math.Clamp(v.x, min.x, max.x));
		}

		public static Double1 Repeat(Double1 v, double min, double max)
		{
			return new Double1(Math.Repeat(v.x, min, max));
		}

		public static Double1 Repeat(Double1 v, Double1 min, Double1 max)
		{
			return new Double1(Math.Repeat(v.x, min.x, max.x));
		}

		public static Double1 Min(Double1 v1, Double1 v2)
		{
			return new Double1(System.Math.Min(v1.x, v2.x));
		}

		public static Double1 Max(Double1 v1, Double1 v2)
		{
			return new Double1(System.Math.Max(v1.x, v2.x));
		}

		public double Magnitude
		{
			get { return System.Math.Abs(x); }
		}

		public Double1 Normalised
		{
			get { return this / Magnitude; }
		}

		public static Double1 Lerp(Double1 v1, Double1 v2, double t)
		{
			return v1 + ((v2 - v1) * t);
		}

		Double1 IInterpolable<Double1>.Interpolant(Double1 v2, double t)
		{
			return Lerp(this, v2, t);
		}

		Double1 ILerpable<Double1>.Lerpolant(Double1 v2, double t)
		{
			return Lerp(this, v2, t);
		}

		public static double Angle(Double1 v1, Double1 v2)
		{
			if (System.Math.Sign(v1.x) != System.Math.Sign(v2.x))
				return 180;
			return 0;
		}

		double IAnglable<Double1>.Angle(Double1 other)
		{
			return Angle(this, other);
		}

		public static bool operator ==(Double1 v1, Double1 v2)
		{
			return (v1.x == v2.x);
		}

		public static bool operator !=(Double1 v1, Double1 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x}";
		}

		public static readonly Double1 left = -1;
		public static readonly Double1 right = 1;
	}
}