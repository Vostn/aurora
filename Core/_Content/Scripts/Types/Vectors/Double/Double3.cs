﻿using System.ComponentModel;

namespace Aurora
{
	[System.Serializable]
	[TypeConverter(typeof(Converters.Double3_Converter))]
	public struct Double3 : IVector<Double3>, ISlerpable<Double3>
	{
		public double x, y, z;

		public int Dimensions => 3;

		public Double3(Double3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Double3(double x, double y, double z = 0)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public Double3(Double2 xy, double z = 0)
		{
			this.x = xy.x;
			this.y = xy.y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Double3 v = (Double3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector4(Double3 v)
		{
			return new UnityEngine.Vector4((float)v.x, (float)v.y, (float)v.z, 0);
		}

		public static explicit operator UnityEngine.Vector3(Double3 v)
		{
			return new UnityEngine.Vector3((float)v.x, (float)v.y, (float)v.z);
		}

		public static explicit operator UnityEngine.Vector2(Double3 v)
		{
			return new UnityEngine.Vector2((float)v.x, (float)v.y);
		}

		public static explicit operator Double3(UnityEngine.Vector4 v)
		{
			return new Double3(v.x, v.y, v.z);
		}

		public static implicit operator Double3(UnityEngine.Vector3 v)
		{
			return new Double3(v.x, v.y, v.z);
		}

		public static implicit operator Double3(UnityEngine.Vector2 v)
		{
			return new Double3(v.x, v.y, 0);
		}

		public static explicit operator UnityEngine.Color(Double3 v)
		{
			return new UnityEngine.Color((float)v.x, (float)v.y, (float)v.z);
		}

		public static explicit operator Double3(UnityEngine.Color c)
		{
			return new Double3(c.r, c.g, c.b);
		}

#endif

		public Double3 Value => this;

		public static explicit operator Double3(Double4 v)
		{
			return new Double3(v.x, v.y, v.z);
		}

		public static implicit operator Double3(Double2 v)
		{
			return new Double3(v.x, v.y, 0);
		}

		public static Double3 operator +(Double3 v1, Double3 v2)
		{
			return new Double3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		void IArithmetic<Double3>.Add(Double3 v)
		{
			this += v;
		}

		Double3 IArithmetic<Double3>.Sum(Double3 v)
		{
			return this + v;
		}

		public static Double3 operator -(Double3 v1, Double3 v2)
		{
			return new Double3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		void IArithmetic<Double3>.Subtract(Double3 v)
		{
			this -= v;
		}

		Double3 IArithmetic<Double3>.Difference(Double3 v)
		{
			return this - v;
		}

		public static Double3 operator -(Double3 v)
		{
			return new Double3(-v.x, -v.y, -v.z);
		}

		Double3 IArithmetic<Double3>.Negative
		{
			get { return -this; }
		}

		public static Double3 operator *(Double3 v1, Double3 v2)
		{
			return new Double3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		void IArithmetic<Double3>.Multiply(Double3 v)
		{
			this *= v;
		}

		Double3 IArithmetic<Double3>.Product(Double3 v)
		{
			return this * v;
		}

		public static Double3 operator /(Double3 v1, Double3 v2)
		{
			return new Double3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		void IArithmetic<Double3>.Divide(Double3 v)
		{
			this /= v;
		}

		Double3 IArithmetic<Double3>.Quotient(Double3 v)
		{
			return this / v;
		}

		public static Double3 operator *(Double3 v1, double s)
		{
			return new Double3(v1.x * s, v1.y * s, v1.z * s);
		}

		void IVector<Double3>.Multiply(double v)
		{
			this *= v;
		}

		Double3 IVector<Double3>.Product(double v)
		{
			return this * v;
		}

		public static Double3 operator /(Double3 v1, double s)
		{
			return new Double3(v1.x / s, v1.y / s, v1.z / s);
		}

		void IVector<Double3>.Divide(double v)
		{
			this /= v;
		}

		Double3 IVector<Double3>.Quotient(double v)
		{
			return this / v;
		}

		public static Double3 Uniform(double s)
		{
			return new Double3(s, s, s);
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Double3 Pow(Double3 v, double p)
		{
			return new Double3(System.Math.Pow(v.x, p), System.Math.Pow(v.y, p), System.Math.Pow(v.z, p));
		}

		public static Double3 Clamp(Double3 v, double min, double max)
		{
			return new Double3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static Double3 Clamp(Double3 v, Double3 min, Double3 max)
		{
			return new Double3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static Double3 Repeat(Double3 v, double min, double max)
		{
			return new Double3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static Double3 Repeat(Double3 v, Double3 min, Double3 max)
		{
			return new Double3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static Double3 Min(Double3 v1, Double3 v2)
		{
			return new Double3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static Double3 Max(Double3 v1, Double3 v2)
		{
			return new Double3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public double MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public double MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double MagnitudeNative
		{
			get { return Magnitude; }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public Double3 Normalised
		{
			get { return this / Magnitude; }
		}

		public static double Dot(Double3 v1, Double3 v2)
		{
			return v1.x * v2.x + v1.y * v2.y + v1.z * v2.z;
		}

		public static Double3 Lerp(Double3 v1, Double3 v2, double t)
		{
			return v1 + ((v2 - v1) * t);
		}

		Double3 ILerpable<Double3>.Lerpolant(Double3 v2, double t)
		{
			return Lerp(this, v2, t);
		}

		public Double3 Interpolant(Double3 v2, double t)
		{
			return Lerp(this, v2, t);
		}

		public static Double3 Slerp(Double3 v1, Double3 v2, double t)
		{
			double dot = Math.Clamp(Dot(v1, v2), -1, 1);
			double theta = System.Math.Acos(dot) * t;
			Double3 relativeVec = (v1 - v2 * dot);
			return (v1 * System.Math.Cos(theta)) + (relativeVec * System.Math.Sin(theta));
		}

		Double3 ISlerpable<Double3>.Slerpolant(Double3 v2, double t)
		{
			return Slerp(this, v2, t);
		}

		public double Angle(Double3 v)
		{
			double denominator = System.Math.Sqrt(v.SqrMagnitude * SqrMagnitude);
			if (denominator <= 0)
				return 0;

			double dot = Math.Clamp(Dot(v, this) / denominator, -1, 1);
			return System.Math.Acos(dot) * Math.RadToDeg;
		}

		double IAnglable<Double3>.Angle(Double3 other)
		{
			return Angle(other);
		}

		public static bool operator ==(Double3 v1, Double3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Double3 v1, Double3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Double3 left = new Double3(-1, 0, 0);
		public static readonly Double3 right = new Double3(1, 0, 0);
		public static readonly Double3 up = new Double3(0, 1, 0);
		public static readonly Double3 down = new Double3(0, -1, 0);
		public static readonly Double3 forward = new Double3(0, 0, 1);
		public static readonly Double3 back = new Double3(0, 0, -1);
	}
}