﻿namespace Aurora
{
	[System.Serializable]
	public struct Double4
	{
		public double x, y, z, w;

		public Double4(Double4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Double4(double x, double y, double z = 0, double w = 0)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Double4 v = (Double4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Vector4(Double4 v)
		{
			return new UnityEngine.Vector4((float)v.x, (float)v.y, (float)v.z, (float)v.w);
		}

		public static explicit operator UnityEngine.Vector3(Double4 v)
		{
			return new UnityEngine.Vector3((float)v.x, (float)v.y, (float)v.z);
		}

		public static explicit operator UnityEngine.Vector2(Double4 v)
		{
			return new UnityEngine.Vector2((float)v.x, (float)v.y);
		}

		public static implicit operator Double4(UnityEngine.Vector4 v)
		{
			return new Double4(v.x, v.y, v.z, v.w);
		}

		public static implicit operator Double4(UnityEngine.Vector3 v)
		{
			return new Double4(v.x, v.y, v.z, 0);
		}

		public static implicit operator Double4(UnityEngine.Vector2 v)
		{
			return new Double4(v.x, v.y, 0, 0);
		}

		public static explicit operator UnityEngine.Color(Double4 v)
		{
			return new UnityEngine.Color((float)v.x, (float)v.y, (float)v.z, (float)v.w);
		}

		public static implicit operator Double4(UnityEngine.Color c)
		{
			return new Double4(c.r, c.g, c.b, c.a);
		}

#endif

		public static implicit operator Double4(Int4 v)
		{
			return new Double4(v.x, v.y, v.z, v.w);
		}

		public static implicit operator Double4(Double3 v)
		{
			return new Double4(v.x, v.y, v.z, 0);
		}

		public static Double4 operator +(Double4 v1, Double4 v2)
		{
			return new Double4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static Double4 operator -(Double4 v1, Double4 v2)
		{
			return new Double4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static Double4 operator *(Double4 v1, Double4 v2)
		{
			return new Double4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static Double4 operator /(Double4 v1, Double4 v2)
		{
			return new Double4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static Double4 operator *(Double4 v1, double s)
		{
			return new Double4(v1.x * s, v1.y * s, v1.z * s, v1.w * s);
		}

		public static Double4 operator /(Double4 v1, double s)
		{
			return new Double4(v1.x / s, v1.y / s, v1.z / s, v1.w / s);
		}

		public static Double4 Uniform(double s)
		{
			return new Double4(s, s, s, s);
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Double4 Pow(Double4 v, double p)
		{
			return new Double4(System.Math.Pow(v.x, p), System.Math.Pow(v.y, p), System.Math.Pow(v.z, p), System.Math.Pow(v.w, p));
		}

		public static Double4 Clamp(Double4 v, double min, double max)
		{
			return new Double4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static Double4 Clamp(Double4 v, Double4 min, Double4 max)
		{
			return new Double4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static Double4 Repeat(Double4 v, double min, double max)
		{
			return new Double4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static Double4 Repeat(Double4 v, Double4 min, Double4 max)
		{
			return new Double4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static Double4 Min(Double4 v1, Double4 v2)
		{
			return new Double4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static Double4 Max(Double4 v1, Double4 v2)
		{
			return new Double4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public double MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public double MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(Double4 v1, Double4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Double4 v1, Double4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Double4 left = new Double4(-1, 0, 0, 0);
		public static readonly Double4 right = new Double4(1, 0, 0, 0);
		public static readonly Double4 up = new Double4(0, 1, 0, 0);
		public static readonly Double4 down = new Double4(0, -1, 0, 0);
		public static readonly Double4 forward = new Double4(0, 0, 1, 0);
		public static readonly Double4 back = new Double4(0, 0, -1, 0);
		public static readonly Double4 ana = new Double4(0, 0, 0, 1);
		public static readonly Double4 kata = new Double4(0, 0, 0, -1);
	}
}