﻿namespace Aurora
{
	[System.Serializable]
	public struct ULong2
	{
		public ulong x, y;

		public ULong2(ULong2 v)
		{
			x = v.x;
			y = v.y;
		}

		public ULong2(ulong x, ulong y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			ULong2 v = (ULong2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(ULong2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(ULong2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(ULong2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator ULong2(UnityEngine.Vector4 v)
		{
			return new ULong2((ulong)v.x, (ulong)v.y);
		}

		public static explicit operator ULong2(UnityEngine.Vector3 v)
		{
			return new ULong2((ulong)v.x, (ulong)v.y);
		}

		public static explicit operator ULong2(UnityEngine.Vector2 v)
		{
			return new ULong2((ulong)v.x, (ulong)v.y);
		}

#endif

		public static ULong2 operator +(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x + v2.x, v1.y + v2.y);
		}

		public static ULong2 operator -(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x - v2.x, v1.y - v2.y);
		}

		public static ULong2 operator *(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x * v2.x, v1.y * v2.y);
		}

		public static ULong2 operator /(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x / v2.x, v1.y / v2.y);
		}

		public static ULong2 operator *(ULong2 v1, double s)
		{
			return new ULong2((ulong)(v1.x * s), (ulong)(v1.y * s));
		}

		public static ULong2 operator /(ULong2 v1, double s)
		{
			return new ULong2((ulong)(v1.x / s), (ulong)(v1.y / s));
		}

		public static ULong2 operator &(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x & v2.x, v1.y & v2.y);
		}

		public static ULong2 operator |(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x | v2.x, v1.y | v2.y);
		}

		public static ULong2 operator ^(ULong2 v1, ULong2 v2)
		{
			return new ULong2(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		public static ULong2 operator ~(ULong2 v)
		{
			return new ULong2(~v.x, ~v.y);
		}

		public static ULong2 Uniform(ulong s)
		{
			return new ULong2(s, s);
		}

		public ulong this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static ULong2 Clamp(ULong2 v, ulong min, ulong max)
		{
			return new ULong2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static ULong2 Clamp(ULong2 v, ULong2 min, ULong2 max)
		{
			return new ULong2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static ULong2 Repeat(ULong2 v, ulong min, ulong max)
		{
			return new ULong2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static ULong2 Repeat(ULong2 v, ULong2 min, ULong2 max)
		{
			return new ULong2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static ULong2 Min(ULong2 v1, ULong2 v2)
		{
			return new ULong2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static ULong2 Max(ULong2 v1, ULong2 v2)
		{
			return new ULong2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public ulong MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public ulong MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(ULong2 v1, ULong2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(ULong2 v1, ULong2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly ULong2 right = new ULong2(1, 0);
		public static readonly ULong2 up = new ULong2(0, 1);
	}
}