﻿namespace Aurora
{
	[System.Serializable]
	public struct ULong4
	{
		public ulong x, y, z, w;

		public ULong4(ULong4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public ULong4(ulong x, ulong y, ulong z, ulong w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			ULong4 v = (ULong4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(ULong4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(ULong4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(ULong4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator ULong4(UnityEngine.Vector4 v)
		{
			return new ULong4((ulong)v.x, (ulong)v.y, (ulong)v.z, (ulong)v.w);
		}

		public static explicit operator ULong4(UnityEngine.Vector3 v)
		{
			return new ULong4((ulong)v.x, (ulong)v.y, (ulong)v.z, 0);
		}

		public static explicit operator ULong4(UnityEngine.Vector2 v)
		{
			return new ULong4((ulong)v.x, (ulong)v.y, 0, 0);
		}

#endif

		public static ULong4 operator +(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static ULong4 operator -(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static ULong4 operator *(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static ULong4 operator /(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static ULong4 operator *(ULong4 v1, double s)
		{
			return new ULong4((ulong)(v1.x * s), (ulong)(v1.y * s), (ulong)(v1.z * s), (ulong)(v1.w * s));
		}

		public static ULong4 operator /(ULong4 v1, double s)
		{
			return new ULong4((ulong)(v1.x / s), (ulong)(v1.y / s), (ulong)(v1.z / s), (ulong)(v1.w / s));
		}

		public static ULong4 operator &(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		public static ULong4 operator |(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		public static ULong4 operator ^(ULong4 v1, ULong4 v2)
		{
			return new ULong4(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		public static ULong4 operator ~(ULong4 v)
		{
			return new ULong4(~v.x, ~v.y, ~v.z, ~v.w);
		}

		public static ULong4 Uniform(ulong s)
		{
			return new ULong4(s, s, s, s);
		}

		public ulong this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static ULong4 Clamp(ULong4 v, ulong min, ulong max)
		{
			return new ULong4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static ULong4 Clamp(ULong4 v, ULong4 min, ULong4 max)
		{
			return new ULong4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static ULong4 Repeat(ULong4 v, ulong min, ulong max)
		{
			return new ULong4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static ULong4 Repeat(ULong4 v, ULong4 min, ULong4 max)
		{
			return new ULong4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static ULong4 Min(ULong4 v1, ULong4 v2)
		{
			return new ULong4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static ULong4 Max(ULong4 v1, ULong4 v2)
		{
			return new ULong4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public ulong MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public ulong MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(ULong4 v1, ULong4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(ULong4 v1, ULong4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly ULong4 right = new ULong4(1, 0, 0, 0);
		public static readonly ULong4 up = new ULong4(0, 1, 0, 0);
		public static readonly ULong4 forward = new ULong4(0, 0, 1, 0);
		public static readonly ULong4 ana = new ULong4(0, 0, 0, 1);
	}
}