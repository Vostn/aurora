﻿namespace Aurora
{
	[System.Serializable]
	public struct Long4
	{
		public long x, y, z, w;

		public Long4(Long4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Long4(long x, long y, long z, long w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Long4 v = (Long4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Long4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(Long4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Long4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Long4(UnityEngine.Vector4 v)
		{
			return new Long4((long)v.x, (long)v.y, (long)v.z, (long)v.w);
		}

		public static explicit operator Long4(UnityEngine.Vector3 v)
		{
			return new Long4((long)v.x, (long)v.y, (long)v.z, 0);
		}

		public static explicit operator Long4(UnityEngine.Vector2 v)
		{
			return new Long4((long)v.x, (long)v.y, 0, 0);
		}

#endif

		public static Long4 operator +(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static Long4 operator -(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static Long4 operator *(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static Long4 operator /(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static Long4 operator *(Long4 v1, double s)
		{
			return new Long4((long)(v1.x * s), (long)(v1.y * s), (long)(v1.z * s), (long)(v1.w * s));
		}

		public static Long4 operator /(Long4 v1, double s)
		{
			return new Long4((long)(v1.x / s), (long)(v1.y / s), (long)(v1.z / s), (long)(v1.w / s));
		}

		public static Long4 operator &(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		public static Long4 operator |(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		public static Long4 operator ^(Long4 v1, Long4 v2)
		{
			return new Long4(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		public static Long4 operator ~(Long4 v)
		{
			return new Long4(~v.x, ~v.y, ~v.z, ~v.w);
		}

		public static Long4 Uniform(long s)
		{
			return new Long4(s, s, s, s);
		}

		public long this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Long4 Clamp(Long4 v, long min, long max)
		{
			return new Long4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static Long4 Clamp(Long4 v, Long4 min, Long4 max)
		{
			return new Long4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static Long4 Repeat(Long4 v, long min, long max)
		{
			return new Long4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static Long4 Repeat(Long4 v, Long4 min, Long4 max)
		{
			return new Long4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static Long4 Min(Long4 v1, Long4 v2)
		{
			return new Long4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static Long4 Max(Long4 v1, Long4 v2)
		{
			return new Long4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public long MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public long MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(Long4 v1, Long4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Long4 v1, Long4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Long4 left = new Long4(-1, 0, 0, 0);
		public static readonly Long4 right = new Long4(1, 0, 0, 0);
		public static readonly Long4 up = new Long4(0, 1, 0, 0);
		public static readonly Long4 down = new Long4(0, -1, 0, 0);
		public static readonly Long4 forward = new Long4(0, 0, 1, 0);
		public static readonly Long4 back = new Long4(0, 0, -1, 0);
		public static readonly Long4 ana = new Long4(0, 0, 0, 1);
		public static readonly Long4 kata = new Long4(0, 0, 0, -1);
	}
}