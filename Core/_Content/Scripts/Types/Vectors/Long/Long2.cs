﻿namespace Aurora
{
	[System.Serializable]
	public struct Long2
	{
		public long x, y;

		public Long2(Long2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Long2(long x, long y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Long2 v = (Long2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Long2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Long2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(Long2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Long2(UnityEngine.Vector4 v)
		{
			return new Long2((long)v.x, (long)v.y);
		}

		public static explicit operator Long2(UnityEngine.Vector3 v)
		{
			return new Long2((long)v.x, (long)v.y);
		}

		public static explicit operator Long2(UnityEngine.Vector2 v)
		{
			return new Long2((long)v.x, (long)v.y);
		}

#endif

		public static Long2 operator +(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x + v2.x, v1.y + v2.y);
		}

		public static Long2 operator -(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x - v2.x, v1.y - v2.y);
		}

		public static Long2 operator *(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x * v2.x, v1.y * v2.y);
		}

		public static Long2 operator /(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x / v2.x, v1.y / v2.y);
		}

		public static Long2 operator *(Long2 v1, double s)
		{
			return new Long2((long)(v1.x * s), (long)(v1.y * s));
		}

		public static Long2 operator /(Long2 v1, double s)
		{
			return new Long2((long)(v1.x / s), (long)(v1.y / s));
		}

		public static Long2 operator &(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x & v2.x, v1.y & v2.y);
		}

		public static Long2 operator |(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x | v2.x, v1.y | v2.y);
		}

		public static Long2 operator ^(Long2 v1, Long2 v2)
		{
			return new Long2(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		public static Long2 operator ~(Long2 v)
		{
			return new Long2(~v.x, ~v.y);
		}

		public static Long2 Uniform(long s)
		{
			return new Long2(s, s);
		}

		public long this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Long2 Clamp(Long2 v, long min, long max)
		{
			return new Long2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static Long2 Clamp(Long2 v, Long2 min, Long2 max)
		{
			return new Long2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static Long2 Repeat(Long2 v, long min, long max)
		{
			return new Long2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static Long2 Repeat(Long2 v, Long2 min, Long2 max)
		{
			return new Long2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static Long2 Min(Long2 v1, Long2 v2)
		{
			return new Long2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static Long2 Max(Long2 v1, Long2 v2)
		{
			return new Long2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public long MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public long MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(Long2 v1, Long2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Long2 v1, Long2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Long2 left = new Long2(-1, 0);
		public static readonly Long2 right = new Long2(1, 0);
		public static readonly Long2 up = new Long2(0, 1);
		public static readonly Long2 down = new Long2(0, -1);
	}
}