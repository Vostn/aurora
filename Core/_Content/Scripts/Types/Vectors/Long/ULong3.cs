﻿namespace Aurora
{
	[System.Serializable]
	public struct ULong3
	{
		public ulong x, y, z;

		public ULong3(ULong3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public ULong3(ulong x, ulong y, ulong z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			ULong3 v = (ULong3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(ULong3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(ULong3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(ULong3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator ULong3(UnityEngine.Vector4 v)
		{
			return new ULong3((ulong)v.x, (ulong)v.y, (ulong)v.z);
		}

		public static explicit operator ULong3(UnityEngine.Vector3 v)
		{
			return new ULong3((ulong)v.x, (ulong)v.y, (ulong)v.z);
		}

		public static explicit operator ULong3(UnityEngine.Vector2 v)
		{
			return new ULong3((ulong)v.x, (ulong)v.y, 0);
		}

#endif

		public static ULong3 operator +(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		public static ULong3 operator -(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		public static ULong3 operator *(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		public static ULong3 operator /(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		public static ULong3 operator *(ULong3 v1, double s)
		{
			return new ULong3((ulong)(v1.x * s), (ulong)(v1.y * s), (ulong)(v1.z * s));
		}

		public static ULong3 operator /(ULong3 v1, double s)
		{
			return new ULong3((ulong)(v1.x / s), (ulong)(v1.y / s), (ulong)(v1.z / s));
		}

		public static ULong3 operator &(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		public static ULong3 operator |(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		public static ULong3 operator ^(ULong3 v1, ULong3 v2)
		{
			return new ULong3(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		public static ULong3 operator ~(ULong3 v)
		{
			return new ULong3(~v.x, ~v.y, ~v.z);
		}

		public static ULong3 Uniform(ulong s)
		{
			return new ULong3(s, s, s);
		}

		public ulong this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static ULong3 Clamp(ULong3 v, ulong min, ulong max)
		{
			return new ULong3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static ULong3 Clamp(ULong3 v, ULong3 min, ULong3 max)
		{
			return new ULong3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static ULong3 Repeat(ULong3 v, ulong min, ulong max)
		{
			return new ULong3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static ULong3 Repeat(ULong3 v, ULong3 min, ULong3 max)
		{
			return new ULong3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static ULong3 Min(ULong3 v1, ULong3 v2)
		{
			return new ULong3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static ULong3 Max(ULong3 v1, ULong3 v2)
		{
			return new ULong3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public ulong MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public ulong MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(ULong3 v1, ULong3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(ULong3 v1, ULong3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly ULong3 right = new ULong3(1, 0, 0);
		public static readonly ULong3 up = new ULong3(0, 1, 0);
		public static readonly ULong3 forward = new ULong3(0, 0, 1);
	}
}