﻿namespace Aurora
{
	[System.Serializable]
	public struct Long3
	{
		public long x, y, z;

		public Long3(Long3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Long3(long x, long y, long z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Long3 v = (Long3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Long3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(Long3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Long3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Long3(UnityEngine.Vector4 v)
		{
			return new Long3((long)v.x, (long)v.y, (long)v.z);
		}

		public static explicit operator Long3(UnityEngine.Vector3 v)
		{
			return new Long3((long)v.x, (long)v.y, (long)v.z);
		}

		public static explicit operator Long3(UnityEngine.Vector2 v)
		{
			return new Long3((long)v.x, (long)v.y, 0);
		}

#endif

		public static Long3 operator +(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		public static Long3 operator -(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		public static Long3 operator *(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		public static Long3 operator /(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		public static Long3 operator *(Long3 v1, double s)
		{
			return new Long3((long)(v1.x * s), (long)(v1.y * s), (long)(v1.z * s));
		}

		public static Long3 operator /(Long3 v1, double s)
		{
			return new Long3((long)(v1.x / s), (long)(v1.y / s), (long)(v1.z / s));
		}

		public static Long3 operator &(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		public static Long3 operator |(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		public static Long3 operator ^(Long3 v1, Long3 v2)
		{
			return new Long3(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		public static Long3 operator ~(Long3 v)
		{
			return new Long3(~v.x, ~v.y, ~v.z);
		}

		public static Long3 Uniform(long s)
		{
			return new Long3(s, s, s);
		}

		public long this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Long3 Clamp(Long3 v, long min, long max)
		{
			return new Long3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static Long3 Clamp(Long3 v, Long3 min, Long3 max)
		{
			return new Long3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static Long3 Repeat(Long3 v, long min, long max)
		{
			return new Long3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static Long3 Repeat(Long3 v, Long3 min, Long3 max)
		{
			return new Long3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static Long3 Min(Long3 v1, Long3 v2)
		{
			return new Long3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static Long3 Max(Long3 v1, Long3 v2)
		{
			return new Long3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public long MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public long MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(Long3 v1, Long3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Long3 v1, Long3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Long3 left = new Long3(-1, 0, 0);
		public static readonly Long3 right = new Long3(1, 0, 0);
		public static readonly Long3 up = new Long3(0, 1, 0);
		public static readonly Long3 down = new Long3(0, -1, 0);
		public static readonly Long3 forward = new Long3(0, 0, 1);
		public static readonly Long3 back = new Long3(0, 0, -1);
	}
}