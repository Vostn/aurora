﻿namespace Aurora
{
	[System.Serializable]
	public struct UInt2
	{
		public uint x, y;

		public UInt2(UInt2 v)
		{
			x = v.x;
			y = v.y;
		}

		public UInt2(uint x, uint y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			UInt2 v = (UInt2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(UInt2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(UInt2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(UInt2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator UInt2(UnityEngine.Vector4 v)
		{
			return new UInt2((uint)v.x, (uint)v.y);
		}

		public static explicit operator UInt2(UnityEngine.Vector3 v)
		{
			return new UInt2((uint)v.x, (uint)v.y);
		}

		public static explicit operator UInt2(UnityEngine.Vector2 v)
		{
			return new UInt2((uint)v.x, (uint)v.y);
		}

#endif

		public static UInt2 operator +(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x + v2.x, v1.y + v2.y);
		}

		public static UInt2 operator -(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x - v2.x, v1.y - v2.y);
		}

		public static UInt2 operator *(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x * v2.x, v1.y * v2.y);
		}

		public static UInt2 operator /(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x / v2.x, v1.y / v2.y);
		}

		public static UInt2 operator *(UInt2 v1, double s)
		{
			return new UInt2((uint)(v1.x * s), (uint)(v1.y * s));
		}

		public static UInt2 operator /(UInt2 v1, double s)
		{
			return new UInt2((uint)(v1.x / s), (uint)(v1.y / s));
		}

		public static UInt2 operator &(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x & v2.x, v1.y & v2.y);
		}

		public static UInt2 operator |(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x | v2.x, v1.y | v2.y);
		}

		public static UInt2 operator ^(UInt2 v1, UInt2 v2)
		{
			return new UInt2(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		public static UInt2 operator ~(UInt2 v)
		{
			return new UInt2(~v.x, ~v.y);
		}

		public static UInt2 Uniform(uint s)
		{
			return new UInt2(s, s);
		}

		public uint this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static UInt2 Clamp(UInt2 v, uint min, uint max)
		{
			return new UInt2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static UInt2 Clamp(UInt2 v, UInt2 min, UInt2 max)
		{
			return new UInt2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static UInt2 Repeat(UInt2 v, uint min, uint max)
		{
			return new UInt2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static UInt2 Repeat(UInt2 v, UInt2 min, UInt2 max)
		{
			return new UInt2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static UInt2 Min(UInt2 v1, UInt2 v2)
		{
			return new UInt2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static UInt2 Max(UInt2 v1, UInt2 v2)
		{
			return new UInt2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public uint MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public uint MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(UInt2 v1, UInt2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(UInt2 v1, UInt2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly UInt2 right = new UInt2(1, 0);
		public static readonly UInt2 up = new UInt2(0, 1);
	}
}