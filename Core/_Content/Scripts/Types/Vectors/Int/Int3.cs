﻿namespace Aurora
{
	[System.Serializable]
	public struct Int3
	{
		public int x, y, z;

		public Int3(Int3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public Int3(int x, int y, int z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Int3 v = (Int3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Int3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(Int3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Int3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Int3(UnityEngine.Vector4 v)
		{
			return new Int3((int)v.x, (int)v.y, (int)v.z);
		}

		public static explicit operator Int3(UnityEngine.Vector3 v)
		{
			return new Int3((int)v.x, (int)v.y, (int)v.z);
		}

		public static explicit operator Int3(UnityEngine.Vector2 v)
		{
			return new Int3((int)v.x, (int)v.y, 0);
		}

#endif

		public static Int3 operator +(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		public static Int3 operator -(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		public static Int3 operator *(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		public static Int3 operator /(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		public static Int3 operator *(Int3 v1, double s)
		{
			return new Int3((int)(v1.x * s), (int)(v1.y * s), (int)(v1.z * s));
		}

		public static Int3 operator /(Int3 v1, double s)
		{
			return new Int3((int)(v1.x / s), (int)(v1.y / s), (int)(v1.z / s));
		}

		public static Int3 operator &(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		public static Int3 operator |(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		public static Int3 operator ^(Int3 v1, Int3 v2)
		{
			return new Int3(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		public static Int3 operator ~(Int3 v)
		{
			return new Int3(~v.x, ~v.y, ~v.z);
		}

		public static Int3 Uniform(int s)
		{
			return new Int3(s, s, s);
		}

		public int this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Int3 Clamp(Int3 v, int min, int max)
		{
			return new Int3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static Int3 Clamp(Int3 v, Int3 min, Int3 max)
		{
			return new Int3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static Int3 Repeat(Int3 v, int min, int max)
		{
			return new Int3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static Int3 Repeat(Int3 v, Int3 min, Int3 max)
		{
			return new Int3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static Int3 Min(Int3 v1, Int3 v2)
		{
			return new Int3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static Int3 Max(Int3 v1, Int3 v2)
		{
			return new Int3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public int MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public int MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(Int3 v1, Int3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(Int3 v1, Int3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly Int3 left = new Int3(-1, 0, 0);
		public static readonly Int3 right = new Int3(1, 0, 0);
		public static readonly Int3 up = new Int3(0, 1, 0);
		public static readonly Int3 down = new Int3(0, -1, 0);
		public static readonly Int3 forward = new Int3(0, 0, 1);
		public static readonly Int3 back = new Int3(0, 0, -1);
	}
}