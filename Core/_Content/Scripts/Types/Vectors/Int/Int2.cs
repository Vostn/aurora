﻿namespace Aurora
{
	[System.Serializable]
	public struct Int2
	{
		public int x, y;

		public Int2(Int2 v)
		{
			x = v.x;
			y = v.y;
		}

		public Int2(int x, int y)
		{
			this.x = x;
			this.y = y;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Int2 v = (Int2)obj;
			return v.x == x && v.y == y;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Int2 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, 0, 0);
		}

		public static implicit operator UnityEngine.Vector3(Int2 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, 0);
		}

		public static implicit operator UnityEngine.Vector2(Int2 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Int2(UnityEngine.Vector4 v)
		{
			return new Int2((int)v.x, (int)v.y);
		}

		public static explicit operator Int2(UnityEngine.Vector3 v)
		{
			return new Int2((int)v.x, (int)v.y);
		}

		public static explicit operator Int2(UnityEngine.Vector2 v)
		{
			return new Int2((int)v.x, (int)v.y);
		}

#endif

		public static Int2 operator +(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x + v2.x, v1.y + v2.y);
		}

		public static Int2 operator -(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x - v2.x, v1.y - v2.y);
		}

		public static Int2 operator *(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x * v2.x, v1.y * v2.y);
		}

		public static Int2 operator /(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x / v2.x, v1.y / v2.y);
		}

		public static Int2 operator *(Int2 v1, double s)
		{
			return new Int2((int)(v1.x * s), (int)(v1.y * s));
		}

		public static Int2 operator /(Int2 v1, double s)
		{
			return new Int2((int)(v1.x / s), (int)(v1.y / s));
		}

		public static Int2 operator &(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x & v2.x, v1.y & v2.y);
		}

		public static Int2 operator |(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x | v2.x, v1.y | v2.y);
		}

		public static Int2 operator ^(Int2 v1, Int2 v2)
		{
			return new Int2(v1.x ^ v2.x, v1.y ^ v2.y);
		}

		public static Int2 operator ~(Int2 v)
		{
			return new Int2(~v.x, ~v.y);
		}

		public static Int2 Uniform(int s)
		{
			return new Int2(s, s);
		}

		public int this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Int2 Clamp(Int2 v, int min, int max)
		{
			return new Int2(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max));
		}

		public static Int2 Clamp(Int2 v, Int2 min, Int2 max)
		{
			return new Int2(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y));
		}

		public static Int2 Repeat(Int2 v, int min, int max)
		{
			return new Int2(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max));
		}

		public static Int2 Repeat(Int2 v, Int2 min, Int2 max)
		{
			return new Int2(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y));
		}

		public static Int2 Min(Int2 v1, Int2 v2)
		{
			return new Int2(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y));
		}

		public static Int2 Max(Int2 v1, Int2 v2)
		{
			return new Int2(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y));
		}

		public int MinVal
		{
			get { return System.Math.Min(x, y); }
		}

		public int MaxVal
		{
			get { return System.Math.Max(x, y); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y; }
		}

		public static bool operator ==(Int2 v1, Int2 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y);
		}

		public static bool operator !=(Int2 v1, Int2 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y}";
		}

		public static readonly Int2 left = new Int2(-1, 0);
		public static readonly Int2 right = new Int2(1, 0);
		public static readonly Int2 up = new Int2(0, 1);
		public static readonly Int2 down = new Int2(0, -1);
	}
}