﻿namespace Aurora
{
	[System.Serializable]
	public struct Int4
	{
		public int x, y, z, w;

		public Int4(Int4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public Int4(int x, int y, int z, int w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Int4 v = (Int4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(Int4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(Int4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(Int4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator Int4(UnityEngine.Vector4 v)
		{
			return new Int4((int)v.x, (int)v.y, (int)v.z, (int)v.w);
		}

		public static explicit operator Int4(UnityEngine.Vector3 v)
		{
			return new Int4((int)v.x, (int)v.y, (int)v.z, 0);
		}

		public static explicit operator Int4(UnityEngine.Vector2 v)
		{
			return new Int4((int)v.x, (int)v.y, 0, 0);
		}

#endif

		public static Int4 operator +(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static Int4 operator -(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static Int4 operator *(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static Int4 operator /(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static Int4 operator *(Int4 v1, double s)
		{
			return new Int4((int)(v1.x * s), (int)(v1.y * s), (int)(v1.z * s), (int)(v1.w * s));
		}

		public static Int4 operator /(Int4 v1, double s)
		{
			return new Int4((int)(v1.x / s), (int)(v1.y / s), (int)(v1.z / s), (int)(v1.w / s));
		}

		public static Int4 operator &(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		public static Int4 operator |(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		public static Int4 operator ^(Int4 v1, Int4 v2)
		{
			return new Int4(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		public static Int4 operator ~(Int4 v)
		{
			return new Int4(~v.x, ~v.y, ~v.z, ~v.w);
		}

		public static Int4 Uniform(int s)
		{
			return new Int4(s, s, s, s);
		}

		public int this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static Int4 Clamp(Int4 v, int min, int max)
		{
			return new Int4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static Int4 Clamp(Int4 v, Int4 min, Int4 max)
		{
			return new Int4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static Int4 Repeat(Int4 v, int min, int max)
		{
			return new Int4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static Int4 Repeat(Int4 v, Int4 min, Int4 max)
		{
			return new Int4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static Int4 Min(Int4 v1, Int4 v2)
		{
			return new Int4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static Int4 Max(Int4 v1, Int4 v2)
		{
			return new Int4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public int MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public int MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(Int4 v1, Int4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(Int4 v1, Int4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly Int4 left = new Int4(-1, 0, 0, 0);
		public static readonly Int4 right = new Int4(1, 0, 0, 0);
		public static readonly Int4 up = new Int4(0, 1, 0, 0);
		public static readonly Int4 down = new Int4(0, -1, 0, 0);
		public static readonly Int4 forward = new Int4(0, 0, 1, 0);
		public static readonly Int4 back = new Int4(0, 0, -1, 0);
		public static readonly Int4 ana = new Int4(0, 0, 0, 1);
		public static readonly Int4 kata = new Int4(0, 0, 0, -1);
	}
}