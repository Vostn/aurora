﻿namespace Aurora
{
	[System.Serializable]
	public struct UInt4
	{
		public uint x, y, z, w;

		public UInt4(UInt4 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
			w = v.w;
		}

		public UInt4(uint x, uint y, uint z, uint w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			UInt4 v = (UInt4)obj;
			return v.x == x && v.y == y && v.z == z && v.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(UInt4 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, v.w);
		}

		public static explicit operator UnityEngine.Vector3(UInt4 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(UInt4 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator UInt4(UnityEngine.Vector4 v)
		{
			return new UInt4((uint)v.x, (uint)v.y, (uint)v.z, (uint)v.w);
		}

		public static explicit operator UInt4(UnityEngine.Vector3 v)
		{
			return new UInt4((uint)v.x, (uint)v.y, (uint)v.z, 0);
		}

		public static explicit operator UInt4(UnityEngine.Vector2 v)
		{
			return new UInt4((uint)v.x, (uint)v.y, 0, 0);
		}

#endif

		public static UInt4 operator +(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z, v1.w + v2.w);
		}

		public static UInt4 operator -(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z, v1.w - v2.w);
		}

		public static UInt4 operator *(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z, v1.w * v2.w);
		}

		public static UInt4 operator /(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z, v1.w / v2.w);
		}

		public static UInt4 operator *(UInt4 v1, double s)
		{
			return new UInt4((uint)(v1.x * s), (uint)(v1.y * s), (uint)(v1.z * s), (uint)(v1.w * s));
		}

		public static UInt4 operator /(UInt4 v1, double s)
		{
			return new UInt4((uint)(v1.x / s), (uint)(v1.y / s), (uint)(v1.z / s), (uint)(v1.w / s));
		}

		public static UInt4 operator &(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z, v1.w & v2.w);
		}

		public static UInt4 operator |(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z, v1.w | v2.w);
		}

		public static UInt4 operator ^(UInt4 v1, UInt4 v2)
		{
			return new UInt4(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z, v1.w ^ v2.w);
		}

		public static UInt4 operator ~(UInt4 v)
		{
			return new UInt4(~v.x, ~v.y, ~v.z, ~v.w);
		}

		public static UInt4 Uniform(uint s)
		{
			return new UInt4(s, s, s, s);
		}

		public uint this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					case 3:
						return w;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					case 3:
						w = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static UInt4 Clamp(UInt4 v, uint min, uint max)
		{
			return new UInt4(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max), Math.Clamp(v.w, min, max));
		}

		public static UInt4 Clamp(UInt4 v, UInt4 min, UInt4 max)
		{
			return new UInt4(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z), Math.Clamp(v.w, min.w, max.w));
		}

		public static UInt4 Repeat(UInt4 v, uint min, uint max)
		{
			return new UInt4(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max), Math.Repeat(v.w, min, max));
		}

		public static UInt4 Repeat(UInt4 v, UInt4 min, UInt4 max)
		{
			return new UInt4(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z), Math.Repeat(v.w, min.w, max.w));
		}

		public static UInt4 Min(UInt4 v1, UInt4 v2)
		{
			return new UInt4(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z), System.Math.Min(v1.w, v2.w));
		}

		public static UInt4 Max(UInt4 v1, UInt4 v2)
		{
			return new UInt4(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z), System.Math.Max(v1.w, v2.w));
		}

		public uint MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, System.Math.Min(z, w))); }
		}

		public uint MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, System.Math.Max(z, w))); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z + w * w; }
		}

		public static bool operator ==(UInt4 v1, UInt4 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z && v1.w == v2.w);
		}

		public static bool operator !=(UInt4 v1, UInt4 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode() ^ w.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z} : {w}";
		}

		public static readonly UInt4 right = new UInt4(1, 0, 0, 0);
		public static readonly UInt4 up = new UInt4(0, 1, 0, 0);
		public static readonly UInt4 forward = new UInt4(0, 0, 1, 0);
		public static readonly UInt4 ana = new UInt4(0, 0, 0, 1);
	}
}