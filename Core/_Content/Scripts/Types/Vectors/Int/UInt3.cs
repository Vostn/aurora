﻿namespace Aurora
{
	[System.Serializable]
	public struct UInt3
	{
		public uint x, y, z;

		public UInt3(UInt3 v)
		{
			x = v.x;
			y = v.y;
			z = v.z;
		}

		public UInt3(uint x, uint y, uint z)
		{
			this.x = x;
			this.y = y;
			this.z = z;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			UInt3 v = (UInt3)obj;
			return v.x == x && v.y == y && v.z == z;
		}

#if UNITY_2017_1_OR_NEWER

		public static implicit operator UnityEngine.Vector4(UInt3 v)
		{
			return new UnityEngine.Vector4(v.x, v.y, v.z, 0);
		}

		public static implicit operator UnityEngine.Vector3(UInt3 v)
		{
			return new UnityEngine.Vector3(v.x, v.y, v.z);
		}

		public static explicit operator UnityEngine.Vector2(UInt3 v)
		{
			return new UnityEngine.Vector2(v.x, v.y);
		}

		public static explicit operator UInt3(UnityEngine.Vector4 v)
		{
			return new UInt3((uint)v.x, (uint)v.y, (uint)v.z);
		}

		public static explicit operator UInt3(UnityEngine.Vector3 v)
		{
			return new UInt3((uint)v.x, (uint)v.y, (uint)v.z);
		}

		public static explicit operator UInt3(UnityEngine.Vector2 v)
		{
			return new UInt3((uint)v.x, (uint)v.y, 0);
		}

#endif

		public static UInt3 operator +(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x + v2.x, v1.y + v2.y, v1.z + v2.z);
		}

		public static UInt3 operator -(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x - v2.x, v1.y - v2.y, v1.z - v2.z);
		}

		public static UInt3 operator *(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x * v2.x, v1.y * v2.y, v1.z * v2.z);
		}

		public static UInt3 operator /(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x / v2.x, v1.y / v2.y, v1.z / v2.z);
		}

		public static UInt3 operator *(UInt3 v1, double s)
		{
			return new UInt3((uint)(v1.x * s), (uint)(v1.y * s), (uint)(v1.z * s));
		}

		public static UInt3 operator /(UInt3 v1, double s)
		{
			return new UInt3((uint)(v1.x / s), (uint)(v1.y / s), (uint)(v1.z / s));
		}

		public static UInt3 operator &(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x & v2.x, v1.y & v2.y, v1.z & v2.z);
		}

		public static UInt3 operator |(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x | v2.x, v1.y | v2.y, v1.z | v2.z);
		}

		public static UInt3 operator ^(UInt3 v1, UInt3 v2)
		{
			return new UInt3(v1.x ^ v2.x, v1.y ^ v2.y, v1.z ^ v2.z);
		}

		public static UInt3 operator ~(UInt3 v)
		{
			return new UInt3(~v.x, ~v.y, ~v.z);
		}

		public static UInt3 Uniform(uint s)
		{
			return new UInt3(s, s, s);
		}

		public uint this[int i]
		{
			get
			{
				switch (i)
				{
					case 0:
						return x;

					case 1:
						return y;

					case 2:
						return z;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0:
						x = value;
						break;

					case 1:
						y = value;
						break;

					case 2:
						z = value;
						break;

					default:
						throw new System.IndexOutOfRangeException();
				}
			}
		}

		public static UInt3 Clamp(UInt3 v, uint min, uint max)
		{
			return new UInt3(Math.Clamp(v.x, min, max), Math.Clamp(v.y, min, max), Math.Clamp(v.z, min, max));
		}

		public static UInt3 Clamp(UInt3 v, UInt3 min, UInt3 max)
		{
			return new UInt3(Math.Clamp(v.x, min.x, max.x), Math.Clamp(v.y, min.y, max.y), Math.Clamp(v.z, min.z, max.z));
		}

		public static UInt3 Repeat(UInt3 v, uint min, uint max)
		{
			return new UInt3(Math.Repeat(v.x, min, max), Math.Repeat(v.y, min, max), Math.Repeat(v.z, min, max));
		}

		public static UInt3 Repeat(UInt3 v, UInt3 min, UInt3 max)
		{
			return new UInt3(Math.Repeat(v.x, min.x, max.x), Math.Repeat(v.y, min.y, max.y), Math.Repeat(v.z, min.z, max.z));
		}

		public static UInt3 Min(UInt3 v1, UInt3 v2)
		{
			return new UInt3(System.Math.Min(v1.x, v2.x), System.Math.Min(v1.y, v2.y), System.Math.Min(v1.z, v2.z));
		}

		public static UInt3 Max(UInt3 v1, UInt3 v2)
		{
			return new UInt3(System.Math.Max(v1.x, v2.x), System.Math.Max(v1.y, v2.y), System.Math.Max(v1.z, v2.z));
		}

		public uint MinVal
		{
			get { return System.Math.Min(x, System.Math.Min(y, z)); }
		}

		public uint MaxVal
		{
			get { return System.Math.Max(x, System.Math.Max(y, z)); }
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(SqrMagnitude); }
		}

		public double SqrMagnitude
		{
			get { return x * x + y * y + z * z; }
		}

		public static bool operator ==(UInt3 v1, UInt3 v2)
		{
			return (v1.x == v2.x && v1.y == v2.y && v1.z == v2.z);
		}

		public static bool operator !=(UInt3 v1, UInt3 v2)
		{
			return !(v1 == v2);
		}

		public override int GetHashCode()
		{
			return x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{x} : {y} : {z}";
		}

		public static readonly UInt3 right = new UInt3(1, 0, 0);
		public static readonly UInt3 up = new UInt3(0, 1, 0);
		public static readonly UInt3 forward = new UInt3(0, 0, 1);
	}
}