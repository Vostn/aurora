﻿namespace Aurora.TimeSystem
{
	public struct Moment
	{
		public double Time { get; private set; }
		public TimeTransform TimeTransform { get; private set; }

		public Moment(double time, TimeTransform timeTransform)
		{
			Time = time;
			TimeTransform = timeTransform;
		}
	}
}