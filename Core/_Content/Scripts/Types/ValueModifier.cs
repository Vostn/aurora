﻿namespace Aurora
{
	/// <summary>
	/// This class encapsulates modifications to values, providing methods to accumulate modifications, and apply them to values.
	/// </summary>
	public class ValueModifier
	{
		/// <summary>
		/// Accumulated numeric change.
		/// </summary>
		private double delta = 0;

		/// <summary>
		/// Accumulated proportional numeric change.
		/// </summary>
		private double proportionalDelta = 0;

		/// <summary>
		/// Accumulated multiplicative change.
		/// </summary>
		private double multiplier = 1;

		/// <summary>
		/// The minimum value the final result can be.
		/// </summary>
		private double minimum = double.NegativeInfinity;

		/// <summary>
		/// The maximum value the final result can be.
		/// </summary>
		private double maximum = double.PositiveInfinity;

		/// <summary>
		/// Create a blank instance of ValueModifier.
		/// </summary>
		public ValueModifier()
		{
		}

		/// <summary>
		/// Create a new instance of ValueModifier, with some custom initial values.
		/// </summary>
		/// <param name="delta">Initial numeric delta to apply.</param>
		/// <param name="proportionalDelta">Initial proportional numeric delta to apply.</param>
		/// <param name="multiplier">Initial multiplicative change to apply.</param>
		public ValueModifier(double delta, double proportionalDelta, double multiplier, double minimum, double maximum)
		{
			this.delta = delta;
			this.proportionalDelta = proportionalDelta;
			this.multiplier = multiplier;
			this.minimum = minimum;
			this.maximum = maximum;
		}

		/// <summary>
		/// Create a new instance of VlaueModifier, using an existing instance's accumulations as a base.
		/// </summary>
		/// <param name="originalModification">The original instance to copy.</param>
		public ValueModifier(ValueModifier originalModification)
		{
			delta = originalModification.delta;
			proportionalDelta = originalModification.proportionalDelta;
			multiplier = originalModification.multiplier;
			minimum = originalModification.minimum;
			maximum = originalModification.maximum;
		}

		/// <summary>
		/// Combine the accumulated changes from multiple instances of ValueModifier together.
		/// </summary>
		/// <param name="modifiers">The instances of ValueModifier to combine.</param>
		/// <returns>A new instance of ValueModifier that combines all of the accumulations of the supplied instances.</returns>
		public static ValueModifier CombineModifications(params ValueModifier[] modifiers)
		{
			ValueModifier combinedModifiers = new ValueModifier();
			foreach (ValueModifier modifier in modifiers)
			{
				combinedModifiers.ApplyDelta(modifier.delta);
				combinedModifiers.ApplyProportionalDelta(modifier.proportionalDelta);
				combinedModifiers.ApplyMultiplier(modifier.multiplier);
				combinedModifiers.ApplyMinimum(modifier.minimum);
				combinedModifiers.ApplyMaximum(modifier.maximum);
			}
			return combinedModifiers;
		}

		/// <summary>
		/// Add a new delta modification to this instance.
		/// </summary>
		/// <param name="delta">Numeric change to apply.</param>
		public void ApplyDelta(double delta)
		{
			this.delta += delta;
		}

		/// <summary>
		/// Adds a new proportional delta modification to this instance.
		/// </summary>
		/// <param name="proportionalDelta">Proportional numeric change to apply.</param>
		public void ApplyProportionalDelta(double proportionalDelta)
		{
			this.proportionalDelta += proportionalDelta;
		}

		/// <summary>
		/// Adds a new multiplicative modification to this instance.
		/// </summary>
		/// <param name="multiplier">Multiplicative change to apply.</param>
		public void ApplyMultiplier(double multiplier)
		{
			this.multiplier *= multiplier;
		}

		/// <summary>
		/// Adds a new maximum modification to this instance.
		/// </summary>
		/// <param name="minimum"></param>
		public void ApplyMinimum(double minimum)
		{
			if (minimum > this.minimum)
				this.minimum = minimum;
		}

		/// <summary>
		/// Added the minimum modification to this instance.
		/// </summary>
		/// <param name="maximum"></param>
		public void ApplyMaximum(double maximum)
		{
			if (maximum < this.maximum)
				this.maximum = maximum;
		}

		/// <summary>
		/// Apply all accumulated modifications to the supplied value.
		/// </summary>
		/// <param name="baseValue">The base value to apply modifications to.</param>
		/// <returns>baseValue parameter, after all accumulated modifications in this instance are applied.</returns>
		public double CalculateFinalValue(double baseValue)
		{
			double finalValue = ((baseValue * proportionalDelta) + baseValue + delta) * multiplier;
			finalValue = minimum > finalValue ? minimum : finalValue;
			finalValue = maximum < finalValue ? maximum : finalValue;
			return finalValue;
		}
	}
}