﻿/*

*Random.cs* is not available under any license.
Copyright (c) 2018 Alex Short

*/

/*

The method *Aurora.Random.Next* in *Random.cs* is derived from the PCG Random Generator.  
*PCG Random Generator* is Copyright (c) 2014 M.E. O'Neill.  
*PCG Random Generator* is licensed under the Apache License Version 2.0 (See *Apache License Version 2.0* in *LICENSES.md*).  

Homepage of PCG Random Generator: http://www.pcg-random.org/  
The Minimal C Implementation that *Aurora.Random.Next* is derived from: http://www.pcg-random.org/download.html

*/

namespace Aurora
{
	public sealed class Random
	{
		private ulong state;
		private ulong sequence;
		private uint cacheValue;
		private byte cacheBitIndex = 64;

		public Random(ulong seed, ulong sequence)
		{
			state = 0;
			this.sequence = (sequence << 1) | 1;
			Next();
			state += seed;
			Next();
		}

		public bool NextBool()
		{
			if (cacheBitIndex > 31)
			{
				cacheValue = Next();
				cacheBitIndex = 0;
			}
			bool returnValue = (1 & (cacheValue >> cacheBitIndex)) == 0;
			cacheBitIndex += 1;
			return returnValue;
		}

		public bool[] NextBools(int count)
		{
			if (count <= 0)
				return new bool[0];
			bool[] bools = new bool[count];
			for (int i = 0; i < count; ++i)
				bools[i] = NextBool();
			return bools;
		}

		public byte NextByte()
		{
			if (cacheBitIndex > 24)
			{
				cacheValue = Next();
				cacheBitIndex = 0;
			}
			byte returnValue = (byte)(cacheValue >> cacheBitIndex);
			cacheBitIndex += 8;
			return returnValue;
		}

		public byte NextByte(byte minimum, byte maximum)
		{
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			byte range = (byte)((maximum - minimum) + 1);
			byte attemptedValue;
			do
			{
				attemptedValue = NextByte();
			} while (attemptedValue >= (byte.MaxValue - (byte.MaxValue % range)));
			return (byte)(minimum + (attemptedValue % range));
		}

		public byte[] NextBytes(int count)
		{
			if (count <= 0)
				return new byte[0];
			byte[] bytes = new byte[count];
			for (int i = 0; i < count; ++i)
				bytes[i] = NextByte();
			return bytes;
		}

		public byte[] NextBytes(int count, byte minimum, byte maximum)
		{
			if (count <= 0)
				return new byte[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			byte[] bytes = new byte[count];
			for (int i = 0; i < count; ++i)
				bytes[i] = NextByte(minimum, maximum);
			return bytes;
		}

		public sbyte NextSByte()
		{
			return UInt8BitsToInt8(NextByte());
		}

		public sbyte[] NextSBytes(int count)
		{
			if (count <= 0)
				return new sbyte[0];
			sbyte[] sbytes = new sbyte[count];
			for (int i = 0; i < count; ++i)
				sbytes[i] = NextSByte();
			return sbytes;
		}

		public sbyte NextSByte(sbyte minimum, sbyte maximum)
		{
			byte byteMin = (byte)(Int8BitsToUInt8(minimum) ^ 0x80);
			byte byteMax = (byte)(Int8BitsToUInt8(maximum) ^ 0x80);
			return UInt8BitsToInt8((byte)(NextByte(byteMin, byteMax) ^ 0x80));
		}

		public sbyte[] NextSBytes(int count, sbyte minimum, sbyte maximum)
		{
			if (count <= 0)
				return new sbyte[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			sbyte[] sbytes = new sbyte[count];
			for (int i = 0; i < count; ++i)
				sbytes[i] = NextSByte(minimum, maximum);
			return sbytes;
		}

		public ushort NextUShort()
		{
			if (cacheBitIndex > 16)
			{
				cacheValue = Next();
				cacheBitIndex = 0;
			}
			ushort returnValue = (ushort)(cacheValue >> cacheBitIndex);
			cacheBitIndex += 16;
			return returnValue;
		}

		public ushort NextUShort(ushort minimum, ushort maximum)
		{
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			ushort attemptedValue;
			ushort range = (ushort)((maximum - minimum) + 1);
			do
			{
				attemptedValue = NextUShort();
			} while (attemptedValue >= (ushort.MaxValue - (ushort.MaxValue % range)));
			return (ushort)(minimum + (attemptedValue % range));
		}

		public ushort[] NextUShorts(int count)
		{
			if (count <= 0)
				return new ushort[0];
			ushort[] ushorts = new ushort[count];
			for (int i = 0; i < count; ++i)
				ushorts[i] = NextUShort();
			return ushorts;
		}

		public ushort[] NextUShorts(int count, ushort minimum, ushort maximum)
		{
			if (count <= 0)
				return new ushort[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			ushort[] ushorts = new ushort[count];
			for (int i = 0; i < count; ++i)
				ushorts[i] = NextUShort(minimum, maximum);
			return ushorts;
		}

		public short NextShort()
		{
			return UInt16BitsToInt16(NextUShort());
		}

		public short NextShort(short minimum, short maximum)
		{
			ushort ushortMin = (ushort)(Int16BitsToUInt16(minimum) ^ 0x8000);
			ushort ushortMax = (ushort)(Int16BitsToUInt16(maximum) ^ 0x8000);
			return UInt16BitsToInt16((ushort)(NextUShort(ushortMin, ushortMax) ^ 0x8000));
		}

		public short[] NextShorts(int count)
		{
			if (count <= 0)
				return new short[0];
			short[] shorts = new short[count];
			for (int i = 0; i < count; ++i)
				shorts[i] = NextShort();
			return shorts;
		}

		public short[] NextShorts(int count, short minimum, short maximum)
		{
			if (count <= 0)
				return new short[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			short[] shorts = new short[count];
			for (int i = 0; i < count; ++i)
				shorts[i] = NextShort(minimum, maximum);
			return shorts;
		}

		public uint NextUInt()
		{
			return Next();
		}

		public uint NextUInt(uint minimum, uint maximum)
		{
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			uint attemptedValue;
			uint range = (maximum - minimum) + 1;
			do
			{
				attemptedValue = NextUInt();
			} while (attemptedValue >= (uint.MaxValue - (uint.MaxValue % range)));
			return minimum + (attemptedValue % range);
		}

		public uint[] NextUInts(int count)
		{
			if (count <= 0)
				return new uint[0];
			uint[] uints = new uint[count];
			for (int i = 0; i < count; ++i)
				uints[i] = NextUInt();
			return uints;
		}

		public uint[] NextUInts(int count, uint minimum, uint maximum)
		{
			if (count <= 0)
				return new uint[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			uint[] uints = new uint[count];
			for (int i = 0; i < count; ++i)
				uints[i] = NextUInt(minimum, maximum);
			return uints;
		}

		public int NextInt()
		{
			return UInt32BitsToInt32(NextUInt());
		}

		public int NextInt(int minimum, int maximum)
		{
			uint uintMin = Int32BitsToUInt32(minimum) ^ 0x80000000;
			uint uintMax = Int32BitsToUInt32(maximum) ^ 0x80000000;
			return UInt32BitsToInt32(NextUInt(uintMin, uintMax) ^ 0x80000000);
		}

		public int[] NextInts(int count)
		{
			if (count <= 0)
				return new int[0];
			int[] ints = new int[count];
			for (int i = 0; i < count; ++i)
				ints[i] = NextInt();
			return ints;
		}

		public int[] NextInts(int count, int minimum, int maximum)
		{
			if (count <= 0)
				return new int[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			int[] ints = new int[count];
			for (int i = 0; i < count; ++i)
				ints[i] = NextInt(minimum, maximum);
			return ints;
		}

		public float NextSingle()
		{
			uint value = 0x3f800000;
			value |= 0x007fffff & NextUInt();
			return UInt32BitsToSingle(value) - 1;
		}

		public float NextSingle(float minimum, float maximum)
		{
			return minimum + (NextSingle() * (maximum - minimum));
		}

		public float[] NextSingles(int count)
		{
			if (count <= 0)
				return new float[0];
			float[] singles = new float[count];
			for (int i = 0; i < count; ++i)
				singles[i] = NextSingle();
			return singles;
		}

		public float[] NextSingles(int count, float minimum, float maximum)
		{
			if (count <= 0)
				return new float[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			float[] singles = new float[count];
			for (int i = 0; i < count; ++i)
				singles[i] = NextSingle(minimum, maximum);
			return singles;
		}

		public double NextDouble()
		{
			ulong value = 0x3ff0000000000000;
			value |= 0x000fffffffffffff & NextULong();
			return UInt64BitsToDouble(value) - 1;
		}

		public double NextDouble(double minimum, double maximum)
		{
			return minimum + (NextDouble() * (maximum - minimum));
		}

		public double[] NextDoubles(int count)
		{
			if (count <= 0)
				return new double[0];
			double[] doubles = new double[count];
			for (int i = 0; i < count; ++i)
				doubles[i] = NextDouble();
			return doubles;
		}

		public double[] NextDoubles(int count, double minimum, double maximum)
		{
			if (count <= 0)
				return new double[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			double[] doubles = new double[count];
			for (int i = 0; i < count; ++i)
				doubles[i] = NextDouble(minimum, maximum);
			return doubles;
		}

		public ulong NextULong()
		{
			ulong value = Next();
			value += ((ulong)Next()) << 32;
			return value;
		}

		public ulong NextULong(ulong minimum, ulong maximum)
		{
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			ulong attemptedValue;
			ulong range = (maximum - minimum) + 1;
			do
			{
				attemptedValue = NextULong();
			} while (attemptedValue >= (ulong.MaxValue - (ulong.MaxValue % range)));
			return minimum + (attemptedValue % range);
		}

		public ulong[] NextULongs(int count)
		{
			if (count <= 0)
				return new ulong[0];
			ulong[] ulongs = new ulong[count];
			for (int i = 0; i < count; ++i)
				ulongs[i] = NextULong();
			return ulongs;
		}

		public ulong[] NextULongs(int count, ulong minimum, ulong maximum)
		{
			if (count <= 0)
				return new ulong[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			ulong[] ulongs = new ulong[count];
			for (int i = 0; i < count; ++i)
				ulongs[i] = NextULong(minimum, maximum);
			return ulongs;
		}

		public long NextLong()
		{
			return UInt64BitsToInt64(NextULong());
		}

		public long NextLong(long minimum, long maximum)
		{
			ulong ulongMin = Int64BitsToUInt64(minimum) ^ 0x8000000000000000;
			ulong ulongMax = Int64BitsToUInt64(maximum) ^ 0x8000000000000000;
			return UInt64BitsToInt64(NextULong(ulongMin, ulongMax) ^ 0x8000000000000000);
		}

		public long[] NextLongs(int count)
		{
			if (count <= 0)
				return new long[0];
			long[] longs = new long[count];
			for (int i = 0; i < count; ++i)
				longs[i] = NextLong();
			return longs;
		}

		public long[] NextLongs(int count, long minimum, long maximum)
		{
			if (count <= 0)
				return new long[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			long[] longs = new long[count];
			for (int i = 0; i < count; ++i)
				longs[i] = NextLong(minimum, maximum);
			return longs;
		}

		public Fixed NextFixed()
		{
			return UInt64BitsToFixed(NextULong());
		}

		public Fixed NextFixed(Fixed minimum, Fixed maximum)
		{
			ulong ulongMin = FixedBitsToUInt64(minimum) ^ 0x8000000000000000;
			ulong ulongMax = FixedBitsToUInt64(maximum) ^ 0x8000000000000000;
			return UInt64BitsToFixed(NextULong(ulongMin, ulongMax) ^ 0x8000000000000000);
		}

		public Fixed[] NextFixeds(int count)
		{
			if (count <= 0)
				return new Fixed[0];
			Fixed[] fixeds = new Fixed[count];
			for (int i = 0; i < count; ++i)
				fixeds[i] = NextFixed();
			return fixeds;
		}

		public Fixed[] NextFixeds(int count, Fixed minimum, Fixed maximum)
		{
			if (count <= 0)
				return new Fixed[0];
			if (minimum > maximum)
				Math.SwapValues(ref minimum, ref maximum);
			Fixed[] fixeds = new Fixed[count];
			for (int i = 0; i < count; ++i)
				fixeds[i] = NextFixed(minimum, maximum);
			return fixeds;
		}

		public Double3 SpreadVector(Double3 v, double minSpread, double maxSpread)
		{
			double spread = Math.Lerp(minSpread, maxSpread, NextSingle());
			double limit = (1 - System.Math.Cos(spread * System.Math.PI)) * 0.5;
			Quaternion offset = Quaternion.Euler(0, 0, NextDouble(-180, 180));
			offset *= Quaternion.Euler(180 - (System.Math.Acos((NextDouble(0, limit) * 2) - 1) * Math.RadToDeg), 0, 0);
			return offset * v;
		}

		private static unsafe sbyte UInt8BitsToInt8(byte value)
		{
			return *(sbyte*)(&value);
		}

		private static unsafe byte Int8BitsToUInt8(sbyte value)
		{
			return *(byte*)(&value);
		}

		private static unsafe short UInt16BitsToInt16(ushort value)
		{
			return *(short*)(&value);
		}

		private static unsafe ushort Int16BitsToUInt16(short value)
		{
			return *(ushort*)(&value);
		}

		private static unsafe int UInt32BitsToInt32(uint value)
		{
			return *(int*)(&value);
		}

		private static unsafe uint Int32BitsToUInt32(int value)
		{
			return *(uint*)(&value);
		}

		private static unsafe long UInt64BitsToInt64(ulong value)
		{
			return *(long*)(&value);
		}

		private static unsafe ulong Int64BitsToUInt64(long value)
		{
			return *(ulong*)(&value);
		}

		private static unsafe float UInt32BitsToSingle(uint value)
		{
			return *(float*)(&value);
		}

		private static unsafe double UInt64BitsToDouble(ulong value)
		{
			return *(double*)(&value);
		}

		private static unsafe Fixed UInt64BitsToFixed(ulong value)
		{
			return *(Fixed*)(&value);
		}

		private static unsafe ulong FixedBitsToUInt64(Fixed value)
		{
			return *(ulong*)(&value);
		}

		private uint Next()
		{
			ulong oldState = state;
			state = oldState * 6364136223846793005ul + sequence;
			uint xorShifted = (uint)(((oldState >> 18) ^ oldState) >> 27);
			int rot = (int)(oldState >> 59);
			return (xorShifted >> rot) | (xorShifted << ((-rot) & 31));
		}
	}
}