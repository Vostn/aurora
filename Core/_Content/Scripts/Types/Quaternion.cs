﻿namespace Aurora
{
	[System.Serializable]
	public struct Quaternion : ISlerpable<Quaternion>, IAnglable<Quaternion>, IInterpolable<Quaternion>
	{
		public double x, y, z, w;

		public Quaternion(Quaternion q)
		{
			x = q.x;
			y = q.y;
			z = q.z;
			w = q.w;
		}

		public Quaternion(double x, double y, double z, double w)
		{
			this.x = x;
			this.y = y;
			this.z = z;
			this.w = w;
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Quaternion q = (Quaternion)obj;
			return q.x == x && q.y == y && q.z == z && q.w == w;
		}

#if UNITY_2017_1_OR_NEWER

		public static explicit operator UnityEngine.Quaternion(Quaternion q)
		{
			return new UnityEngine.Quaternion((float)q.x, (float)q.y, (float)q.z, (float)q.w);
		}

		public static implicit operator Quaternion(UnityEngine.Quaternion q)
		{
			return new Quaternion(q.x, q.y, q.z, q.w);
		}

#endif

		public static Quaternion operator *(Quaternion q1, Quaternion q2)
		{
			return new Quaternion(
				q1.w * q2.x + q1.x * q2.w + q1.y * q2.z - q1.z * q2.y,
				q1.w * q2.y + q1.y * q2.w + q1.z * q2.x - q1.x * q2.z,
				q1.w * q2.z + q1.z * q2.w + q1.x * q2.y - q1.y * q2.x,
				q1.w * q2.w - q1.x * q2.x - q1.y * q2.y - q1.z * q2.z);
		}

		public static Quaternion operator /(Quaternion q, double s)
		{
			return new Quaternion(q.x / s, q.y / s, q.z / s, q.w / s);
		}

		public static Double3 operator *(Quaternion q, Double3 v)
		{
			double x2 = q.x * 2;
			double y2 = q.y * 2;
			double z2 = q.z * 2;
			double xx = q.x * x2;
			double yy = q.y * y2;
			double zz = q.z * z2;
			double xy = q.x * y2;
			double xz = q.x * z2;
			double yz = q.y * z2;
			double wx = q.w * x2;
			double wy = q.w * y2;
			double wz = q.w * z2;

			Double3 returnVec;
			returnVec.x = (1 - (yy + zz)) * v.x + (xy - wz) * v.y + (xz + wy) * v.z;
			returnVec.y = (xy + wz) * v.x + (1 - (xx + zz)) * v.y + (yz - wx) * v.z;
			returnVec.z = (xz - wy) * v.x + (yz + wx) * v.y + (1 - (xx + yy)) * v.z;
			return returnVec;
		}

		public static Quaternion operator *(Quaternion q, double v)
		{
			return new Quaternion(q.x * v, q.y * v, q.z * v, q.w * v);
		}

		public double this[int i]
		{
			get
			{
				switch (i)
				{
					case 0: return w;
					case 1: return x;
					case 2: return y;
					case 3: return z;
					default: throw new System.IndexOutOfRangeException();
				}
			}
			set
			{
				switch (i)
				{
					case 0: w = value; break;
					case 1: x = value; break;
					case 2: y = value; break;
					case 3: z = value; break;
					default: throw new System.IndexOutOfRangeException();
				}
			}
		}

		public double Magnitude
		{
			get { return System.Math.Sqrt(x * x + y * y + z * z + w * w); }
		}

		public Quaternion Normalised
		{
			get { return this / Magnitude; }
		}

		public static Quaternion Euler(double x, double y, double z)
		{
			double halfRoll = z * 0.5 * Math.DegToRad;
			double halfPitch = y * 0.5 * Math.DegToRad;
			double halfYaw = x * 0.5 * Math.DegToRad;

			double sY = System.Math.Sin(halfYaw);
			double cY = System.Math.Cos(halfYaw);
			double sP = System.Math.Sin(halfPitch);
			double cP = System.Math.Cos(halfPitch);
			double sR = System.Math.Sin(halfRoll);
			double cR = System.Math.Cos(halfRoll);

			Quaternion returnQuat;
			returnQuat.x = sY * cP * cR + cY * sP * sR;
			returnQuat.y = cY * sP * cR - sY * cP * sR;
			returnQuat.z = cY * cP * sR - sY * sP * cR;
			returnQuat.w = cY * cP * cR + sY * sP * sR;
			return returnQuat;
		}

		public static Quaternion Euler(Double3 eulerAngles)
		{
			return Euler(eulerAngles.x, eulerAngles.y, eulerAngles.z);
		}

		public static Quaternion Inverse(Quaternion q)
		{
			Quaternion returnQuat;
			double c = 1 / (q.x * q.x + q.y * q.y + q.z * q.z + q.w * q.w);
			returnQuat.x = -q.x * c;
			returnQuat.y = -q.y * c;
			returnQuat.z = -q.z * c;
			returnQuat.w = q.w * c;
			return returnQuat;
		}

		public static Quaternion Slerp(Quaternion q1, Quaternion q2, double t)
		{
			double cosOmega;
			double q1Scale, q2Scale;

			double q1Length = q1.Magnitude;
			double q2Length = q2.Magnitude;
			q1 /= q1Length;
			q2 /= q2Length;

			cosOmega = q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w;

			if (cosOmega < 0.0)
			{
				cosOmega = -cosOmega;
				q2.x = -q2.x;
				q2.y = -q2.y;
				q2.z = -q2.z;
				q2.w = -q2.w;
			}

			if (cosOmega > 1.0)
				cosOmega = 1.0;

			const double maxCosine = 1.0 - 1e-6;
			const double minCosine = 1e-10 - 1.0;

			if (cosOmega > maxCosine)
			{
				q1Scale = 1.0 - t;
				q2Scale = t;
			}
			else if (cosOmega < minCosine)
			{
				q2 = new Quaternion(-q1.y, q1.x, -q1.w, q1.z);
				double theta = t * Math.PI;
				q1Scale = System.Math.Cos(theta);
				q2Scale = System.Math.Sin(theta);
			}
			else
			{
				double omega = System.Math.Acos(cosOmega);
				double sinOmega = System.Math.Sqrt(1.0 - cosOmega * cosOmega);
				q1Scale = System.Math.Sin((1.0 - t) * omega) / sinOmega;
				q2Scale = System.Math.Sin(t * omega) / sinOmega;
			}

			double lengthOut = q1Length * System.Math.Pow(q2Length / q1Length, t);
			q1Scale *= lengthOut;
			q2Scale *= lengthOut;

			return new Quaternion(q1Scale * q1.x + q2Scale * q2.x,
								  q1Scale * q1.y + q2Scale * q2.y,
								  q1Scale * q1.z + q2Scale * q2.z,
								  q1Scale * q1.w + q2Scale * q2.w);
		}

		Quaternion IInterpolable<Quaternion>.Interpolant(Quaternion o2, double t)
		{
			return Slerp(this, o2, t);
		}

		Quaternion ISlerpable<Quaternion>.Slerpolant(Quaternion o2, double t)
		{
			return Slerp(this, o2, t);
		}

		public static double Dot(Quaternion q1, Quaternion q2)
		{
			return q1.x * q2.x + q1.y * q2.y + q1.z * q2.z + q1.w * q2.w;
		}

		public static double Angle(Quaternion q1, Quaternion q2)
		{
			return System.Math.Acos(System.Math.Min(System.Math.Abs(Dot(q1, q2)), 1)) * 2 * Math.RadToDeg;
		}

		double IAnglable<Quaternion>.Angle(Quaternion o)
		{
			return Angle(this, o);
		}

		public static bool operator ==(Quaternion q1, Quaternion q2)
		{
			return (q1.w == q2.w && q1.x == q2.x && q1.y == q2.y && q1.z == q2.z);
		}

		public static bool operator !=(Quaternion q1, Quaternion q2)
		{
			return !(q1 == q2);
		}

		public override int GetHashCode()
		{
			return w.GetHashCode() ^ x.GetHashCode() ^ y.GetHashCode() ^ z.GetHashCode();
		}

		public override string ToString()
		{
			return $"{w} : {x} : {y} : {z}";
		}

		public static readonly Quaternion identity = new Quaternion(0, 0, 0, 1);
	}
}