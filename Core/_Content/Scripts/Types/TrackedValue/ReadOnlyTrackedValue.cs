﻿namespace Aurora
{
	public class ReadOnlyTrackedValue<T> where T : struct, System.IEquatable<T>
	{
		private TrackedValue<T> targetInstance;
		public event System.Action<TrackedValue<T>.ChangeInfo> OnChanged;
		public T Value { get { return targetInstance.Value; } }

		public ReadOnlyTrackedValue(TrackedValue<T> targetInstance)
		{
			this.targetInstance = targetInstance;
			targetInstance.OnChanged += CallbackOnChange;
		}

		private void CallbackOnChange(TrackedValue<T>.ChangeInfo changeInfo)
		{
			OnChanged?.Invoke(changeInfo);
		}
	}
}