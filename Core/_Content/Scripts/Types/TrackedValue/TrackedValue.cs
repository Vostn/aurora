﻿using System.Collections.Generic;

namespace Aurora
{
	public class TrackedValue<T> where T : struct, System.IEquatable<T>
	{
		private T currentValue;
		private bool executingChange;
		private Queue<T> changeQueue;

		public event System.Action<ChangeInfo> OnChanged;

		public T Value
		{
			get { return currentValue; }
			set
			{
				if (!currentValue.Equals(value))
				{
					if (executingChange)
					{
						changeQueue.Enqueue(value);
						return;
					}
					ChangeInfo changeInfo = new ChangeInfo(currentValue, value);
					currentValue = value;
					executingChange = true;
					OnChanged?.Invoke(changeInfo);
					executingChange = false;
					while (changeQueue.Count > 0)
						Value = changeQueue.Dequeue();
				}
			}
		}

		public TrackedValue(T initialValue)
		{
			currentValue = initialValue;
			OnChanged = delegate { };
			executingChange = false;
			changeQueue = new Queue<T>();
		}

		public class ChangeInfo
		{
			public T OldState { get; private set; }

			public T NewState { get; private set; }

			public ChangeInfo(T oldState, T newState)
			{
				OldState = oldState; NewState = newState;
			}
		}
	}
}