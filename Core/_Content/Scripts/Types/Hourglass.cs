﻿using Aurora.Diagnostics;
using Aurora.TimeSystem;

namespace Aurora
{
	/// <summary>
	/// Hourglass is used to countdown to an expiry. This expiry can be a set time, or the end of a
	/// time frame. Hourglass is a lazy evaluator, so will only calculate if it has expired when queried.
	/// </summary>
	public class Hourglass
	{
		private TimeTransform timeTransform;
		private double timer;
		private bool paused = false;

		public event System.Action OnChanged;

		/// <summary>
		/// Create a new instance of Hourglass.
		/// </summary>
		/// <param name="duration">The time until expiry of this Hourglass.</param>
		/// <param name="timeTransform">The TimeTransform on which the timing for this Hourglass will occur.</param>
		public Hourglass(double duration, TimeTransform timeTransform)
		{
			if (timeTransform == null)
				throw new System.ArgumentNullException($"Argument {nameof(timeTransform)} is null!");
			if (duration < 0)
				Debug.LogWarning("Duration of less than zero in Hourglass construction is not supported. Clamping to zero.");
			timer = System.Math.Max(duration, 0);
			TimeTransform = timeTransform;
			TimeTransform.OnChanged += TriggerChangeEvent;
		}
		
		/// <summary>
		/// Return or modify the expiry time of this Hourglass instance.
		/// </summary>
		public double ExpiryTime
		{
			get { if (paused) return timer + TimeTransform.Time; return timer; }
			set { if (paused) timer = value - TimeTransform.Time; else timer = value; TriggerChangeEvent(); }
		}

		/// <summary>
		/// Return or modify the amount of time remaining on this Hourglass instance.
		/// </summary>
		public double TimeRemaining
		{
			get { if (!paused) return timer - TimeTransform.Time; return timer; }
			set { if (!paused) timer = value + TimeTransform.Time; else timer = value; TriggerChangeEvent(); }
		}

		public TimeTransform TimeTransform
		{
			get { return timeTransform; }
			set { if (TimeTransform != value) { if(TimeTransform != null) TimeTransform.OnChanged -= TriggerChangeEvent; if (!paused) timer += value.Time - (TimeTransform==null?0:TimeTransform.Time); timeTransform = value; TimeTransform.OnChanged += TriggerChangeEvent; TriggerChangeEvent(); } }
		}

		/// <summary>
		/// Has this hourglass expired?
		/// </summary>
		public bool Expired
		{
			get { if (!paused) return timer <= TimeTransform.Time; return timer <= 0; }
		}

		private void TriggerChangeEvent()
		{
			OnChanged?.Invoke();
		}

		/// <summary>
		/// Return or modify the current paused state of this Hourglass instance.
		/// </summary>
		public bool Paused
		{
			get { return paused; }
			set
			{
				if (paused != value)
				{
					if (value)
						timer -= TimeTransform.Time;
					else
						timer += TimeTransform.Time;
					paused = value;
					TriggerChangeEvent();
				}
			}
		}
	}
}