﻿namespace Aurora
{
	public delegate NoiseSampleFixed NoiseMethodFixed(Fixed3 point, Fixed frequency, int seed);
	public delegate Fixed NoiseSimpleMethodFixed(Fixed3 point, Fixed frequency, int seed);

	public static class NoiseFixed
	{
		public static NoiseMethodFixed[] simplexMethods =
		{
			Simplex3D
		};

		public static NoiseSimpleMethodFixed[] simplexSimpleMethods =
		{
			Simplex3DSimple
		};

		private const int hashMask = 255;

		private const int simplexGradientsMask3D = 31;

		private static int[] hash = {
		52, 44,157,204,218, 18,152, 72, 65, 19,255,  3, 37,141,171, 26,
		149,253,220, 34, 67,140,176,244,189, 57,245,162,227,235,102,208,
		 97,250, 92,252, 28,  0,182, 11,236,214,144,139,133,210,135, 38,
		 29,155, 79,172,216,225, 31, 69,197, 30,254,106,151,122,238,196,
		  9, 27,177,222,  2, 48,103,126, 62, 64, 63, 61,239,101, 88, 93,
		240,  5, 24,163,147, 14,  4, 55,193,228, 53, 17,109,251,185, 49,
		128, 23, 40,158, 78, 56,104, 89, 73, 80,153, 50, 33,105,132,226,
		 75,188,129, 76,165,232, 36,231,137,159,248,229,148, 39,170,224,
		131,112,190,186, 13, 22,124, 12,242,156,136, 41,100,118,207,199,
		  6,160,249, 91, 66, 45,125,178, 68,213,  8,187, 54,  7,110, 82,
		130, 43,219, 85, 81,209, 59, 87,167, 47,166,243,198,211, 32,234,
		191,195,201,184,119,202, 70,150,247, 84, 74,205, 96,117,192,115,
		145,121,233, 10, 60,173,114,212, 51,175, 90, 71, 83,138,161,107,
		217,134,180,120,116, 98,230,183, 58,127,221,154, 15,215,246, 35,
		 16, 25,143,203,194,123, 99,200, 77,142, 42,111,241,223, 94, 95,
		168,146, 86,174,164,237, 20,108,181,179, 21,169,113, 46,  1,206
	};

		private static Fixed3[] simplexGradients3D = {
		new Fixed3( 1, 1, 0).Normalised,
		new Fixed3(-1, 1, 0).Normalised,
		new Fixed3( 1,-1, 0).Normalised,
		new Fixed3(-1,-1, 0).Normalised,
		new Fixed3( 1, 0, 1).Normalised,
		new Fixed3(-1, 0, 1).Normalised,
		new Fixed3( 1, 0,-1).Normalised,
		new Fixed3(-1, 0,-1).Normalised,
		new Fixed3( 0, 1, 1).Normalised,
		new Fixed3( 0,-1, 1).Normalised,
		new Fixed3( 0, 1,-1).Normalised,
		new Fixed3( 0,-1,-1).Normalised,

		new Fixed3( 1, 1, 0).Normalised,
		new Fixed3(-1, 1, 0).Normalised,
		new Fixed3( 1,-1, 0).Normalised,
		new Fixed3(-1,-1, 0).Normalised,
		new Fixed3( 1, 0, 1).Normalised,
		new Fixed3(-1, 0, 1).Normalised,
		new Fixed3( 1, 0,-1).Normalised,
		new Fixed3(-1, 0,-1).Normalised,
		new Fixed3( 0, 1, 1).Normalised,
		new Fixed3( 0,-1, 1).Normalised,
		new Fixed3( 0, 1,-1).Normalised,
		new Fixed3( 0,-1,-1).Normalised,

		new Fixed3( 1, 1, 1).Normalised,
		new Fixed3(-1, 1, 1).Normalised,
		new Fixed3( 1,-1, 1).Normalised,
		new Fixed3(-1,-1, 1).Normalised,
		new Fixed3( 1, 1,-1).Normalised,
		new Fixed3(-1, 1,-1).Normalised,
		new Fixed3( 1,-1,-1).Normalised,
		new Fixed3(-1,-1,-1).Normalised
	};

		private static Fixed simplexScale3D = Fixed.FromRawValue(79350417);

		public static NoiseSampleFixed Sum(NoiseMethodFixed method, Fixed3 point, Fixed frequency, int octaves, Fixed lacunarity, Fixed persistence, int seed)
		{
			NoiseSampleFixed sum = method(point, frequency, seed);
			Fixed amplitude = 1;
			Fixed range = 1;
			for (int o = 0; o < octaves; o++)
			{
				frequency *= lacunarity;
				amplitude *= persistence;
				range += amplitude;
				sum += method(point, frequency, seed) * amplitude;
			}
			return sum / range;
		}

		public static Fixed SumSimple(NoiseSimpleMethodFixed method, Fixed3 point, Fixed frequency, int octaves, Fixed lacunarity, Fixed persistence, int seed)
		{
			Fixed sum = method(point, frequency, seed);
			Fixed amplitude = 1;
			Fixed range = 1;
			for (int o = 0; o < octaves; o++)
			{
				frequency *= lacunarity;
				amplitude *= persistence;
				range += amplitude;
				sum += method(point, frequency, seed) * amplitude;
			}
			return sum / range;
		}

		public static NoiseSampleFixed Simplex3D(Fixed3 point, Fixed frequency, int seed)
		{
			point *= frequency;
			Fixed skew = (point.x + point.y + point.z) / 3;
			Fixed sx = point.x + skew;
			Fixed sy = point.y + skew;
			Fixed sz = point.z + skew;
			int ix = (int)sx;
			int iy = (int)sy;
			int iz = (int)sz;
			NoiseSampleFixed sample = Simplex3DPart(point, ix, iy, iz, seed);
			sample += Simplex3DPart(point, ix + 1, iy + 1, iz + 1, seed);
			Fixed x = sx - ix;
			Fixed y = sy - iy;
			Fixed z = sz - iz;

			if (x >= y)
			{
				if (x >= z)
				{
					sample += Simplex3DPart(point, ix + 1, iy, iz, seed);
					if (y >= z)
						sample += Simplex3DPart(point, ix + 1, iy + 1, iz, seed);
					else
						sample += Simplex3DPart(point, ix + 1, iy, iz + 1, seed);
				}
				else
				{
					sample += Simplex3DPart(point, ix, iy, iz + 1, seed);
					sample += Simplex3DPart(point, ix + 1, iy, iz + 1, seed);
				}
			}
			else
			{
				if (y >= z)
				{
					sample += Simplex3DPart(point, ix, iy + 1, iz, seed);
					if (x >= z)
						sample += Simplex3DPart(point, ix + 1, iy + 1, iz, seed);
					else
						sample += Simplex3DPart(point, ix, iy + 1, iz + 1, seed);
				}
				else
				{
					sample += Simplex3DPart(point, ix, iy, iz + 1, seed);
					sample += Simplex3DPart(point, ix, iy + 1, iz + 1, seed);
				}
			}
			sample.derivative *= frequency;
			return sample * simplexScale3D;
		}

		public static Fixed Simplex3DSimple(Fixed3 point, Fixed frequency, int seed)
		{
			point *= frequency;
			Fixed skew = (point.x + point.y + point.z) / 3;
			Fixed sx = point.x + skew;
			Fixed sy = point.y + skew;
			Fixed sz = point.z + skew;
			int ix = (int)sx;
			int iy = (int)sy;
			int iz = (int)sz;
			Fixed sample = Simplex3DPartSimple(point, ix, iy, iz, seed);
			sample += Simplex3DPartSimple(point, ix + 1, iy + 1, iz + 1, seed);
			Fixed x = sx - ix;
			Fixed y = sy - iy;
			Fixed z = sz - iz;

			if (x >= y)
			{
				if (x >= z)
				{
					sample += Simplex3DPartSimple(point, ix + 1, iy, iz, seed);
					if (y >= z)
						sample += Simplex3DPartSimple(point, ix + 1, iy + 1, iz, seed);
					else
						sample += Simplex3DPartSimple(point, ix + 1, iy, iz + 1, seed);
				}
				else
				{
					sample += Simplex3DPartSimple(point, ix, iy, iz + 1, seed);
					sample += Simplex3DPartSimple(point, ix + 1, iy, iz + 1, seed);
				}
			}
			else
			{
				if (y >= z)
				{
					sample += Simplex3DPartSimple(point, ix, iy + 1, iz, seed);
					if (x >= z)
						sample += Simplex3DPartSimple(point, ix + 1, iy + 1, iz, seed);
					else
						sample += Simplex3DPartSimple(point, ix, iy + 1, iz + 1, seed);
				}
				else
				{
					sample += Simplex3DPartSimple(point, ix, iy, iz + 1, seed);
					sample += Simplex3DPartSimple(point, ix, iy + 1, iz + 1, seed);
				}
			}
			return sample * simplexScale3D;
		}

		private static NoiseSampleFixed Simplex3DPart(Fixed3 point, int ix, int iy, int iz, int seed)
		{
			Fixed unskew = (Fixed)(ix + iy + iz) / 6;
			point.x = point.x - ix + unskew;
			point.y = point.y - iy + unskew;
			point.z = point.z - iz + unskew;
			Fixed f = Fixed.Half - point.x * point.x - point.y * point.y - point.z * point.z;
			NoiseSampleFixed sample = new NoiseSampleFixed();
			if (f > 0)
			{
				Fixed f2 = f * f;
				Fixed f3 = f * f2;
				Fixed3 g = simplexGradients3D[hash[hash[hash[hash[SeedHash(seed)] + ix & hashMask] + iy & hashMask] + iz & hashMask] & simplexGradientsMask3D];
				Fixed v = Fixed3.Dot(g, point);
				Fixed v6f2 = v * f2 * -6;
				sample.value = (v * f3);
				sample.derivative.x = (g.x * f3 + v6f2 * point.x);
				sample.derivative.y = (g.y * f3 + v6f2 * point.y);
				sample.derivative.z = (g.z * f3 + v6f2 * point.z);
			}
			return sample;
		}

		private static Fixed Simplex3DPartSimple(Fixed3 point, int ix, int iy, int iz, int seed)
		{
			Fixed unskew = (Fixed)(ix + iy + iz) / 6;
			point.x = point.x - ix + unskew;
			point.y = point.y - iy + unskew;
			point.z = point.z - iz + unskew;
			Fixed f = Fixed.Half - point.x * point.x - point.y * point.y - point.z * point.z;
			Fixed sample = 0;
			if (f > 0)
			{
				Fixed3 g = simplexGradients3D[hash[hash[hash[hash[SeedHash(seed)] + ix & hashMask] + iy & hashMask] + iz & hashMask] & simplexGradientsMask3D];
				sample = (Fixed3.Dot(g, point) * f * f * f);
			}
			return sample;
		}

		private static int SeedHash(int seed)
		{
			return hash[hash[hash[hash[(seed >> 16) & hashMask] + (seed >> 8) & hashMask] + (seed >> 24) & hashMask] + seed & hashMask];
		}
	}
}