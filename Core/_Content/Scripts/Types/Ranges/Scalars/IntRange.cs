﻿namespace Aurora
{
	[System.Serializable]
	public struct IntRange
	{
		/// <summary>
		/// The minimum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private int minimum;

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private int maximum;

		/// <summary>
		/// The minimum value of this range.
		/// </summary>
		public int Minimum
		{
			get { return minimum; }
			set { minimum = value; }
		}

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
		public int Maximum
		{
			get { return maximum; }
			set { maximum = value; }
		}

		/// <summary>
		/// Basic Constructor.
		/// </summary>
		/// <param name="minimum"></param>
		/// <param name="maximum"></param>
		public IntRange(int minimum, int maximum)
		{
			this.minimum = minimum;
			this.maximum = maximum;
		}

		/// <summary>
		/// Linear Interpolation function, allows you to interpolate between two ranges.
		/// </summary>
		/// <param name="a">Start range</param>
		/// <param name="b">End range</param>
		/// <param name="t">Interpolation value (0-1 ideally)</param>
		/// <returns></returns>
		public static IntRange Lerp(IntRange a, IntRange b, double t)
		{
			return new IntRange((int)(a.minimum + (b.minimum - (double)a.minimum) * t), (int)(a.maximum + (b.maximum - (double)a.maximum) * t));
		}
	}
}