﻿namespace Aurora
{
	[System.Serializable]
	public struct FloatRange
	{
		/// <summary>
		/// The minimum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private float minimum;

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private float maximum;

		/// <summary>
		/// The minimum value of this range.
		/// </summary>
		public float Minimum
		{
			get { return minimum; }
			set { minimum = value; }
		}

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
		public float Maximum
		{
			get { return maximum; }
			set { maximum = value; }
		}

		/// <summary>
		/// Basic Constructor.
		/// </summary>
		/// <param name="minimum"></param>
		/// <param name="maximum"></param>
		public FloatRange(float minimum, float maximum)
		{
			this.minimum = minimum;
			this.maximum = maximum;
		}

		/// <summary>
		/// Linear Interpolation function, allows you to interpolate between two ranges.
		/// </summary>
		/// <param name="a">Start range</param>
		/// <param name="b">End range</param>
		/// <param name="t">Interpolation value (0-1 ideally)</param>
		/// <returns></returns>
		public static FloatRange Lerp(FloatRange a, FloatRange b, float t)
		{
			return new FloatRange(a.minimum + (b.minimum - a.minimum) * t, a.maximum + (b.maximum - a.maximum) * t);
		}
	}
}