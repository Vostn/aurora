﻿namespace Aurora
{
	[System.Serializable]
	public struct Double3Range
	{
		/// <summary>
		/// The minimum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private Double3 minimum;

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private Double3 maximum;

		/// <summary>
		/// The minimum value of this range.
		/// </summary>
		public Double3 Minimum
		{
			get { return minimum; }
		}

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
		public Double3 Maximum
		{
			get { return maximum; }
		}

		/// <summary>
		/// Basic Constructor.
		/// </summary>
		/// <param name="minimum"></param>
		/// <param name="maximum"></param>
		public Double3Range(Double3 minimum, Double3 maximum)
		{
			this.minimum = minimum;
			this.maximum = maximum;
		}

		public override string ToString()
		{
			return $"Min: ({minimum}) Max: ({maximum})";
		}

		/// <summary>
		/// Linear Interpolation function, allows you to interpolate between two ranges.
		/// </summary>
		/// <param name="a">Start range</param>
		/// <param name="b">End range</param>
		/// <param name="t">Interpolation value (0-1 ideally)</param>
		/// <returns></returns>
		public static Double3Range Lerp(Double3Range a, Double3Range b, double t)
		{
			return new Double3Range(a.minimum + (b.minimum - a.minimum) * t, a.maximum + (b.maximum - a.maximum) * t);
		}
	}
}