﻿namespace Aurora
{
	[System.Serializable]
	public struct Float3Range
	{
		/// <summary>
		/// The minimum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private Float3 minimum;

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
#if UNITY_2017_1_OR_NEWER
		[UnityEngine.SerializeField]
#endif
		private Float3 maximum;

		/// <summary>
		/// The minimum value of this range.
		/// </summary>
		public Float3 Minimum
		{
			get { return minimum; }
			set { minimum = value; }
		}

		/// <summary>
		/// The maximum value of this range.
		/// </summary>
		public Float3 Maximum
		{
			get { return maximum; }
			set { maximum = value; }
		}

		/// <summary>
		/// Basic Constructor.
		/// </summary>
		/// <param name="minimum"></param>
		/// <param name="maximum"></param>
		public Float3Range(Float3 minimum, Float3 maximum)
		{
			this.minimum = minimum;
			this.maximum = maximum;
		}

		public override string ToString()
		{
			return $"Min: ({minimum}) Max: ({maximum})";
		}

		/// <summary>
		/// Linear Interpolation function, allows you to interpolate between two ranges.
		/// </summary>
		/// <param name="a">Start range</param>
		/// <param name="b">End range</param>
		/// <param name="t">Interpolation value (0-1 ideally)</param>
		/// <returns></returns>
		public static Float3Range Lerp(Float3Range a, Float3Range b, double t)
		{
			return new Float3Range(a.minimum + (b.minimum - a.minimum) * t, a.maximum + (b.maximum - a.maximum) * t);
		}
	}
}