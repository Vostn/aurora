﻿using System;
using System.Runtime.CompilerServices;

namespace Aurora
{
	public struct Fixed : System.IComparable<Fixed>, System.IEquatable<Fixed>, IComparable
	{
		public static readonly Fixed PI = FromRawValue(6588397);
		public static readonly Fixed PI2 = FromRawValue(13176795);
		public static readonly Fixed MilliTauToDeg = FromRawValue(754975);
		public static readonly Fixed DegToMilliTau = FromRawValue(5825422);
		public static readonly Fixed MilliTauToRad = FromRawValue(13177);
		public static readonly Fixed RadToMilliTau = FromRawValue(333772107);
		public static readonly Fixed TauToDeg = new Fixed(360);
		public static readonly Fixed DegToTau = FromRawValue(5825);
		public static readonly Fixed TauToRad = PI2;
		public static readonly Fixed RadToTau = FromRawValue(333772);
		public static readonly Fixed Half = FromRawValue(1048576);
		public static readonly Fixed Zero = FromRawValue(0);
		public static readonly Fixed One = FromRawValue(2097152);
		public static readonly Fixed Epsilon = FromRawValue(1);
		private static readonly Fixed SqrtErrorDefault = FromRawValue(5);
		private const int decimalBits = 21;
		private const long scalingValue = ((long)1) << decimalBits;
		private long value;

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Fixed(int value)
		{
			this.value = (long)value << decimalBits;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Fixed(long value)
		{
			this.value = value << decimalBits;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public Fixed(Fixed value)
		{
			this.value = value.value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static implicit operator Fixed(int v)
		{
			return new Fixed(v);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator Fixed(long v)
		{
			return new Fixed(v);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator int(Fixed v)
		{
			return (int)(v.value >> decimalBits);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator long(Fixed v)
		{
			return v.value >> decimalBits;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator Fixed(double v)
		{
			return new Fixed() { value = (long)(v * scalingValue) };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator float(Fixed v)
		{
			return ((float)v.value) / scalingValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static explicit operator double(Fixed v)
		{
			return ((double)v.value) / scalingValue;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator +(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = v1.value + v2.value };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator -(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = v1.value - v2.value };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator -(Fixed v1)
		{
			return new Fixed() { value = -v1.value };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator *(Fixed v1, int v2)
		{
			return new Fixed() { value = v1.value * v2 };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator *(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = (v1.value * v2.value) >> decimalBits };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator /(Fixed v1, int v2)
		{
			return new Fixed() { value = v1.value / v2 };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator /(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = (v1.value << decimalBits) / v2.value };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator %(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = v1.value % v2.value };
		}

		public static Fixed Wrap(Fixed v, Fixed min, Fixed max)
		{
			if (min > max)
			{
				Fixed oldMin = min;
				min = max;
				max = oldMin;
			}
			Fixed range = (max - min);
			if (v < min)
				return max + (((v + 1) - min) % range) - 1;
			return min + ((v - min) % range);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator &(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = v1.value & v2.value };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator |(Fixed v1, Fixed v2)
		{
			return new Fixed() { value = v1.value | v2.value };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >(Fixed v1, Fixed v2)
		{
			return v1.value > v2.value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <(Fixed v1, Fixed v2)
		{
			return v1.value < v2.value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator >=(Fixed v1, Fixed v2)
		{
			return v1.value >= v2.value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator <=(Fixed v1, Fixed v2)
		{
			return v1.value <= v2.value;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator ^(Fixed v1, Fixed v2)
		{
			return new Fixed(v1.value ^ v2.value);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed operator ~(Fixed v)
		{
			return new Fixed(~v.value);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator ==(Fixed v1, Fixed v2)
		{
			return (v1.value == v2.value);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static bool operator !=(Fixed v1, Fixed v2)
		{
			return !(v1 == v2);
		}

		public static Fixed operator <<(Fixed v, int amount)
		{
			return new Fixed() { value = v.value << amount };
		}

		public static Fixed operator >>(Fixed v, int amount)
		{
			return new Fixed() { value = v.value >> amount };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed Sqrt(Fixed v, Fixed acceptableError)
		{
			if (v.value < 0) //NaN in Math.Sqrt
				throw new System.ArithmeticException($"Input Error: Value was {v}");

			if (v.value == 0)
				return 0;

			Fixed x = v;
			Fixed y = v / x;
			while (x - y > acceptableError)
			{
				x = (x + y) / 2;
				y = v / x;
			}
			return x;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed Sqrt(Fixed v)
		{
			return Sqrt(v, SqrtErrorDefault);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed Lerp(Fixed v1, Fixed v2, Fixed t)
		{
			return v1 + ((v2 - v1) * t);
		}

		public static Fixed Clamp(Fixed value, Fixed min, Fixed max)
		{
			if (value < min)
				return min;
			if (value > max)
				return max;
			return value;
		}

		public static Fixed Repeat(Fixed s, Fixed min, Fixed max)
		{
			if (min > max)
				Math.SwapValues(ref min, ref max);
			Fixed range = max - min;
			return (((s - min) % range) + range) % range + min;
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static int Sign(Fixed v)
		{
			return System.Math.Sign(v.value);
		}
		
		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed FromRawValue(long rawValue)
		{
			return new Fixed() { value = rawValue };
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed Min(Fixed v1, Fixed v2)
		{
			Fixed diff = v1 - v2;
			return v2 + (diff & diff >> 63);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed Max(Fixed v1, Fixed v2)
		{
			Fixed diff = v1 - v2;
			return v1 - (diff & diff >> 63);
		}

		[MethodImpl(MethodImplOptions.AggressiveInlining)]
		public static Fixed Abs(Fixed value)
		{
			long mask = value.value >> 63;
			return new Fixed() { value = (value.value + mask) ^ mask };
		}

		public override bool Equals(object obj)
		{
			if (obj == null || obj.GetType() != GetType())
				return false;
			Fixed v = (Fixed)obj;
			return v.value == value;
		}

		public override int GetHashCode()
		{
			return value.GetHashCode();
		}

		public override string ToString()
		{
			bool negative = value < 0;
			string signString = negative ? "-" : "";
			double fractionValue = System.Math.Abs(value & (scalingValue - 1)) / (double)scalingValue;
			long wholeValue = System.Math.Abs(value) >> decimalBits;
			if (negative && fractionValue != 0)
				fractionValue = 1 - fractionValue;
			return $"{signString}{wholeValue}{fractionValue.ToString(".#####################")}";
		}

		int IComparable<Fixed>.CompareTo(Fixed other)
		{
			return value < other.value ? -1 : (value == other.value ? 0 : 1);
		}

		bool IEquatable<Fixed>.Equals(Fixed other)
		{
			return other.value == value;
		}

		int IComparable.CompareTo(object obj)
		{
			if (obj == null)
				return 1;
			bool otherIsFixed = obj is Fixed;
			if (otherIsFixed)
			{
				Fixed other = (Fixed)obj;
				return value < other.value ? -1 : (value == other.value ? 0 : 1);
			}
			else
				throw new ArgumentException("Object is not of type Fixed");
		}
	}
}