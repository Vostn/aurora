﻿using System.ComponentModel;
using System.Globalization;

namespace Aurora.Converters
{
	public class Double3_Converter : TypeConverter
	{
		public override bool CanConvertFrom(ITypeDescriptorContext context, System.Type sourceType)
		{
			if (sourceType != typeof(string))
				return base.CanConvertFrom(context, sourceType);
			return true;
		}
		
		public override object ConvertFrom(ITypeDescriptorContext context, CultureInfo culture, object value)
		{
			if (value.GetType() != typeof(string))
				return base.ConvertFrom(context, culture, value);

			string val = (string)value;
			string[] components = val.Split(',');
			if (components.Length < 3)
				throw new System.NotSupportedException();
			double x, y, z;
			if (!double.TryParse(components[0], out x))
				throw new System.NotSupportedException();
			if (!double.TryParse(components[1], out y))
				throw new System.NotSupportedException();
			if (!double.TryParse(components[2], out z))
				throw new System.NotSupportedException();
			return new Double3(x, y, z);
		}

		public override bool IsValid(ITypeDescriptorContext context, object value)
		{
			if (value.GetType() != typeof(string))
				return base.IsValid(context, value);

			try { ConvertFrom(context, null, value); }
			catch { return false; }
			return true;
		}
	}
}
