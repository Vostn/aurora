﻿namespace Aurora.Collections
{
	/// <summary>
	/// ReadOnly interface variant for Path collection type, a class that tracks a set of vector points, and provides lazy cached evaluation of the distances between them, and the total path length.
	/// </summary>
	public interface IReadOnlyPath<T> : IReadOnlyPath<T, T> where T : struct, IVector<T> { }

	/// <summary>
	/// ReadOnly interface variant for Path collection type, a class that tracks a set of vector points, and provides lazy cached evaluation of the distances between them, and the total path length.
	/// </summary>
	public interface IReadOnlyPath<T, U> where U : struct, IVector<U> where T : IVector<U>
	{
		int NodeCount { get; }
		double TotalLength { get; }
		double TotalLengthLooped { get; }
		T this[int i] { get; }

		double DistanceToNextNode(int firstNodeIndex);

		double DistanceToPreviousNode(int secondNodeIndex);
	}
}