﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace Aurora.Collections
{
	public class IndexedQueue<T> : IReadOnlyList<T>
	{
		private T[] array;
		private int firstIndex;

		public IndexedQueue()
		{
			array = new T[4];
			Count = 0;
			firstIndex = 0;
		}

		public IndexedQueue(IEnumerable<T> collection)
		{
			if (collection == null)
				throw new ArgumentNullException(nameof(collection), "Argument \"collection\" is null!");

			Count = 0;

			var enumerator = collection.GetEnumerator();
			while (enumerator.MoveNext())
				++Count;
			firstIndex = 0;
			array = new T[Count];
		}

		public IndexedQueue(int capacity)
		{
			if (capacity < 0)
				throw new ArgumentOutOfRangeException(nameof(capacity), "Argument \"capacity\" is less than zero!");

			Count = 0;
			firstIndex = 0;
			array = new T[capacity];
		}

		public int Count { get; private set; }

		public T this[int index]
		{
			get
			{
				if (index < 0)
					throw new ArgumentOutOfRangeException(nameof(index), "Argument \"index\" must be positive!");
				if (index >= Count)
					throw new ArgumentOutOfRangeException(nameof(index), "Argument \"index\" must be less than \"Count\"!");

				int targetIndex = index + firstIndex;
				if (targetIndex >= array.Length)
					targetIndex -= array.Length;
				return array[targetIndex];
			}
			set
			{
				if (index < 0)
					throw new ArgumentOutOfRangeException(nameof(index), "Argument \"index\" must be positive!");
				if (index >= Count)
					throw new ArgumentOutOfRangeException(nameof(index), "Argument \"index\" must be less than \"Count\"!");

				int targetIndex = index + firstIndex;
				if (targetIndex >= array.Length)
					targetIndex -= array.Length;
				array[targetIndex] = value;
			}
		}

		public void Clear()
		{
			Count = 0;
			firstIndex = 0;
		}

		public bool Contains(T element)
		{
			for (int i = 0; i < Count; ++i)
			{
				if (EqualityComparer<T>.Default.Equals(this[i], element))
					return true;
			}
			return false;
		}

		public void CopyTo(T[] array, int index)
		{
			if (array == null)
				throw new ArgumentNullException($"Argument \"{nameof(array)}\" is null!");
			if (index < 0)
				throw new ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" is less than zero!");
			if (index + Count >= array.Length)
				throw new ArgumentException($"The available space in \"{nameof(array)}\" after index {index} is less than the number of elements in this IndexedQueue!");

			for (int i = 0; i < Count; ++i)
				array[i + index] = this[i];
		}

		public void Enqueue(T element)
		{
			if (Count >= array.Length)
				Resize(Count * 3);

			int index = firstIndex + Count;
			if (index >= array.Length)
				index -= array.Length;

			array[index] = element;
			++Count;
		}

		public T Unqueue()
		{
			if (Count <= 0)
				throw new InvalidOperationException("Queue is empty!");
			return array[--Count];
		}

		public T Dequeue()
		{
			if (Count <= 0)
				throw new InvalidOperationException("Queue is empty!");

			T element = array[firstIndex];

			--Count;
			++firstIndex;
			if (firstIndex >= array.Length)
				firstIndex = 0;

			return element;
		}

		public IEnumerator<T> GetEnumerator()
		{
			for (int i = 0; i < Count; ++i)
				yield return this[i];
		}

		public T[] ToArray()
		{
			T[] newArray = new T[Count];
			for (int i = 0; i < Count; ++i)
				newArray[i] = this[i];
			return newArray;
		}

		public void TrimExcess()
		{
			if (((double)Count / array.Length) < 0.9)
				Resize(Count);
		}

		IEnumerator<T> IEnumerable<T>.GetEnumerator()
		{
			return GetEnumerator();
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return GetEnumerator();
		}

		private void Resize(int newSize)
		{
			if (newSize < Count)
				throw new InvalidOperationException("Argument \"newSize\" is less than member field \"count\"!");

			T[] newArray = new T[newSize];
			for (int i = 0; i < Count; ++i)
			{
				newArray[i] = this[i];
			}
			array = newArray;
			firstIndex = 0;
		}
	}
}