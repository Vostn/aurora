﻿using System.Collections.Generic;

namespace Aurora.Collections
{
	/// <summary>
	/// Tracks a set of vector points, and provides lazy cached evaluation of the distances between them, and the total path length.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class QueuePath<T> : QueuePath<T, T>, IReadOnlyPath<T> where T : struct, IVector<T> { }

	/// <summary>
	/// Tracks a set of vector points, and provides lazy cached evaluation of the distances between them, and the total path length.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class QueuePath<T, U> : IReadOnlyPath<T, U> where T : struct, IVector<U> where U : struct, IVector<U>
	{
		private readonly IndexedQueue<T> nodes = new IndexedQueue<T>();
		private readonly IndexedQueue<double> distances = new IndexedQueue<double>();

		public int NodeCount { get { return nodes.Count; } }

		private bool totalLengthValid = false;

		private double totalLength = -1;

		public double TotalLength
		{
			get
			{
				if (!totalLengthValid)
					CalculateTotalLength();
				return totalLength;
			}
		}

		private double totalLengthLooped = -1;

		public double TotalLengthLooped
		{
			get
			{
				if (!totalLengthValid)
					CalculateTotalLength();
				return totalLengthLooped;
			}
		}

		public QueuePath()
		{
		}

		public QueuePath(IEnumerable<T> nodes)
		{
			if (nodes == null)
				throw new System.ArgumentNullException($"Argument {nameof(nodes)} is null!");
			foreach (T node in nodes)
				EnqueueNode(node);
		}

		public T this[int i]
		{
			get { return GetNode(i); }
		}

		public double DistanceToNextNode(int firstNodeIndex)
		{
			if (firstNodeIndex < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be positive!");
			if (firstNodeIndex >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be less than {nameof(NodeCount)}!");

			if (distances[firstNodeIndex] == -1)
				CalculateDistance(firstNodeIndex);
			return distances[firstNodeIndex];
		}

		public double DistanceToPreviousNode(int secondNodeIndex)
		{
			if (secondNodeIndex < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(secondNodeIndex)}\" must be positive!");
			if (secondNodeIndex >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(secondNodeIndex)}\" must be less than {nameof(NodeCount)}!");

			int firstNodeIndex = Math.Repeat(secondNodeIndex - 1, 0, NodeCount - 1);
			return DistanceToNextNode(firstNodeIndex);
		}

		private void CalculateDistance(int firstNodeIndex)
		{
			if (firstNodeIndex < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be positive!");
			if (firstNodeIndex >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be less than {nameof(NodeCount)}!");

			int secondNodeIndex = Math.Repeat(firstNodeIndex + 1, 0, NodeCount - 1);
			distances[firstNodeIndex] = nodes[firstNodeIndex].Difference(nodes[secondNodeIndex].Value).Magnitude;
		}

		private void CalculateTotalLength()
		{
			totalLength = totalLengthLooped = 0;
			for (int i = 0; i < NodeCount; ++i)
			{
				double distance = DistanceToNextNode(i);
				totalLengthLooped += distance;
				if (i != NodeCount - 1)
					totalLength += distance;
			}
			totalLengthValid = true;
		}

		private T GetNode(int index)
		{
			if (index < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be positive!");
			if (index >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be less than {nameof(NodeCount)}!");
			return nodes[index];
		}

		public void EnqueueNode(T newNode)
		{
			totalLengthValid = false;
			if(distances.Count > 0)
				distances[distances.Count - 1] = -1;
			distances.Enqueue(-1);
			nodes.Enqueue(newNode);
		}

		public T DequeueNode()
		{
			totalLengthValid = false;
			if(distances.Count > 0)
				distances[distances.Count - 1] = -1;
			distances.Dequeue();
			return nodes.Dequeue();
		}

		public void Clear()
		{
			totalLength = totalLengthLooped = 0;
			totalLengthValid = true;
			nodes.Clear();
			distances.Clear();
		}
	}
}