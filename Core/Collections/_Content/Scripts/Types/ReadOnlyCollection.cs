﻿using System.Collections;
using System.Collections.Generic;

namespace Aurora.Collections
{
	public class ReadOnlyCollection<T> : IReadOnlyCollection<T>
	{
		private readonly ICollection<T> targetCollection;

		public ReadOnlyCollection(ICollection<T> targetCollection)
		{
			this.targetCollection = targetCollection;
		}

		public int Count => targetCollection.Count;

		public IEnumerator<T> GetEnumerator() => targetCollection.GetEnumerator();

		IEnumerator IEnumerable.GetEnumerator() => targetCollection.GetEnumerator();
	}
}