﻿namespace Aurora.Collections
{
	public class Heap<T, U> where T : System.IComparable
	{
		private readonly int dimensions;
		private HeapNode[] heapArray;

		public int Count { get; private set; }

		public Heap(int dimensions = 2, int initialSize = 8)
		{
			if (dimensions < 2)
			{
				dimensions = 2;
				UnityEngine.Debug.LogWarning("Argument \"dimensions\" must be greater than or equal to 2! Clamping to 2.");
			}
			this.dimensions = dimensions;
			if (initialSize < 0)
				throw new System.ArgumentOutOfRangeException(nameof(initialSize), "Argument \"initialSize\" must be positive!");
			heapArray = new HeapNode[initialSize];
		}

		public void Push(T key, U element)
		{
			int index = Count++;
			if (heapArray.Length < Count)
				ExpandArray();

			HeapNode newHeapNode = new HeapNode(key, element);
			if (index == 0)
			{
				heapArray[index] = newHeapNode;
				return;
			}

			bool nodePlaced = false;
			while (!nodePlaced)
			{
				heapArray[index] = newHeapNode;
				int parentIndex = CalculateParentArrayIndex(index);
				HeapNode parent = heapArray[parentIndex];
				if (parent.Key.CompareTo(key) > 0)
				{
					heapArray[index] = parent;
					heapArray[parentIndex] = newHeapNode;
					index = parentIndex;
					if (index == 0)
						nodePlaced = true;
				}
				else
					nodePlaced = true;
			}
		}

		public HeapNode Peek()
		{
			if (Count == 0)
				throw new System.InvalidOperationException("Heap is empty!");
			return heapArray[0];
		}

		public HeapNode Pop()
		{
			if (Count == 0)
				throw new System.InvalidOperationException("Heap is empty!");

			HeapNode returnHeapNode = heapArray[0];
			int index = --Count;
			HeapNode targetHeapNode = heapArray[index];
			heapArray[0] = targetHeapNode;
			index = 0;
			bool nodePlaced = false;
			while (!nodePlaced)
			{
				int smallestChildIndex = 0;
				HeapNode smallestChild = targetHeapNode;
				for (int i = 0; i < dimensions; ++i)
				{
					int childIndex = CalculateChildArrayIndex(index, i);
					if (childIndex < Count)
					{
						HeapNode child = heapArray[childIndex];
						if (smallestChild.Key.CompareTo(child.Key) > 0)
						{
							smallestChildIndex = childIndex;
							smallestChild = child;
						}
					}
					else
						break;
				}
				if (smallestChildIndex > 0)
				{
					HeapNode child = heapArray[smallestChildIndex];
					heapArray[index] = child;
					heapArray[smallestChildIndex] = targetHeapNode;
					index = smallestChildIndex;
				}
				else
					nodePlaced = true;
			}

			return returnHeapNode;
		}

		public void Clear()
		{
			Count = 0;
		}

		private bool CheckHeapness(int index = 0)
		{
			for (int i = 0; i < dimensions; ++i)
			{
				int childIndex = CalculateChildArrayIndex(index, i);
				if (childIndex < Count)
				{
					if (heapArray[index].Key.CompareTo(heapArray[childIndex].Key) > 0)
						return false;
					if (!CheckHeapness(childIndex))
						return false;
				}
			}
			return true;
		}

		private void ExpandArray()
		{
			if(Count == 0)
				System.Array.Resize(ref heapArray, 8);
			else
				System.Array.Resize(ref heapArray, Count * 2);
		}

		private int CalculateParentArrayIndex(int index)
		{
			return (index - 1) / dimensions;
		}

		private int CalculateChildArrayIndex(int index, int childLocalIndex)
		{
			return (dimensions * index) + (1 + childLocalIndex);
		}

		public struct HeapNode
		{
			public T Key { get; }
			public U Element { get; }

			public HeapNode(T key, U element)
			{
				Key = key;
				Element = element;
			}
		}
	}
}