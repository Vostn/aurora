﻿using System.Collections;
using System.Collections.Generic;

namespace Aurora.Collections
{
	public class ReadOnlyDictionary<K, V> : IReadOnlyDictionary<K, V>
	{
		private readonly IDictionary<K, V> targetDictionary;
		private readonly ReadOnlyCollection<K> keysCache;
		private readonly ReadOnlyCollection<V> valuesCache;

		public ReadOnlyDictionary(IDictionary<K, V> targetDictionary)
		{
			this.targetDictionary = targetDictionary;
			keysCache = new ReadOnlyCollection<K>(targetDictionary.Keys);
			valuesCache = new ReadOnlyCollection<V>(targetDictionary.Values);
		}

		public V this[K key]
		{
			get
			{
				if (key == null)
					throw new System.ArgumentNullException(nameof(key), "Argument \"key\" is null!");
				if (targetDictionary.ContainsKey(key))
					return targetDictionary[key];
				throw new KeyNotFoundException($"Key \"{key}\" doesn't exist in this dictionary!");
			}
		}

		public IEnumerable<K> Keys => keysCache;

		public IEnumerable<V> Values => valuesCache;

		public int Count => targetDictionary.Count;

		public bool ContainsKey(K key)
		{
			return targetDictionary.ContainsKey(key);
		}

		IEnumerator IEnumerable.GetEnumerator()
		{
			return targetDictionary.GetEnumerator();
		}

		public IEnumerator<KeyValuePair<K, V>> GetEnumerator()
		{
			return targetDictionary.GetEnumerator();
		}

		public bool TryGetValue(K key, out V value)
		{
			return targetDictionary.TryGetValue(key, out value);
		}
	}
}