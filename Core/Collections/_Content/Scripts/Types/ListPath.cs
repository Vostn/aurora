﻿using System.Collections.Generic;

namespace Aurora.Collections
{
	/// <summary>
	/// Tracks a set of vector points, and provides lazy cached evaluation of the distances between them, and the total path length.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ListPath<T> : ListPath<T, T>, IReadOnlyPath<T> where T : struct, IVector<T> { }

	/// <summary>
	/// Tracks a set of vector points, and provides lazy cached evaluation of the distances between them, and the total path length.
	/// </summary>
	/// <typeparam name="T"></typeparam>
	public class ListPath<T, U> : IReadOnlyPath<T, U> where T : struct, IVector<U> where U : struct, IVector<U>
	{
		private readonly List<T> nodes = new List<T>();
		private readonly List<double> distances = new List<double>();

		public int NodeCount { get { return nodes.Count; } }

		private bool totalLengthValid = false;

		private double totalLength = -1;

		public double TotalLength
		{
			get
			{
				if (!totalLengthValid)
					CalculateTotalLength();
				return totalLength;
			}
		}

		private double totalLengthLooped = -1;

		public double TotalLengthLooped
		{
			get
			{
				if (!totalLengthValid)
					CalculateTotalLength();
				return totalLengthLooped;
			}
		}

		public ListPath()
		{
		}

		public ListPath(IEnumerable<T> nodes)
		{
			if (nodes == null)
				throw new System.ArgumentNullException($"Argument {nameof(nodes)} is null!");
			foreach (T node in nodes)
				AddNode(node);
		}

		public T this[int i]
		{
			get { return GetNode(i); }
			set { SetNode(i, value); }
		}

		public double DistanceToNextNode(int firstNodeIndex)
		{
			if (firstNodeIndex < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be positive!");
			if (firstNodeIndex >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be less than {nameof(NodeCount)}!");

			if (distances[firstNodeIndex] == -1)
				CalculateDistance(firstNodeIndex);
			return distances[firstNodeIndex];
		}

		public double DistanceToPreviousNode(int secondNodeIndex)
		{
			if (secondNodeIndex < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(secondNodeIndex)}\" must be positive!");
			if (secondNodeIndex >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(secondNodeIndex)}\" must be less than {nameof(NodeCount)}!");

			int firstNodeIndex = Math.Repeat(secondNodeIndex - 1, 0, NodeCount - 1);
			return DistanceToNextNode(firstNodeIndex);
		}

		private void CalculateDistance(int firstNodeIndex)
		{
			if (firstNodeIndex < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be positive!");
			if (firstNodeIndex >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(firstNodeIndex)}\" must be less than {nameof(NodeCount)}!");

			int secondNodeIndex = Math.Repeat(firstNodeIndex + 1, 0, NodeCount - 1);
			distances[firstNodeIndex] = nodes[firstNodeIndex].Difference(nodes[secondNodeIndex].Value).Magnitude;
		}

		private void CalculateTotalLength()
		{
			totalLength = totalLengthLooped = 0;
			for (int i = 0; i < NodeCount; ++i)
			{
				double distance = DistanceToNextNode(i);
				totalLengthLooped += distance;
				if (i != NodeCount - 1)
					totalLength += distance;
			}
			totalLengthValid = true;
		}

		public T GetNode(int index)
		{
			if (index < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be positive!");
			if (index >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be less than {nameof(NodeCount)}!");
			return nodes[index];
		}

		public void AddNode(T newNode)
		{
			if(NodeCount > 0)
				distances[NodeCount - 1] = -1;
			nodes.Add(newNode);
			distances.Add(-1);
			totalLengthValid = false;
		}

		public void InsertNode(int index, T newNode)
		{
			if (index < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be positive!");
			if (index > nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be less than or equal to {nameof(NodeCount)}!");

			totalLengthValid = false;
			nodes.Insert(index, newNode);
			distances.Insert(index, -1);
			distances[Math.Repeat(index - 1, 0, NodeCount - 1)] = -1;
		}

		public void SetNode(int index, T newNode)
		{
			if (index < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be positive!");
			if (index >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be less than or equal to {nameof(NodeCount)}!");

			totalLengthValid = false;
			nodes[index] = newNode;
			distances[index] = -1;
			distances[Math.Repeat(index - 1, 0, NodeCount - 1)] = -1;
		}

		public void RemoveNode(int index)
		{
			if (index < 0)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be positive!");
			if (index >= nodes.Count)
				throw new System.ArgumentOutOfRangeException($"Argument \"{nameof(index)}\" must be less than {nameof(NodeCount)}!");

			totalLengthValid = false;
			nodes.RemoveAt(index);
			distances.RemoveAt(index);
			if (NodeCount > 0)
				distances[Math.Repeat(index - 1, 0, NodeCount - 1)] = -1;
		}

		public void Clear()
		{
			totalLengthValid = true;
			totalLength = totalLengthLooped = 0;
			nodes.Clear();
			distances.Clear();
		}
	}
}