﻿using UnityEngine;
using UnityEditor;
using System.Linq;

namespace Aurora.LocalisationSystem.Unity.Editor
{
	[CustomPropertyDrawer(typeof(LanguageCode))]
	public class LanguageCode_Drawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			var allCultures = Localisation.GetAllCultures();
			string[] languageCodes = allCultures.Select(x => x.Name).ToArray();
			string[] languageNames = allCultures.Select(x => $"{x.NativeName} ({x.DisplayName})").ToArray();
			SerializedProperty targetProperty = property.FindPropertyRelative("languageCode");
			if (targetProperty.hasMultipleDifferentValues)
			{
				EditorGUI.showMixedValue = true;
				EditorGUI.BeginChangeCheck();
				string newKey = languageCodes[EditorGUI.Popup(position, label.text, 0, languageNames)];
				if (EditorGUI.EndChangeCheck())
					targetProperty.stringValue = newKey;
				EditorGUI.showMixedValue = false;
			}
			else
			{
				string currentSelection = targetProperty.stringValue;
				int selectionIndex = 0;
				for (int i = 0; i < languageCodes.Length; ++i)
				{
					if (languageCodes[i] == currentSelection)
						selectionIndex = i;
				}
				EditorGUI.BeginChangeCheck();
				string newKey = languageCodes[EditorGUI.Popup(position, label.text, selectionIndex, languageNames)];
				if (EditorGUI.EndChangeCheck())
					targetProperty.stringValue = newKey;
			}
		}
	}
}