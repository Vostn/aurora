﻿using UnityEngine;
using UnityEditor;
using System.Linq;

namespace Aurora.LocalisationSystem.Unity.Editor
{
	[CustomPropertyDrawer(typeof(LocalisedStringKey))]
	public class LocalisedStringKey_Drawer : PropertyDrawer
	{
		private string filterText = string.Empty;

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			string[] stringKeys = Localisation.GetAllStringKeys();
			SerializedProperty targetProperty = property.FindPropertyRelative("stringKey");
			Rect topRect = position;
			topRect.height = EditorGUIUtility.singleLineHeight;

			Rect controlRect = EditorGUI.PrefixLabel(topRect, new GUIContent(property.displayName));

			string filterLabelString = "Filter:";
			GUIContent filterLabel = new GUIContent(filterLabelString);
			Vector2 filterLabelSize = EditorStyles.label.CalcSize(filterLabel);
			Rect filterLabelRect = controlRect;
			filterLabelRect.width = filterLabelSize.x;
			EditorGUI.LabelField(filterLabelRect, filterLabelString);

			Rect filterTextBoxRect = controlRect;
			filterTextBoxRect.x += filterLabelRect.width;
			filterTextBoxRect.width -= filterLabelRect.width;
			filterText = filterText.ToUpper();
			filterText = EditorGUI.TextField(filterTextBoxRect, filterText);
			filterText = filterText.ToUpper();

			Rect bottomRect = position;
			bottomRect.y += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
			bottomRect.height = EditorGUIUtility.singleLineHeight;

			if (!string.IsNullOrWhiteSpace(filterText))
			{
				string[] filterTerms = filterText.Split(new char[] { ' ' }, System.StringSplitOptions.RemoveEmptyEntries);
				stringKeys = stringKeys.Where(x =>
				{
					foreach (string filterTerm in filterTerms)
					{
						if (!x.Contains(filterTerm))
							return false;
					}
					return true;
				}).ToArray();
			}

			if (stringKeys.Length > 0)
			{
				if (targetProperty.hasMultipleDifferentValues)
				{
					EditorGUI.showMixedValue = true;
					EditorGUI.BeginChangeCheck();
					string newKey = stringKeys[EditorGUI.Popup(bottomRect, " ", 0, stringKeys)];
					if (EditorGUI.EndChangeCheck())
						targetProperty.stringValue = newKey;
					EditorGUI.showMixedValue = false;
				}
				else
				{
					string currentSelection = targetProperty.stringValue;
					int selectionIndex = 0;
					for (int i = 0; i < stringKeys.Length; ++i)
					{
						if (stringKeys[i] == currentSelection)
							selectionIndex = i;
					}
					EditorGUI.BeginChangeCheck();
					string newKey = stringKeys[EditorGUI.Popup(bottomRect, " ", selectionIndex, stringKeys)];
					if (EditorGUI.EndChangeCheck())
						targetProperty.stringValue = newKey;
				}
			}
			else
			{
				EditorGUI.LabelField(bottomRect, " ", "No strings match this filter!");
			}

			EditorGUI.EndProperty();
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return (EditorGUIUtility.singleLineHeight * 2) + EditorGUIUtility.standardVerticalSpacing;
		}
	}
}