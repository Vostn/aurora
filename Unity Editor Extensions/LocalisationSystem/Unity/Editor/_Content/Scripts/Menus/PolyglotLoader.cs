﻿using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.Networking;

namespace Aurora.LocalisationSystem.Unity.Editor
{
	public static class PolyglotLoader
	{
		[MenuItem("Aurora/Load Default Localisation", priority = 20)]
		public static void LoadPolyglot()
		{
			const string documentID = "17f0dQawb-s_Fd7DHgmVvJoEGDMH_yoSd8EYigrb0zmM";
			const string sheetID = "296134756";
			string url = $"http://docs.google.com/spreadsheets/d/{documentID}/export?format=tsv&gid={sheetID}";
			UnityWebRequest webRequest = UnityWebRequest.Get(url);
			webRequest.SendWebRequest();
			while (!webRequest.isDone)
			{
				//Waiting for completion
			}
			if (!webRequest.isNetworkError)
				Import(webRequest.downloadHandler.text);
			else
				Debug.LogError($"Failed to download polyglot sheet: {webRequest.error}");
		}

		private static void Import(string sheetContents)
		{
			List<List<string>> languageCodes = new List<List<string>>();
			List<bool> langaugeIsRTL = new List<bool>();
			List<Dictionary<string, string>> languageStrings = new List<Dictionary<string, string>>();

			List<string> lines = new List<string>();
			while (true)
			{
				int lineEndIndex = sheetContents.IndexOf("\r\n");
				bool lastLine = lineEndIndex == -1;

				if(lastLine)
					lineEndIndex = sheetContents.Length;

				string line = sheetContents.Substring(0, lineEndIndex);
				lines.Add(line);

				if (lastLine)
					break;

				sheetContents = sheetContents.Substring(line.Length + 2);
			}

			//Get Language codes
			string key;
			IterateOverLine(lines[1], out key, (x, y) =>
			{
				string[] codes = x.Split(' ');
				for (int i = 0; i < codes.Length; ++i)
					codes[i] = $"{codes[i].Substring(0, 2)}-{codes[i].Substring(2, codes[i].Length - 2)}";
				languageCodes.Add(new List<string>(codes));
				languageStrings.Add(new Dictionary<string, string>());
			});

			//Get Language Direction
			IterateOverLine(lines[3], out key, (x, y) =>
			{
				langaugeIsRTL.Add(x == "rtl");
			});

			//Get all keys
			for (int i = 6; i < lines.Count; ++i)
			{
				IterateOverLine(lines[i], out key, (x, y) =>
				{
					languageStrings[y].Add(key, x);
				});
			}

			List<string> fileNames = languageCodes.Select(x => $"{string.Join(" ", x)}.txt").ToList();
			List<string> fileLocations = fileNames.Select(x => $"{Application.dataPath}/Aurora/Unity Extensions/LocalisationSystem/Unity/_Content/LanguageFiles/Resources/Language/{x}").ToList();

			// Add all existing keys that haven't just been updated
			for (int i = 0; i < languageCodes.Count; ++i)
			{
				if(!File.Exists(fileLocations[i]))
					continue;
				Language existingLangauge = Language.CreateFromFile(File.ReadAllText(fileLocations[i]));
				if (existingLangauge == null)
					continue;
				foreach(string stringKey in existingLangauge.GetAllKeys())
				{
					if (!languageStrings[i].ContainsKey(stringKey))
						languageStrings[i][stringKey] = existingLangauge.GetString(stringKey);
				}
			}

			// Build languages
			for (int i = 0; i < languageCodes.Count; ++i)
			{
				StringBuilder fileContents = new StringBuilder();

				fileContents.Append(string.Join("\t", languageCodes[i]));

				foreach (KeyValuePair<string, string> stringPair in languageStrings[i].OrderBy(x => x.Key))
				{
					fileContents.Append('\n');
					fileContents.Append(stringPair.Key);
					fileContents.Append('\t');
					fileContents.Append(stringPair.Value);
				}

				File.WriteAllText(fileLocations[i], fileContents.ToString());
			}

			AssetDatabase.Refresh();
		}

		private static void IterateOverLine(string line, out string key, System.Action<string, int> action)
		{
			string[] contents = line.Split('\t');
			key = contents[0];
			for (int langaugeIndex = 2; langaugeIndex < contents.Length; ++langaugeIndex)
			{
				action.Invoke(contents[langaugeIndex], langaugeIndex - 2);
			}
		}
	}
}