﻿using UnityEditor;

namespace Aurora.Unity.Editor
{
	public static class EditorGUIExt
	{
		public static void BeginChangeCheckMulti(SerializedProperty multiCheckProperty)
		{
			EditorGUI.BeginChangeCheck();
			EditorGUI.showMixedValue = multiCheckProperty.hasMultipleDifferentValues;
		}

		public static void BeginChangeCheckMulti(MaterialProperty multiCheckProperty)
		{
			EditorGUI.BeginChangeCheck();
			EditorGUI.showMixedValue = multiCheckProperty.hasMixedValue;
		}

		public static bool EndChangeCheckMulti()
		{
			EditorGUI.showMixedValue = false;
			return EditorGUI.EndChangeCheck();
		}
	}
}