﻿using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	public static class GameObjectExt
	{
		public static void SetExpandState(this GameObject go, bool expand)
		{
			System.Type originalFocusedWindowType = EditorWindow.focusedWindow.GetType();
			System.Type hierarchyWindowType = typeof(EditorWindow).Assembly.GetType("UnityEditor.SceneHierarchyWindow");

			EditorWindow.FocusWindowIfItsOpen(hierarchyWindowType);
			if (EditorWindow.focusedWindow.GetType() == hierarchyWindowType)
			{
				MethodInfo method = hierarchyWindowType.GetMethod("ExpandTreeViewItem", BindingFlags.NonPublic | BindingFlags.Instance);
				method.Invoke(EditorWindow.focusedWindow, new object[] { go.GetInstanceID(), expand });
			}
			EditorWindow.FocusWindowIfItsOpen(originalFocusedWindowType);
		}
	}
}