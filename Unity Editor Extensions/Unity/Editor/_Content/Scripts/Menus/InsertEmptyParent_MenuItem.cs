﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	public static class InsertEmptyParent_MenuItem
	{
		[MenuItem("GameObject/Insert Empty Parent (Child Zeroed)", priority = 0, validate = true)]
		[MenuItem("GameObject/Insert Empty Parent (Parent Zeroed)", priority = 0, validate = true)]
		private static bool InsertEmptyParent_Validate()
		{
			return Selection.activeGameObject != null;
		}

		[MenuItem("GameObject/Insert Empty Parent (Parent Zeroed)", priority = 0)]
		public static void InsertEmptyParent_ParentZeroed()
		{
			Undo.IncrementCurrentGroup();
			Undo.SetCurrentGroupName("Insert Empty Parent (Batch)");
			foreach (GameObject activeGameObject in Selection.gameObjects)
			{
				Undo.IncrementCurrentGroup();
				Undo.SetCurrentGroupName("Insert Empty Parent");
				GameObject newParent = new GameObject("GameObject");
				Undo.RegisterCreatedObjectUndo(newParent, "Create Game Object");
				if (activeGameObject != null)
				{
					GameObject originalSelection = activeGameObject;

					newParent.name = originalSelection.name + " Parent";

					Undo.SetTransformParent(newParent.transform, originalSelection.transform.parent, "Set Transform Parent");

					Undo.RecordObject(newParent.transform, "Zero New Empty Parent Transform");
					newParent.transform.localPosition = Vector3.zero;
					newParent.transform.localRotation = UnityEngine.Quaternion.identity;
					newParent.transform.localScale = Vector3.one;

					int originalSiblingIndex = originalSelection.transform.GetSiblingIndex();

					Undo.SetTransformParent(originalSelection.transform, newParent.transform, "Set Transform Parent");

					Undo.RecordObject(newParent.transform, "Change Sibling Index");
					newParent.transform.SetSiblingIndex(originalSiblingIndex);
				}
				newParent.SetExpandState(true);
				Undo.IncrementCurrentGroup();
			}
			Undo.IncrementCurrentGroup();
		}

		[MenuItem("GameObject/Insert Empty Parent (Child Zeroed)", priority = 0)]
		private static void InsertEmptyParent_ChildZeroed()
		{
			Undo.IncrementCurrentGroup();
			Undo.SetCurrentGroupName("Insert Empty Parent (Batch)");
			foreach (GameObject activeGameObject in Selection.gameObjects)
			{
				Undo.IncrementCurrentGroup();
				Undo.SetCurrentGroupName("Insert Empty Parent");
				GameObject newParent = new GameObject("GameObject");
				Undo.RegisterCreatedObjectUndo(newParent, "Create Game Object");
				if (activeGameObject != null)
				{
					GameObject originalSelection = activeGameObject;

					newParent.name = originalSelection.name + " Parent";

					Undo.SetTransformParent(newParent.transform, originalSelection.transform, "Set Transform Parent");

					Undo.RecordObject(newParent.transform, "Zero New Empty Parent Transform");
					newParent.transform.localPosition = Vector3.zero;
					newParent.transform.localRotation = UnityEngine.Quaternion.identity;
					newParent.transform.localScale = Vector3.one;

					int originalSiblingIndex = originalSelection.transform.GetSiblingIndex();

					Undo.SetTransformParent(newParent.transform, originalSelection.transform.parent, "Set Transform Parent");
					Undo.SetTransformParent(originalSelection.transform, newParent.transform, "Set Transform Parent");

					Undo.RecordObject(newParent.transform, "Change Sibling Index");
					newParent.transform.SetSiblingIndex(originalSiblingIndex);
				}

				newParent.SetExpandState(true);
				Undo.IncrementCurrentGroup();
			}
			Undo.IncrementCurrentGroup();
		}
	}
}