﻿using System.Collections.Generic;
using System.Diagnostics;
using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	public static class FindReferencesInProject
	{
		private static string resultsTitle = "Find Reference Results";
		private static string resultsConfirm = "Okay";
		private static string resultPendingTitle = "Finding References...";
		
		[MenuItem("Assets/Find References In Project/First", priority = 25)]
		private static void SelectFirstReference()
		{
			SelectReferences(1);
		}

		[MenuItem("Assets/Find References In Project/All", priority = 26)]
		private static void SelectAllReferences()
		{
			SelectReferences(int.MaxValue);
		}

		[MenuItem("Assets/Find References In Project/First", validate = true)]
		[MenuItem("Assets/Find References In Project/All", validate = true)]
		private static bool ValidateSelection()
		{
			return Selection.objects.Length > 0;
		}

		private static void SelectReferences(int max)
		{
			Stopwatch stopwatch = new Stopwatch();
			stopwatch.Start();
			List<Object> references = new List<Object>(System.Math.Min(max, 10));
			string targetObjectName = Selection.activeObject.name;
			string targetPath = AssetDatabase.GetAssetPath(Selection.activeObject);
			string[] objects = AssetDatabase.FindAssets("t:object");
			bool cancel = false;
			float updateTime = 1000.0f / 24.0f;
			for (int i = 0; i < objects.Length; ++i)
			{
				string path = AssetDatabase.GUIDToAssetPath(objects[i]);
				if (targetPath != path)
				{
					string[] dependencies = AssetDatabase.GetDependencies(path,false);
					for (int j = 0; j < dependencies.Length; ++j)
					{
						if (stopwatch.Elapsed.TotalMilliseconds > updateTime)
						{
							stopwatch.Reset();
							stopwatch.Start();
							float objectsProgress = (float)i / (float)objects.Length;
							cancel = EditorUtility.DisplayCancelableProgressBar(resultPendingTitle, string.Format("{0} found", references.Count), objectsProgress);
						}
						if (dependencies[j] == targetPath)
						{
							references.Add(AssetDatabase.LoadAssetAtPath<Object>(path));
							if (references.Count >= max)
								break;
						}
						if (cancel)
							break;
					}
					if (cancel || references.Count >= max)
						break;
				}
			}
			EditorUtility.ClearProgressBar();

			if (references.Count > 0)
				Selection.objects = references.ToArray();

			string resultsInfo = string.Empty;
			
			if(references.Count == 0 && !cancel)
				resultsInfo = $"There are no references to the object \"{targetObjectName}\" in the project!";
			else if (references.Count == 1 && max > 1 && !cancel)
				resultsInfo = $"Only reference to the object \"{targetObjectName}\" was found!";
			else if (max > references.Count && !cancel)
				resultsInfo = $"All {references.Count} references to the object \"{targetObjectName}\" were found!";
			else if (references.Count > 1)
				resultsInfo = $"First {references.Count} references to the object \"{targetObjectName}\" were found!";
			else if (references.Count == 1 && max == 1)
				resultsInfo = $"First reference to the object \"{targetObjectName}\" was found!";
			else if (cancel)
				resultsInfo = "Search was cancelled!";

			if(AssetDatabase.LoadAssetAtPath<MonoScript>(targetPath) != null)
			{
				resultsInfo += System.Environment.NewLine + System.Environment.NewLine;
				resultsInfo += "NOTE: This object is a Script! Scripts may be referenced in other scripts!";
			}
			if (targetPath.Contains("Resources/"))
			{
				resultsInfo += System.Environment.NewLine + System.Environment.NewLine;
				resultsInfo += "NOTE: This object is in a Resources folder! Objects in Resources folders may be referenced in scripts!";
			}
			if (AssetDatabase.LoadAssetAtPath<Shader>(targetPath) != null)
			{
				resultsInfo += System.Environment.NewLine + System.Environment.NewLine;
				resultsInfo += "NOTE: This object is a Shader! Shaders may be referenced in scripts!";
			}

			EditorUtility.DisplayDialog(resultsTitle, resultsInfo, resultsConfirm);
		}
	}
}