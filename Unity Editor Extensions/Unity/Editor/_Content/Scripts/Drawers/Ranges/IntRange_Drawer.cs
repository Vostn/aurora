﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(IntRange))]
	public class IntRange_Drawer : PropertyDrawer
	{
		private static GUIContent minLabel = new GUIContent("Min");
		private static GUIContent maxLabel = new GUIContent("Max");

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			return EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			
			Rect labelField = position;
			labelField.width = EditorGUIUtility.labelWidth;
			labelField.height = EditorGUIUtility.singleLineHeight;

			EditorGUI.LabelField(labelField, label.text);
			
			Rect minmaxField = position;
			minmaxField.width -= EditorGUIUtility.labelWidth;
			minmaxField.x += EditorGUIUtility.labelWidth;
			minmaxField.height = EditorGUIUtility.singleLineHeight;
			
			Rect minField = minmaxField;
			minField.width *= 0.5f;

			Rect maxField = minField;
			maxField.x += minField.width;

			int cachedIndent = EditorGUI.indentLevel;
			float cachedLabelWidth = EditorGUIUtility.labelWidth;
			EditorGUI.indentLevel = 0;
			EditorGUIUtility.labelWidth = 30.0f;
			
			SerializedProperty minProperty = property.FindPropertyRelative("minimum");
			SerializedProperty maxProperty = property.FindPropertyRelative("maximum");

			EditorGUIExt.BeginChangeCheckMulti(minProperty);
			EditorGUI.PropertyField(minField, minProperty, minLabel);
			if (EditorGUIExt.EndChangeCheckMulti() && minProperty.intValue > maxProperty.intValue)
				minProperty.intValue = maxProperty.intValue;

			EditorGUIExt.BeginChangeCheckMulti(maxProperty);
			EditorGUI.PropertyField(maxField, maxProperty, maxLabel);
			if (EditorGUIExt.EndChangeCheckMulti() && maxProperty.intValue < minProperty.intValue)
				maxProperty.intValue = minProperty.intValue;

			EditorGUI.indentLevel = cachedIndent;
			EditorGUIUtility.labelWidth = cachedLabelWidth;

			EditorGUI.EndProperty();
		}
	}
}