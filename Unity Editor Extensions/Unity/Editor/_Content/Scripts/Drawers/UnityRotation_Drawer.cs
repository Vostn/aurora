﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(UnityRotation))]
	public class UnityRotation_Drawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			SerializedProperty rotationHintProperty = property.FindPropertyRelative("eulerRotation");
			EditorGUI.BeginChangeCheck();
			if (property.hasMultipleDifferentValues)
				EditorGUI.showMixedValue = true;
			Vector3 newRotationHintValue = EditorGUI.Vector3Field(position, label, rotationHintProperty.vector3Value);
			if (EditorGUI.EndChangeCheck())
			{
				rotationHintProperty.vector3Value = newRotationHintValue;
				property.FindPropertyRelative("rotation").quaternionValue = UnityEngine.Quaternion.Euler(newRotationHintValue);
			}
			EditorGUI.showMixedValue = false;

			EditorGUI.EndProperty();
		}
	}
}