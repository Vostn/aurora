﻿using UnityEngine;
using UnityEditor;
using System.Reflection;
using System.Collections.Generic;
using System;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(ScriptableObject), true)]
	public class ScriptableObject_Drawer : PropertyDrawer
	{
		private const float BUTTON_WIDTH = 45.0f;
		private const float BUTTON_GAP = 2.0f;

		private static Dictionary<Type, List<Type>> typeCache = new Dictionary<Type, List<Type>>();

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			List<Type> usableTypes = GetAcceptableTypes(fieldInfo.FieldType);

			if (usableTypes.Count > 0)
			{
				Rect shortenedRect = position;
				shortenedRect.width -= BUTTON_WIDTH + BUTTON_GAP;

				Rect buttonRect = position;
				buttonRect.width = BUTTON_WIDTH;
				buttonRect.x = shortenedRect.xMax + BUTTON_GAP;

				EditorGUI.PropertyField(shortenedRect, property, label);

				if (usableTypes.Count > 1)
				{
					if (EditorGUI.DropdownButton(buttonRect, new GUIContent("New..."), FocusType.Passive))
					{
						GenericMenu menu = new GenericMenu();
						foreach (Type type in usableTypes)
							menu.AddItem(new GUIContent($"New {type.ToString()}..."), false, () => CreateType(type, property));
						menu.DropDown(buttonRect);
					}
				}
				else if (usableTypes.Count == 1 && GUI.Button(buttonRect, "New..."))
					CreateType(usableTypes[0], property);
			}
			else
				EditorGUI.PropertyField(position, property, label);

			EditorGUI.EndProperty();
		}

		private static List<Type> GetAcceptableTypes(Type baseType)
		{
			if (baseType.IsArray)
				baseType = baseType.GetElementType();
			if (!typeCache.ContainsKey(baseType))
			{
				List<Type> usableTypes = new List<Type>();
				Type[] allTypes = Assembly.GetAssembly(baseType).GetTypes();
				foreach (Type type in allTypes)
					if (!type.IsAbstract && baseType.IsAssignableFrom(type))
						usableTypes.Add(type);
				typeCache.Add(baseType, usableTypes);
			}
			return typeCache[baseType];
		}

		private static void CreateType(Type type, SerializedProperty property)
		{
			string newAssetLocation = EditorUtility.SaveFilePanel($"Create new {type.Name}...", Application.dataPath, $"New {type.Name}", "asset");
			if (newAssetLocation != string.Empty)
			{
				int startIndex = newAssetLocation.IndexOf("Assets");
				newAssetLocation = newAssetLocation.Substring(startIndex, newAssetLocation.Length - startIndex);
				AssetDatabase.CreateAsset(ScriptableObject.CreateInstance(type.ToString()), newAssetLocation);
				property.objectReferenceValue = AssetDatabase.LoadAssetAtPath(newAssetLocation, type);
			}
		}
	}
}