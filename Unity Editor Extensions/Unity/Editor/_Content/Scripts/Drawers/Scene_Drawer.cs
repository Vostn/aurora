﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(Scene))]
	public class Scene_Editor : PropertyDrawer
	{
		private static readonly float warningBoxHeight = EditorGUIUtility.singleLineHeight * 2.0f;

		private SceneAsset sceneAsset;

		private string scenePath;

		private SceneState sceneState;

		private enum SceneState
		{
			NoSceneAsset,
			NotAdded,
			NotEnabled,
			Valid,
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			ValidateState(property);

			position.height = EditorGUIUtility.singleLineHeight;
			EditorGUI.BeginProperty(position, label, property.FindPropertyRelative("scene"));
			EditorGUI.PropertyField(position, property.FindPropertyRelative("scene"));
			EditorGUI.EndProperty();

			if (sceneState != SceneState.Valid)
			{
				string helpBoxText = string.Empty;
				if (sceneState == SceneState.NotAdded)
					helpBoxText = "\"" + sceneAsset.name + "\" is not included in Build Settings!";
				else if (sceneState == SceneState.NotEnabled)
					helpBoxText = "\"" + sceneAsset.name + "\" is not enabled in Build Settings!";

				Rect warningArea = new Rect(position.x, position.y + EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing, position.width, warningBoxHeight);

				for (int i = 1; i < EditorGUI.indentLevel; ++i)
					warningArea = EditorGUI.IndentedRect(warningArea);

				float halfWidth = warningArea.width * 0.5f;

				Rect boxPosition = new Rect(warningArea.x, warningArea.y, halfWidth - 1, warningBoxHeight);

				if (sceneState != SceneState.NoSceneAsset)
				{
					EditorGUI.HelpBox(boxPosition, helpBoxText, MessageType.Warning);
					Rect buttonPosition = new Rect((boxPosition.x + halfWidth) + 1, boxPosition.y, boxPosition.width, boxPosition.height);
					string buttonText = sceneState == SceneState.NotAdded ? "Auto-Add To Build Settings" : "Auto-Enable in Build Settings";
					if (GUI.Button(buttonPosition, buttonText))
						ForceValidateScene();
				}
			}
		}

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			ValidateState(property);

			float height = EditorGUIUtility.singleLineHeight;
			if (sceneState != SceneState.Valid && sceneState != SceneState.NoSceneAsset)
				height += warningBoxHeight + EditorGUIUtility.standardVerticalSpacing;
			return height;
		}

		private static SceneAsset GetSceneAsset(SerializedProperty property)
		{
			SerializedProperty sceneAssetProperty = property.FindPropertyRelative("scene");
			return (SceneAsset)sceneAssetProperty.objectReferenceValue;
		}

		private void ValidateState(SerializedProperty property)
		{
			sceneAsset = GetSceneAsset(property);
			scenePath = AssetDatabase.GetAssetPath(sceneAsset);
			UpdateSceneState();
		}

		private void ForceValidateScene()
		{
			// Here we retrieve and store the array of scenes in the build settings.
			// The reason that we sample, modify, and re-set this array is because "EditorBuildSettings.scenes" is a
			// property that returns a copy of the scene array, not a pointer to the actual array stored by EditorBuildSettings.
			EditorBuildSettingsScene[] scenesArray = EditorBuildSettings.scenes;

			// We iterate over all of the scenes currently in the build settings
			for (int i = 0; i < scenesArray.Length; ++i)
			{
				// We check to see if this is the scene that we are trying to validate
				if (scenesArray[i].path == scenePath)
				{
					// If it is, then we enable it
					scenesArray[i].enabled = true;
					// Set the array back to the BuildSettings
					EditorBuildSettings.scenes = scenesArray;
					// And then we escape.
					return;
				}
			}

			// If we were unable to find a valid scene in the build settings matching the scene asset, we manually include the scene in the build settings.
			// We create a modifiable collection of all of the scenes...
			List<EditorBuildSettingsScene> scenesList = new List<EditorBuildSettingsScene>(scenesArray);
			// ...and then add our scene to that collection.
			scenesList.Add(new EditorBuildSettingsScene(scenePath, true));
			// Then we just convert it back to a new array and set it back to the EditorBuildSettings, thereby adding the new scene.
			EditorBuildSettings.scenes = scenesList.ToArray();
		}

		private void UpdateSceneState()
		{
			if (sceneAsset == null)
			{
				sceneState = SceneState.NoSceneAsset;
				return;
			}
			EditorBuildSettingsScene[] scenesArray = EditorBuildSettings.scenes;
			for (int i = 0; i < scenesArray.Length; ++i)
			{
				if (scenesArray[i].path == scenePath)
				{
					sceneState = (scenesArray[i].enabled ? SceneState.Valid : SceneState.NotEnabled);
					return;
				}
			}
			sceneState = SceneState.NotAdded;
		}
	}
}