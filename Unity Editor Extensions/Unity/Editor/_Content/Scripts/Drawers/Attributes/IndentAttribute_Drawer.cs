﻿using UnityEngine;
using UnityEditor;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(IndentAttribute))]
	public class IndentAttribute_Drawer : StackablePropertyAttribute_Drawer
	{
		protected override void OnGUIStart(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.indentLevel += ((IndentAttribute)attribute).IndentLevel;
		}

		protected override void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.indentLevel -= ((IndentAttribute)attribute).IndentLevel;
		}

		protected override float GetInspectorHeight(SerializedProperty property, GUIContent label) => 0;

		protected override bool InturruptsStack(SerializedProperty property) => false;
	}
}