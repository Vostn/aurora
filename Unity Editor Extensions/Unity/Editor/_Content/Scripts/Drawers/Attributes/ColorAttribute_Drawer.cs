﻿using UnityEngine;
using UnityEditor;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(ColorAttribute))]
	public class ColorAttribute_Drawer : StackablePropertyAttribute_Drawer
	{
		private Color cacheColor;

		protected override void OnGUIStart(Rect position, SerializedProperty property, GUIContent label)
		{
			cacheColor = GUI.color;
			GUI.color = ((ColorAttribute)attribute).DisplayColor;
		}

		protected override void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label)
		{
			GUI.color = cacheColor;
		}

		protected override float GetInspectorHeight(SerializedProperty property, GUIContent label) => 0;

		protected override bool InturruptsStack(SerializedProperty property) => false;
	}
}