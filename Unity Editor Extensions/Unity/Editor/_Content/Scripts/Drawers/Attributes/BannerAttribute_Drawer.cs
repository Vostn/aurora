﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(BannerAttribute))]
	public class BannerAttribute_Drawer : DecoratorDrawer
	{
		private const float BUFFER = 5.0f;
		private static GUIStyle largeLabel;

		static BannerAttribute_Drawer()
		{
			largeLabel = new GUIStyle(EditorStyles.largeLabel);
			largeLabel.fontSize = 14;
		}

		public static void DrawLayoutDivider(string title, string subtitle)
		{
			DrawDivider(EditorGUILayout.GetControlRect(false, CalculateHeight(title, subtitle)), title, subtitle);
		}

		public override void OnGUI(Rect position)
		{
			BannerAttribute note = attribute as BannerAttribute;
			DrawDivider(position, note.title, note.subtitle);
		}

		public override float GetHeight()
		{
			BannerAttribute note = attribute as BannerAttribute;
			return CalculateHeight(note.title, note.subtitle);
		}

		private static float CalculateHeight(string title, string subtitle)
		{
			float totalHeight = BUFFER*2.0f;

			totalHeight += CalculateTitleHeight(title);
			totalHeight += EditorGUIUtility.standardVerticalSpacing;
			totalHeight += CalculateSubtitleHeight(subtitle);

			return totalHeight;
		}

		private static float CalculateTitleHeight(string title)
		{
			return largeLabel.CalcHeight(new GUIContent(title), EditorGUIUtility.currentViewWidth);
		}

		private static float CalculateSubtitleHeight(string subtitle)
		{
			if (!string.IsNullOrEmpty(subtitle))
				return EditorStyles.wordWrappedMiniLabel.CalcHeight(new GUIContent(subtitle), EditorGUIUtility.currentViewWidth);
			return 0;
		}

		private static void DrawDivider(Rect position, string title, string subtitle)
		{
			Rect posLabel = position;

			float titleHeight = CalculateTitleHeight(title);

			if (!string.IsNullOrEmpty(title))
			{
				posLabel.y += BUFFER;
				posLabel.height = titleHeight;
				EditorGUI.LabelField(posLabel, title, largeLabel);
			}

			if (!string.IsNullOrEmpty(subtitle))
			{
				Color color = GUI.color;
				Color faded = color;
				faded.a = 0.8f;
				GUI.color = faded;

				Rect posExplain = posLabel;
				posExplain.y += titleHeight;
				posExplain.height = CalculateSubtitleHeight(subtitle);
				EditorGUI.LabelField(posExplain, subtitle, EditorStyles.wordWrappedMiniLabel);

				GUI.color = color;
			}
			
		}
	}
}