﻿using UnityEditor;
using System.Reflection;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Collections.ObjectModel;
using UnityEditor.Callbacks;

namespace Aurora.Unity.Editor
{
	public abstract class StackablePropertyAttribute_Drawer : PropertyDrawer
	{
		private static MethodInfo getDrawerTypeMethod;
		private static MethodInfo getFieldAttributesMethod;
		private static MethodInfo getFieldInfoFromPropertyMethod;

		private static List<PropertyAttribute> currentPropertyAttributeList;
		private static FieldInfo currentFieldInfo;
		private static PropertyAttribute currentAttribute;

		private static bool HasCurrentProperty
		{
			get { return currentAttribute != null; }
		}

		public override sealed void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (!HasCurrentProperty)
				GetPropertyInfo(property);
			float heightCache = position.height;
			float height = GetInspectorHeight(property, label);
			position.height = height;
			OnGUIStart(position, property, label);
			if (!InturruptsStack(property))
			{
				position.y += height;
				position.height = heightCache;
				DrawNextPropertyDrawer(position, property, label);
			}
			OnGUIEnd(position, property, label);
			ClearPropertyInfo();
		}

		public override sealed float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			if (!HasCurrentProperty)
				GetPropertyInfo(property);
			float height = GetInspectorHeight(property, label);
			if (!InturruptsStack(property))
				height += GetNextPropertyDrawerHeight(property, label);
			ClearPropertyInfo();
			return height;
		}

		protected abstract float GetInspectorHeight(SerializedProperty property, GUIContent label);

		protected abstract void OnGUIStart(Rect position, SerializedProperty property, GUIContent label);

		protected abstract void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label);

		protected abstract bool InturruptsStack(SerializedProperty property);

		[InitializeOnLoadMethod]
		private static void SetupReflectedMethods()
		{
			Assembly assembly = Assembly.GetAssembly(typeof(EditorGUI));
			Type sruType = assembly.GetType("UnityEditor.ScriptAttributeUtility", true);
			getDrawerTypeMethod = sruType.GetMethod("GetDrawerTypeForType", BindingFlags.Static | BindingFlags.NonPublic);
			getFieldAttributesMethod = sruType.GetMethod("GetFieldAttributes", BindingFlags.Static | BindingFlags.NonPublic);
			getFieldInfoFromPropertyMethod = sruType.GetMethod("GetFieldInfoFromProperty", BindingFlags.Static | BindingFlags.NonPublic);
		}

		private static void GetPropertyInfo(SerializedProperty property)
		{
			currentFieldInfo = (FieldInfo)getFieldInfoFromPropertyMethod.Invoke(null, new object[] { property, null });
			currentPropertyAttributeList = (List<PropertyAttribute>)getFieldAttributesMethod.Invoke(null, new object[] { currentFieldInfo });
			if (currentPropertyAttributeList.Count > 0)
				currentAttribute = currentPropertyAttributeList[0];
		}

		[DidReloadScripts]
		private static void ClearPropertyInfo()
		{
			currentPropertyAttributeList = null;
			currentFieldInfo = null;
			currentAttribute = null;
		}

		private static PropertyAttribute GetNextPropertyAttribute(SerializedProperty property)
		{
			bool pastTarget = false;
			for (int i = 0; i < currentPropertyAttributeList.Count; ++i)
			{
				if (pastTarget)
				{
					currentAttribute = currentPropertyAttributeList[i];
					return currentAttribute;
				}
				else if (currentAttribute == currentPropertyAttributeList[i])
					pastTarget = true;
			}
			currentAttribute = null;
			return currentAttribute;
		}

		private static PropertyDrawer GetTypeDrawer(Type type)
		{
			Type drawerType = getDrawerTypeMethod.Invoke(null, new object[] { type }) as Type;
			if (drawerType != null)
			{
				PropertyDrawer drawer = (PropertyDrawer)Activator.CreateInstance(drawerType);
				InitialiseDrawer(drawer);
				return drawer;
			}
			return null;
		}

		private static void InitialiseDrawer(PropertyDrawer drawer)
		{
			typeof(PropertyDrawer).GetField("m_FieldInfo", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(drawer, currentFieldInfo);
			typeof(PropertyDrawer).GetField("m_Attribute", BindingFlags.NonPublic | BindingFlags.Instance).SetValue(drawer, currentAttribute);
		}

		private void DrawNextPropertyDrawer(Rect position, SerializedProperty property, GUIContent label)
		{
			PropertyDrawer nextDrawer = LoadNextDrawer(property);
			if (nextDrawer == null)
			{
				nextDrawer = GetTypeDrawer(fieldInfo.FieldType);
				if (nextDrawer == null)
				{
					EditorGUI.PropertyField(position, property, label, true);
					return;
				}
			}
			nextDrawer.OnGUI(position, property, label);
		}

		private float GetNextPropertyDrawerHeight(SerializedProperty property, GUIContent label)
		{
			PropertyDrawer nextDrawer = LoadNextDrawer(property);
			if (nextDrawer == null)
			{
				nextDrawer = GetTypeDrawer(fieldInfo.FieldType);
				if (nextDrawer == null)
					return EditorGUI.GetPropertyHeight(property, label, true);
			}
			return nextDrawer.GetPropertyHeight(property, label);
		}

		private PropertyDrawer LoadNextDrawer(SerializedProperty property)
		{
			PropertyDrawer nextDrawer = null;
			while (nextDrawer == null)
			{
				GetNextPropertyAttribute(property);
				if (currentAttribute == null)
					return null;

				nextDrawer = GetTypeDrawer(currentAttribute.GetType());
				if (nextDrawer == null)
					continue;

				InitialiseDrawer(nextDrawer);
			}
			return nextDrawer;
		}
	}
}