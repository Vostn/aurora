﻿using UnityEngine;
using UnityEditor;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(AssetsOnlyAttribute))]
	public class AssetsOnly_Drawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			if (property.propertyType == SerializedPropertyType.ObjectReference)
			{
				EditorGUI.BeginChangeCheck();
				EditorGUI.showMixedValue = property.hasMultipleDifferentValues;
				Object newReference = EditorGUI.ObjectField(position, label, property.objectReferenceValue, fieldInfo.FieldType, false);
				EditorGUI.showMixedValue = false;
				if (EditorGUI.EndChangeCheck())
				{
					property.objectReferenceValue = newReference;
				}
			}
			else
				EditorGUI.HelpBox(position, "AssetsOnly attribute can only be used on Object Reference fields!", MessageType.Error);
		}
	}
}