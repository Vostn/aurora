﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(InfoBoxAttribute))]
	public class InfoBoxAttribute_Drawer : DecoratorDrawer
	{
		public override void OnGUI(Rect position)
		{
			InfoBoxAttribute targetAttribute = attribute as InfoBoxAttribute;
			MessageType messageType = (MessageType)(int)targetAttribute.BoxType;
			EditorGUI.HelpBox(position, targetAttribute.Text, messageType);
		}
	}
}