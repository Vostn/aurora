﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(EnumMaskAttribute))]
	public class EnumMaskAttribute_Drawer : PropertyDrawer
	{
		private readonly GUIContent label = new GUIContent();

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUIExt.BeginChangeCheckMulti(property);
			this.label.text = label.text;
			this.label.tooltip = label.tooltip;
			int propertyValue = EditorGUI.MaskField(position, label, property.intValue, property.enumNames);
			if (EditorGUIExt.EndChangeCheckMulti())
				property.intValue = propertyValue;
		}
	}
}