﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(HideAttribute))]
	public class HideAttribute_Drawer : StackablePropertyAttribute_Drawer
	{
		protected override float GetInspectorHeight(SerializedProperty property, GUIContent label) => -2;

		protected override void OnGUIStart(Rect position, SerializedProperty property, GUIContent label)
		{
		}

		protected override void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label)
		{
		}

		protected override bool InturruptsStack(SerializedProperty property) => true;
	}
}