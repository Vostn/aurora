﻿using UnityEditor;
using UnityEngine;
using System.Reflection;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(VisibilityAttribute))]
	public class VisibilityAttribute_Drawer : StackablePropertyAttribute_Drawer
	{
		protected override void OnGUIStart(Rect position, SerializedProperty property, GUIContent label)
		{
			if (!IsVisible(this, property))
				return;
		}

		protected override void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label)
		{
		}

		protected override float GetInspectorHeight(SerializedProperty property, GUIContent label)
		{
			if (!IsVisible(this, property))
				return -2;
			return 0;
		}

		protected override bool InturruptsStack(SerializedProperty property)
		{
			return !IsVisible(this, property);
		}

		public static bool IsVisible(VisibilityAttribute attribute, SerializedProperty property)
		{
			if (property.propertyPath.EndsWith("]"))
				return true;

			string targetPropertyName = attribute.TargetProperty;

			int propertyValue = 0;

			int pathLastDotIndex = property.propertyPath.LastIndexOf('.');
			string targetPropertyPath = targetPropertyName;
			if (pathLastDotIndex != -1)
			{
				string propertyParentPath = property.propertyPath.Substring(0, pathLastDotIndex);
				targetPropertyPath = propertyParentPath + '.' + targetPropertyName;
			}
			SerializedProperty targetProperty = property.serializedObject.FindProperty(targetPropertyPath);
			if (targetProperty == null)
			{
				Debug.LogError($"Target property named \"{targetPropertyName}\" does not exist!");
				return true;
			}
			if (targetProperty.propertyType == SerializedPropertyType.Enum)
				propertyValue = targetProperty.enumValueIndex;
			else if (targetProperty.propertyType == SerializedPropertyType.Integer)
				propertyValue = targetProperty.intValue;
			else if (targetProperty.propertyType == SerializedPropertyType.Boolean)
				propertyValue = targetProperty.boolValue ? 1 : 0;
			else
			{
				Debug.LogError($"Target property named \"{targetProperty.name}\" is not a valid comparison type!");
				return true;
			}
			return IsVisible(attribute.MatchType, attribute.ComparisonValue, propertyValue);
		}

		private static bool IsVisible(MatchType matchType, int comparisonValue, int targetValue)
		{
			if (matchType == MatchType.Equal)
				return targetValue == comparisonValue;
			else if (matchType == MatchType.NotEqual)
				return targetValue != comparisonValue;
			else if (matchType == MatchType.HasFlag)
				return (targetValue & comparisonValue) == comparisonValue;
			else if (matchType == MatchType.NotHasFlag)
				return (targetValue & comparisonValue) != comparisonValue;
			else if (matchType == MatchType.Less)
				return targetValue < comparisonValue;
			else if (matchType == MatchType.LessOrEqual)
				return targetValue <= comparisonValue;
			else if (matchType == MatchType.Greater)
				return targetValue > comparisonValue;
			else if (matchType == MatchType.GreaterOrEqual)
				return targetValue >= comparisonValue;
			else
				throw new System.NotImplementedException();
		}

		private static bool IsVisible(VisibilityAttribute_Drawer target, SerializedProperty property)
		{
			foreach (VisibilityAttribute otherAttibute in target.fieldInfo.GetCustomAttributes<VisibilityAttribute>())
				if (!IsVisible(otherAttibute, property))
					return false;
			return true;
		}
	}
}