﻿using UnityEngine;
using UnityEditor;
using System.Reflection;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(ButtonAttribute))]
	public class ButtonAttribute_Drawer : StackablePropertyAttribute_Drawer
	{
		protected override void OnGUIStart(Rect position, SerializedProperty property, GUIContent label)
		{
			ButtonAttribute buttonAttribute = attribute as ButtonAttribute;
			position.height = EditorGUIUtility.singleLineHeight;
			if (GUI.Button(position, buttonAttribute.LabelText))
			{
				object[] targets = property.serializedObject.targetObjects;
				MethodInfo methodInfo = targets[0].GetType().GetMethod(buttonAttribute.FunctionName, BindingFlags.NonPublic | BindingFlags.Public | BindingFlags.Instance);
				foreach (object target in targets)
					methodInfo.Invoke(target, null);
			}
		}

		protected override bool InturruptsStack(SerializedProperty property) => false;

		protected override void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label)
		{
		}

		protected override float GetInspectorHeight(SerializedProperty property, GUIContent label) => EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
	}
}