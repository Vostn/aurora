﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(LabelAttribute))]
	public class LabelAttribute_Drawer : StackablePropertyAttribute_Drawer
	{
		protected override float GetInspectorHeight(SerializedProperty property, GUIContent label) => 0;

		protected override bool InturruptsStack(SerializedProperty property) => false;

		protected override void OnGUIStart(Rect position, SerializedProperty property, GUIContent label)
		{
			label.text = ((LabelAttribute)attribute).LabelTitle;
		}

		protected override void OnGUIEnd(Rect position, SerializedProperty property, GUIContent label)
		{
		}
	}
}