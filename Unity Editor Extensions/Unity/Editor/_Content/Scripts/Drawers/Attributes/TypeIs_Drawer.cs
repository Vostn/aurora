﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(TypeIs))]
	public class TypeIs_Drawer : PropertyDrawer
	{
		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);

			if (property.propertyType == SerializedPropertyType.ObjectReference)
			{
				EditorGUIExt.BeginChangeCheckMulti(property);
				Object newReference = EditorGUI.ObjectField(position, label, property.objectReferenceValue, ((TypeIs)attribute).RequiredType, true);
				if (EditorGUIExt.EndChangeCheckMulti())
					property.objectReferenceValue = newReference;
			}
			else
				EditorGUI.PropertyField(position, property, label);

			EditorGUI.EndProperty();
		}
	}
}