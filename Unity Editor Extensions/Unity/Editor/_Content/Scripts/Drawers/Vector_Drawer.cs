﻿using UnityEditor;
using UnityEngine;

namespace Aurora.Unity.Editor
{
	[CustomPropertyDrawer(typeof(Bool2))]
	[CustomPropertyDrawer(typeof(Bool3))]
	[CustomPropertyDrawer(typeof(Bool4))]

	[CustomPropertyDrawer(typeof(Byte2))]
	[CustomPropertyDrawer(typeof(Byte3))]
	[CustomPropertyDrawer(typeof(Byte4))]

	[CustomPropertyDrawer(typeof(SByte2))]
	[CustomPropertyDrawer(typeof(SByte3))]
	[CustomPropertyDrawer(typeof(SByte4))]

	[CustomPropertyDrawer(typeof(Double1))]
	[CustomPropertyDrawer(typeof(Double2))]
	[CustomPropertyDrawer(typeof(Double3))]
	[CustomPropertyDrawer(typeof(Double4))]

	[CustomPropertyDrawer(typeof(Fixed2))]
	[CustomPropertyDrawer(typeof(Fixed3))]
	[CustomPropertyDrawer(typeof(Fixed4))]

	[CustomPropertyDrawer(typeof(Float2))]
	[CustomPropertyDrawer(typeof(Float3))]
	[CustomPropertyDrawer(typeof(Float4))]

	[CustomPropertyDrawer(typeof(Int2))]
	[CustomPropertyDrawer(typeof(Int3))]
	[CustomPropertyDrawer(typeof(Int4))]

	[CustomPropertyDrawer(typeof(UInt2))]
	[CustomPropertyDrawer(typeof(UInt3))]
	[CustomPropertyDrawer(typeof(UInt4))]

	[CustomPropertyDrawer(typeof(Long2))]
	[CustomPropertyDrawer(typeof(Long3))]
	[CustomPropertyDrawer(typeof(Long4))]

	[CustomPropertyDrawer(typeof(ULong2))]
	[CustomPropertyDrawer(typeof(ULong3))]
	[CustomPropertyDrawer(typeof(ULong4))]

	[CustomPropertyDrawer(typeof(Short2))]
	[CustomPropertyDrawer(typeof(Short3))]
	[CustomPropertyDrawer(typeof(Short4))]

	[CustomPropertyDrawer(typeof(UShort2))]
	[CustomPropertyDrawer(typeof(UShort3))]
	[CustomPropertyDrawer(typeof(UShort4))]
	public class Vector_Drawer : PropertyDrawer
	{
		private static readonly GUIContent[] subLabelDefinitions = new GUIContent[]
		{
			new GUIContent("X"),
			new GUIContent("Y"),
			new GUIContent("Z"),
			new GUIContent("W"),
		};

		public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
		{
			float height = base.GetPropertyHeight(property, label);
			if (!EditorGUIUtility.wideMode)
				height += EditorGUIUtility.singleLineHeight + EditorGUIUtility.standardVerticalSpacing;
			return height;
		}

		public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
		{
			EditorGUI.BeginProperty(position, label, property);
			SerializedProperty x = property.FindPropertyRelative("x");
			SerializedProperty iterator = x.Copy();
			int count = 0;
			int depth = iterator.depth;
			while (depth == iterator.depth)
			{
				++count;
				if (!iterator.Next(false))
					break;
			}
			GUIContent[] subLabels = new GUIContent[count];
			for (int i = 0; i < count; ++i)
				subLabels[i] = new GUIContent(subLabelDefinitions[i]);
			EditorGUI.MultiPropertyField(position, subLabels, x, label);
			EditorGUI.EndProperty();
		}
	}
}