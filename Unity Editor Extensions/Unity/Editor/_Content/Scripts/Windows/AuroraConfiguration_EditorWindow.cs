﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Text;

namespace Aurora.Unity.Editor
{
	public class AuroraConfiguration_EditorWindow : EditorWindow
	{
		private readonly DefineData[] defineData = new DefineData[]
		{
			new DefineData("AURORA_CONSOLE", "Allow Aurora Console in Release Build"),
			new DefineData("AURORA_TEXTMESHPRO",  "Aurora Text Mesh Pro Support"),
			new DefineData("AURORA_PLAYFAB_UNITY",  "Aurora PlayFab Unity SDK Support"),
			new DefineData("AURORA_STEAMWORKS_NET_UNITY",  "Aurora Steamworks.NET Unity SDK Support")
		};

		[MenuItem("Aurora/Configuration Window", priority = 10)]
		private static void Open()
		{
			GetWindow<AuroraConfiguration_EditorWindow>(true, "Aurora Configuration", true);
		}

		private void OnGUI()
		{
			float maxWidth = 0;
			GUI.Label(EditorGUILayout.GetControlRect(), "Settings for " + EditorUserBuildSettings.selectedBuildTargetGroup, EditorStyles.boldLabel);

			for (int i = 0; i < defineData.Length; ++i)
				maxWidth = System.Math.Max(maxWidth, DrawDefineToggle(defineData[i]));

			maxSize = minSize = new Vector2(25 + maxWidth, 10 + ((defineData.Length + 1) * (EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight)));
		}

		private static float DrawDefineToggle(DefineData data)
		{
			bool defineInList = DoesScriptingDefineExist(data.Define);
			EditorGUI.BeginChangeCheck();
			string displayString = $"{data.Description} ({data.Define})";
			defineInList = EditorGUILayout.ToggleLeft(displayString, defineInList);
			if (EditorGUI.EndChangeCheck())
			{
				if (defineInList)
					AddScriptingDefine(data.Define);
				else
					RemoveScriptingDefine(data.Define);
			}
			return GUI.skin.label.CalcSize(new GUIContent(displayString)).x;
		}

		private static bool DoesScriptingDefineExist(string define)
		{
			string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string[] allDefines = defines.Split(';');
			for (int i = 0; i < allDefines.Length; ++i)
				if (allDefines[i] == define)
					return true;
			return false;
		}

		private static void AddScriptingDefine(string define)
		{
			string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			defines = $"{defines};{define}";
			PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, defines);
		}

		private static void RemoveScriptingDefine(string define)
		{
			string defines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			List<string> allDefines = defines.Split(';').ToList();
			StringBuilder stringBuilder = new StringBuilder();
			for (int i = 0; i < allDefines.Count; ++i)
				if (allDefines[i] != define)
					stringBuilder.Append($"{allDefines[i]};");
			PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, stringBuilder.ToString());
		}

		private struct DefineData
		{
			public string Define { get; private set; }
			public string Description { get; private set; }

			public DefineData(string define, string description)
			{
				Define = define;
				Description = description;
			}
		}
	}
}