﻿using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using System.Collections.ObjectModel;

namespace Aurora.Unity.Editor
{
	public class BuildOptions_EditorWindow : EditorWindow
	{
		private static Vector2 windowSize;
		private static BuildPlayerOptions currentUnityBuildOptions;
		private static BuildOptions_EditorWindow instance;
		private static Vector2 location;
		private static Dictionary<BuildTargetGroup, List<BuildButton>> allBuildButtons = new Dictionary<BuildTargetGroup, List<BuildButton>>();

		public void OnGUI()
		{
			float halfPadding = EditorGUIUtility.standardVerticalSpacing;
			float fullPadding = halfPadding * 2;
			List<BuildButton> buildButtonsList = allBuildButtons[EditorUserBuildSettings.selectedBuildTargetGroup];
			windowSize = new Vector2(0, halfPadding);
			Vector2 buttonSize = new Vector2(0, EditorGUIUtility.singleLineHeight);
			for (int i = 0; i < buildButtonsList.Count; ++i)
			{
				Vector2 size = GUI.skin.button.CalcSize(new GUIContent(buildButtonsList[i].Name));
				buttonSize.x = System.Math.Max(buttonSize.x, size.x);
				windowSize.y += EditorGUIUtility.singleLineHeight;
				windowSize.y += halfPadding;
			}
			windowSize.x = buttonSize.x + fullPadding;
			Vector2 buttonPosition = new Vector2(halfPadding, halfPadding);
			for (int i = 0; i < buildButtonsList.Count; ++i)
			{
				if (GUI.Button(new Rect(buttonPosition, buttonSize), buildButtonsList[i].Name))
				{
					List<string> defines = new List<string>(buildButtonsList[i].SelfScriptingDefines);
					for (int j = 0; j < buildButtonsList.Count; ++j)
					{
						if (i == j)
							continue;
						defines.AddRange(buildButtonsList[j].OtherScriptingDefines);
					}
					Build(defines);
				}
				buttonPosition.y += halfPadding;
				buttonPosition.y += EditorGUIUtility.singleLineHeight;
			}
			maxSize = minSize = windowSize;
		}

		private static void RegisterStandaloneOptions()
		{
#if AURORA_STEAMWORKS_NET_UNITY
			RegisterBuildButton(BuildTargetGroup.Standalone, new BuildButton("Steam", null, new string[] { "DISABLESTEAMWORKS" }));
#endif
			RegisterBuildButton(BuildTargetGroup.Standalone, new BuildButton("Default"));
		}

		public static void RegisterBuildButton(BuildTargetGroup buildTargetGroup, BuildButton buildButton)
		{
			if (!allBuildButtons.ContainsKey(buildTargetGroup))
				allBuildButtons.Add(buildTargetGroup, new List<BuildButton>());
			allBuildButtons[buildTargetGroup].Add(buildButton);
		}

		[InitializeOnLoadMethod]
		private static void Initialise()
		{
			RegisterStandaloneOptions();
			BuildPlayerWindow.RegisterBuildPlayerHandler(CustomBuildSelectionDropdown);
		}

		private static void Build(IEnumerable<string> additionalScriptDefines)
		{	
			string originalScriptingDefines = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
			string newScriptingDefines = originalScriptingDefines;
			foreach (string define in additionalScriptDefines)
				newScriptingDefines += $";{define};";

			PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, newScriptingDefines);
			BuildPipeline.BuildPlayer(currentUnityBuildOptions);
			PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, originalScriptingDefines);
		}

		private static void CustomBuildSelectionDropdown(BuildPlayerOptions options)
		{
			location = GetWindow<BuildPlayerWindow>().position.max - new Vector2(242, 34);
			currentUnityBuildOptions = options;
			if (instance == null)
				instance = CreateInstance<BuildOptions_EditorWindow>();
			instance.ShowAsDropDown(new Rect(location.x, location.y, 65, 25), windowSize);
			instance.OnGUI();
		}

		public class BuildButton
		{
			/// <summary>
			/// Display name of the Build Button.
			/// </summary>
			public string Name { get; private set; }

			/// <summary>
			/// Scripting defines that will be defined for this build button.
			/// </summary>
			public ReadOnlyCollection<string> SelfScriptingDefines { get; private set; }

			/// <summary>
			/// Scripting defines that will be defined for all other build buttons.
			/// </summary>
			public ReadOnlyCollection<string> OtherScriptingDefines { get; private set; }

			private BuildButton()
			{
			}

			/// <summary>
			/// Create a new instance of a BuildButton.
			/// </summary>
			/// <param name="name">Display name of the Build Button.</param>
			/// <param name="selfScriptingDefines">Scripting defines that will be defined for this build button.</param>
			/// <param name="otherScriptingDefines">Scripting defines that will be defined for all other build buttons.</param>
			public BuildButton(string name, IEnumerable<string> selfScriptingDefines = null, IEnumerable<string> otherScriptingDefines = null)
			{
				Name = name;

				if (selfScriptingDefines != null)
					SelfScriptingDefines = selfScriptingDefines.ToList().AsReadOnly();
				else
					SelfScriptingDefines = new List<string>().AsReadOnly();

				if (otherScriptingDefines != null)
					OtherScriptingDefines = otherScriptingDefines.ToList().AsReadOnly();
				else
					OtherScriptingDefines = new List<string>().AsReadOnly();
			}
		}
	}
}