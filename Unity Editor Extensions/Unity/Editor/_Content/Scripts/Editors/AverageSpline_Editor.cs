﻿using UnityEngine;
using UnityEditor;
using System.Linq;

namespace Aurora.Unity.Editor
{
	[CustomEditor(typeof(AverageSpline))]
	public class AverageSpline_Editor : UnityEditor.Editor
	{
		private const double POSITION_HANDLE_SCALE = 0.6;
		private const double DISC_OUTER_SCALE = 0.2;
		private const double DISC_INNER_SCALE = DISC_OUTER_SCALE * 0.8;

		private AverageSpline smoothSpline;
		private int selectedIndex = -1;

		private void OnEnable()
		{
			SceneView.duringSceneGui += OnSceneViewGUI;
			selectedIndex = -1;
			smoothSpline = target as AverageSpline;
		}

		private void OnDisable()
		{
			SceneView.duringSceneGui -= OnSceneViewGUI;
		}

		public override void OnInspectorGUI()
		{
			serializedObject.UpdateIfRequiredOrScript();
			base.OnInspectorGUI();
			if (GUILayout.Button("New Spline Point"))
			{
				Undo.RecordObject(smoothSpline, "Add Spline Point");
				smoothSpline.AddNode(Double3.Uniform(0), Space.Self);
				selectedIndex = smoothSpline.NodeCount - 1;
				SceneView.RepaintAll();
			}
			if (selectedIndex != -1 && GUILayout.Button("Subdivide Selected Point"))
			{
				SubdivideSplinePoint();
				SceneView.RepaintAll();
			}
			if (selectedIndex != -1 && GUILayout.Button("Delete Selected Point"))
			{
				DeleteSplinePoint();
				SceneView.RepaintAll();
			}
			serializedObject.ApplyModifiedProperties();
		}

		private void CheckNodeDeletion()
		{
			if (selectedIndex < 0)
				return;
			if (Event.current.type != EventType.KeyDown || Event.current.keyCode != KeyCode.Delete)
				return;
			DeleteSplinePoint();
		}

		private void DeleteSplinePoint()
		{
			if (selectedIndex < 0)
				return;

			Undo.RecordObject(smoothSpline, "Delete Spline Point");
			smoothSpline.RemoveNode(selectedIndex);
			selectedIndex = -1;
			Event.current.Use();
			SceneView.RepaintAll();
		}

		private void CheckNodeDuplication()
		{
			if (selectedIndex < 0)
				return;
			if (Event.current.type != EventType.ExecuteCommand || Event.current.commandName != "Duplicate")
				return;
			SubdivideSplinePoint();
			Event.current.Use();
			SceneView.RepaintAll();
		}

		private void SubdivideSplinePoint()
		{
			Undo.RecordObject(smoothSpline, "Subdivide Spline Point");
			if (selectedIndex > 0)
			{
				Double3 start = smoothSpline.GetNode(selectedIndex - 1, Space.Self);
				Double3 end = smoothSpline.GetNode(selectedIndex, Space.Self);
				smoothSpline.InsertNode(selectedIndex, start.Interpolant(end, 0.5), Space.Self);
				++selectedIndex;
			}
			if (selectedIndex < smoothSpline.NodeCount - 1)
			{
				Double3 start = smoothSpline.GetNode(selectedIndex, Space.Self);
				Double3 end = smoothSpline.GetNode(selectedIndex + 1, Space.Self);
				smoothSpline.InsertNode(selectedIndex + 1, start.Interpolant(end, 0.5), Space.Self);
			}
		}

		private void OnSceneViewGUI(SceneView view)
		{
			CheckNodeDeletion();
			CheckNodeDuplication();

			serializedObject.Update();
			UnityEngine.Quaternion rotation = Tools.pivotRotation == PivotRotation.Local ? UnityEngine.Quaternion.identity : UnityEngine.Quaternion.Inverse(smoothSpline.transform.rotation);
			Vector3 cameraDirection = UnityEngine.Quaternion.Inverse(smoothSpline.transform.rotation) * (SceneView.currentDrawingSceneView.camera.transform.rotation * Vector3.forward);
			Matrix4x4 matCache = Handles.matrix;
			Color handleColorCache = Handles.color;
			using (new Handles.DrawingScope(Handles.color, Handles.matrix))
			{
				//Here we define the space for our handles.
				//We work in positions relative to the spline's root, but scales relative to the required handle size.
				//The functions LocalToHandleSpace and HandleToLocalSpace manage these odd conversions.
				Handles.matrix = Matrix4x4.TRS(smoothSpline.transform.position, smoothSpline.transform.rotation, (Vector3)Double3.Uniform(POSITION_HANDLE_SCALE));

				//Draw every node
				for (int i = 0; i < smoothSpline.NodeCount; ++i)
				{
					//First, get the node's position in the spline's local space, and convert it to our custom handle space.
					Double3 node = LocalToHandleSpace(smoothSpline.GetNode(i, Space.Self));

					//If we are drawing the selected node, then we also draw the circle selection indicator, and smooth
					if (selectedIndex == i)
					{
						float discSize = HandleUtility.GetHandleSize((Vector3)node);
						Handles.color = Color.black;
						Handles.DrawSolidDisc((Vector3)node, cameraDirection, (float)(discSize * DISC_OUTER_SCALE));
						Handles.color = Color.white;
						Handles.DrawSolidDisc((Vector3)node, cameraDirection, (float)(discSize * DISC_INNER_SCALE));
						Handles.color = handleColorCache;
					}

					//Draw the transform position handles for the node
					EditorGUI.BeginChangeCheck();
					node = Handles.PositionHandle((Vector3)node, rotation);
					//If the user has moved this node...
					if (EditorGUI.EndChangeCheck())
					{
						//...we make it the selected node
						selectedIndex = i;
						//..prepare an undo step for the movement
						Undo.RecordObject(smoothSpline, "Move Spline Point");
						//...and then convert and apply the value back to the spline.
						smoothSpline.SetNode(i, HandleToLocalSpace(node), Space.Self);
					}
				}

				Handles.matrix = matCache;
			}
			//Make sure all the changes have been applied to the component.
			serializedObject.ApplyModifiedProperties();

			//Draw the spline visualisation.
			DrawSplineLines();
		}

		private void DrawSplineLines()
		{
			if (smoothSpline.NodeCount < 2)
				return;

			Double3[] splineDrawPoints = new Double3[(int)((smoothSpline.SplineLength) / 0.01) + 1];
			Double3[] splineWayPoints = new Double3[smoothSpline.NodeCount];
			int j = 0;
			for (int i = 0; i < smoothSpline.NodeCount; ++i)
				splineWayPoints[i] = smoothSpline.GetNode(i, Space.World);

			AverageSpline<Double3>.Traveller traveller = new AverageSpline<Double3>.Traveller();
			for (double d = 0; d < smoothSpline.SplineLength; d += 0.01)
				splineDrawPoints[j++] = smoothSpline.SamplePosition(d, Space.World, false, ref traveller);
			Handles.DrawAAPolyLine(3.0f, splineDrawPoints.Select(x => (Vector3)x).ToArray());
			Handles.DrawAAPolyLine(1.5f, splineWayPoints.Select(x => (Vector3)x).ToArray());
			if (smoothSpline.Looped)
			{
				Handles.DrawAAPolyLine(3.0f, new Vector3[] { (Vector3)splineDrawPoints[0], (Vector3)splineDrawPoints[splineDrawPoints.Length - 1] });
				Handles.DrawAAPolyLine(1.5f, new Vector3[] { (Vector3)splineWayPoints[0], (Vector3)splineWayPoints[splineWayPoints.Length - 1] });
			}
		}

		private Double3 LocalToHandleSpace(Double3 pos)
		{
			return (pos / POSITION_HANDLE_SCALE) * smoothSpline.transform.localScale;
		}

		private Double3 HandleToLocalSpace(Double3 pos)
		{
			return (pos / smoothSpline.transform.localScale) * POSITION_HANDLE_SCALE;
		}
	}
}