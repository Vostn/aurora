# Console

The Decal System is a projected deferred decal system, that allows a developer to project PBR decals onto any geometry.

It's made of three parts; a **[mega shader](./Assets/Aurora/Unity Extensions/Unity/_Content/Shaders/Projected Deferred Decal.shader)**, an **[editor interface](./Assets/Aurora/Unity Editor Extensions/Unity/Editor/_Content/Scripts/Editors/DeferredDecal_ShaderGUI.cs)** for configuring the shader, and a **[component script](./Assets/Aurora/Unity Extensions/Unity/_Content/Scripts/Components/DeferredDecal.cs)** that manages rendering and culling.